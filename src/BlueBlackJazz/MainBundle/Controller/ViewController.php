<?php

namespace BlueBlackJazz\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

use BlueBlackJazz\MainBundle\Entity\Artist;
use BlueBlackJazz\MainBundle\Entity\Page;
use BlueBlackJazz\MainBundle\Entity\Book;
use BlueBlackJazz\MainBundle\Entity\Style;
use BlueBlackJazz\MainBundle\Entity\Transcription;

class ViewController extends Controller
{
    public function initializeSeo()
    {
        $this->seoPage = $this->get('sonata.seo.page');
        $this->translator = $this->get('translator');
        $this->router = $this->get('router');
        $locale = $this->get('request')->getLocale();

        $this->seoPage->addHtmlAttributes("lang", $locale);
        $this->seoPage->addHeadAttribute("lang", $locale);
    }
    /**
     * @Route("/{_locale}/books",
     *      name="all_books",
     *      requirements={"_locale" = "en|fr"}
     *      )
     * @Template
     */
    public function booksAction(Request $request)
    {
        $this->initializeSeo();

        $pageEntity = $this->getDoctrine()->getManager()->getRepository('BlueBlackJazzMainBundle:Page')->findOneBySlug('books');
        $this->seoPage
            ->setTitle($pageEntity->translate()->getSeoTitle())
            ->addMeta('name', 'description', $pageEntity->translate()->getSeoDescription())
            ->addMeta('name', 'keywords', $pageEntity->translate()->getSeoKeywords())
            ->setLinkCanonical($this->router->generate($request->get('_route'), []))
            ;

        foreach(['en', 'fr'] as $locale){
            $this->seoPage->addLangAlternate($this->router->generate($request->get('_route'), ['_locale' => $locale]), $locale);
        }
        return ["page" => $pageEntity];
    }
    /**
     * @Route("/{_locale}/page/{slug}",
     *      name="page",
     *      requirements={"_locale" = "en|fr"}
     *      )
     * @Template
     */
    public function PageAction(Page $page, Request $request)
    {
        $this->initializeSeo();

        $this->seoPage
            ->setTitle($page->translate()->getSeoTitle())
            ->addMeta('name', 'description', $page->translate()->getSeoDescription())
            ->addMeta('name', 'keywords', $page->translate()->getSeoKeywords())
            ->setLinkCanonical($this->router->generate($request->get('_route'), ['slug' => $page->getSlug()]))
            ;

        foreach(['en', 'fr'] as $locale){
            $this->seoPage->addLangAlternate($this->router->generate($request->get('_route'), ['slug' => $page->getSlug(), '_locale' => $locale]), $locale);
        }
        return ["page" => $page];
    }
    /**
     * @Route("/{_locale}/artist/{id}/{slug}", name="artist",
     *      requirements={"_locale" = "en|fr"}
     *      )
     * @Template
     */
    public function ArtistAction(Request $request, $id, $slug)
    {
        $em = $this->getDoctrine()->getManager();
        $artist = $em
            ->getRepository('BlueBlackJazzMainBundle:Artist')
            ->find($id)
            ;

        if (!$artist) {
            throw $this->createNotFoundException("Unable to find Entity with id=$id");
        }
        $theTrueSlug = $artist->getSlug();


        if($theTrueSlug !== $slug){
            return
                $this->redirect(
                    $this->generateUrl(
                        $request->get('_route'),
                        [
                            'id' => $id,
                            'slug' => $theTrueSlug
                        ]
                    )
                    , 301)
                    ;
        }
        $this->initializeSeo();

        $this->seoPage
            ->setTitle($artist->translate()->getSeoTitle())
            ->addMeta('name', 'description', $artist->translate()->getSeoDescription())
            ->addMeta('name', 'keywords', $artist->translate()->getSeoKeywords())
            ->setLinkCanonical($this->router->generate($request->get('_route'), ['id' => $id, 'slug' => $theTrueSlug]))
            ;

        foreach(['en', 'fr'] as $locale){
            $this->seoPage->addLangAlternate($this->router->generate($request->get('_route'), ['id' => $id, 'slug' => $theTrueSlug, '_locale' => $locale]), $locale);
        }
        return ["artist" => $artist];
    }

    /**
     * @Route("/{_locale}/book/{id}/{slug}", name="book",
     *      requirements={"_locale" = "en|fr"}
     *      )
     *
     * @Template
     */
    public function BookAction(Request $request, $id, $slug)
    {
        $em = $this->getDoctrine()->getManager();
        $book = $em
            ->getRepository('BlueBlackJazzMainBundle:Book')
            ->find($id)
            ;

        if (!$book) {
            throw $this->createNotFoundException("Unable to find Entity with id=$id");
        }
        $theTrueSlug = $book->getSlug();


        if($theTrueSlug !== $slug){
            return
                $this->redirect(
                    $this->generateUrl(
                        $request->get('_route'),
                        [
                            'id' => $id,
                            'slug' => $theTrueSlug
                        ]
                    )
                    , 301)
                    ;
        }
        $this->initializeSeo();

        $this->seoPage
            ->setTitle($book->translate()->getSeoTitle())
            ->addMeta('name', 'description', $book->translate()->getSeoDescription())
            ->addMeta('name', 'keywords', $book->translate()->getSeoKeywords())
            ->setLinkCanonical($this->router->generate($request->get('_route'), ['id' => $id, 'slug' => $theTrueSlug]))
            ;

        foreach(['en', 'fr'] as $locale){
            $this->seoPage->addLangAlternate($this->router->generate($request->get('_route'), ['id' => $id, 'slug' => $theTrueSlug, '_locale' => $locale]), $locale);
        }
        return ["book" => $book];
    }

    /**
     * @Route("/{_locale}/style/{id}/{slug}", name="style",
     *      requirements={"_locale" = "en|fr"}
     *      )
     * @Template
     */
    public function StyleAction(Request $request, $id, $slug)
    {
        $em = $this->getDoctrine()->getManager();
        $style = $em
            ->getRepository('BlueBlackJazzMainBundle:Style')
            ->find($id)
            ;

        if (!$style) {
            throw $this->createNotFoundException("Unable to find Entity with id=$id");
        }
        $theTrueSlug = $style->getSlug();


        if($theTrueSlug !== $slug){
            return
                $this->redirect(
                    $this->generateUrl(
                        $request->get('_route'),
                        [
                            'id' => $id,
                            'slug' => $theTrueSlug
                        ]
                    )
                    , 301)
                    ;
        }
        $this->initializeSeo();

        $this->seoPage
            ->setTitle($style->translate()->getSeoTitle())
            ->addMeta('name', 'description', $style->translate()->getSeoDescription())
            ->addMeta('name', 'keywords', $style->translate()->getSeoKeywords())
            ->setLinkCanonical($this->router->generate($request->get('_route'), ['id' => $id, 'slug' => $theTrueSlug]))
            ;

        foreach(['en', 'fr'] as $locale){
            $this->seoPage->addLangAlternate($this->router->generate($request->get('_route'), ['id' => $id, 'slug' => $theTrueSlug, '_locale' => $locale]), $locale);
        }
        return ["style" => $style];
    }

    /**
     * @Route("/{_locale}/transcription/{id}/{slug}", name="transcription",
     *      requirements={"_locale" = "en|fr"}
     *      )
     * @Template
     */
    public function TranscriptionAction(Request $request, $id, $slug)
    {
        $em = $this->getDoctrine()->getManager();
        $transcription = $em
            ->getRepository('BlueBlackJazzMainBundle:Transcription')
            ->find($id)
            ;

        if (!$transcription) {
            throw $this->createNotFoundException("Unable to find Entity with id=$id");
        }
        $theTrueSlug = $transcription->getSlug();

        $theTrueUrl = $this->generateUrl(
                        $request->get('_route'),
                        [
                            'id' => $id,
                            'slug' => $theTrueSlug
                        ]
                    )
                    ;
        if($theTrueSlug !== $slug){
            return
                $this->redirect($theTrueUrl, 301)  ;
        }

        $this->initializeSeo();

        $this->seoPage
            ->setTitle("{$transcription->getName()} by {$transcription->getArtist()->getName()}")
            ->addMeta('name', 'description', "TODO")
            ->addMeta('name', 'keywords', "TODO")
            ->setLinkCanonical($theTrueUrl)
            ;

        foreach(['en', 'fr'] as $locale){
            $this->seoPage->addLangAlternate($this->router->generate($request->get('_route'), ['id' => $id, 'slug' => $theTrueSlug, '_locale' => $locale]), $locale);
        }

        return ["transcription" => $transcription];
    }

    /**
     * @Route("/{_locale}/all/", name="all",
     *      requirements={"_locale" = "en|fr"}
     *      )
     * @Template()
     */
    public function AllAction(Request $request)
    {
        $this->initializeSeo();
        $pageEntity = $this->getDoctrine()->getManager()->getRepository('BlueBlackJazzMainBundle:Page')->findOneBySlug('all');
        $this->seoPage
            ->setTitle($pageEntity->translate()->getSeoTitle())
            ->addMeta('name', 'description', $pageEntity->translate()->getSeoDescription())
            ->addMeta('name', 'keywords', $pageEntity->translate()->getSeoKeywords())
            ->setLinkCanonical($this->router->generate($request->get('_route'), []))
            ;

        foreach(['en', 'fr'] as $locale){
            $this->seoPage->addLangAlternate($this->router->generate($request->get('_route'), ['_locale' => $locale]), $locale);
        }

        return ["page" => $pageEntity];
    }

    /**
     * @Route("/{_locale}/", name="home",
     *      requirements={"_locale" = "en|fr"}
     *      )
     * @Route("/")
     * @Template()
     */
    public function HomeAction()
    {
        $pageEntity = $this->getDoctrine()->getManager()->getRepository('BlueBlackJazzMainBundle:Page')->findOneBySlug('homepage');

        $this->initializeSeo();

        $this->seoPage
            ->setTitle($pageEntity->translate()->getSeoTitle())
            ->addMeta('name', 'description', $pageEntity->translate()->getSeoDescription())
            ->addMeta('name', 'keywords', $pageEntity->translate()->getSeoKeywords())
            ->setLinkCanonical($this->router->generate('home'))
            ;
        foreach(['en', 'fr'] as $locale){
            $this->seoPage->addLangAlternate($this->router->generate('home', ['_locale' => $locale]), $locale);
        }

        return array('page' => $pageEntity);
    }
}
