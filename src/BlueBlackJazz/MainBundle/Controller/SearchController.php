<?php

namespace BlueBlackJazz\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;

class SearchController extends Controller
{
    /**
     * @Route("/{_locale}/search", name="search",
     *      requirements={"_locale" = "en|fr"}
     *      )
     */
    public function SearchAction()
    {
        $response = new JsonResponse();

        $transcriptions = $this->getDoctrine()->getRepository('BlueBlackJazzMainBundle:Transcription')->findAll();
        $books = $this->getDoctrine()->getRepository('BlueBlackJazzMainBundle:Book')->findAll();

        $converter = $this->get('lexik_currency.converter');

        $response->setData(array(
                "transcriptions" => array_map(function($t) use ($converter) {
                    $artistName = null;
                    if($t->getArtist()){
                        $artistName =  $t->getArtist()->getName();
                    }
                    $matches = [];
                    preg_match("/\d\d\d\d/", $t->translate()->getDate(), $matches);
                    if(isset($matches[0])){
                        $year = $matches[0];
                    } else {
                        $year = "";
                    }

                    return array(
                        "name" => $t->getName(),
                        "note" => $t->getNote(),
                        "date" => $year,
                        "artist" => $artistName,
                        "url" => $this->generateUrl('transcription', array('id'=> $t->getId(), 'slug' => $t->getSlug())),
                        "cart_url" => $this->generateUrl('cart_add_transcription', array('id' => $t->getId(), 'slug' => $t->getSlug())),
                        "priceEuro" => $t->getPriceEuro(),
                        "priceDollar" => $converter->convert($t->getPriceEuro(), "USD")
                    );
                }, $transcriptions),

                "books" => array_map(function($b) use ($converter){
                    $artistName = null;
                    if($b->getArtist()){
                        $artistName =  $b->getArtist()->getName();
                    }
                    return array(
                        "title" => $b->getTitle(),
                        "artist" => $artistName,
                        "url" => $this->generateUrl('book', array('id'=> $b->getId(), 'slug' => $b->getSlug())),
                        "cart_url" => $this->generateUrl('cart_add_book', array('id' => $b->getId(), 'slug' => $b->getSlug())),
                        "priceEuro" => $b->getPriceEuro(),
                        "priceDollar" => $converter->convert($b->getPriceEuro(), "USD")
                    );
                }, $books),
        ));

        return $response;
    }
}
