<?php

namespace BlueBlackJazz\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;

use BlueBlackJazz\MainBundle\Entity\Book;
use BlueBlackJazz\MainBundle\Entity\Transcription;

class CartController extends Controller
{

    /**
     * @Route("/{_locale}/cart/set/shipping/{shipping}", name="cart_set_shipping",
     *      requirements={"_locale" = "en|fr"}
     *      )
     *
     */
    public function cartSetShipping($shipping, Request $request)
    {
        $this->get('bbb.cart')->setShipping($shipping);

        if($request->get('next')){
            return $this->redirect($request->get('next'));
        } else {
            if($request->headers->get('referer')){
                return $this->redirect($request->headers->get('referer'));
            }
            return $this->redirect($this->generateUrl('cart_view'));
        }
    }

    /**
     * @Route("/{_locale}/cart/set/currency/{currency}", name="cart_set_currency",
     *      requirements={"_locale" = "en|fr"}
     *      )
     */
    public function cartSetCurrencyAction($currency, Request $request)
    {
        $this->get('bbb.cart')->setFavoriteCurrency($currency);

        if($request->get('next')){
            return $this->redirect($request->get('next'));
        } else {
            if($request->headers->get('referer')){
                return $this->redirect($request->headers->get('referer'));
            }
            return $this->redirect($this->generateUrl('cart_view'));
        }
    }

    /**
     * @Route("/{_locale}/cart/add/transcription/{id}/{slug}", name="cart_add_transcription",
     *      requirements={"_locale" = "en|fr"}
     *      )
     */
    public function addToCartTranscriptionAction($id, $slug, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $transcription = $em
            ->getRepository('BlueBlackJazzMainBundle:Transcription')
            ->find($id)
            ;
        if (!$transcription) {
            throw $this->createNotFoundException("Unable to find Entity with id=$id");
        }
        $theTrueSlug = $transcription->getSlug();


        if($theTrueSlug !== $slug){
            return
                $this->redirect(
                    $this->generateUrl(
                        $request->get('_route'),
                        [
                            'id' => $id,
                            'slug' => $theTrueSlug
                        ]
                    )
                    , 301)
                    ;
        }



        $this->get('bbb.cart')->addTranscriptionById($id);

        if($request->get('next')){
            return $this->redirect($request->get('next'));
        } else {
            if($request->headers->get('referer')){
                return $this->redirect($request->headers->get('referer'));
            }
            return $this->redirect($this->generateUrl('cart_view'));
        }
    }
    /**
     * @Route("/{_locale}/cart/remove/transcription/{id}/{slug}", name="cart_remove_transcription",
     *      requirements={"_locale" = "en|fr"}
     *      )
     */
    public function removeCartTranscriptionAction($id, $slug, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $transcription = $em
            ->getRepository('BlueBlackJazzMainBundle:Transcription')
            ->find($id)
            ;
        if (!$transcription) {
            throw $this->createNotFoundException("Unable to find Entity with id=$id");
        }
        $theTrueSlug = $transcription->getSlug();


        if($theTrueSlug !== $slug){
            return
                $this->redirect(
                    $this->generateUrl(
                        $request->get('_route'),
                        [
                            'id' => $id,
                            'slug' => $theTrueSlug
                        ]
                    )
                    , 301)
                    ;
        }
        $this->get('bbb.cart')->removeTranscriptionById($id);

        if($request->get('next')){
            return $this->redirect($request->get('next'));
        } else {
            if($request->headers->get('referer')){
                return $this->redirect($request->headers->get('referer'));
            }
            return $this->redirect($this->generateUrl('cart_view'));
        }
    }


    /**
     * @Route("/{_locale}/cart/add/book/{id}/{slug}", name="cart_add_book",
     *      requirements={"_locale" = "en|fr"}
     *      )
     */
    public function addToCartBookAction($id, $slug, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $book = $em
            ->getRepository('BlueBlackJazzMainBundle:Book')
            ->find($id)
            ;
        if (!$book) {
            throw $this->createNotFoundException("Unable to find Entity with id=$id");
        }
        $theTrueSlug = $book->getSlug();


        if($theTrueSlug !== $slug){
            return
                $this->redirect(
                    $this->generateUrl(
                        $request->get('_route'),
                        [
                            'id' => $id,
                            'slug' => $theTrueSlug
                        ]
                    )
                    , 301)
                    ;
        }
        $this->get('bbb.cart')->addBookById($id);

        if($request->get('next')){
            return $this->redirect($request->get('next'));
        } else {
            if($request->headers->get('referer')){
                return $this->redirect($request->headers->get('referer'));
            }
            return $this->redirect($this->generateUrl('cart_view'));
        }
    }
    /**
     * @Route("/{_locale}/cart/remove/book/{id}/{slug}", name="cart_remove_book",
     *      requirements={"_locale" = "en|fr"}
     *      )
     */
    public function removeCartBookAction($id, $slug, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $book = $em
            ->getRepository('BlueBlackJazzMainBundle:Book')
            ->find($id)
            ;
        if (!$book) {
            throw $this->createNotFoundException("Unable to find Entity with id=$id");
        }
        $theTrueSlug = $book->getSlug();


        if($theTrueSlug !== $slug){
            return
                $this->redirect(
                    $this->generateUrl(
                        $request->get('_route'),
                        [
                            'id' => $id,
                            'slug' => $theTrueSlug
                        ]
                    )
                    , 301)
                    ;
        }
        $this->get('bbb.cart')->removeBookById($id);

        $session = $this->get('session');
        $session->remove("cart/books/$id");

        if($request->get('next')){
            return $this->redirect($request->get('next'));
        } else {
            if($request->headers->get('referer')){
                return $this->redirect($request->headers->get('referer'));
            }
            return $this->redirect($this->generateUrl('cart_view'));
        }
    }

    /**
     * @Route("/{_locale}/cart/view", name="cart_view",
     *      requirements={"_locale" = "en|fr"}
     *      )
     */
    public function cartViewAction()
    {
        $response = $this->render('BlueBlackJazzMainBundle:Cart:cartView.html.twig');

        // prevent caching for back button
        // source: http://stackoverflow.com/a/16753260

        $response->setPrivate();
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('no-store', true);

        return $response;
    }
}
