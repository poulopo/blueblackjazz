<?php

namespace BlueBlackJazz\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use BlueBlackJazz\MainBundle\Entity\Redirect;

class AdminController extends Controller
{
    /**
     * @Route("/admin/createRedirects")
     * @Template()
     */
    public function createRedirectsAction()
    {
        $em = $this->get('doctrine')->getManager();
        $transcriptionRepo = $em->getRepository('BlueBlackJazzMainBundle:Transcription');
        $styleRepo = $em->getRepository('BlueBlackJazzMainBundle:Style');
        $bookRepo = $em->getRepository('BlueBlackJazzMainBundle:Book');
        $artistRepo = $em->getRepository('BlueBlackJazzMainBundle:Artist');
        $redirectRepo = $em->getRepository('BlueBlackJazzMainBundle:Redirect');
        foreach($redirectRepo->findAll() as $red){
            $em->remove($red);
            $em->flush();
        }
        $transcriptions = $transcriptionRepo->findAll();
        foreach($transcriptions as $t){
            foreach(['fr', 'en'] as $locale){
                if($locale === 'fr'){
                    $otherLocale = 'en';
                } else {
                    $otherLocale = 'fr';
                }
                $oldPaths = ["$locale/transcription/{$t->getSlug()}"];

                $menuLinkLanguage = $t->translate($locale)->getMenuLinkLanguage();
                if(strlen($menuLinkLanguage) >= 1){
                    $oldPaths[] = 'part/' . str_replace('.html', '', $t->translate($locale)->getMenuLinkLanguage());
                }
                foreach($oldPaths as $oldPath){
                    $r = new Redirect();
                    $r->setOldPath($oldPath);
                    $r->setRoute('transcription');
                    $r->setParameters(
                        array(
                            'id' => $t->getId(),
                            'slug' => $t->getSlug(),
                            '_locale' => $otherLocale
                        )
                    );
                    $em->persist($r);
                }
            }

        }
        $em->flush();
        $a = [
            [
                'path' => 'partitions_boogie-woogie',
                'route' => 'style',
                'parameters' => [
                    '_locale' => 'fr',
                    'slug' => $styleRepo->findOneBySlug('boogie-woogie')->getSlug(),
                    'id' => $styleRepo->findOneBySlug('boogie-woogie')->getId()
                ]
            ],
            [
                'path' => 'sheetmusic_boogie-woogie',
                'route' => 'style',
                'parameters' => [
                    '_locale' => 'en',
                    'slug' => $styleRepo->findOneBySlug('boogie-woogie')->getSlug(),
                    'id' => $styleRepo->findOneBySlug('boogie-woogie')->getId()
                ]
            ],
            [
                'path' => 'stride_piano',
                'route' => 'style',
                'parameters' => [
                    '_locale' => 'en',
                    'slug' => $styleRepo->findOneBySlug('stride-piano')->getSlug(),
                    'id' => $styleRepo->findOneBySlug('stride-piano')->getId()
                ]
            ],
            [
                'path' => 'piano_stride',
                'route' => 'style',
                'parameters' => [
                    '_locale' => 'fr',
                    'slug' => $styleRepo->findOneBySlug('stride-piano')->getSlug(),
                    'id' => $styleRepo->findOneBySlug('stride-piano')->getId()
                ]
            ],

            [
                'path' => 'sheetmusic',
                'route' => 'all',
                'parameters' => ['_locale' => 'en']
            ],
            [
                'path' => 'transcription_en',
                'route' => 'all',
                'parameters' => ['_locale' => 'en']
            ],
            [
                'path' => 'transcription_fr',
                'route' => 'all',
                'parameters' => ['_locale' => 'fr']
            ],
            [
                'path' => 'partitions',
                'route' => 'all',
                'parameters' => ['_locale' => 'fr']
            ],



            [
                'path' => 'johnson_fr',
                'route' => 'artist',
                'parameters' => [
                    '_locale' => 'fr',
                    'slug' => $artistRepo->findOneBySlug('james-p-johnson')->getSlug(),
                    'id' => $artistRepo->findOneBySlug('james-p-johnson')->getId()
                ]
            ],
            [
                'path' => 'johnson_en',
                'route' => 'artist',
                'parameters' => [
                    '_locale' => 'en',
                    'slug' => $artistRepo->findOneBySlug('james-p-johnson')->getSlug(),
                    'id' => $artistRepo->findOneBySlug('james-p-johnson')->getId()
                ]
            ],

            [
                'path' => 'lion_fr',
                'route' => 'artist',
                'parameters' => [
                    '_locale' => 'fr',
                    'slug' => $artistRepo->findOneBySlug('willie-the-lion-smith')->getSlug(),
                    'id' => $artistRepo->findOneBySlug('willie-the-lion-smith')->getId()
                ]
            ],
            [
                'path' => 'lion_en',
                'route' => 'artist',
                'parameters' => [
                    '_locale' => 'en',
                    'slug' => $artistRepo->findOneBySlug('willie-the-lion-smith')->getSlug(),
                    'id' => $artistRepo->findOneBySlug('willie-the-lion-smith')->getId()
                ]
            ],


            [
                'path' => 'lambert_fr',
                'route' => 'artist',
                'parameters' => [
                    '_locale' => 'fr',
                    'slug' => $artistRepo->findOneBySlug('donald-lambert')->getSlug(),
                    'id' => $artistRepo->findOneBySlug('donald-lambert')->getId()
                ]
            ],
            [
                'path' => 'lambert_en',
                'route' => 'artist',
                'parameters' => [
                    '_locale' => 'en',
                    'slug' => $artistRepo->findOneBySlug('donald-lambert')->getSlug(),
                    'id' => $artistRepo->findOneBySlug('donald-lambert')->getId()
                ]
            ],


            [
                'path' => 'fats_waller_fr',
                'route' => 'artist',
                'parameters' => [
                    '_locale' => 'fr',
                    'slug' => $artistRepo->findOneBySlug('fats-waller')->getSlug(),
                    'id' => $artistRepo->findOneBySlug('fats-waller')->getId()
                ]
            ],
            [
                'path' => 'fats_waller_en',
                'route' => 'artist',
                'parameters' => [
                    '_locale' => 'en',
                    'slug' => $artistRepo->findOneBySlug('fats-waller')->getSlug(),
                    'id' => $artistRepo->findOneBySlug('fats-waller')->getId()
            ]
            ],

        ];
        foreach($a as $arr){
            $oldPath = $arr['path'];
            $r = new Redirect();
            $r->setOldPath($oldPath);
            $r->setRoute($arr['route']);
            $r->setParameters(
                $arr['parameters']
            );
            $em->persist($r);
        }
        $em->flush();
        foreach(['book' => $bookRepo->findAll(), 'artist' => $artistRepo->findAll(), 'style' => $styleRepo->findAll()] as $route => $entities){
            foreach(['en', 'fr'] as $locale){
                foreach($entities as $entity){
                    $oldPath2 = $locale . "/" . $route . "/" . $entity->getSlug();
                    $r2 = new Redirect();
                    $r2->setOldPath($oldPath2);
                    $r2->setRoute($route);
                    $r2->setParameters(['_locale' => $locale, 'id' => $entity->getId(), 'slug' => $entity->getSlug()]);
                    $em->persist($r2);
                }
            }
        }
        $em->flush();
        $this->get('session')->getFlashBag()->add(
            'success',
            'Redirects were created.'
        );
        return $this->redirect($this->generateUrl("home"));
    }
    /**
     * @Route("/admin/checkMp3")
     * @Template()
     */
    public function checkMp3Action()
    {
        $transcriptionRepo = $this->getDoctrine()->getRepository('BlueBlackJazzMainBundle:Transcription'); 
        $transcriptions = $transcriptionRepo->findAll();

        $checkedTranscriptions = [];
        $rootDir = $this->get('kernel')->getRootDir();


        foreach($transcriptions as $t){
            $mp3exists = file_exists($rootDir . '/../../media/mp3/' . $t->getMp3());
            $pdfexists = file_exists($rootDir . '/../' . $t->getPdf());

            $checkedTranscriptions[] = [
                'transcription' => $t,
                'mp3exists' => $mp3exists,
                'pdfexists' => $pdfexists,
            ];
        }

        $styles = $this->getDoctrine()->getRepository('BlueBlackJazzMainBundle:Style')->findAll();

        $artists = $this->getDoctrine()->getRepository('BlueBlackJazzMainBundle:Artist')->findAll();

        $things = array_merge(
            $artists,
            $styles
        );

        $checkedThings = [];
        foreach($things as $thing){
            $checked = ['thing' => $thing];
            $mp3s = [];
            foreach(['en', 'fr'] as $locale){
                $matches = [];
                $markdown = $thing->translate($locale)->getMarkdown();
                preg_match_all(
                    '/MP3\(\(([\._\w\s-]+)\)\)/',
                    $markdown,
                    $matches
                );
                foreach($matches[1] as $mp3filename){
                    $t = $transcriptionRepo->findOneByMp3($mp3filename);
                    if(!$t){
                        $t = $transcriptionRepo->findOneByOtherMp3s($mp3filename);   
                    }
                    $mp3exists = file_exists($rootDir . '/../../media/mp3/' . $mp3filename);
                    $mp3s[] =
                        [
                            'transcription' => $t,
                            'filename' => $mp3filename,
                            'mp3exists' => $mp3exists,
                            'locale' => $locale
                        ];
                }
            }
            $checked['mp3s'] = $mp3s;
            $checkedThings[] = $checked;
            
        }


        return [
            'checkedThings' => $checkedThings,
            'checkedTranscriptions' => $checkedTranscriptions
        ];
    }

}
