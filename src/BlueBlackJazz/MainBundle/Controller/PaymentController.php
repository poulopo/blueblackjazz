<?php

namespace BlueBlackJazz\MainBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use JMS\Payment\CoreBundle\PluginController\Result;
use JMS\Payment\CoreBundle\Entity\ExtendedData;
use JMS\Payment\CoreBundle\Entity\PaymentInstruction;
use JMS\Payment\CoreBundle\Entity\Payment;
use JMS\Payment\CoreBundle\Plugin\Exception\ActionRequiredException;
use JMS\Payment\CoreBundle\Plugin\Exception\Action\VisitUrl;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;

use BlueBlackJazz\MainBundle\Entity\MyOrder;

/**
 * @Route("/payments")
 */
class PaymentController extends Controller
{
    /**
     * @Route("/details", name = "details")
     * @Template
     */
    public function detailsAction()
    {
        $em = $this->getDoctrine()->getManager();
	$order = $this->get('bbb.cart')->getOrder();
	
        $currency = $order->getCurrency();
        $method = 'paypal_express_checkout';
        $extendedData = new ExtendedData();

	$extendedData->set('return_url', $this->generateUrl('payment_complete', array(
                        'salt' => $order->getSalt(),
                    ), true) );
	$extendedData->set('cancel_url', $this->generateUrl('payment_cancel', array(
                        'salt' => $order->getSalt(),
                    ), true) );

        $amount = $order->getAmount();
        $checkoutParams = [
            'LANDINGPAGE' => 'Billing',
            'SOLUTIONTYPE' => 'Sole',
        ];
        $counter = 0;
        $items = array_merge(
            $order->getBooks()->toArray(),
            $order->getTranscriptions()->toArray()
        );
        foreach($items as $item){
            $checkoutParams = array_merge($checkoutParams,
                [
                "L_PAYMENTREQUEST_0_NAME$counter" => $item->__toString(),
                "L_PAYMENTREQUEST_0_DESC$counter" => $item->__toString(),
                "L_PAYMENTREQUEST_0_QTY$counter" => 1,
                "L_PAYMENTREQUEST_0_AMT$counter" => $this->get('lexik_currency.converter')->convert($item->getPriceEuro(), $currency) // if you get 10413 , then visit the api errors documentation , this number should be the total amount (usually the same as the price )
                // 'L_PAYMENTREQUEST_0_ITEMCATEGORY0'=> 'Digital',
                ]
            );
            $counter += 1;
        }
        if($order->getShipping() !== 'PDF'){
            $checkoutParams = array_merge($checkoutParams,
                [
                "L_PAYMENTREQUEST_0_NAME$counter" => 'Shipping',
                "L_PAYMENTREQUEST_0_DESC$counter" => 'Total shipping cost.',
                "L_PAYMENTREQUEST_0_QTY$counter" => 1,
                "L_PAYMENTREQUEST_0_AMT$counter" => $order->getShippingCost()
                ]
            );
        }

        $extendedData->set('checkout_params', $checkoutParams);


	$instruction = new PaymentInstruction($amount, $currency, $method, $extendedData);
	$ppc = $this->get('payment.plugin_controller');

	$ppc->createPaymentInstruction($instruction);

	$order->setPaymentInstruction($instruction);
	$em->persist($order);
	$em->flush($order);

	return new RedirectResponse($this->generateUrl('payment_complete', array(
					'salt' => $order->getSalt(),
					)));
    }

    /**
     * @Route("/complete/{salt}", name = "payment_complete")
     * @Template
     */
    public function completeAction(MyOrder $myOrder, Request $request)
    {
        if($myOrder->getCancelled() || $myOrder->getConfirmed()){
            throw $this->createNotFoundException('Order already complete or cancelled');
        }
        if($myOrder->getAmount() === 0.0){
            $this->get('session')->getFlashBag()->add(
                'error',
                'Your cart is empty. Add some transcriptions to your cart first!'
            );

            return new RedirectResponse($this->generateUrl('cart_view'));
        }
	$ppc = $this->get('payment.plugin_controller');
        $instruction = $myOrder->getPaymentInstruction();
        if (null === $pendingTransaction = $instruction->getPendingTransaction()) {
            $payment = $ppc->createPayment($instruction->getId(), $instruction->getAmount() - $instruction->getDepositedAmount());
        } else {
            $payment = $pendingTransaction->getPayment();
        }

        $result = $ppc->approveAndDeposit($payment->getId(), $payment->getTargetAmount());
        if (Result::STATUS_PENDING === $result->getStatus()) {
            $ex = $result->getPluginException();

            if ($ex instanceof ActionRequiredException) {
                $action = $ex->getAction();

                if ($action instanceof VisitUrl) {
                    return new RedirectResponse($action->getUrl());
                }

                throw $ex;
            }
        } else if (Result::STATUS_SUCCESS !== $result->getStatus()) {
            throw new \RuntimeException('Transaction was not successful: '.$result->getReasonCode());
        }
        $em = $this->getDoctrine()->getManager();

        $myOrder->setConfirmed(true);
        $myOrder->setExpiresAt(new \DateTime("+2 days"));
	$em->persist($myOrder);
	$em->flush($myOrder);

        $myOrder->setPayerId($request->get('PayerID'));
	$em->persist($myOrder);
	$em->flush($myOrder);

        $this->get('bbb.cart')->emptyAll();

        $message = \Swift_Message::newInstance()
            ->setSubject('Your transcriptions - blueblackjazz.com')
            ->setFrom('paul.marcorelles@blueblackjazz.com')
            ->setTo($myOrder->getEmail())
            ->setBcc(['pierroweb@gmail.com', 'paul.marcorelles@gmail.com'])
            ->setBody(
                $this->renderView(
                    'BlueBlackJazzMainBundle:Payment:email.txt.twig',
                    array('order' => $myOrder)
                )
            )
            ;
        $this->get('mailer')->send($message);


        return ['order' => $myOrder];
    }

    /**
     * @Route("/cancel/{salt}", name = "payment_cancel")
     */
    public function cancelAction(MyOrder $myOrder)
    {
        if($myOrder->getCancelled() || $myOrder->getConfirmed()){
            throw $this->createNotFoundException('Order already complete');
        }
        $em = $this->getDoctrine()->getManager();
        $myOrder->setCancelled(true);
	$em->persist($myOrder);
	$em->flush($myOrder);

        $this->get('session')->getFlashBag()->add(
            'error',
            'Your payment has been cancelled'
        );

        return new RedirectResponse($this->generateUrl('cart_view'));
    }

}
