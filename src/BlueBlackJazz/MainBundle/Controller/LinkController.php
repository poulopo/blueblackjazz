<?php

namespace BlueBlackJazz\MainBundle\Controller;

use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;


class LinkController extends Controller
{
    /**
     * @Route("/link/b/{bookId}/{orderSalt}", name="link_book")
     */
    public function downloadBookAction($bookId, $orderSalt)
    {
        $em = $this->getDoctrine()->getManager();
        $bookRepository = $this->get('doctrine')->getRepository('BlueBlackJazzMainBundle:Book');
        $book = $bookRepository->findOneById($bookId);
        if(! $book){
            throw $this->createNotFoundException('book not found');
        }


        $orderRepository = $this->get('doctrine')->getRepository('BlueBlackJazzMainBundle:MyOrder');
        $order = $orderRepository->findOneBySalt($orderSalt);

        if(! $order){
            throw $this->createNotFoundException('Order not found');
        }

        if(! $order->getBooks()->contains($book)){
            throw $this->createNotFoundException('Book is not part of this order');
        }

        if ($order->getExpiresAt() < new \DateTime()){
            $id = $order->getId();
            $salt = $order->getSalt();
            $this->get('session')->getFlashBag()->add(
                'error',
                "Your link has expired. Please send an email to paul.marcorelles@blueblackjazz.com with the reference $id - $salt ; and I will send you the transcriptions manually."
            );

            return new RedirectResponse($this->generateUrl('home'));

        }

        $order->setNDownloads($order->getNDownloads() + 1);
        $em->persist($order);
        $em->flush();

        $file = $this->container->getParameter('kernel.root_dir') . "/../" . $book->getPdf();

        BinaryFileResponse::trustXSendfileTypeHeader();

        $response = new BinaryFileResponse($file);
        $response->headers->set('Content-Type', 'application/pdf');
        $response->setContentDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                    $book->getSlug() . ".pdf"
                );
        return $response;
    }

    /**
     * @Route("/link/t/{transcriptionId}/{orderSalt}", name="link_transcription")
     */
    public function downloadTranscriptionAction($transcriptionId, $orderSalt)
    {
        $em = $this->getDoctrine()->getManager();
        $transcriptionRepository = $this->get('doctrine')->getRepository('BlueBlackJazzMainBundle:Transcription');
        $transcription = $transcriptionRepository->findOneById($transcriptionId);
        if(! $transcription){
            throw $this->createNotFoundException('transcription not found');
        }


        $orderRepository = $this->get('doctrine')->getRepository('BlueBlackJazzMainBundle:MyOrder');
        $order = $orderRepository->findOneBySalt($orderSalt);

        if(! $order){
            throw $this->createNotFoundException('Order not found');
        }

        if(! $order->getTranscriptions()->contains($transcription)){
            throw $this->createNotFoundException('Transcription is not part of this order');
        }

        if ($order->getExpiresAt() < new \DateTime()){
            $id = $order->getId();
            $salt = $order->getSalt();
            $this->get('session')->getFlashBag()->add(
                'error',
                "Your link has expired. Please send an email to paul.marcorelles@blueblackjazz.com with the reference $id - $salt ; and I will send you the transcriptions manually."
            );

            return new RedirectResponse($this->generateUrl('home'));
        }
        $file = $this->container->getParameter('kernel.root_dir') . "/../" . $transcription->getPdf();

        BinaryFileResponse::trustXSendfileTypeHeader();

        $response = new BinaryFileResponse($file);
        $response->headers->set('Content-Type', 'application/pdf');
        $response->setContentDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                    $transcription->getSlug() . ".pdf"
                );
        return $response;
    }

}
