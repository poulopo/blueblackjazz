<?php

namespace BlueBlackJazz\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

use BlueBlackJazz\MainBundle\Entity\Redirect;

class ZRedirectController extends Controller
{
    /**
     * @Route("/{_locale}/cart/add/transcription/{id}-{restOfSlug}",
     *      requirements={"_locale" = "en|fr", "id" = "\d+"}
     *      )
     */
    public function oldAddToCartTranscriptionAction($id, $restOfSlug, Request $request)
    {
        return $this->redirect(
                $this->generateUrl(
                    "cart_add_transcription",
                    [
                        'id' => $id,
                        'slug' => "$id-$restOfSlug"
                        ]
                    )
                    , 301)
        ;
    }
    /**
     * @Route("/{_locale}/cart/add/transcription/{slug}",
     *      requirements={"_locale" = "en|fr"}
     *      )
     */

    public function oldOldAddToCartTranscriptionAction($slug, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $transcription = $em
            ->getRepository('BlueBlackJazzMainBundle:Transcription')
            ->findOneBySlug($slug)
            ;

        if (!$transcription) {
            $transcriptions = $this->getDoctrine()->getRepository('BlueBlackJazzMainBundle:Transcription')->findAll();

            $bestTrans = $transcriptions[0];
            $minDis = PHP_INT_MAX;

            foreach($transcriptions as $t) {
                $dis = levenshtein($slug, $t->getSlug());
                if ($dis < $minDis) {
                    $minDis = $dis;
                    $bestTrans = $t;
                }
            }
            $transcription = $bestTrans;
        }
        return $this->redirect(
                $this->generateUrl(
                    "cart_add_transcription",
                    [
                        'id' => $transcription->getId(),
                            'slug' => $transcription->getSlug()
                        ]
                    )
                    , 301)
        ;
    }

    /**
     * @Route("/{_locale}/cart/add/book/{slug}",
     *      requirements={"_locale" = "en|fr"}
     *      )
     */
    public function oldAddToCartBookAction($slug, Request $request)
    {
        $books = $this->getDoctrine()->getRepository('BlueBlackJazzMainBundle:Book')->findAll();

        $bestBook = $books[0];
        $minDis = PHP_INT_MAX;

        foreach($books as $book) {
            $dis = levenshtein($slug, $book->getSlug());
            if ($dis < $minDis) {
                $minDis = $dis;
                $bestBook = $book;
            }
        }
        return
            $this->redirect(
                $this->generateUrl(
                    "cart_add_book",
                    [
                        'id' => $bestBook->getId(),
                        'slug' => $bestBook->getSlug()
                    ]
                )
                , 301)
                ;
    }

    /**
     * @Route("/{_locale}/book/{slug}",
     *      requirements={"_locale" = "en|fr"}
     * )
     */
    public function redirectOldBookAction($slug){
        $books = $this->getDoctrine()->getRepository('BlueBlackJazzMainBundle:Book')->findAll();

        $bestBook = $books[0];
        $minDis = PHP_INT_MAX;

        foreach($books as $book) {
            $dis = levenshtein($slug, $book->getSlug());
            if ($dis < $minDis) {
                $minDis = $dis;
                $bestBook = $book;
            }
        }

        return $this->redirect(
            $this->generateUrl(
                "book",
                [
                    'id' => $bestBook->getId(),
                    'slug' => $bestBook->getSlug()
                ]
            )
            , 301)
            ;
    }

    /**
     * @Route("/{_locale}/transcription/{id}-{slug}",
     *      requirements={"id" = "\d+", "_locale" = "en|fr"}
     *  )
     */
    public function redirectOldTranscriptionAction($id, $slug){

        return $this->redirect(
            $this->generateUrl(
                "transcription",
                [
                    'id' => $id,
                    'slug' => $slug
                ]
            )
            , 301)
            ;
    }

    /**
     * @Route("/{oldPath}.html", requirements={"oldPath" = ".+"})
     */
    public function redirectHtmlAction(Redirect $redirect)
    {
        return $this->redirect(
            $this->generateUrl(
                $redirect->getRoute(),
                $redirect->getParameters()
            ),
            301
        );
    }
    /**
     * @Route("/{oldPath}", requirements={"oldPath" = ".+"})
     */
    public function redirectAction(Redirect $redirect)
    {
        return $this->redirect(
            $this->generateUrl(
                $redirect->getRoute(),
                $redirect->getParameters()
            ),
            301
        );
    }
}
