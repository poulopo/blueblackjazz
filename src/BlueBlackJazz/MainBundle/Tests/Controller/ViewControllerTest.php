<?php

namespace BlueBlackJazz\MainBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ViewControllerTest extends WebTestCase
{
    public function testArtist()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/fr/artist/fats-waller');
        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode()
        );
        $link = $crawler->selectLink('See page in English')->link();
        $crawler = $client->click($link);
        $this->assertEquals(
            '/en/artist/fats-waller',
            $client->getRequest()->getPathInfo()
        );
        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode()
        );
        $link = $crawler->selectLink('Voir la page en français')->link();
        $crawler = $client->click($link);
        $this->assertEquals(
            '/fr/artist/fats-waller',
            $client->getRequest()->getPathInfo()
        );
        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode()
        );
    }

    public function testBook()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/en/book/fats-waller-17-solos-for-piano-volume-1');
        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode()
        );
        $link = $crawler->selectLink("Blue Black Bottom")->link();
        $crawler = $client->click($link);
        $this->assertEquals(
            '/en/transcription/1-fats-waller-blue-black-bottom',
            $client->getRequest()->getPathInfo()
        );
        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode()
        );
    }

    public function testTranscription()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/fr/transcription/1-fats-waller-blue-black-bottom');
        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode()
        );
    }

    public function testAll()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/fr/all/');
        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode()
        );
    }

}
