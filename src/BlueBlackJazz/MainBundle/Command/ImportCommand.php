<?php
namespace BlueBlackJazz\MainBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use BlueBlackJazz\MainBundle\Entity\Book;
use BlueBlackJazz\MainBundle\Entity\Page;
use BlueBlackJazz\MainBundle\Entity\Transcription;
use BlueBlackJazz\MainBundle\Entity\Artist;
use BlueBlackJazz\MainBundle\Entity\Style;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class ImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('bbb:import')
            ->setDescription('Import former BBB data')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $json = json_decode(file_get_contents("./bbb-migration/CONST.json"));
        $partitions = $json->partitions;
        $recueils = $json->recueils;
        $descriptions = $json->part_desc;
        $em = $this->getContainer()->get('doctrine')->getManager();
        $bookRepository = $em->getRepository('BlueBlackJazzMainBundle:Book');
        $transcriptionRepository = $em->getRepository('BlueBlackJazzMainBundle:Transcription');

        $boogie = new Style();
        $boogie->setTitle('Boogie-Woogie');
        $boogie->translate('fr')->setName('Boogie-Woogie');
        $boogie->translate('en')->setName('Boogie-Woogie');
        $em->persist($boogie);
        $boogie->mergeNewTranslations();

        $stride = new Style();
        $stride->setTitle('Stride Piano');
        $stride->translate('fr')->setName('Piano Stride');
        $stride->translate('en')->setName('Stride Piano');
        $em->persist($stride);
        $stride->mergeNewTranslations();
        $em->flush();



        $artistRepository = $em->getRepository('BlueBlackJazzMainBundle:Artist');
        $categories_en = json_decode(file_get_contents("./bbb-migration/categorie.json"));
        $output->writeln("------- CREATING artists ----------");
        foreach ($categories_en as $author){
                $artist = new Artist();
                $artist->setName(ucwords(strtolower($author->name)));
                $em->persist($artist);
                $em->flush();

                $output->writeln($author->name . " CREATED ");
        }
        $output->writeln("DONE ");

        $output->writeln("------- CREATING books ----------");
        foreach ($recueils as $recueilName => $recueil){
                $itemNumber = substr($recueil->item_number, -4);
                $b = $bookRepository->findOneByItemNumber($itemNumber);
                if(! $b ){
                    $output->writeln($recueilName . ' NOT FOUND, created');
                    $b = new Book();

                    $b->setItemNumber($itemNumber);
                } else {
                    $output->writeln($recueilName . ' FOUND, enriched');
                }
                $b->setImgCover($recueil->img_cover);
                $b->setImgTable($recueil->img_table);

                $output->writeln($recueil->title);
                $artist = $artistRepository->findOneByName(str_replace(' Piano Rolls', '', ucwords(strtolower(strip_tags($recueil->title)))));
                if($artist){
                        $b->setArtist($artist);
                }

                if(strpos($recueilName, 'sheetmusi') === 0){
                        $b->translate('en')->setDescription($recueil->description);
                        $b->setTitle(ucwords(strtolower(strip_tags($recueil->title_desc))));
                } else if(strpos($recueilName, 'partitio') === 0){
                        $b->translate('fr')->setDescription($recueil->description);
                        $b->setTitle(ucwords(strtolower(strip_tags($recueil->title_desc))));
                } else { throw new Exception; }

                $em->persist($b);
                $b->mergeNewTranslations();
                $em->flush();
        }

        $output->writeln("DONE ");
        $output->writeln("------- CREATING transcriptions ----------");

        foreach ($partitions as $partitionName => $partition){
                $itemNumber = substr($partition->item_number, -4);
                $t = $transcriptionRepository->findOneByItemNumber($itemNumber);
                if(! $t ){
                    $output->write($partitionName . ' NOT FOUND, created --- ');
                    $t = new Transcription();

                    $t->setItemNumber($itemNumber);
                } else {
                    $output->write($partitionName . ' FOUND, enriched --- ');
                }
                $t->setName($partition->alt_img_title);
                $t->setImgTitle($partition->img_title);
                $t->setTrack($partition->order);
                $t->setNote($partition->note);
                $t->setMp3($partition->mp3);

                if($partition->lang === 'fr'){
                        $t->translate('fr')->setPartDesc($partition->meta_description);
                        $t->translate('fr')->setDate($partition->date);
                        $t->translate('fr')->setMenuLinkLanguage($partition->menu_link_language);
                        if(property_exists($descriptions, $partitionName)){
                            $t->translate('fr')->setDescription($descriptions->{$partitionName});
                        }
                } else if($partition->lang === 'en'){
                        $t->translate('en')->setPartDesc($partition->meta_description);
                        $t->translate('en')->setDate($partition->date);
                        $t->translate('en')->setMenuLinkLanguage($partition->menu_link_language);
                        if(property_exists($descriptions, $partitionName)){
                            $t->translate('en')->setDescription($descriptions->{$partitionName});
                        }
                } else { throw new Exception; }

                $em->persist($t);
                $t->mergeNewTranslations();
                $em->flush();
        }
        $output->writeln("DONE ");
        $output->writeln("DONE ");

        $output->writeln("------- Enrich Transcriptions with paypal, pdf info----------");

        foreach ($categories_en as $author){
            $artist = $artistRepository->findOneByName(ucwords(strtolower($author->name)));
            foreach($author->products as $partition){
                $transcription = $transcriptionRepository->findOneByItemNumber(substr($partition->item_number, -4));
                $recueil = $bookRepository->findOneByItemNumber(substr($partition->recueil_ref, -4));
                $transcription->setPdf($partition->url_pdf);
                $transcription->setBook($recueil);
                $transcription->translate('en')->setPaypalButton($partition->paypal_button);
                $transcription->setArtist($artist);
                $em->persist($transcription);
                $transcription->mergeNewTranslations();
                $em->flush();
            }
        }

        $categories_fr = json_decode(file_get_contents("./bbb-migration/categorie_fr.json"));

        foreach ($categories_fr as $author){
            $artist = $artistRepository->findOneByName(ucwords(strtolower($author->name)));
            foreach($author->products as $partition){
                $transcription = $transcriptionRepository->findOneByItemNumber(substr($partition->item_number, -4));
                $recueil = $bookRepository->findOneByItemNumber(substr($partition->recueil_ref, -4));
                $transcription->setPdf($partition->url_pdf);
                $transcription->setBook($recueil);
                $transcription->translate('fr')->setPaypalButton($partition->paypal_button);
                $transcription->setArtist($artist);
                $em->persist($transcription);
                $transcription->mergeNewTranslations();
                $em->flush();
            }
        }

        $output->writeln("DONE ");
        $output->writeln("------- Enrich Books with paypal, pdf info----------");

        $recueils_fr = json_decode(file_get_contents("./bbb-migration/recueils_fr.json"));
        foreach ($recueils_fr as $recueil){
            $book = $bookRepository->findOneByItemNumber(substr($recueil[5], -4));
            $paypal = $recueil[3];
            $pdf = $recueil[4];
            $book->translate('fr')->setPaypal($paypal);
            $book->setPdf($pdf);
            $output->write($paypal);
            $output->write($pdf);
            $output->write($book->getTitle());
            $output->writeln($recueil[0]);
            $em->persist($book);
            $book->mergeNewTranslations();
        }

        $recueils_en  = json_decode(file_get_contents("./bbb-migration/recueils.json"));
        foreach ($recueils_en as $recueil){
            $book = $bookRepository->findOneByItemNumber(substr($recueil[5], -4));
            $paypal = $recueil[3];
            $pdf = $recueil[4];
            $book->translate('en')->setPaypal($paypal);
            $book->setPdf($pdf);
            $output->write($paypal);
            $output->write($pdf);
            $output->write($book->getTitle());
            $output->writeln($recueil[0]);
            $book->mergeNewTranslations();

            if($book->getArtist()){
                $book->getArtist()->setStyle($stride);
                $em->persist($book->getArtist());
                $book->setStyle($stride);
                foreach($book->getTranscriptions() as $t){
                    $t->setStyle($stride);
                    $em->persist($t);
                }
                $em->persist($book->getArtist());
            } else {
                $book->setStyle($boogie);
                foreach($book->getTranscriptions() as $t){
                    $t->setStyle($boogie);
                    $t->getArtist()->setStyle($boogie);
                    $em->persist($t);
                    $em->persist($t->getArtist());
                }
            }
            $em->persist($book);
        } 
        $em->flush();

        foreach($artistRepository->findAll() as $artist){
            if(!$artist->getStyle()){
                $artist->setStyle($boogie);
                $em->persist($artist);
                $em->flush();
            }
        }

        $johnson = $artistRepository->findOneByName('James P. Johnson');
        $johnson->translate('en')->setSubtitle('The Father Of Stride Piano" - (1894 - 1955)');
        $johnson->translate('fr')->setSubtitle('Le Père Du Piano Stride" - (1894 - 1955)');
        $johnson->translate('fr')->setMarkdown(<<<EOF

![](http://www.blueblackjazz.com/media/images/johnson.jpg)

C'est le pianiste James Price Johnson qui a donné naissance au Piano Stride de Harlem Il apprend le piano trés jeune découvre les grand compositeurs de ragtime, ainsi que des pianistes comme Eubie Blake et Luckey Roberts au jeu déjà trés novateur. Débutant sa carrière dans les bars, les cabarets et les rent-parties, il a rapidement acquis une trés grande notoriété en tant que virtuose et compositeur et se met à enregistrer et produire des piano rolls dés 1916 dans un style emprunté aux maitres du ragtime, mais qui n'en est plus vraiment : par son jeu révolutionnaire, il a su exploiter toutes les palettes sonores du clavier, et dès 1921 il enregistra ses premières compositions de ce genre, où se mêlaient syncopes et blues notes. Carolina Shout est ainsi considéré comme le premier enregistrement de piano en Jazz. Il enregistre vers la même époque au côté des plus grandes chanteuses de blues, et devient notamment l'accompagnateur privilégié de Bessie Smith. 

MP3((Bleeding-Hearted Blues))

![](http://www.blueblackjazz.com/media/images/jamesp.jpg)


Vers 1920, il rencontre le jeune Fats Waller fasciné par le jeu de son aîné et à qui il donnera une solide formation de piano durant plusieurs années et l'introduira dans le milieux musical de Harlem et le rent-parties. Par la suite, James P. Johnson continuera à enregistrer un grand nombre de morceaux en solo ou accompagnant des orchestres. 

On reconnait le style de James P. Johnson par son toucher assez léger, un jeu rythmique voir percussif. Sa detxérité et son inventivité hors du commun font du lui un virtuose du piano, le premier "vrai" pianiste de jazz dont la portée musicale a su se développer au fil des générations. Il est déjà réputé dés la fin des années 1910 pour sa technique infaillible, une parfaite maitrise de l'instrument qui culminera dans les années 30 et 40 ou il enregistrera de mémorables solos de piano. 

![](http://www.blueblackjazz.com/media/images/johnson3.jpg)


James P. Johnson aura joué un role déterminant dans l'histoire jazz et dans l'évolution du ragtime vers une musique improvisée et exempte de toutes barrières formelles. Il a aussi laissé de nombreuses compositions et standards, dont Charleston qui est l'un des airs les plus enregistrés durant les années 20, mais aussi Old Fashioned Love, If I Could Be With You, A Porter's Love Song to a Chambermaid. Malgré tout, il ne recevra pas la reconnaissance qu'il mérite et mourra presque dans l'oubli en 1955. 

MP3((Carolina Balmoral))


EOF
        );
        $johnson->translate('en')->setMarkdown(<<<EOF
The pianist James P. Johnson (1894-1955) created Stride piano by developing the game of the great Ragtime pianists in a less restrictive music . He started to play in bars, cabarets and rent-parties, and quickly became famous for his virtuosity and his compositions. From 1916, he recorded and published piano rolls in a style which is no longer ragtime. With a revolutionary game, he used all the sounds he could play on the keyboard. From 1921, he recorded his first Stride piano compositions where he mingled syncopations with blues notes. Carolina Shout is considered as the first recording of Jazz on a piano. In the same period, James P. Johnson recorded with the greatest Blues singers and became Bessie Smith's favourite accompanist. 

MP3((Bleeding-Hearted Blues))

![](http://www.blueblackjazz.com/media/images/jamesp.jpg)


Around 1919, he looked after the 15 years old Fats Waller whose mother had just died. During several years he gave piano lessons to the young boy and launched him then into an extraordinary career. For his part, he continued to record a lot of solo pieces or to accompany orchestras. 

James P. Johnson's playing can be recognized by his light touch and a rhythmic, almost percussive game. His dexterity and his unusual inventiveness make him a piano virtuoso, the first "real" jazz pianist whose musicality managed to grow over generations. It is already known since the late 1910s for his flawless technique, perfect mastery of the instrument will culminate in the 30 and 40. 

![](http://www.blueblackjazz.com/media/images/johnson3.jpg)


James P. Johnson played an important part in Jazz history. Thanks to him, Ragtime has evolved towards an improvised music free from rules. He left us a lot of compositions and standards like Charleston, one of the most recorded tunes in the 20's. Even so, he never received the recognition he deserved and died in 1955 nearly forgotten by everyone. 

MP3((Carolina Balmoral))


EOF
        );
        $em->persist($johnson);
        $johnson->mergeNewTranslations();
        $em->flush();


        $em->persist((new Page())->setName('services'));
        $em->persist((new Page())->setName('how-to-order'));
        $em->persist((new Page())->setName('contacts'));
        $em->persist((new Page())->setName('homepage'));
        $em->flush();

        $testimonials = [
            "The transcriptions you have done are accurate and overall amazing"=> "William P., Pianist, USA",
            "Congratulations on your great work "=>"David A., USA",
            "Your books of transcriptions are the answer to my prayers! I find your transcriptions to be the most accurate I've come across. "=>"Andrew C., USA",
            "Your sense of earing and your ability to understand this music is just phenomenal"=> "Daniel P., pianist, France",
            "It's just fantastic! It sounds exactly like the recording. You have a rare and great talent for transcribing music! "=>"Luigi R., Italy",
            "Great job with these wonderful stride piano transcriptions. "=>"Chris D., pianist, USA",
            "With your transcriptions there are those moments of pure happiness felt by listening the original recordings"=> "Pilippe G., France",
            "Thanks for the transcription that I've been waiting 40 years!"=> "Bernard C., France",
            "Thank you so much for transcribing these great Jazz piano solos that would otherwise be lost and never available for us piano players. I have purchased every folio that you have published and have never been dissappointed. Keep up the good work... "=>"Steve S., USA",
            "I think your transcriptions are fantastic and very accurate. These are the third set i have bought "=>"Jon W., U.K",
            "Many thanks, this is great work you're doing!! It's great that you are making these arrangements accessible "=>"Shoshana M., USA",
            "I'm a great fan of yours, you write excellent transcriptions! "=>"Eldad S., IsraÃ«l",
            "Thank you again for making your wonderfully accurate transcriptions! "=>"Dave J., USA",
            "The extraordinary side of your work is that it's perfect!"=> "Mike S., Switzerland",
            "Keep up the great work! "=>"Ari K., USA",
            "You are doing an amazing job keeping all this fantastic music alive "=>"Nicholas C. U-K",
            "Thank you for making your wonderful transcriptions! I get enjoyment from practicing them every day "=>"Dave J., USA",
            "I am very impressed with your ability to transcribe these wonderful but exclusive pieces "=>"Thomas M., USA",
            "I am really very happy because of Your transcriptions "=>"Norbert W., Austria",
            "Your transcriptions are wonderful! Very much appreciated. "=>"Cliff M., USA",
            "Thanks once again for your great transcriptions! They are a real treasure. "=>"Chris S., USA"
        ];
        $testim = (new Page())->setName('testimonials');
        $md = '';
        foreach($testimonials as $quote => $by){
            $md .= <<<EOL

> $quote
>
> <footer> -- <cite>$by</cite></footer>

* * *


EOL;
        }
        $testim->translate('en')->setMarkdown($md);
        $testim->translate('en')->setTitle('Testimonials');
        $testim->translate('fr')->setMarkdown($md);
        $testim->translate('fr')->setTitle('Témoignages');
        $testim->mergeNewTranslations();
        $em->persist($testim);
        $em->flush();





    }
}
