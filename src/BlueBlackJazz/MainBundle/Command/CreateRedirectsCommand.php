<?php
namespace BlueBlackJazz\MainBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use BlueBlackJazz\MainBundle\Entity\Redirect;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class CreateRedirectsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('bbb:redirects')
            ->setDescription('create redirects')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {



    }
}

