<?php

namespace BlueBlackJazz\MainBundle\Twig;

class Mp3Extension extends \Twig_Extension
{

    public function __construct($doctrine, $router)
    {
        $this->transcriptionRepository = $doctrine->getRepository('BlueBlackJazzMainBundle:Transcription');
        $this->router = $router;
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('mp3', array($this, 'mp3Filter')),
        );
    }

    public function mp3Filter($text)
    {
        $repo = $this->transcriptionRepository;
        $router = $this->router;

        return preg_replace_callback(
                '/MP3\(\(([\)\(,\._\w\s-]+)\)\)/',
                function($matches) use ($router, $repo) {
                        $mp3 = $matches[1];
                        $t = $repo->findOneByMp3($mp3);
                        if(!$t){
                            $t = $repo->findOneByOtherMp3s($mp3);
                        }
                        if($t){
                            return <<<EOS
<hr>
<p class="text-center" style="clear:both;">
    <small>
            <a href="{$router->generate('transcription', array('id' => $t->getId(), 'slug' => $t->getSlug()))}">{$t->getName()}</a>
            by <a href="">{$t->getArtist()->getName()}</a>

    </small>
</p>
<div style="width:420px; margin: 0 auto;">
<audio src="http://www.blueblackjazz.com/media/mp3/{$mp3}" />
</div>
<hr>
EOS
                        ;
                        } else {
                            $mp3name = $mp3;
                            $mp3song = str_replace(".mp3", "", $mp3name);
                            $mp3song = str_replace("_", " ", $mp3song);
                            $mp3song = ucwords($mp3song);
                            return <<<EOS
<hr>
<p class="text-center" style="clear:both;">
    <small>
                {$mp3song}
    </small>
</p>
<div style="width:420px; margin: 0 auto;">
<audio src="http://www.blueblackjazz.com/media/mp3/{$mp3}" />
</div>
<hr>
EOS
                            ;

                        }

                },
                $text
        );
    }

    public function getName()
    {
        return 'bbb_mp3_extension';
    }
}
