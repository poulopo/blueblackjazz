<?php

namespace BlueBlackJazz\MainBundle\Twig;

class GlobalsExtension extends \Twig_Extension
{

    public function __construct($doctrine, $cart)
    {
        $this->artistRepository = $doctrine->getRepository('BlueBlackJazzMainBundle:Artist');
        $this->bookRepository = $doctrine->getRepository('BlueBlackJazzMainBundle:Book');
        $this->transcriptionRepository = $doctrine->getRepository('BlueBlackJazzMainBundle:Transcription');
        $this->styleRepository = $doctrine->getRepository('BlueBlackJazzMainBundle:Style');
        $this->pageRepository = $doctrine->getRepository('BlueBlackJazzMainBundle:Page');
        $this->stylesheetRepository = $doctrine->getRepository('BlueBlackJazzMainBundle:Stylesheet');
        $this->cart = $cart;
    }
    public function getGlobals() {
        $stylesheets = $this->stylesheetRepository
            ->findAll()
            ;

        $artistsNumbers = $this->artistRepository
            ->createQueryBuilder('a')
            ->from('BlueBlackJazzMainBundle:Transcription', 't')
            ->join('t.artist', 'ar')
            ->groupBy('a.id')
            ->addSelect('COUNT(DISTINCT t.id) as n')
            ->addOrderBy('n', 'DESC')
            ->where('t.artist = a')
            ->getQuery()
            ->getResult()
        ;
        $stylesNumbers = $this->styleRepository
            ->createQueryBuilder('s')
            ->from('BlueBlackJazzMainBundle:Transcription', 't')
            ->join('t.book', 'b')
            ->join('b.style', 'st')
            ->groupBy('s.id')
            ->addSelect('COUNT(DISTINCT t.id) as n')
            ->addOrderBy('n', 'DESC')
            ->where('b.style = s')
            ->getQuery()
            ->getResult()
        ;
        return array(
            'stylesheets' => $stylesheets,
            'menu_books' => $this->bookRepository->findAll(),
            'menu_artists' => $this->artistRepository->findAll(),
            'menu_artists_with_numbers' => $artistsNumbers,
            'menu_styles' => $this->styleRepository->findAll(),
            'menu_styles_with_numbers' => $stylesNumbers,
            'cart' => $this->cart
        );
    }

    public function getName()
    {
        return 'my_globals_extension';
    }
}
