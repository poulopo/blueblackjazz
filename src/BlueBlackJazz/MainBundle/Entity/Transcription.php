<?php

namespace BlueBlackJazz\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

use BlueBlackJazz\MainBundle\Entity\Book;
use BlueBlackJazz\MainBundle\Entity\Artist;
use BlueBlackJazz\MainBundle\Entity\Style;

/**
 * Transcription
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="BlueBlackJazz\MainBundle\Entity\TranscriptionRepository")
 */
class Transcription

{
    use ORMBehaviors\Translatable\Translatable,
        ORMBehaviors\Sluggable\Sluggable;

    public function getSluggableFields()
    {
        return ['id', 'artist', 'name'];
    }

    /**
     *  * @ORM\Column(type="decimal", scale=2)
     *   */
    protected $priceEuro = 7.5;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Book",inversedBy="transcriptions")
     */
    private $book;

    /**
     * @ORM\ManyToOne(targetEntity="Artist",inversedBy="transcriptions")
     */
    private $artist;

    /**
     * @ORM\ManyToOne(targetEntity="Style",inversedBy="transcriptions")
     */
    private $style;


    /**
     * @var string
     *
     * @ORM\Column(nullable=true, name="img_title", type="string", length=255)
     */
    private $imgTitle;

    /**
     * @var string
     *
     * @ORM\Column(nullable=true, name="otherMp3s", type="string", length=255)
     */
    private $otherMp3s;



    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    public function __toString(){
        return $this->getName();
    }

    /**
     * @var string
     *
     * @ORM\Column(nullable=true, name="note", type="string", length=255)
     */
    private $note;

    /**
     * @var integer
     *
     * @ORM\Column(nullable=true, name="track", type="smallint")
     */
    private $track;


    /**
     * @var string
     *
     * @ORM\Column(nullable=true, name="pdf", type="string", length=255)
     */
    private $pdf;

    /**
     * @var string
     *
     * @ORM\Column(nullable= true, name="mp3", type="string", length=255)
     */
    private $mp3;

    /**
     * @var string
     *
     * @ORM\Column(nullable=true, name="item_number", type="string", length=255)
     */
    private $itemNumber;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set imgTitle
     *
     * @param string $imgTitle
     * @return Transcription
     */
    public function setImgTitle($imgTitle)
    {
        $this->imgTitle = $imgTitle;
    
        return $this;
    }

    /**
     * Get imgTitle
     *
     * @return string 
     */
    public function getImgTitle()
    {
        return $this->imgTitle;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Transcription
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return Transcription
     */
    public function setNote($note)
    {
        $this->note = $note;
    
        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set track
     *
     * @param integer $track
     * @return Transcription
     */
    public function setTrack($track)
    {
        $this->track = $track;
    
        return $this;
    }

    /**
     * Get track
     *
     * @return integer 
     */
    public function getTrack()
    {
        return $this->track;
    }

    /**
     * Set pdf
     *
     * @param string $pdf
     * @return Transcription
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;
    
        return $this;
    }

    /**
     * Get pdf
     *
     * @return string 
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set mp3
     *
     * @param string $mp3
     * @return Transcription
     */
    public function setMp3($mp3)
    {
        $this->mp3 = $mp3;
    
        return $this;
    }

    /**
     * Get mp3
     *
     * @return string 
     */
    public function getMp3()
    {
        return $this->mp3;
    }

    /**
     * Set itemNumber
     *
     * @param string $itemNumber
     * @return Transcription
     */
    public function setItemNumber($itemNumber)
    {
        $this->itemNumber = $itemNumber;
    
        return $this;
    }

    /**
     * Get itemNumber
     *
     * @return string 
     */
    public function getItemNumber()
    {
        return $this->itemNumber;
    }

    /**
     * Set book
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Book $book
     * @return Transcription
     */
    public function setBook(\BlueBlackJazz\MainBundle\Entity\Book $book = null)
    {
        $this->book = $book;
    
        return $this;
    }

    /**
     * Get book
     *
     * @return \BlueBlackJazz\MainBundle\Entity\Book 
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * Set artist
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Artist $artist
     * @return Transcription
     */
    public function setArtist(\BlueBlackJazz\MainBundle\Entity\Artist $artist = null)
    {
        $this->artist = $artist;
    
        return $this;
    }

    /**
     * Get artist
     *
     * @return \BlueBlackJazz\MainBundle\Entity\Artist 
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * Set style
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Style $style
     * @return Transcription
     */
    public function setStyle(\BlueBlackJazz\MainBundle\Entity\Style $style = null)
    {
        $this->style = $style;
    
        return $this;
    }

    /**
     * Get style
     *
     * @return \BlueBlackJazz\MainBundle\Entity\Style 
     */
    public function getStyle()
    {
        return $this->style;
    }
    /**
     * @var string
     */


    /**
     * Set priceEuro
     *
     * @param float $priceEuro
     * @return Transcription
     */
    public function setPriceEuro($priceEuro)
    {
        $this->priceEuro = $priceEuro;
    
        return $this;
    }

    /**
     * Get priceEuro
     *
     * @return float 
     */
    public function getPriceEuro()
    {
        return $this->priceEuro;
    }
    /**
     * Set otherMp3s
     *
     * @param string $otherMp3s
     * @return Transcription
     */
    public function setOtherMp3s($otherMp3s)
    {
        $this->otherMp3s = $otherMp3s;
    
        return $this;
    }

    /**
     * Get otherMp3s
     *
     * @return string 
     */
    public function getOtherMp3s()
    {
        return $this->otherMp3s;
    }
}