<?php

namespace BlueBlackJazz\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 */
class BookTranslation
{
    use ORMBehaviors\Translatable\Translation,
        SeoTranslationTrait
        ;

    /**
     * @var string
     *
     * @ORM\Column(nullable=true, type="string", length=255)
     */
    private $description;

    /**
     * Set description
     *
     * @param string $description
     * @return BookTranslation
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

}
