<?php

namespace BlueBlackJazz\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 */
class TranscriptionTranslation
{
    use ORMBehaviors\Translatable\Translation;


    /**
     * @var string
     *
     * @ORM\Column(nullable=true, type="string", length=255)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(nullable=true, type="string", length=255)
     */
    private $menuLinkLanguage;

    /**
     * @var string
     *
     * @ORM\Column(nullable=true, type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(nullable=true, type="text")
     */
    private $partDesc;

    /**
     * Set date
     *
     * @param string $date
     * @return TranscriptionTranslation
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return string 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set menuLinkLanguage
     *
     * @param string $menuLinkLanguage
     * @return TranscriptionTranslation
     */
    public function setMenuLinkLanguage($menuLinkLanguage)
    {
        $this->menuLinkLanguage = $menuLinkLanguage;
    
        return $this;
    }

    /**
     * Get menuLinkLanguage
     *
     * @return string 
     */
    public function getMenuLinkLanguage()
    {
        return $this->menuLinkLanguage;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return TranscriptionTranslation
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set partDesc
     *
     * @param string $partDesc
     * @return TranscriptionTranslation
     */
    public function setPartDesc($partDesc)
    {
        $this->partDesc = $partDesc;
    
        return $this;
    }

    /**
     * Get partDesc
     *
     * @return string 
     */
    public function getPartDesc()
    {
        return $this->partDesc;
    }


}
