<?php

namespace BlueBlackJazz\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Redirect
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Redirect
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="route", type="string", length=255)
     */
    private $route;

    /**
     * @var string
     *
     * @ORM\Column(name="oldPath", type="string", length=255, unique=true)
     */
    private $oldPath;

    /**
     * @var array
     *
     * @ORM\Column(name="parameters", type="array")
     */
    private $parameters;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set route
     *
     * @param string $route
     * @return Redirect
     */
    public function setRoute($route)
    {
        $this->route = $route;
    
        return $this;
    }

    /**
     * Get route
     *
     * @return string 
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set oldPath
     *
     * @param string $oldPath
     * @return Redirect
     */
    public function setOldPath($oldPath)
    {
        $this->oldPath = $oldPath;
    
        return $this;
    }

    /**
     * Get oldPath
     *
     * @return string 
     */
    public function getOldPath()
    {
        return $this->oldPath;
    }

    /**
     * Set parameters
     *
     * @param array $parameters
     * @return Redirect
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    
        return $this;
    }

    /**
     * Get parameters
     *
     * @return array 
     */
    public function getParameters()
    {
        return $this->parameters;
    }
}
