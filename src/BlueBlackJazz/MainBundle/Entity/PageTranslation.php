<?php

namespace BlueBlackJazz\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 */
class PageTranslation
{
    use ORMBehaviors\Translatable\Translation,
        SeoTranslationTrait
        ;

    /**
     * @var string
     *
     * @ORM\Column(nullable=true, type="string", length=255)
     */
    private $title;

    /**
     *
     * @ORM\Column(nullable=true, type="text")
     */
    private $markdown;


    /**
     * Set title 
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set markdown
     *
     * @param string $markdown
     */
    public function setMarkdown($markdown)
    {
        $this->markdown = $markdown;
    
        return $this;
    }

    /**
     * Get markdown
     *
     * @return string 
     */
    public function getMarkdown()
    {
        return $this->markdown;
    }
}

