<?php

namespace BlueBlackJazz\MainBundle\Entity;


trait SeoTranslationTrait
    {
        /**
         * @ORM\Column(nullable=true, type="string")
         */
        private $seoDescription;
        /**
         * @ORM\Column(nullable=true, type="string")
         */
        private $seoKeywords;
        /**
         * @ORM\Column(nullable=true, type="string")
         */
        private $seoTitle;


        public function getSeoDescription() {
            return $this->seoDescription;
        }
        public function getSeoKeywords() {
            return $this->seoKeywords;
        }
        public function getSeoTitle() {
            return $this->seoTitle;
        }


        public function setSeoDescription($desc) {
            $this->seoDescription = $desc;
            return $this;
        }
        public function setSeoKeywords($kw) {
            $this->seoKeywords = $kw;
            return $this;
        }
        public function setSeoTitle($t) {
            $this->seoTitle = $t;
            return $this;
        }


    }
