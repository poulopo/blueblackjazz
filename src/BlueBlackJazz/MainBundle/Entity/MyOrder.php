<?php

namespace BlueBlackJazz\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Payment\CoreBundle\Entity\PaymentInstruction;

/**
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class MyOrder
{
    public function __construct()
    {
        $iv = mcrypt_create_iv(32, MCRYPT_DEV_URANDOM);
        $this->setSalt(bin2hex($iv));
        $this->transcriptions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->books = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="nDownloads", type="integer")
     */
    private $nDownloads = 0;

    /**
     * @var Datetime
     *
     * @ORM\Column(name="expiresAt", type="datetime", nullable=true)
     */
    private $expiresAt;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255, unique=true)
     */
    private $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="payerId", type="string", length=255, nullable=true)
     */
    private $payerId;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=3)
     */
    private $currency;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping", type="string", length=10)
     */
    private $shipping;


    /** @ORM\OneToOne(targetEntity="JMS\Payment\CoreBundle\Entity\PaymentInstruction") */
    private $paymentInstruction;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="decimal", scale=2)
     */
    private $amount;

    /**
     * @var float
     *
     * @ORM\Column(name="shippingCost", type="decimal", scale=2)
     */
    private $shippingCost = 0.0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cancelled", type="boolean")
     */
    private $cancelled = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="confirmed", type="boolean")
     */
    private $confirmed = false;


    /**
     * @ORM\ManyToMany(targetEntity="Transcription")
     */
    private $transcriptions;

    /**
     * @ORM\ManyToMany(targetEntity="Book")
     */
    private $books;


    /**
     * Add transcriptions
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Transcription $transcriptions
     * @return Book
     */
    public function addTranscription(\BlueBlackJazz\MainBundle\Entity\Transcription $transcriptions)
    {
        $this->transcriptions[] = $transcriptions;
    
        return $this;
    }

    /**
     * Remove transcriptions
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Transcription $transcriptions
     */
    public function removeTranscription(\BlueBlackJazz\MainBundle\Entity\Transcription $transcriptions)
    {
        $this->transcriptions->removeElement($transcriptions);
    }

    /**
     * Get transcriptions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranscriptions()
    {
        return $this->transcriptions;
    }

    /**
     * Add books
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Book $books
     * @return Artist
     */
    public function addBook(\BlueBlackJazz\MainBundle\Entity\Book $books)
    {
        $this->books[] = $books;
    
        return $this;
    }

    /**
     * Remove books
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Book $books
     */
    public function removeBook(\BlueBlackJazz\MainBundle\Entity\Book $books)
    {
        $this->books->removeElement($books);
    }

    /**
     * Get books
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBooks()
    {
        return $this->books;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Payment
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return Payment
     */
    public function setAmount($amount)
    {
        $this->amount= $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set cancelled
     *
     * @param boolean $cancelled
     * @return Payment
     */
    public function setCancelled($cancelled)
    {
        $this->cancelled = $cancelled;
    
        return $this;
    }

    /**
     * Get cancelled
     *
     * @return boolean 
     */
    public function getCancelled()
    {
        return $this->cancelled;
    }

    /**
     * Set confirmed
     *
     * @param boolean $confirmed
     * @return Payment
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;
    
        return $this;
    }

    /**
     * Get confirmed
     *
     * @return boolean 
     */
    public function getConfirmed()
    {
        return $this->confirmed;
    }

    public function getPaymentInstruction()
    {
        return $this->paymentInstruction;
    }

    public function setPaymentInstruction(PaymentInstruction $instruction)
    {
        $this->paymentInstruction = $instruction;
    }
    /**
     *
     * @param string $payerId
     * @return MyOrder
     */
    public function setPayerId($payerId)
    {
        $this->payerId= $payerId;
    
        return $this;
    }

    /**
     * @return string 
     */
    public function getPayerId()
    {
        return $this->payerId;
    }

    /**
     *
     * @param string $cur
     * @return MyOrder
     */
    public function setCurrency($cur)
    {
        $this->currency = $cur;
    
        return $this;
    }

    /**
     * @return string 
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set salt 
     *
     * @param string $salt
     * @return MyOrder
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    
        return $this;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set shipping
     *
     * @param string $shipping
     * @return MyOrder
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;
    
        return $this;
    }

    /**
     * Get shipping
     *
     * @return string 
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * Set shippingCost
     *
     * @param float $shippingCost
     * @return MyOrder
     */
    public function setShippingCost($shippingCost)
    {
        $this->shippingCost = $shippingCost;
    
        return $this;
    }

    /**
     * Get shippingCost
     *
     * @return float 
     */
    public function getShippingCost()
    {
        return $this->shippingCost;
    }

    /**
     * Set nDownloads
     *
     * @param integer $nDownloads
     * @return MyOrder
     */
    public function setNDownloads($nDownloads)
    {
        $this->nDownloads = $nDownloads;
    
        return $this;
    }

    /**
     * Get nDownloads
     *
     * @return integer 
     */
    public function getNDownloads()
    {
        return $this->nDownloads;
    }

    /**
     * Set expiresAt
     *
     * @param \DateTime $expiresAt
     * @return MyOrder
     */
    public function setExpiresAt($expiresAt)
    {
        $this->expiresAt = $expiresAt;
    
        return $this;
    }

    /**
     * Get expiresAt
     *
     * @return \DateTime 
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }
}
