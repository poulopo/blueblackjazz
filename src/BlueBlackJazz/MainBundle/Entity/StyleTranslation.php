<?php

namespace BlueBlackJazz\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 */
class StyleTranslation
{
    use ORMBehaviors\Translatable\Translation,
        SeoTranslationTrait
        ;

    /**
     * @var string
     *
     * @ORM\Column(nullable=true, type="string", length=255)
     */
    private $name;

    /**
     *
     * @ORM\Column(nullable=true, type="text")
     */
    private $markdown;



    /**
     * Set markdown
     *
     * @param string $markdown
     * @return ArtistTranslation
     */
    public function setMarkdown($markdown)
    {
        $this->markdown = $markdown;
    
        return $this;
    }

    /**
     * Get markdown
     *
     * @return string 
     */
    public function getMarkdown()
    {
        return $this->markdown;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return StyleTranslation
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}
