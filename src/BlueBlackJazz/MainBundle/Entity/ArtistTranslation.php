<?php

namespace BlueBlackJazz\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 */
class ArtistTranslation
{
    use ORMBehaviors\Translatable\Translation,
        SeoTranslationTrait
        ;

    /**
     * @var string
     *
     * @ORM\Column(nullable=true, type="string", length=255)
     */
    private $subtitle;

    /**
     *
     * @ORM\Column(nullable=true, type="text")
     */
    private $markdown;


    /**
     * Set subtitle
     *
     * @param string $subtitle
     * @return ArtistTranslation
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    
        return $this;
    }

    /**
     * Get subtitle
     *
     * @return string 
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Set markdown
     *
     * @param string $markdown
     * @return ArtistTranslation
     */
    public function setMarkdown($markdown)
    {
        $this->markdown = $markdown;
    
        return $this;
    }

    /**
     * Get markdown
     *
     * @return string 
     */
    public function getMarkdown()
    {
        return $this->markdown;
    }
}
