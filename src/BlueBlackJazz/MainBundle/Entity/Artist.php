<?php

namespace BlueBlackJazz\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

use BlueBlackJazz\MainBundle\Entity\Transcription;
use BlueBlackJazz\MainBundle\Entity\Book;
use BlueBlackJazz\MainBundle\Entity\Style;

/**
 * Artist
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Artist
{
    use ORMBehaviors\Translatable\Translatable,
        ORMBehaviors\Sluggable\Sluggable
        ;

    public function getSluggableFields()
    {
        return ['name'];
    }
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Transcription", mappedBy="artist")
     */
    private $transcriptions;

    /**
     * @ORM\OneToMany(targetEntity="Book", mappedBy="artist")
     */
    private $books;

    /**
     * @ORM\ManyToOne(targetEntity="Style",inversedBy="artists")
     */
    private $style;

    /**
     * @var string
     *
     * @ORM\Column(unique=true, name="name", type="string", length=255)
     */
    private $name;

    public function __toString(){
        return $this->getName();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Artist
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->transcriptions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->books = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add transcriptions
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Transcription $transcriptions
     * @return Artist
     */
    public function addTranscription(\BlueBlackJazz\MainBundle\Entity\Transcription $transcriptions)
    {
        $this->transcriptions[] = $transcriptions;
    
        return $this;
    }

    /**
     * Remove transcriptions
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Transcription $transcriptions
     */
    public function removeTranscription(\BlueBlackJazz\MainBundle\Entity\Transcription $transcriptions)
    {
        $this->transcriptions->removeElement($transcriptions);
    }

    /**
     * Get transcriptions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranscriptions()
    {
        return $this->transcriptions;
    }

    /**
     * Add books
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Book $books
     * @return Artist
     */
    public function addBook(\BlueBlackJazz\MainBundle\Entity\Book $books)
    {
        $this->books[] = $books;
    
        return $this;
    }

    /**
     * Remove books
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Book $books
     */
    public function removeBook(\BlueBlackJazz\MainBundle\Entity\Book $books)
    {
        $this->books->removeElement($books);
    }

    /**
     * Get books
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBooks()
    {
        return $this->books;
    }

    /**
     * Set style
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Style $style
     * @return Artist
     */
    public function setStyle(\BlueBlackJazz\MainBundle\Entity\Style $style = null)
    {
        $this->style = $style;
    
        return $this;
    }

    /**
     * Get style
     *
     * @return \BlueBlackJazz\MainBundle\Entity\Style 
     */
    public function getStyle()
    {
        return $this->style;
    }
}
