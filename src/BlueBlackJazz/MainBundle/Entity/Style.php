<?php

namespace BlueBlackJazz\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

use BlueBlackJazz\MainBundle\Entity\Transcription;
use BlueBlackJazz\MainBundle\Entity\Book;
use BlueBlackJazz\MainBundle\Entity\Artist;

/**
 * Artist
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Style
{
    use ORMBehaviors\Translatable\Translatable,
        ORMBehaviors\Sluggable\Sluggable
        ;

    public function getSluggableFields()
    {
        return ['title'];
    }
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    public function __toString(){
        return $this->getTitle();
    }

    /**
     * @ORM\OneToMany(targetEntity="Transcription", mappedBy="style")
     */
    private $transcriptions;

    /**
     * @ORM\OneToMany(targetEntity="Book", mappedBy="style")
     */
    private $books;

    /**
     * @ORM\OneToMany(targetEntity="Artist", mappedBy="style")
     */
    private $artists;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->transcriptions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->books = new \Doctrine\Common\Collections\ArrayCollection();
        $this->artists = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add transcriptions
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Transcription $transcriptions
     * @return Style
     */
    public function addTranscription(\BlueBlackJazz\MainBundle\Entity\Transcription $transcriptions)
    {
        $this->transcriptions[] = $transcriptions;
    
        return $this;
    }

    /**
     * Remove transcriptions
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Transcription $transcriptions
     */
    public function removeTranscription(\BlueBlackJazz\MainBundle\Entity\Transcription $transcriptions)
    {
        $this->transcriptions->removeElement($transcriptions);
    }

    /**
     * Get transcriptions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranscriptions()
    {
        return $this->transcriptions;
    }

    /**
     * Add books
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Book $books
     * @return Style
     */
    public function addBook(\BlueBlackJazz\MainBundle\Entity\Book $books)
    {
        $this->books[] = $books;
    
        return $this;
    }

    /**
     * Remove books
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Book $books
     */
    public function removeBook(\BlueBlackJazz\MainBundle\Entity\Book $books)
    {
        $this->books->removeElement($books);
    }

    /**
     * Get books
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBooks()
    {
        return $this->books;
    }

    /**
     * Add artists
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Artist $artists
     * @return Style
     */
    public function addArtist(\BlueBlackJazz\MainBundle\Entity\Artist $artists)
    {
        $this->artists[] = $artists;
    
        return $this;
    }

    /**
     * Remove artists
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Artist $artists
     */
    public function removeArtist(\BlueBlackJazz\MainBundle\Entity\Artist $artists)
    {
        $this->artists->removeElement($artists);
    }

    /**
     * Get artists
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArtists()
    {
        return $this->artists;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Book
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }
}
