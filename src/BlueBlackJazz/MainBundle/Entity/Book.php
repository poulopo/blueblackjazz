<?php

namespace BlueBlackJazz\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

use BlueBlackJazz\MainBundle\Entity\Transcription;
use BlueBlackJazz\MainBundle\Entity\Artist;
use BlueBlackJazz\MainBundle\Entity\Style;

/**
 * Book
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Book
{
    use ORMBehaviors\Translatable\Translatable,
        ORMBehaviors\Sluggable\Sluggable
        ;

    public function getSluggableFields()
    {
        return ['artist', 'title'];
    }

    /**
     *  * @ORM\Column(type="decimal", scale=2)
     *   */
    protected $priceEuro = 44.95;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\OneToMany(targetEntity="Transcription", mappedBy="book")
     * @ORM\OrderBy({"track" = "ASC"})
     */
    private $transcriptions;
    /**
     * @ORM\ManyToOne(targetEntity="Artist",inversedBy="books")
     */
    private $artist;

    /**
     * @ORM\ManyToOne(targetEntity="Style",inversedBy="books")
     */
    private $style;

    /**
     * @var integer
     *
     * @ORM\Column(nullable=true, unique=true, name="item_number", type="integer")
     */
    private $itemNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    public function __toString(){
        if ($this->getArtist()){
            return $this->getArtist()->getName() . ' - ' . $this->getTitle();
        } else {
            return $this->getTitle();
        }
    }

    /**
     * @var string
     *
     * @ORM\Column(nullable=true, name="pdf", type="string", length=255)
     */
    private $pdf;



    /**
     * @var string
     *
     * @ORM\Column(nullable=true, name="img_cover", type="string", length=255)
     */
    private $imgCover;

    /**
     * @var string
     *
     * @ORM\Column(nullable=true, name="img_other", type="string", length=255)
     */
    private $imgOther;

    /**
     * @var string
     *
     * @ORM\Column(nullable=true, name="img_table", type="string", length=255)
     */
    private $imgTable;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->transcriptions = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set itemNumber
     *
     * @param integer $itemNumber
     * @return Book
     */
    public function setItemNumber($itemNumber)
    {
        $this->itemNumber = $itemNumber;
    
        return $this;
    }

    /**
     * Get itemNumber
     *
     * @return integer 
     */
    public function getItemNumber()
    {
        return $this->itemNumber;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Book
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set pdf
     *
     * @param string $pdf
     * @return Book
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;
    
        return $this;
    }

    /**
     * Get pdf
     *
     * @return string 
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set imgCover
     *
     * @param string $imgCover
     * @return Book
     */
    public function setImgCover($imgCover)
    {
        $this->imgCover = $imgCover;
    
        return $this;
    }

    /**
     * Get imgCover
     *
     * @return string 
     */
    public function getImgCover()
    {
        return $this->imgCover;
    }

    /**
     * Set imgOther
     *
     * @param string $imgOther
     * @return Book
     */
    public function setImgOther($imgOther)
    {
        $this->imgOther = $imgOther;
    
        return $this;
    }

    /**
     * Get imgOther
     *
     * @return string 
     */
    public function getImgOther()
    {
        return $this->imgOther;
    }

    /**
     * Set imgTable
     *
     * @param string $imgTable
     * @return Book
     */
    public function setImgTable($imgTable)
    {
        $this->imgTable = $imgTable;
    
        return $this;
    }

    /**
     * Get imgTable
     *
     * @return string 
     */
    public function getImgTable()
    {
        return $this->imgTable;
    }

    /**
     * Add transcriptions
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Transcription $transcriptions
     * @return Book
     */
    public function addTranscription(\BlueBlackJazz\MainBundle\Entity\Transcription $transcriptions)
    {
        $this->transcriptions[] = $transcriptions;
    
        return $this;
    }

    /**
     * Remove transcriptions
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Transcription $transcriptions
     */
    public function removeTranscription(\BlueBlackJazz\MainBundle\Entity\Transcription $transcriptions)
    {
        $this->transcriptions->removeElement($transcriptions);
    }

    /**
     * Get transcriptions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranscriptions()
    {
        return $this->transcriptions;
    }

    /**
     * Set artist
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Artist $artist
     * @return Book
     */
    public function setArtist(\BlueBlackJazz\MainBundle\Entity\Artist $artist = null)
    {
        $this->artist = $artist;
    
        return $this;
    }

    /**
     * Get artist
     *
     * @return \BlueBlackJazz\MainBundle\Entity\Artist 
     */
    public function getArtist()
    {
        return $this->artist;
    }
    /**
     * Set style
     *
     * @param \BlueBlackJazz\MainBundle\Entity\Style $style
     * @return Book
     */
    public function setStyle(\BlueBlackJazz\MainBundle\Entity\Style $style = null)
    {
        $this->style = $style;
    
        return $this;
    }

    /**
     * Get style
     *
     * @return \BlueBlackJazz\MainBundle\Entity\Style 
     */
    public function getStyle()
    {
        return $this->style;
    }


    /**
     * Set priceEuro
     *
     * @param float $priceEuro
     * @return Book
     */
    public function setPriceEuro($priceEuro)
    {
        $this->priceEuro = $priceEuro;
    
        return $this;
    }

    /**
     * Get priceEuro
     *
     * @return float 
     */
    public function getPriceEuro()
    {
        return $this->priceEuro;
    }

}
