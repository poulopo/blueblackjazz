<?php

namespace BlueBlackJazz\MainBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class MyOrderAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            // ->add('expiresAt')
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('email')
            ->add('id')
            ->add('currency')
            ->add('shipping')
            ->add('payerId')
            ->add('amount')
            ->add('expiresAt')
            ->add('confirmed')
            ->add('cancelled')
            ->add('books')
            ->add('transcriptions')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('email')
            ->add('currency')
            ->add('shipping')
            ->add('payerId')
            ->add('amount')
            ->add('expiresAt')
            ->add('confirmed')
            ->add('cancelled')
            ->add('books')
            ->add('transcriptions')
        ;
    }
}

