<?php

namespace BlueBlackJazz\MainBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class TranscriptionAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('priceEuro')
            ->add('style')
            ->add('artist')
            ->add('book')
            ->add('mp3')
            ->add('otherMp3s')
            ->add('track')
            ->add('note')
            ->add('pdf')
            ->add('imgTitle')
            ->add('itemNumber')
            ->add('translations', 'a2lix_translations')
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('artist')
            ->add('book')
            ->add('name')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('priceEuro')
            ->add('artist')
            ->add('style')
            ->add('book')
            ->add('id')
            ->add('pdf')
            ->add('mp3')
            ->add('itemNumber')
            ->add('track')
            ->add('slug')
        ;
    }
}
