<?php

namespace BlueBlackJazz\MainBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class BookAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title')
            ->add('priceEuro')
            ->add('style')
            ->add('artist')
            ->add('pdf')
            ->add('imgCover')
            ->add('imgOther')
            ->add('imgTable')
            ->add('itemNumber')
            ->add('translations', 'a2lix_translations')
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('artist')
            ->add('title')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->addIdentifier('title')
            ->add('priceEuro')
            ->add('artist')
            ->add('style')
            ->add('pdf')
            ->add('itemNumber')
            ->add('slug')
        ;
    }
}

