<?php

namespace BlueBlackJazz\MainBundle\Service;
use BlueBlackJazz\MainBundle\Entity\MyOrder;

use Symfony\Component\HttpFoundation\Request;

class Cart {
    public function __construct($doctrine, $session, $converter)
    {
        $this->artistRepository = $doctrine->getRepository('BlueBlackJazzMainBundle:Artist');
        $this->bookRepository = $doctrine->getRepository('BlueBlackJazzMainBundle:Book');
        $this->transcriptionRepository = $doctrine->getRepository('BlueBlackJazzMainBundle:Transcription');
        $this->styleRepository = $doctrine->getRepository('BlueBlackJazzMainBundle:Style');
        $this->session = $session;
        $this->myOrder = null;
        $this->converter = $converter;
    }
    protected $request;

    public function setRequest(Request $request = null){
        $this->request = $request;
    }
    public function addTranscriptionById($id){
        $this->session->set("cart/transcriptions/$id", $id);
    }

    public function removeTranscriptionById($id){
        $this->session->remove("cart/transcriptions/$id");
    }

    public function addBookById($id){
        $this->session->set("cart/books/$id", $id);
    }

    public function removeBookById($id){
        $this->session->remove("cart/books/$id");
    }

    public function setFavoriteCurrency($currency){
        if($currency === 'EUR' || $currency === 'USD'){
            $this->session->set("cart/currency", $currency);
        } else {
            Throw Exception("Invalid currency value $currency");
        }
    }
    public function getFavoriteCurrency(){
        return $this->session->get('cart/currency', 'EUR');
    }

    public function getShipping(){
        return $this->session->get('cart/shipping', 'PDF');
    }

    public function setShipping($shipping){
        if(
            $shipping === 'PDF'
            || $shipping === 'FRANCE'
            || $shipping === 'EUROPE'
            || $shipping === 'WORLD'
        ){
            $this->session->set("cart/shipping", $shipping);
        } else {
            Throw Exception("Invalid shipping value $shipping");
        }
    }
    public function getCurrentShippingCostPerBookEuro(){
        $shipping = $this->getShipping();
        return $this->getShippingCostPerBookEuro($shipping);
    }
    public function getShippingCostPerBookEuro($shipping){
        if($shipping === 'PDF'){return 0.0;}
        if($shipping === 'FRANCE'){return 4.0;}
        if($shipping === 'EUROPE'){return 7.0;}
        if($shipping === 'WORLD'){return 9.0;}
        Throw Exception("Invalid shipping value $shipping");

    }

    public function getOrder(){
        if($this->myOrder !== null){
            return $this->myOrder;
        }
        $order = new MyOrder();
        if($this->session->has('cart/email')){
            $order->setEmail($this->session->get('cart/email'));
        }
        if($this->request->get('email')){
            $order->setEmail($this->request->get('email'));
        }

        $order->setCurrency($this->getFavoriteCurrency());
        $order->setShipping($this->getShipping());

        if($this->session->has('cart/books')){
            $cartBookIds = array_values($this->session->get('cart/books'));
            if($cartBookIds){
                $books = $this->bookRepository->findById($cartBookIds);
                foreach($books as $book){
                    $order->addBook($book);
                    $order->setShippingCost(
                        $order->getShippingCost()
                        +
                        $this->converter->convert(
                            $this->getCurrentShippingCostPerBookEuro(),
                            $order->getCurrency()
                        )
                    );
                }
            }
        }

        if($this->session->has('cart/transcriptions')){
            $cartTranscriptionIds = array_values($this->session->get('cart/transcriptions'));
            if($cartTranscriptionIds){
                $transcriptions = $this->transcriptionRepository->findById($cartTranscriptionIds);
                foreach($transcriptions as $transcription){
                    $order->addTranscription($transcription);
                }
            }
        }

        $total = $order->getShippingCost();
        foreach($order->getBooks() as $book){
            $total += $convertedAmount = $this->converter->convert($book->getPriceEuro(), $order->getCurrency());
        }
        foreach($order->getTranscriptions() as $transcription){
            $total += $convertedAmount = $this->converter->convert($transcription->getPriceEuro(), $order->getCurrency());
        }
        $order->setAmount($total);

        $this->myOrder = $order;

        return $order;
    }
    public function emptyAll(){
        $this->session->remove('cart');
    }
}
