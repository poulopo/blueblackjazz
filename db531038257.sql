-- phpMyAdmin SQL Dump
-- version 4.1.14.5
-- http://www.phpmyadmin.net
--
-- Client :  db531038257.db.1and1.com
-- Généré le :  Jeu 02 Octobre 2014 à 21:39
-- Version du serveur :  5.1.73-log
-- Version de PHP :  5.4.4-14+deb7u14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `db531038257`
--

-- --------------------------------------------------------

--
-- Structure de la table `Artist`
--

CREATE TABLE IF NOT EXISTS `Artist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `style_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_6F593B15E237E06` (`name`),
  KEY `IDX_6F593B1BACD6074` (`style_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

--
-- Contenu de la table `Artist`
--

INSERT INTO `Artist` (`id`, `style_id`, `name`, `slug`) VALUES
(1, 2, 'Fats Waller', 'fats-waller'),
(2, 2, 'Willie "the Lion" Smith', 'willie-the-lion-smith'),
(3, 2, 'James P. Johnson', 'james-p-johnson'),
(4, 2, 'Donald Lambert', 'donald-lambert'),
(5, 1, 'Albert Ammons', 'albert-ammons'),
(6, 1, 'Pete Johnson', 'pete-johnson'),
(7, 1, 'Jimmy Yancey', 'jimmy-yancey'),
(8, 1, 'Meade Lux Lewis', 'meade-lux-lewis'),
(9, 1, '"Pine Top" Smith', 'pine-top-smith'),
(10, 1, 'Jimmy Blythe', 'jimmy-blythe'),
(11, 2, 'Mary Lou Williams', 'mary-lou-williams'),
(12, 1, 'Count Basie', 'count-basie'),
(13, 1, 'Hersal Thomas', 'hersal-thomas'),
(14, 2, 'Pat Flowers', 'pat-flowers'),
(15, 1, 'Sammy Price', 'sammy-price'),
(16, 2, 'Cliff Jackson', 'cliff-jackson'),
(17, 2, 'Ralph Sutton', 'ralph-sutton'),
(18, 2, 'Don Ewell', 'don-ewell'),
(19, 2, 'Fletcher Henderson', 'fletcher-henderson'),
(20, 2, 'Eubie Blake', 'eubie-blake'),
(21, 2, 'Earl Hines', 'earl-hines'),
(22, 2, 'Alex Hill', 'alex-hill');

-- --------------------------------------------------------

--
-- Structure de la table `ArtistTranslation`
--

CREATE TABLE IF NOT EXISTS `ArtistTranslation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) DEFAULT NULL,
  `subtitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `markdown` longtext COLLATE utf8_unicode_ci,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seoDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seoKeywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seoTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9C62D7432C2AC5D34180C698` (`translatable_id`,`locale`),
  KEY `IDX_9C62D7432C2AC5D3` (`translatable_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=45 ;

--
-- Contenu de la table `ArtistTranslation`
--

INSERT INTO `ArtistTranslation` (`id`, `translatable_id`, `subtitle`, `markdown`, `locale`, `seoDescription`, `seoKeywords`, `seoTitle`) VALUES
(1, 3, 'The Father Of Stride Piano" - (1894 - 1955)', '<img style="width: 250px;float: right" src="http://www.blueblackjazz.com/media/images/johnson.jpg" />\r\n\r\nThe pianist James P. Johnson (1894-1955) created Stride piano by developing the game of the great Ragtime pianists in a less restrictive music . He started to play in bars, cabarets and rent-parties, and quickly became famous for his virtuosity and his compositions. From 1916, he recorded and published piano rolls in a style which is no longer ragtime. With a revolutionary game, he used all the sounds he could play on the keyboard. From 1921, he recorded his first Stride piano compositions where he mingled syncopations with blues notes. <a href="http://www.blueblackjazz.com/en/transcription/54/54-james-p-johnson-carolina-shout">Carolina Shout</a> is considered as the first recording of Jazz on a piano. In the same period, James P. Johnson recorded with the greatest Blues singers and became Bessie Smith''s favourite accompanist. \r\n\r\nMP3((bleeding.mp3))\r\n\r\n<img style="width: 250px;float: left" src="http://www.blueblackjazz.com/media/images/jamesp.jpg" />\r\n\r\nAround 1919, he looked after the 15 years old <a href="http://blueblackjazz.com/en/artist/1/fats-waller">Fats Waller</a> whose mother had just died. During several years he gave piano lessons to the young boy and launched him then into an extraordinary career. For his part, he continued to record a lot of solo pieces or to accompany orchestras. \r\n\r\nJames P. Johnson''s playing can be recognized by his light touch and a rhythmic, almost percussive game. His dexterity and his unusual inventiveness make him a piano virtuoso, the first "real" jazz pianist whose musicality managed to grow over generations. It is already known since the late 1910s for his flawless technique, perfect mastery of the instrument will culminate in the 30 and 40. \r\n\r\n<img style="width: 250px;float: right" src="http://www.blueblackjazz.com/media/images/johnson3.jpg" />\r\n\r\nJames P. Johnson played an important part in Jazz history. Thanks to him, Ragtime has evolved towards an improvised music free from rules. He left us a lot of compositions and standards like Charleston, one of the most recorded tunes in the 20''s. Even so, he never received the recognition he deserved and died in 1955 nearly forgotten by everyone. \r\n\r\nMP3((balmoral.mp3))', 'en', 'James P. Johnson biography. James P. Johnson transcriptions for piano. Stride piano sheet music', 'james p. johnson, stride piano, sheet music, harlem stride, ragtime, stride, transcription, jazz piano, mp3, blueblackjazz', 'James P. Johnson | BlueBlackJazz'),
(2, 3, '1894 - 1955', '<img style="width: 250px;float: right" src="http://www.blueblackjazz.com/media/images/johnson.jpg" />\r\n\r\nC''est le pianiste James Price Johnson qui a donné naissance au Piano Stride de Harlem. Il apprend le piano très jeune découvre les grand compositeurs de ragtime, ainsi que des pianistes comme <a href="http://blueblackjazz.com/fr/artist/20/eubie-blake">Eubie Blake</a> et Luckey Roberts au jeu déjà très novateur. Débutant sa carrière dans les bars, les cabarets et les rent-parties, il a rapidement acquis une très grande notoriété en tant que virtuose et compositeur et se met à enregistrer et produire des piano rolls dés 1916 dans un style emprunté aux maitres du ragtime, mais qui n''en est plus vraiment : par son jeu révolutionnaire, il a su exploiter toutes les palettes sonores du clavier, et dès 1921 il enregistra ses premières compositions de ce genre, où se mêlaient syncopes et blues notes. <a href="http://www.blueblackjazz.com/fr/transcription/54/54-james-p-johnson-carolina-shout">Carolina Shout</a> est ainsi considéré comme le premier enregistrement de piano en Jazz. Il enregistre vers la même époque au côté des plus grandes chanteuses de blues, et devient notamment l''accompagnateur privilégié de Bessie Smith. \r\n\r\nMP3((bleeding.mp3))\r\n\r\n<img style="width: 250px;float: left" src="http://www.blueblackjazz.com/media/images/jamesp.jpg" />\r\n\r\nVers 1920, il rencontre le jeune <a href="http://blueblackjazz.com/fr/artist/1/fats-waller">Fats Waller</a> fasciné par le jeu de son aîné et à qui il donnera une solide formation de piano durant plusieurs années et l''introduira dans le milieu musical de Harlem et le rent-parties. Par la suite, James P. Johnson continuera à enregistrer un grand nombre de morceaux en solo ou accompagnant des orchestres. \r\n\r\nOn reconnait le style de James P. Johnson par son toucher assez léger, un jeu rythmique voir percussif. Sa dextérité et son inventivité hors du commun font du lui un virtuose du piano, le premier "vrai" pianiste de jazz dont la portée musicale a su se développer au fil des générations. Il est déjà réputé dès la fin des années 1910 pour sa technique infaillible, une parfaite maîtrise de l''instrument qui culminera dans les années 20 et 30, ou il enregistrera de mémorables solos de piano. Bien que des ennuis de santé entachent ses capacités physiques dès 1940, on peut l’entendre jouer encore merveilleusement entre 1942 et 1947. Il finit par être complètement paralysé à la suite d’une attaque survenue en 1951.\r\n\r\n<img style="width: 250px;float: right" src="http://www.blueblackjazz.com/media/images/johnson3.jpg" />\r\n\r\nJames P. Johnson aura joué un rôle déterminant dans l''histoire jazz et dans l''évolution du ragtime vers une musique improvisée et à la pulsation swing. Il a aussi laissé de nombreuses compositions et standards, dont Charleston qui est l''un des airs les plus enregistrés durant les années 20, mais aussi <a href="http://blueblackjazz.com/fr/transcription/97/97-james-p-johnson-old-fashioned-love">Old Fashioned Love</a>, If I Could Be With You, A Porter''s Love Song to a Chambermaid. Son influence est colossale puisque les grands chefs d’orchestre de l’ère du Jazz (<a href="http://blueblackjazz.com/fr/artist/19/fletcher-henderson">Fletcher Henderson</a>, Duke Ellington, <a href="http://blueblackjazz.com/fr/artist/12/count-basie">Count Basie</a>) sont des pianistes qui ont subi son influence directe. Malgré tout, il ne recevra pas la reconnaissance qu''il mérite et mourra presque dans l''oubli en 1955. \r\n\r\nMP3((balmoral.mp3))', 'fr', 'Biographie de James P. Johnson. Transcriptions pour piano. Partitions de piano stride', 'james p. johnson, piano stride, partition, harlem stride, ragtime, stride, transcription, piano jazz, mp3, blueblackjazz', 'James P. Johnson | BlueBlackJazz'),
(3, 1, '1904 - 1943', 'Thomas "Fats" Waller fut une des personnalités majeurs du Jazz. Il fut l''élève de <a href="http://www.blueblackjazz.com/fr/artist/3/james-p-johnson">James P. Johnson</a>, mais dépassa bien vite son maître en notoriété. Avec lui, la perfection du phrasé, l''originalité, le swing semblent d''une évidence déconcertante. Non content d''être un pianiste hors paire, Fats Waller était aussi chanteur, chef d''orchestre, compositeur prolifique de chansons à succès, et amuseur de scène extraverti. Une main gauche de fer soutenant une solide rythmique, de l''audace, de l''imagination, ou très souvent des loufoquerie se laissent entendre par dessus les riffs tissés au clavier, voila ce qui caractérise la spontanéité et la joie de vivre de ce gros bonhomme, personnalité unique dans l''histoire du Jazz. \r\n\r\nMP3((when_you_and_i_where_young_maggie_-_1939.mp3))\r\n\r\n<img style="float: left" src="http://www.blueblackjazz.com/media/images/waller10.jpg" />\r\n\r\nLe jeune Fats sera d''abord sensibilisé à la musique en écoutant de l''orgue à l''église. Son père étant pasteur, il apprendra très tôt à en jouer et c''est la pratique de cet instrument complexe qui lui permettra d''acquérir une très bonne précision dans son jeu de piano. Il restera d''ailleurs attaché a l''orgue toute sa vie. Il est le premier à avoir intégré cet intrument dans le jazz. \r\n\r\nIl perd sa mère à l''âge de 16 ans, se brouille avec son père et fait un fugue. Il s''installe chez le pianiste <a href="http://www.blueblackjazz.com/fr/artist/3/james-p-johnson">James P. Johnson</a>, grande figure pianistique de Harlem qui lui donnera de solides leçons de piano. Il gardera cependant des sequelles de cette époque et restera toute sa vie un personnage ambigu à deux facette, a la fois joyeux, mais aussi hanté par son passé, et en proie à de gros coups de blues.\r\n\r\nMP3((saint_louis_blues_-_(organ,_1926).mp3))\r\n\r\nSa toute première scéance d''enregistrement a lieu en 1922, alors qu''il a 18 ans. Il grave <a href="http://www.blueblackjazz.com/fr/transcription/18/18-fats-waller-muscle-shoals-blues">Muscle Shoals Blues</a> and <a href="http://www.blueblackjazz.com/fr/transcription/19/19-fats-waller-birmingham-blues">Birmingham Blues</a>, qui constituent ses premières pièces de piano solo. Fats se sent déjà très à l''aise dans les studios d''enregistrement et on perçoit clairement une aisance technique et artistique.\r\n\r\nMP3((birmingham_blues_-_1922.mp3))\r\n\r\n<img style="width: 200px;float: right" src="http://www.blueblackjazz.com/media/images/muscleshoalsblues.JPG" />\r\n\r\nPar la suite, avant de devenir célèbre, Fats va accompagner plusieurs chanteuses de blues et produire des Piano Rolls, très populaires à l''époque où le disque n''était pas encore très répandu. Il est trés présent dans la vie nocturne de Harlem et dans les ''rent parties'', soirées organisées chez les pianistes et qui leur permettaient de payer leur loyer.\r\n\r\nSa rencontre avec le parolier Andy Razaf sera déterminante,et ils formeront un duo prolifique dans la composition de chansons. Ils leur arrivaient au début de leur carrière de revendre pour des broutilles des compositions qui deviendront d''énormes succès. Ainsi, plusieurs standards célèbres sont supposés être de sa composition.\r\n\r\nMP3((beale_street_blues_-_(organ,_1927).mp3))\r\n\r\n<img style="width: 250px;float: left" src="http://www.blueblackjazz.com/media/images/waller-organ2.jpg" />\r\n\r\nSa notoriété est grandissante, Fats abandonne peu à peu les rent partie s, étant d''avantage sollicité pour des soirées en ville données par des millionnaires. Pour autant, Fats reste une personne peu soucieuse de l''argent et il lui arrive de jouer en échange de rien, si ce n''est une soirée bien arrosée. Il sortait alors entouré d''une foule de personnes enthousiasmées et parfois un bon cigare.\r\n\r\nDans les années 1920, les disques de piano avaient beaucoup de succès bien que l''instrument soit difficile à enregistrer. Fats sera invité à graver une belle série solos en 1929. Ces sessions d''enregistrement ont laissé quelques compositions originales parmi les plus célèbres du pianiste : <a href="http://www.blueblackjazz.com/fr/transcription/20/20-fats-waller-handful-of-keys">Handful of Keys</a>, <a href="http://www.blueblackjazz.com/fr/transcription/22/22-fats-waller-sweet-savannah-sue">Sweet Savannah Sue</a>, <a href="http://www.blueblackjazz.com/fr/transcription/6/6-fats-waller-smashing-thirds">Smashing Thirds</a>, <a href="http://www.blueblackjazz.com/fr/transcription/4/4-fats-waller-valentine-stomp">Valentine Stomp</a>, <a href="http://www.blueblackjazz.com/fr/transcription/2/2-fats-waller-numb-fumblin">Numb Fumblin''</a>. \r\n\r\nMP3((aint_misbehavin_-_1929.mp3))\r\n\r\n<img style="width: 200px;float: right" src="http://www.blueblackjazz.com/media/images/willie10.jpg" />\r\n\r\nDurant les années de la crise, Fats n''enregistrera que très peu : Deux morceaux au piano en duo avec Bennie Payne (futur pianiste de Cab Calloway) en 1930, puis deux autres en 1931, I''m Crazy ''Bout my Baby et Draggin'' My Heart Around dans lesquelles on peut l''entendre chanter pour la première fois. Il accompagnera aussi plusieurs orchestres en vogue. \r\n\r\nC''est en 1934 au cours d''une soirée organisée par Georges Gershwin, pendant laquelle il faisait le clown au piano, qu''il se fait remarquer par un représentant des studios Victor. Celui-ci le fait signer, et Fats enregistrera jusqu''à la fin de sa vie avec cette société. Il forme la même année son orchestre de 5 ou 6 musiciens, Fats Waller & His Rhythm, avec qui il connaîtra un immense succès. Très bon leader d''orchestre par son énergie et sa vitalité, Fats se produira jusqu''à la fin de sa vie avec cette même formation en enregistrant plus de 400 morceaux.\r\n\r\nMP3((latch_on_-_fats_waller_and_his_rhythm,_1938.mp3))\r\n\r\n<img style="width: 400px" src="http://www.blueblackjazz.com/media/images/rhythm22.jpg" />\r\n\r\nC''est aussi par le biais de la radio que Fats devient une vedette. Il fut l''un des premiers jazzmen à utiliser ce média, et aussi l''un des premiers afro-américains à disposer d''une émission radiophonique régulière. Bout-en-train par excellence, et doté d''une spontanéité à toute épreuve, il était une personnalité très appréciée du public. Il apparaîtra dans de nombreux shows radiophoniques à partir de 1935.\r\n\r\nMP3((go_down_moses_(radio_broadcast,_c.1941).mp3))\r\n\r\n<img style="width: 200px;float: left" src="http://www.blueblackjazz.com/media/images/waller12.jpg" />\r\n\r\nDe son vivant Fats Waller, bien que très apprécié du public, était plus considéré comme un clown et un amuseur de scène que comme un réel musicien de jazz. Il a toujours été affecté par ce manque de considération. Pourtant, de nombreux enregistrements nous prouvent que Fats savait s''arrêter dans ses pitreries et était un musicien très inspiré, qui a laissé quelques belles pages de poésie musicale...\r\nMP3((honeysuckle_rose_-_1941.mp3))\r\n\r\n![](http://www.blueblackjazz.com/media/images/waller3.jpg)\r\n\r\nComme beaucoup d''autres génies, Fats Waller a disparu bien trop tôt... Il aura mené une vie intense dans laquelle ses nombreux excès (nourriture, alcool) sont sans doute responsables de sa disparition prématurée :\r\nUne pneumonie l''emporta un soir de décembre, dans ce train qui le ramenait de la côte ouest.\r\nIl était à l''apogée de sa gloire, il avait 39 ans. Qui sait jusqu''où sa musique aurait pu nous transporter... \r\n"One never knows, do one...?"', 'fr', 'Biographie de Thomas "Fats" Waller. Fats Waller transcriptions pour piano. Partitions de piano stride', 'fats waller, piano stride, partition, harlem stride, ragtime, stride, transcription, piano jazz, mp3, blueblackjazz', 'Fats Waller | BlueBlackJazz'),
(4, 1, '1904 - 1943', 'Thomas « Fats » Waller was one of the greatest personalities of jazz. He was <a href="http://www.blueblackjazz.com/en/artist/3/james-p-johnson">James P. Johnson</a>''s pupil but quickly exceeded him in notability and technique. The perfection of his "phrasé", the originality of his game, his "swing", seem naturally obvious. He not only was an unrivaled pianist. he also was a singer, a conductor, an abounding successful composer and a great entertainer. An iron left hand supported a solid rhythmic, boldness, imagination and strange sounds that we can often hear over « riffs » weaved in the keyboard. Here is what characterizes the spontaneity and exuberance of this big fellow,a unique person and one of the finest musician in the history of jazz music. \r\n\r\nMP3((when_you_and_i_where_young_maggie_-_1939.mp3))\r\n\r\n<img style="float: left" src="http://www.blueblackjazz.com/media/images/waller10.jpg" />\r\n\r\nThe young Fats was first drawn to music by listening to the organ in his local church, his father being a clergyman. He hence learnt to play the organ at a very early age and it was in mastering such a complex musical instrument that he was subsequently able to develop a strong command of the piano. He in fact remained very fond of the organ for the rest of his life and was the first person to use one in jazz sessions.\r\n\r\nHis mother died when he was sixteen and he began to live to  <a href="http://www.blueblackjazz.com/en/artist/3/james-p-johnson">James P. Johnson''s</a>, a great figure of music in Harlem who will give him strong piano lessons. However, this hard period of his life will be a trauma for Fats and he will always have an ambiguous personality, both joyful and sometimes really depressed.\r\n\r\nMP3((saint_louis_blues_-_(organ,_1926).mp3))\r\n\r\nHis first recording session took place in 1922, when he was only 18 years old. He recorded <a href="http://www.blueblackjazz.com/en/transcription/18/18-fats-waller-muscle-shoals-blues">Muscle Shoals Blues</a> and <a href="http://www.blueblackjazz.com/en/transcription/19/19-fats-waller-birmingham-blues">Birmingham Blues</a> these constituting his first solo piano pieces. His relaxed technique and artistry already showed how he was "at home" in the recording studio.\r\n\r\nMP3((birmingham_blues_-_1922.mp3))\r\n\r\n<img style="width: 200px;float: right" src="http://www.blueblackjazz.com/media/images/muscleshoalsblues.JPG" />\r\n\r\nSubsequently, but before becoming famous, Fats worked with various blues singers (like Caroline Johnson and Sara Martin) and produced a number of piano "rolls", a popular way of listening to music in those days before records came on the scene. In the meantime, he began to enter in Harlem night live and play in ''rent parties'', organized by pianists in their own apartments to pay their rents.  \r\n\r\nHis meeting with the lyricist Andy Razaf will be decisive, and they will form a prolific duet in songs composition. During the first years of their collaboration, they often sold for almost nothing, songs which would become great hits. Today, many popular songs are supposed to be Fats'' composition.  \r\n\r\nMP3((beale_street_blues_-_(organ,_1927).mp3))\r\n\r\n<img style="width: 250px;float: left" src="http://www.blueblackjazz.com/media/images/waller-organ2.jpg" />\r\n\r\nHis fame is growing but, however Fats slowly deserts the rent-parties to animate millionaires'' evening parties. However, he wouldn''t worry about money and would play just for enjoyment and free alcohol. He often left parties surrounded by enthusiastic people and sometimes a good cigar.  \r\n\r\nIn the 20''s, piano records had a great success although this instrument was difficult to record. Fats was therefore able to record a lot in 1929. These recording sessions include some original compositions among the most famous of the pianist : <a href="http://www.blueblackjazz.com/en/transcription/20/20-fats-waller-handful-of-keys">Handful of Keys</a>, <a href="http://www.blueblackjazz.com/en/transcription/22/22-fats-waller-sweet-savannah-sue">Sweet Savannah Sue</a>, <a href="http://www.blueblackjazz.com/en/transcription/6/6-fats-waller-smashing-thirds">Smashing Thirds</a>, <a href="http://www.blueblackjazz.com/en/transcription/4/4-fats-waller-valentine-stomp">Valentine Stomp</a>, <a href="http://www.blueblackjazz.com/en/transcription/2/2-fats-waller-numb-fumblin">Numb Fumblin''</a>. \r\n\r\nMP3((aint_misbehavin_-_1929.mp3))\r\n\r\n<img style="width: 200px;float: right" src="http://www.blueblackjazz.com/media/images/willie10.jpg" />\r\n\r\nDuring the years of crisis, Fats recorded few pieces : two piano duets with Bennie Payne (who will become Cab Calloway''pianist) in 1930 and two solos in 1931, I''m Crazy ''Bout My Baby and Draggin'' My Heart Around where he sings for the first time. He also played with some popular bands.\r\n\r\nIn 1934, while he was playing the clown on the piano in a party organized by Gershwin, a member of the Victor Studios noticed him. Fats signed a contract and then recorded till the end of his life with these studios. In the same year, he formed an orchestra with 5 or 6 musicians, Fats Waller & His Rhythm. They were a huge success. Thanks to his vigorous activity he was a very good conductor. He appeared until the end of his life with the same group and recorded more than 400 pieces. \r\n\r\nMP3((latch_on_-_fats_waller_and_his_rhythm,_1938.mp3))\r\n\r\n<img style="width: 400px" src="http://www.blueblackjazz.com/media/images/rhythm22.jpg" />\r\n\r\nThanks to the radio, Fats became a star. He was one of the first jazz musicians to use this mass media and one of the first Afro-Americans to have a regular radio show. The public loved him because he was a great entertainer with great spontaneity. From 1935 he performed in a lot of radio shows. \r\n\r\nMP3((go_down_moses_(radio_broadcast,_c.1941).mp3))\r\n\r\n<img style="width: 200px;float: left" src="http://www.blueblackjazz.com/media/images/waller12.jpg" />\r\n\r\nWhilst alive, although being much appreciated by the public, Fats Waller was often seen more as a joker and an entertainer than a real jazz musician. This was always somewhat upsetting to him. However, we can see through his many recordings that he could be very serious and was an inspired and talented musician who created some beautiful pieces of poetic music. \r\nMP3((honeysuckle_rose_-_1941.mp3))\r\n\r\n![](http://www.blueblackjazz.com/media/images/waller3.jpg)\r\n\r\nAs many other geniuses, Fats Waller was gone too soon\r\nHe died from pneumonia one evening of December, in a train taking him back from the west.\r\nHe was only 39 years old and at the peak of his fame. Who knows how his music will have delighted us ? \r\n"One never knows, do one...?"', 'en', 'Thomas "Fats" Waller biography. Fats Waller transcriptions for piano. Stride piano sheet music', 'fats waller, stride piano, sheet music, harlem stride, ragtime, stride, transcription, jazz piano, mp3, blueblackjazz', 'Fats Waller | BlueBlackJazz'),
(5, 2, '1897 - 1973', '<img style="width: 250px;float: right" src="http://www.blueblackjazz.com/media/images/willie3.jpg" />\r\n\r\nAutre grande figure du Jazz, Willie "The Lion" Smith s''est rapidement imposé comme un pianiste original et soliste remarquable. Dans ce milieu musical nocturne, les rent-parties, le ''Lion'' était comme le roi de la jungle et attendait quiconque voudrait se mesurer à lui au piano. La plupart repartaient lacérés pas ses griffes. Son allure (habillé d''un constant chapeau melon et d''un cigare) son charisme et son jeu tout à fait original, l''ont propulsé au panthéon des personnalités du <a href="http://www.blueblackjazz.com/fr/style/2/stride-piano">stride</a>.\r\n\r\nMP3((contrary.mp3))\r\n\r\n<img style="width: 250px;float: left" src="http://www.blueblackjazz.com/media/images/willie1.jpg" />\r\n\r\nBien que son jeu soit empreint de ses contemporains <a href="http://www.blueblackjazz.com/fr/artist/3/james-p-johnson">James P. Johnson</a> et <a href="http://www.blueblackjazz.com/fr/artist/20/eubie-blake">Eubie Blake</a>, Willie Smith a développé une technique pianistique bien à lui : un toucher très nuancé, quoiqu''un peu plus hasardeux et moins virtuose que <a href="http://www.blueblackjazz.com/fr/artist/3/james-p-johnson">James P. Johnson</a>. Il met davantage de côté le jeu de pompe à la main gauche en préférant utiliser un balancement d''arpèges qui apporte un lyrisme et une poésie toute particulière dans ses morceaux : <a href="http://www.blueblackjazz.com/fr/transcription/109/109-willie-the-lion-smith-fading-star">Fading Star</a>, <a href="http://www.blueblackjazz.com/fr/transcription/114/114-willie-the-lion-smith-what-is-there-to-say">What Is There To Say?</a>, <a href="http://www.blueblackjazz.com/fr/transcription/112/112-willie-the-lion-smith-ill-follow-you">I''ll Follow You</a>...). Billy Strayhorn (compositeur et arranger dans l''orchestre de Duke Ellington) définissait ainsi son style : "C''est un mélange étrange de contrepoint, d''harmonie chromatique et de figures en arabesques aussi rafraîchissantes pour l''oreille que l''eau de source pour le palais". Il suffit d''écouter <a href="http://www.blueblackjazz.com/fr/transcription/106/106-willie-the-lion-smith-echoes-of-spring">Echoes Of Spring</a> pour se faire une bonne idée de l''originalité du jeu de Willie Smith :\r\n\r\nMP3((echoes.mp3))\r\n\r\nDernier survivant du ''Big Three'' du <a href="http://www.blueblackjazz.com/fr/style/2/stride-piano">stride</a>, The Lion continuera à enregistrer et à donner des concerts jusque dans les années 60, perpétuant son style, en remémorant ainsi les années de Harlem et en rendant hommage à ses confrères disparus.', 'fr', 'Biographie de Willie "The Lion" Smith. Transcriptions pour piano. Partitions de piano stride', 'willie the lion smith, piano stride, partition, harlem stride, ragtime, stride, transcription, piano jazz, mp3, blueblackjazz', 'Willie "The Lion" Smith | BlueBlackJazz'),
(6, 2, '1897 - 1973', '<img style="width: 250px;float: right" src="http://www.blueblackjazz.com/media/images/willie3.jpg" />\r\n\r\nWillie Smith the Lion was another famous Stride piano player of Harlem nights. He quickly stood as an unusual pianist and a wonderful soloist, a matchless « keyboard tickler ». He was given the nickname of « the Lion » during the rent-parties and the well-known « cutting contests » improvised at the end of the nights in the smoky clubs where artists confronted each other using their most impressive feats on the piano. In this night world of music, he was as the « king of the jungle » waiting for those who would dare to confront (challenge) him on the piano. Most of his opponents gave up in front of his virtuosity. Because of his appearance (he always wore a Derby hat -or bowler hat - and smoked a cigar), his strong personality and his unique way to play the piano, he was one of the greatest <a href="http://www.blueblackjazz.com/en/style/2/stride-piano">stride piano</a> players.\r\n\r\nMP3((contrary.mp3))\r\n\r\n<img style="width: 250px;float: left" src="http://www.blueblackjazz.com/media/images/willie1.jpg" />\r\n\r\nFrom the 30''s, when nights in Harlem were more quiet, he started recordings. We discover then his original compositions recorded in 1939 under the quality label Commodore, 14 titles among which some masterpieces : Echoes Of Spring, Rippling Waters, Sneakaway, Finger Buster... Although he borrowed to the game of his contemporaries <a href="http://www.blueblackjazz.com/en/artist/3/james-p-johnson">James P. Johnson</a> and <a href="http://www.blueblackjazz.com/en/artist/20/eubie-blake">Eubie Blake</a>, Willie Smith developed his own technique : a lighter touch on the keyboard than Waller though a little more risky, but with less virtuosity than Johnson. He neglected the "game of pomp" with the left hand and preferred to use a "swing of arpeggios" which gave poetry and lyricism to his pieces : <a href="http://www.blueblackjazz.com/en/transcription/109/109-willie-the-lion-smith-fading-star">Fading Star</a>, <a href="http://www.blueblackjazz.com/en/transcription/114/114-willie-the-lion-smith-what-is-there-to-say">What Is There To Say?</a>, <a href="http://www.blueblackjazz.com/en/transcription/112/112-willie-the-lion-smith-ill-follow-you">I''ll Follow You</a>...). Billy Strayhorn (composer and arranger for Duke Ellington''s orchestra) described his style in this way : « It is a strange mixing of "counterpoint", chromatic harmony and arabesque devices as fresh to the ear as a spring water for the palate ». You just have to listen to <a href="http://www.blueblackjazz.com/en/transcription/106/106-willie-the-lion-smith-echoes-of-spring">Echoes Of Spring</a> to understand the very peculiar sounds which Willie Smith achieved to play. \r\n\r\nMP3((echoes.mp3))\r\nAs the last survivor of the ''Big Three'' of stride piano , The Lion made recordings and concerts onto the sixties, with the same style and always remembering the old time of Harlem and his old friends.', 'en', 'Willie "The Lion" Smith biography. Transcriptions for piano. Stride piano sheet music', 'willie the lion smith, lion, stride piano, sheet music, harlem stride, ragtime, stride, transcription, jazz piano, mp3, blueblackjazz', 'Willie "The Lion" Smith  | BlueBlackJazz'),
(7, 4, '1904 - 1962', '<img style="width: 250px;float: right" src="http://www.blueblackjazz.com/media/images/lambert2.jpg" />\r\n\r\nIn a way, he has been left out of stride history. He was a formidable pianist in the rent parties of Harlem but he spent most of his life discreetly playing piano in the clubs around his town. He recorded very few pieces and only twice in a studio, in 1941 and 1961. But he mainly recorded in his favorite bar in Orange, New jersey. At last, he was discovered by a large public during the Newport festival in 1960. Nevertheless, thanks to his few recordings he could enter the great names of stride. \r\n\r\nHe didn''t composed, but he had a strong musical personality and was able to appropriate anyone''s music. He liked to play classical tunes his own way, which some were among his greatest pieces like his famous <a href="http://www.blueblackjazz.com/en/transcription/120/120-donald-lambert-anitras-dance">Anitra''s Dance</a>. \r\n\r\nMP3((lambert.mp3))\r\n\r\nDonald Lambert was largely an autodidact and has a very particular way to play the piano : he was left-handed and had a fearsome but reliable left hand playing which he almost seemed to forget when he played. Furthermore he used to play in unusual tonalities which made way to new acoustic and digital possibilities.', 'en', 'Donald Lambert biography. Transcriptions for piano. Stride piano sheet music', 'donald lambert, stride piano, sheet music, harlem stride, ragtime, stride, transcription, jazz piano, mp3, blueblackjazz', 'Donald Lambert | BlueBlackJazz'),
(8, 4, '1904 - 1962', '<img style="width: 250px;float: right" src="http://www.blueblackjazz.com/media/images/lambert2.jpg" />\r\n\r\nIl est un peu l''oublié de la famille du stride. Il fut un pianiste redoutable dans les rent-parties à Harlem , puis passa le plus clair de sa vie à jouer dans l''ombre des clubs de sa région. Il n''enregistra que très peu : deux séances studio en 1941 et 1961, le reste sont des enregistrements fait sans doute dans son bar de prédilection à Orange, New-Jersey. Il se fit découvrir très tard par le grand public en 1960 au festival de Newport. Néanmoins, le peu d''enregistrements lui suffirent à entrer dans le panthéon du stride. \r\n\r\nIl n''a que très peu composé, mais avait une forte personnalité de jeu et était capable de s''approprier n''importe quelle musique. Il aimait d''ailleurs rejouer les airs de classique à sa propre façon, et a ainsi laissé quelques morceaux des plus remarquables comme son fameux <a href="http://www.blueblackjazz.com/fr/transcription/120/120-donald-lambert-anitras-dance">Anitra''s Dance</a>.\r\n\r\nMP3((lambert.mp3))\r\n\r\nDonald Lambert était en grande partie autodidacte et avait une manière de jouer du piano très particulière : Il était gaucher et possédait de ce fait un jeu de main gauche redoutable et d''un fiabilité sans faille, qu''il semblait presque oublier lorsqu''il jouait. Il avait de plus l''habitude de jouer dans des tonalités peu conventionnelles ce qui a ouvert la voie à de nouvelles possibilités digitales et sonores.', 'fr', 'Biographie de Donald Lambert. Transcriptions pour piano. Partitions de piano stride', 'donald lambert, piano stride, partition, harlem stride, ragtime, stride, transcription, piano jazz, mp3, blueblackjazz', 'Donald Lambert | BlueBlackJazz'),
(9, 5, '1907 - 1949', '<img style="width: 250px;float: right" src="http://www.blueblackjazz.com/media/images/ammons.jpg" />\r\n\r\nBorn in Chicago, and he learned piano from the age of 10. He quickly became interested in blues and learnt by listening pianists like <a href="http://blueblackjazz.com/en/artist/7/jimmy-yancey">Jimmy Yancey</a> and <a href="http://blueblackjazz.com/en/artist/13/hersal-thomas">Hersal Thomas</a>. In the early 20s, he worked as a taxi driver in Chicago. At that time, he met <a href="http://blueblackjazz.com/en/artist/8/meade-lux-lewis">Meade Lux Lewis</a>, a taxi driver too and passionate for piano blues. The two men played together during their free time and perfected their technique. From 1934 to 1936, Albert Ammons formed his own band, the "Rhythm Kings". Following the success, Albert Ammons moved to New York, where he met Pete Johnson. The two pianists became friends and performed together and at "Cafe Society".\r\n\r\nMP3((albert_ammons_-_boogie_boogie_stomp.mp3))\r\n\r\nAfter the success of the Carnegie Hall concert <i>From Spirituals to Swing</i> in 1938 and the growing popularity of the Boogie Woogie, Albert Ammons continued recording alongside many musicians, featured in the film <i>Boogie-Woogie Dream</i> with <a href="http://blueblackjazz.com/en/artist/6/pete-johnson">Pete Johnson</a>. He had a severe injury to a finger whilst cooking in 1941, but continued playing and recording until his death in December 1949. His playing is powerful and joyful, and has one of the best swing playing among the Boogie Woogie players.', 'en', 'Albert Ammons biography. Transcriptions for piano. Boogie Woogie sheet music', 'Biographie de Albert Ammons. Transcriptions pour piano. Partitions de Woogie Woogie', 'Albert Ammons | BlueBlackJazz'),
(10, 5, '1907 - 1949', '<img style="width: 250px;float: right" src="http://www.blueblackjazz.com/media/images/ammons.jpg" />\r\n\r\nNé à Chicago de parents pianistes, et il apprend le piano à partir de l''âge de 10 ans. Il s''intéresse alors rapidement au blues et l''apprend en écoutant les pianistes <a href="http://blueblackjazz.com/fr/artist/7/jimmy-yancey">Jimmy Yancey</a> et <a href="http://blueblackjazz.com/fr/artist/13/hersal-thomas">Hersal Thomas</a>. Au début des années 20, il travaille comme chauffeur de taxi à Chicago. C''est à cette époque qu''il rencontre <a href="http://blueblackjazz.com/fr/artist/8/meade-lux-lewis">Meade Lux Lewis</a>, lui aussi chauffeur de taxi et passionné par le piano blues. Les deux hommes jouent ensemble durant leur temps libre et perfectionnent leur technique. De 1934 à 1936, Albert Ammons forme son propre groupe, les Rhythm Kings . Suite au succès rencontré, Albert Ammons part s''installer à New-York, ou il rencontre <a href="http://blueblackjazz.com/fr/artist/6/pete-johnson">Pete Johnson</a>. Les deux pianistes sympathisent et se produisent ensemble au Cafe Society où ils côtoient le chanteur Big Joe Turner.\r\n\r\nMP3((albert_ammons_-_boogie_boogie_stomp.mp3))\r\n\r\nAprès le succès du concert <i>From Spirituals to Swing</i> au Carnegie Hall en 1938 et la popularité croissante du Boogie Woogie, Albert Ammons continue à enregistrer au côté de nombreux musiciens, apparaît dans le film <i>Boogie-Woogie Dream</i> avec <a href="http://blueblackjazz.com/fr/artist/6/pete-johnson">Pete Johnson</a>. Il se sectionne un doigt en cuisinant en 1941 mais continue à jouer et à enregistrer, et ce jusqu''à peu de temps avant sa mort en décembre 1949. Son jeu est puissant et joyeux, et possède l''un des meilleur swing parmi les pianistes de Boogie Woogie.', 'fr', 'Biographie de Albert Ammons. Transcriptions pour piano. Partitions de Woogie Woogie', 'albert ammons, boogie-woogie, partition, boogie woogie, blues, stride, transcription, piano jazz, mp3, blueblackjazz', 'Albert Ammons | BlueBlackJazz'),
(11, 6, '1904 - 1967', '<img style="width: 220px;float: right" src="http://www.blueblackjazz.com/media/images/pete_johnson.jpg" />\r\n\r\nBoogie Woogie owes much to Pete Johnson, pianist from Kansas City. He began his musical career as a drummer before becoming a pianist. He accompanies the singer Big Joe Turner with whom he recorded <i>Roll ''Em Pete</i>, one of the first rock'' n roll. He played in many clubs in New York including the famous "Cafe Society", sometimes alongside colleagues <a href="http://blueblackjazz.com/en/artist/5/albert-ammons">Albert Ammons</a> and <a href="http://blueblackjazz.com/en/artist/8/meade-lux-lewis">Meade Lux Lewis</a>. He recorded many piano solos in the ''40s before moving to Buffalo in 1950.\r\n\r\nMP3((pete_johnson.mp3))\r\n\r\nOn December 23 1938, was held at Carnergie Hall the concert that launched the Boogie Woogie madness: <i>From Spirituals to Swing</i> involving <a href="http://blueblackjazz.com/en/artist/6/pete-johnson">Pete Johnson</a>, <a href="http://blueblackjazz.com/en/artist/5/albert-ammons">Albert Ammons</a> and <a href="http://blueblackjazz.com/en/artist/8/meade-lux-lewis">Meade Lux Lewis</a> and other great jazz names such as <a href="http://blueblackjazz.com/en/artist/3/james-p-johnson">James P. Johnson</a>, <a href="http://blueblackjazz.com/en/artist/12/count-basie">Count Basie</a> and Benny Goodman. It was the first time that black artists played in this prestigious hall.  It was a great success, and the beginning of the Boogie Woogie craze.', 'en', 'Pete Johnson biography. Transcriptions for piano. Boogie Woogie sheet music', 'pete johnson, boogie-woogie, sheet music, boogie woogie, blues, stride, transcription, jazz piano, mp3, blueblackjazz', 'Pete Johnson | BlueBlackJazz'),
(12, 6, '1904 - 1967', '<img style="width: 220px;float: right" src="http://www.blueblackjazz.com/media/images/pete_johnson.jpg" />\r\n\r\nLe Boogie Woogie doit beaucoup à Pete Johnson, pianiste originaire de Kansas City. Il commence sa carrière musicale comme batteur avant de devenir pianiste. Il accompagne le chanteur Big Joe Turner avec qui il enregistre Roll ''Em Pete , l''un des tout premiers rock ''n roll. Il se produit dans de nombreux club de New-York dont le fameux Cafe Society , parfois aux côtés de ses collègues <a href="http://blueblackjazz.com/fr/artist/5/albert-ammons">Albert Ammons</a> et <a href="http://blueblackjazz.com/fr/artist/8/meade-lux-lewis">Meade Lux Lewis</a>. Il enregistre un grand nombre de piano solo dans les années 40 avant de s''installer à Buffalo en 1950. \r\n\r\nMP3((pete_johnson.mp3))\r\n\r\nLe 23 décembre 1938 a lieu au Carnergie Hall le concert qui a lancé la popularité du Boogie Woogie : <i>From Spirituals to Swing</i> auquel participent Pete Johnson ainsi que <a href="http://blueblackjazz.com/fr/artist/5/albert-ammons">Albert Ammons</a> et <a href="http://blueblackjazz.com/fr/artist/8/meade-lux-lewis">Meade Lux Lewis</a> mais aussi d''autres grands noms du jazz tels que <a href="http://blueblackjazz.com/fr/artist/3/james-p-johnson">James P. Johnson</a>, <a href="http://blueblackjazz.com/fr/artist/12/count-basie">Count Basie</a> et Benny Goodman. C''est la première fois que des artistes Afro-Américains jouent dans cette salle aussi prestigieuse. Ce sera un succès énorme qui marquera le début d''un engouement pour le Boogie Woogie aux Etats-Unis.', 'fr', 'Biographie de Pete Johnson. Transcriptions pour piano. Partitions de Woogie Woogie', 'pete johnson, boogie-woogie, partition, boogie woogie, blues, stride, transcription, piano jazz, mp3, blueblackjazz', 'Pete Johnson | BlueBlackJazz'),
(13, 7, '1898 - 1951', '<img style="width: 235px;float: right" src="http://www.blueblackjazz.com/media/images/yancey.jpg" />\r\n\r\nJimmy Yancey played an important role in the history of Blues and Boogie Woogie. Born in Chicago, he started playing the piano rather late and developed his own way of playing the blues. Unwillingly, he became an influential figure in the Chicago musical life: his playing was a major source of inspiration for <a href="http://blueblackjazz.com/en/artist/8/meade-lux-lewis">Meade Lux Lewis</a> and <a href="http://blueblackjazz.com/en/artist/5/albert-ammons">Albert Ammons</a>. However, Jimmy Yancey chose the job security to the artist''s life and worked for 25 years as a gardener at the Comiskey Park. But in 1939, recording studios became interested in his music and from 1939 to 1940, he was invited to cut a number of piano solos: sometimes ingenious, sometimes unpredictable. He particularly liked to finish each track with a short coda, still the same, a E flat, and whatever the tone he was playing.', 'en', 'Jimmy Yancey biography. Transcriptions for piano. Boogie Woogie sheet music', 'jimmy yancey, boogie-woogie, sheet music, boogie woogie, blues, stride, transcription, jazz piano, mp3, blueblackjazz', 'Jimmy Yancey | BlueBlackJazz'),
(14, 7, '1898 - 1951', '<img style="width: 235px;float: right" src="http://www.blueblackjazz.com/media/images/yancey.jpg" />\r\n\r\nJimmy Yancey a joué un rôle important dans l''histoire du Blues et du Boogie Woogie. Originaire de Chicago, il commence le piano assez tard et développe une façon particulière de jouer le blues. Sans vraiment le savoir, deviendra une personnalité influente dans le milieu musical de Chicago : son jeu est cité comme principale source d''inspiration par <a href="http://blueblackjazz.com/fr/artist/8/meade-lux-lewis">Meade Lux Lewis</a> et <a href="http://blueblackjazz.com/fr/artist/5/albert-ammons">Albert Ammons</a>. Cependant, Jimmy Yancey choisira la sécurité de l''emploi à la vie d''artiste : il sera pendant 25 ans employé au Comiskey Park comme jardinier. Ce n''est qu''en 1939 que les studios s''intéressent à Jimmy Yancey. Il enregistrera en 1939 et 1940 un certains nombre de solo au piano et fera ainsi découvrir son style particulier : tantôt ingénieux, parfois imprévisible. Il avait notamment la particularité de finir chacun de ses morceaux par une courte coda, toujours identique, en mi bémol, et ce quelle que soit la tonalité dans laquelle il jouait.', 'fr', 'Biographie de Jimmy Yancey. Transcriptions pour piano. Partitions de Woogie Woogie', 'jimmy yancey, boogie-woogie, partition, boogie woogie, blues, stride, transcription, piano jazz, mp3, blueblackjazz', 'Jimmy Yancey | BlueBlackJazz'),
(15, 8, '1905 - 1964', '<img style="width: 235px;float: right" src="http://www.blueblackjazz.com/media/images/meade_lux.jpg" />\r\n\r\nBorn in Chicago in a musical family, he began music by learning violin, but turned to the piano after hearing <a href="http://blueblackjazz.com/en/artist/8/meade-lux-lewis">Meade Lux Lewis</a> and <a href="http://blueblackjazz.com/en/artist/13/hersal-thomas">Hersal Thomas</a>. Completely self-taught, he began performing and perfecting his style with his friend <a href="http://blueblackjazz.com/en/artist/5/albert-ammons">Albert Ammons</a>, whom he met when he was a taxi driver. In 1927, he recorded one of his compositions that would become his most famous song <a href="http://www.blueblackjazz.com/en/transcription/145/145-meade-lux-lewis-honky-tonk-train-blues">Honky Tonk Train Blues</a>.\r\n\r\nBut Meade Lux lewis only came into the public eye around 1935, when he began recording for Parlophone and Victor. After the Carnegie Hall concert in 1938, he became, with <a href="http://blueblackjazz.com/en/artist/5/albert-ammons">Albert Ammons</a>s and <a href="http://blueblackjazz.com/en/artist/6/pete-johnson">Pete Johnson</a>, a legendary Boogie Woogie player. The trio continued to perform at the "Cafe Society", while the popularity of the Boogie Woogie was in full swing. In 1941 he moved to Los Angeles and continued giving public performances, even appearing in some motion pictures. He died in 1964 in a car accident.', 'en', 'Meade Lux Lewis biography. Transcriptions for piano. Boogie Woogie sheet music', 'meade lux lewis, boogie-woogie, sheet music, boogie woogie, blues, stride, transcription, jazz piano, mp3, blueblackjazz', 'Meade Lux Lewis | BlueBlackJazz'),
(16, 8, '1905 - 1964', '<img style="width: 235px;float: right" src="http://www.blueblackjazz.com/media/images/meade_lux.jpg" />\r\n\r\nNé à Chicago dans une famille de musicien, il commence par apprendre le violon mais se tourne vers le piano après avoir entendu <a href="http://blueblackjazz.com/fr/artist/7/jimmy-yancey">Jimmy Yancey</a> et <a href="http://blueblackjazz.com/fr/artist/13/hersal-thomas">Hersal Thomas</a>. Entièrement autodidacte, il commence à se produire et perfectionne son style en compagnie de <a href="http://blueblackjazz.com/fr/artist/5/albert-ammons">Albert Ammons</a>, qu''il rencontre alors qu''il est chauffeur de taxi. En 1927, il enregistre une de ses compositions qui deviendra son morceau le plus célèbre <a href="http://www.blueblackjazz.com/fr/transcription/145/145-meade-lux-lewis-honky-tonk-train-blues">Honky Tonk Train Blues</a>. \r\n\r\nCe n''est cependant qu''en 1935 que Meade Lux Lewis sort totalement de l''ombre et se met à enregistrer pour Victor et Parlophone. Après le concert du Carnegie Hall en 1938, il devient, avec <a href="http://blueblackjazz.com/fr/artist/5/albert-ammons">Albert Ammons</a> et <a href="http://blueblackjazz.com/fr/artist/6/pete-johnson">Pete Johnson</a>, un joueur de Boogie de légende. Le trio continue à se produire au Cafe Society, alors que la popularité du Boogie Woogie bat son plein. En 1941, il s''installe à Los Angeles et continue à se produire, apparaissant même dans quelques productions cinématographiques. Il meurt en 1964 dans un accident de voiture.', 'fr', 'Biographie de Meade Lux Lewis. Transcriptions pour piano. Partitions de Woogie Woogie', 'meade lux lewis, boogie-woogie, partition, boogie woogie, blues, stride, transcription, piano jazz, mp3, blueblackjazz', 'Meade Lux Lewis | BlueBlackJazz'),
(17, 9, '1904 - 1929', 'It is Clarence "Pine Top" Smith who first used the term "Boogie Woogie" in his 1928 recording "Pine Top''s Boogie Woogie". This song had a great influence on all other tunes of its kind and will be used by <a href="http://blueblackjazz.com/en/artist/5/albert-ammons">Albert Ammons</a> in his <a href="http://blueblackjazz.com/en/transcription/140/140-albert-ammons-boogie-woogie-stomp">"Boogie Woogie Stomp"</a>. He was a regular in barrel-houses and rent-parties, and died accidentally in a shooting in Chicago.\r\n\r\nMP3((pinetop-boogiewoogie.mp3))', 'en', 'Clarence "Pine Top" Smith biography. Transcriptions for piano. Boogie Woogie sheet music', 'pine top smith, pinetop smith, boogie-woogie, sheet music, boogie woogie, blues, stride, transcription, jazz piano, mp3, blueblackjazz', 'Clarence "Pine Top" Smith | BlueBlackJazz'),
(18, 9, '1904 - 1929', 'C''est Clarence « Pine Top » Smith qui officialise le terme « Boogie Woogie » dans son titre enregistré en 1928 « Pine Top''s Boogie Woogie ». Ce morceau aura une grande influence sur toutes les autres pièces de ce genre et sera repris notamment par <a href="http://blueblackjazz.com/fr/artist/5/albert-ammons">Albert Ammons</a> dans son morceau <a href="http://blueblackjazz.com/fr/transcription/140/140-albert-ammons-boogie-woogie-stomp">"Boogie Woogie Stomp"</a>. Habitué des barrel-houses et des rent-parties, il meurt accidentellement lors d''une fusillade à Chicago.\r\n\r\nMP3((pinetop-boogiewoogie.mp3))', 'fr', 'Biographie de Clarence "Pine Top" Smith. Transcriptions pour piano. Partitions de Woogie Woogie', 'pine top smith, pinetop smith, boogie-woogie, partition, boogie woogie, blues, stride, transcription, piano jazz, mp3, blueblackjazz', 'Clarence "Pine Top" Smith | BlueBlackJazz'),
(19, 10, '1901 - 1931', '<img style="width: 135px;float: left" src="http://www.blueblackjazz.com/media/images/blythe.jpg" />\r\nJimmy Blythe was a very prolific pianist and took an active part in the musical live of his time.  He accompanied many jazz artists and led a great number of recording sessions.\r\nHe was a Boogie Woogie pioneer but was able to play many other piano styles like Stride piano. From the early 20’s, he recorded hundreds of piano rolls which are an important testimony of his piano playing. His title “Chicago Stomp”, recorded in 1924 is considered to be one the very first Boogie Woogie of all time.\r\nMP3((chicago-stomp-blythe.mp3))', 'en', 'Jimmy Blythe biography. Transcriptions for piano. Boogie Woogie sheet music', 'jimmy blythe, boogie-woogie, sheet music, boogie woogie, blues, stride, transcription, jazz piano, mp3, blueblackjazz', 'Jimmy Blythe | BlueBlackJazz'),
(20, 10, '1901 - 1931', '<img style="width: 135px;float: left" src="http://www.blueblackjazz.com/media/images/blythe.jpg" />\r\nPianiste prolifique et très actif, il accompagne de nombreux musiciens, chanteurs et dirige un nombre important de sessions d’enregistrements. \r\nPionnier du Boogie Woogie, son style de jeu était très versatile et il pouvait également jouer Stride. A partir des années 20, il grave des centaines de rouleaux de piano qui sont un témoignage important de son style de jeu. Son titre « Chicago Stomp » enregistré en 1924 serait l''un des tout premiers Boogie Woogie de l’histoire.\r\n\r\nMP3((chicago-stomp-blythe.mp3))', 'fr', 'Biographie de Jimmy Blythe. Transcriptions pour piano. Partitions de Woogie Woogie', 'jimmy blythe, boogie-woogie, partition, boogie woogie, blues, stride, transcription, piano jazz, mp3, blueblackjazz', 'Jimmy Blythe | BlueBlackJazz');
INSERT INTO `ArtistTranslation` (`id`, `translatable_id`, `subtitle`, `markdown`, `locale`, `seoDescription`, `seoKeywords`, `seoTitle`) VALUES
(21, 13, '1906 - 1926', '<img style="width: 140px;float: right" src="http://www.blueblackjazz.com/media/images/hthomas.jpg" />\r\n\r\nHersal Thomas n’as pas vécu 20 ans, mais est pourtant cité parmi les principales sources d’inspiration des grands maîtres du Boogie Woogie de l’histoire du jazz. Pianiste talentueux, il fut l’auteur de quelques compositions notables à commencer par son <a href="http://blueblackjazz.com/fr/transcription/141/141-albert-ammons-suitcase-blues">"Suitcase Blues"</a> qui est repris par tout joueur de Boogie Woogie digne de ce nom. Il enregistre en solos en 1925 et on peut l’entendre avec Louis Armstrong en 1926. \r\nIl serait mort d’intoxication alimentaire alors qu’il travaillait dans un club de Détroit.\r\n\r\nIl était le frère de George W. Thomas, pianiste à qui l’on doit les toutes premières walking bass de l’histoire du boogie dans « The Rock » en 1923.', 'fr', 'Biographie de Hersal Thomas. Transcriptions pour piano. Partitions de Woogie Woogie', 'Hersal Thomas, boogie-woogie, partition, boogie woogie, blues, stride, transcription, piano jazz, mp3, blueblackjazz', 'Hersal Thomas | BlueBlackJazz'),
(22, 13, '1906 - 1926', '<img style="width: 140px;float: right" src="http://www.blueblackjazz.com/media/images/hthomas.jpg" />\r\n\r\nHersal Thomas didn''t live until the age of 20, he''s however cited as one of the main source of inspiration for the Boogie masters. He was a gifted pianist composing some notable pieces starting with <a href="http://blueblackjazz.com/en/transcription/141/141-albert-ammons-suitcase-blues">"Suitcase Blues"</a> which is played by every boogie woogie pianist worthy of the name. He recorded a couple of piano solos in 1925 and can be heard with Louis Armstrong on a few recordings of 1926. He died while performing in Detroit, probably because of  food intoxication.\r\n\r\nHe was the brother of George W. Thomas who recorded "The Rock" in 1923 in which can be heard the very first walking bass of history.', 'en', 'Hersal Thomas biography. Transcriptions for piano. Boogie Woogie sheet music', 'Hersal Thomas, boogie-woogie, sheet music, boogie woogie, blues, stride, transcription, jazz piano, mp3, blueblackjazz', 'Hersal Thomas | BlueBlackJazz'),
(23, 11, '1910 - 1981', '<img style="width: 200px;float: right" src="http://www.blueblackjazz.com/media/images/mary_lou_williams.jpg" />\r\n\r\nTremendously gifted, Mary Lou Wiliams fully deserved the status of "First Lady Of Jazz". She begins the piano very early by rubbing against popular rags and songs of the twenties, and she quickly built a reputation among the greatest jazzmen from that time. She was also a talented composer and arranger who worked with Benny Goodman and Duke Ellington.\r\n\r\nHis style evolved with the arrival of bebop, and Mary Lou worked with young and promising musicians such as Thelonious Monk et Dizzy Gillespie.\r\nAs the first female solist to join the ranks of jazz legends, she inspired respect from his peers throughout her career.', 'en', 'Mary Lou Williams biography. Transcriptions for piano. Jazz piano sheet music', 'Mary Lou Williams, stride piano, sheet music, blues, stride, transcription, jazz piano, mp3, blueblackjazz', 'Mary Lou Williams | BlueBlackJazz'),
(24, 11, '1910 - 1981', '<img style="width: 200px;float: right" src="http://www.blueblackjazz.com/media/images/mary_lou_williams.jpg" />\r\n\r\nPianiste remarquablement douée, Mary Lou Wiliams a amplement mérité sont status de "Première Dame du Jazz". Elle débute le piano très tôt en se frottant aux rags et aux mélodies populairs des années 20 et se fait rapidement connaître des grands jazzmen de l''époque. Egalement compositrice et arrangeuse de talent, elle aura travaillé pour Benny Goodman et Duke Ellington. \r\n\r\nSon style évoluera avec l''arrivée du bebop et Mary Lou s''entourera de jeunes musiciens prometteurs comme Thelonious Monk et Dizzy Gillespie. Première femme soliste à entrer dans le panthéon du Jazz, elle aura inspiré le respect de ses paires masculins tout au long de sa carrière.', 'fr', 'Biographie de Mary Lou Williams. Transcriptions pour piano. Partitions de piano jazz', 'Mary Lou Williams, piano stride, partition, blues, stride, transcription, piano jazz, mp3, blueblackjazz', 'Mary Lou Williams | BlueBlackJazz'),
(25, 14, '1917 - 2000', '<img style="width: 200px;float: right" src="http://www.blueblackjazz.com/media/images/pat_flowers.jpg" />\r\n\r\nIvelee Patrick Flowers learned classical piano but soon showed interest in jazz music, more specifically in <a href="http://blueblackjazz.com/en/artist/1/fats-waller">Fats Waller</a>''s stride piano style.\r\nHe never missed an opportunity to listen to his idol, and he quickly built a strong piano technique and a reputation of fine stride pianists.\r\nAfter <a href="http://blueblackjazz.com/en/artist/1/fats-waller">Fats Waller</a>''s death, he carried on the leading of his orchestra under the name of "Pat Flowers & His Rhythm" perpetuating his music and exuberant style.', 'en', 'Pat Flowers biography. Transcriptions for piano. Stride piano sheet music', 'pat flowers, stride piano, sheet music, harlem stride, ragtime, stride, transcription, jazz piano, mp3, blueblackjazz', 'Pat Flowers | BlueBlackJazz'),
(26, 14, '1917 - 2000', '<img style="width: 200px;float: right" src="http://www.blueblackjazz.com/media/images/pat_flowers.jpg" />\r\n\r\nDe formation classique, le jeune Ivelee Patrick Flowers s''interesse rapidement au jazz et plus précisément au jeu Stride de <a href="http://blueblackjazz.com/fr/artist/1/fats-waller">Fats Waller</a>. Ne manquant pas une occasion d''aller écouter son idole, il acquiert un solide technique au piano ainsi qu''une réputation de brillant pianiste Stride. A la mort de <a href="http://blueblackjazz.com/fr/artist/1/fats-waller">Fats Waller</a>, il reprend l''orchestre de ce dernier sous le nom de "Pat Flowers & His Rhythm" et perpétuant la  musique et le style truculent de son aîné.', 'fr', 'Biographie de Pat Flowers. Transcriptions pour piano. Partitions de piano stride', 'pat flowers, piano stride, partition, harlem stride, ragtime, stride, transcription, piano jazz, mp3, blueblackjazz', 'Pat Flowers | BlueBlackJazz'),
(27, 22, '1906 - 1937', '<img style="width: 180px;float: right" src="http://www.blueblackjazz.com/media/images/alex_hill.jpg" />\r\n\r\nHe was a child prodigy of the piano, and he led his own group from 1924 with whom he gave performances in the west coast. After a time at Chicago, he settled in New-York where he is hired at the Savoy Ballroom. In addition to his talents as a pianist, Alex Hill work as an arranger for many musicians such as Paul Whiteman, Benny Carter and Duke Ellington. He briefly worked along with <a href="http://blueblackjazz.com/en/artist/1/fats-waller">Fats Waller</a> and Adelaïde Hall in December 1930 in a show called "Hello 1931".\r\n\r\nHillness put an end to his career, he died of tuberculosis in 1937.', 'en', 'Alex Hill biography. Transcriptions for piano. Stride piano sheet music', 'Alex Hill, stride piano, sheet music, blues, stride, transcription, jazz piano, mp3, blueblackjazz', 'Alex Hill | BlueBlackJazz'),
(28, 22, '1906 - 1937', '<img style="width: 180px;float: right" src="http://www.blueblackjazz.com/media/images/alex_hill.jpg" />\r\n\r\nEnfant prodige du piano, Alex Hill dirige son propre ensemble à partir de 1924 avec qui il se produit ans les Etats-Unis. Après être passé par Chicago, il s''installe à New-York en 1930 ou il est engagé au Savoy Ballroom. Outre ses talents de pianiste, Alex Hill travaille comme arrangeur pour de nombreux musiciens tels que Paul Whiteman, Benny Carter et Duke Ellington. Il collabore avec <a href="http://blueblackjazz.com/fr/artist/1/fats-waller">Fats Waller</a> et Adelaïde Hall en décembre 1930 dans un show appellé "Hello 1931".\r\n\r\nLa maladie met fin à sa carrière et il meurt de tuberculose en 1937.', 'fr', 'Biographie de Alex Hill. Transcriptions pour piano. Partitions de piano stride', 'Alex Hill, piano stride, partition, blues, stride, transcription, piano jazz, mp3, blueblackjazz', 'Alex Hill | BlueBlackJazz'),
(29, 15, '1908 - 1992', '<img style="width: 220px;float: right" src="http://www.blueblackjazz.com/media/images/sammy_price.jpg" />\r\n\r\nNative from Texas, Sammy Price first learned alto horn before studying piano. At age 20, he was working professionally with his own band between Texas, Oklahoma City and Kansas City. It was in this town he absorbed the Boogie Woogie foundations, especially with <a href="http://blueblackjazz.com/en/artist/6/pete-johnson">Pete Johnson</a>. Thereafter, he moved to New-York and began working for Decca label for a 15 years collaboration while using his skills has a composer, arranger and band leader. Besides a talented musician, Sammy Price has distinguished himself in political commitment in civil rights during the 60''s. He was also a great businessman : he opened a couple of night clubs in Texas and launched a meat products company. He was a highly accurate blues pianist and accompanist and deserves a much better recognition today.', 'en', 'Sammy Price biography. Transcriptions for piano. Boogie Woogie sheet music', 'sammy price, boogie-woogie, sheet music, boogie woogie, blues, stride, transcription, jazz piano, mp3, blueblackjazz', 'Sammy Price | BlueBlackJazz'),
(30, 15, '1908 - 1992', '<img style="width: 220px;float: right" src="http://www.blueblackjazz.com/media/images/sammy_price.jpg" />\r\n\r\nOriginaire du Texas, Sammy Price apprend d''abord le cor alto avant de se tourner vers le piano. Vers 20 ans, il se produit déjà avec son propre orchestre au Texas, Oklahoma City et Kansas City. C''est dans cette ville qu''il apprend les fondements du Boogie Woogie, notamment auprès de <a href="http://blueblackjazz.com/fr/artist/6/pete-johnson">Pete Johnson</a>. Il s''installe par la suite à New-York et travaille pour le label Decca, avec qui il collaborera pendant 15 ans en mettant à profit ses talents de compositeur, arranger et chef d''orchestre. Outre ses qualités de musicien, Sammy Price s''est illustré par son engagement politique pour les droits civiques dans les années 60, et par son sens des affaires : il ouvre plusieurs clubs au Texas et se lance dans le business florissant de la viande. Pianiste de blues d''une grande justesse et accompagnateur de talent, Sammy Price mérite davantage de reconnaissance aujourd''hui.', 'fr', 'Biographie de Sammy Price. Transcriptions pour piano. Partitions de Woogie Woogie', 'sammy price, boogie-woogie, partition, boogie woogie, blues, stride, transcription, piano jazz, mp3, blueblackjazz', 'Sammy Price | BlueBlackJazz'),
(31, 20, '1887 - 1983', '<img style="width: 180px;float: right" src="http://www.blueblackjazz.com/media/images/eubie_blake.jpg" /> \r\n\r\nHe was originally a ragtime composer. His style evolved in the nineteen-tens toward a more syncopated piano playing : he was, with Luckey Roberts one of the pioneers of Stride piano, which was defined a few years later by <a href="http://blueblackjazz.com/en/artist/3/james-p-johnson">James P. Johnson</a>. His long collaboration with Noble Sissle was very successful : they wrote "Shuffle Along" in 1921, the first Broadway show written by African Americans. \r\n\r\nAfter the world war II, Eubie Blake came back to ragtime, which he became the most authentic representative until his death at the age of 96. He is the author of jazz standards such as "Memories Of You", <a href="http://blueblackjazz.com/en/transcription/132/132-donald-lambert-im-just-wild-about-harry">"I''m Just Wild About Harry"</a> and "Love Will find A Way".', 'en', 'Eubie Blake biography. Transcriptions for piano. Jazz piano sheet music', 'Eubie Blake, stride piano, ragtime, sheet music, blues, stride, transcription, jazz piano, mp3, blueblackjazz', 'Eubie Blake | BlueBlackJazz'),
(32, 20, '1887 - 1983', '<img style="width: 180px;float: right" src="http://www.blueblackjazz.com/media/images/eubie_blake.jpg" />\r\n\r\nA l''origine compositeur de ragtime, le style de Eubie Blake évolue dans les années 1910 vers un jeu plus syncopée : il est, avec Luckey Roberts l''un des précurseurs du piano Stride, style définit par <a href="http://blueblackjazz.com/fr/artist/3/james-p-johnson">James P. Johnson</a> quelques années plus tard. Sa longue collaboration avec Noble Sissle sera une réussite : ils écrivent en 1921 le spectacle "Shuffle Along", premier show de Broadway écrit par, et mettant en scène des afro-américains.\r\n\r\nAprès la guerre, Eubie Blake revient vers le ragtime, style dont il devient le plus authentique représentant jusqu''à sa mort à l''âge de 96 ans. On lui doit quelques standards comme "Memories Of You", <a href="http://blueblackjazz.com/fr/transcription/132/132-donald-lambert-im-just-wild-about-harry">"I''m Just Wild About Harry"</a> ou "Love Will find A Way".', 'fr', 'Biographie de Eubie Blake. Transcriptions pour piano. Partitions de piano jazz', 'Eubie Blake, piano stride, ragtime, partition, blues, stride, transcription, piano jazz, mp3, blueblackjazz', 'Eubie Blake | BlueBlackJazz'),
(33, 12, '1904 - 1984', '<img style="width: 250px;float: right" src="http://www.blueblackjazz.com/media/images/count_basie.jpg" />\r\n\r\nWilliam Basie was born in 1904 in New Jersey. In the early 20''s he moved in Harlem where he discoverd the great stride pianists. His meeting with <a href="http://blueblackjazz.com/en/artist/1/fats-waller">Fats Waller</a> made a great impression on him : the latter learned him the organ basics when the young Basie used to listen <a href="http://blueblackjazz.com/en/artist/1/fats-waller">Fats Waller</a> playing at the Lincoln Theater. His career really took off in 1936 when he joined the Bennie Moten big band, and became leader of the group after Moten''s death. The band is renamed "Barons of Rhythm" and get hired at the Reno Club in Kansas City where he drawn attention of producer John Hammond. The orchestra begins to record several titles for Decca label from 1937. Basie''s qualities in composing and arranging pushed the group to the rank of best big band of that time. In January 1938, Basie''s orchestra played at the Carnegie Hall, the legendary concert hall where Duke Ellington and Benny Goodman already played.\r\n\r\nDuring the 60''s he recorded a great number of sides under his name and with several singers as Frank Sinatra. His collaboration with Oscar Peterson was also very prolific since they recorded about ten discs together. Basie''s minimalist and smart playing, his impeccable swing and his skills as a conductor made him one of the major personalities of Jazz era.', 'en', 'Count Basie biography. Transcriptions for piano. Jazz piano sheet music', 'count basie, jazz piano, sheet music, swing piano, boogie woogie, stride, transcription, jazz piano, mp3, blueblackjazz', 'Count Basie | BlueBlackJazz'),
(34, 12, '1904 - 1984', '<img style="width: 250px;float: right" src="http://www.blueblackjazz.com/media/images/count_basie.jpg" />\r\n\r\nWilliam Basie né en 1904 dans le New Jersey. Au début des années 20 il s''installe à Harlem où il découvre les grands pianistes de Stride. Sa rencontre avec <a href="http://blueblackjazz.com/fr/artist/1/fats-waller">Fats Waller</a> le marquera tout particulièrement : celui-ci lui apprendra les rudiments de l''orgue lorsque le jeune Basie viendra l''écouter jouer au Lincoln Theater. Sa carrière démarre véritablement en 1936 lorsqu''il intègre le big band de Bennie Moten, et prend la tête du groupe à la mort de ce dernier. L''orchestre est rebaptisé "Barons of Rhythm" et obtient un engagement Reno Club de Kansas City où il se fait remarquer par le producteur John Hammond. En 1937 l’orchestre enregistre ses premiers titres pour Decca. Les qualités de compositeur et d''arrangeur de Basie propulse le big band au rang de meilleur orchestre de swing de l''époque. En janvier 1938, Basie est au Carnegie Hall salle de concert mythique qui a vu passer Duke Ellington ou Benny Goodman.\r\n\r\nAu cours des années 60 il enregistre de nombreux disques sous son nom et avec divers chanteurs, dont Frank Sinatra. Sa collaboration avec Oscar Peterson sera également très fructueuse puisqu''ils enregistreront une dizaine de disques ensemble. Le jeu minimaliste et élégant de Count Basie, son swing irréprochable et ses qualités de chef d''orchestre en font l''un des artistes majeurs de l''ère du jazz.', 'fr', 'Biographie de Count Basie. Transcriptions pour piano. Partitions de piano jazz', 'count basie, piano jazz, partition, piano swing, boogie, stride, transcription, piano jazz, mp3, blueblackjazz', 'Count Basie | BlueBlackJazz'),
(35, 16, '1902 - 1970', '<img style="width: 210px;float: right" src="http://www.blueblackjazz.com/media/images/cliff_jackson.jpg" />\r\n\r\nA native of Virginia, Cliff Jackson played in New-York during the 20''s along with Stride piano masters. He led his own ensemble, the Krazy Kats with whom he recorded a few sides in 1930. Thereafter, Cliff Jackson played almost exclusively as a soloist, apart from a few recording as an accompanist especially with Sidney Bechet.\r\n\r\nAt the same time powerful and very accurate, Cliff Jackson is one of the great Stride Piano players in Harlem. Neither forerunner, nor obsolete, he played with a deep respect of harmony and melody, with a great swing. His strong pulse on the left hand can be recognized among thousands, by his way of doubling basses.', 'en', 'Cliff Jackson biography. Transcriptions for piano. Stride piano sheet music', 'cliff jackson, stride piano, sheet music, harlem stride, ragtime, stride, transcription, jazz piano, mp3, blueblackjazz', 'Cliff Jackson | BlueBlackJazz'),
(36, 16, '1902 - 1970', '<img style="width: 210px;float: right" src="http://www.blueblackjazz.com/media/images/cliff_jackson.jpg" />\r\n\r\nOriginaire de Virginie, Cliff Jackson sévit à New-York dans les années 20 aux côtés des maîtres du Stride. Il monte son propre ensemble, les "Krazy Cats" avec qui il enregistre en 1930. Par la suite, Cliff Jackson ne jouera presque exclusivement qu''en solo mis à part quelques enregistrements, notamment avec Sidney Bechet.\r\n\r\nAvec son jeu à la fois puissant et d''une grande justesse, Cliff Jackson figure parmi les grands du piano Stride de Harlem. Ni précurseur, ni obsolète , il joue dans le strict respect de l''harmonie et de la mélodie avec un swing sûr. La pulsation robuste de sa main gauche se reconnaît entre mille par sa façon de systématiquement doubler la basse.', 'fr', 'Biographie de Cliff Jackson. Transcriptions pour piano. Partitions de piano stride', 'cliff jackson, piano stride, partition, harlem stride, ragtime, stride, transcription, piano jazz, mp3, blueblackjazz', 'Cliff Jackson | BlueBlackJazz'),
(37, 21, '1903 - 1983', NULL, 'en', 'Earl Hines biography. Transcriptions for piano. Jazz piano sheet music', 'earl hines, jazz piano, sheet music, swing piano, stride piano, stride, transcription, jazz piano, mp3, blueblackjazz', 'Earl Hines | BlueBlackJazz'),
(38, 21, '1903 - 1983', NULL, 'fr', 'Biographie de Earl Hines. Transcriptions pour piano. Partitions de piano jazz', 'earl hines, piano jazz, partition, piano swing, ragtime, stride, transcription, piano jazz, mp3, blueblackjazz', 'Earl Hines | BlueBlackJazz'),
(39, 17, '1922 - 2001', '<img style="width: 200px;float: right" src="http://www.blueblackjazz.com/media/images/ralph-sutton.jpg" />\r\n\r\nHe was one of the worthiest representative of stride piano since world war, along with Dick Wellstood and Dick Hyman. \r\n\r\nHis exciting swing, the inventiveness of his improvisations, but mostly his powerful playing make him one of the convincing followers of <a href="http://blueblackjazz.com/en/artist/1/fats-waller">Fats Waller</a>.', 'en', 'Ralph Sutton biography. Transcriptions for piano. Stride piano sheet music', 'ralph sutton, stride piano, sheet music, harlem stride, ragtime, stride, transcription, jazz piano, mp3, blueblackjazz', 'Ralph Sutton | BlueBlackJazz'),
(40, 17, '1922 - 2001', '<img style="width: 200px;float: right" src="http://www.blueblackjazz.com/media/images/ralph-sutton.jpg" />\r\n\r\nC''est l''un des plus dignes représentants du piano Stride de l''après guerre, aux côtés de Dick Wellstood et Dick Hyman.\r\n\r\nSon swing électrisant, l''inventivité de ses improvisations et surtout sa puissance de jeu font de Ralph Sutton l''un des plus honorables héritiers de <a href="http://blueblackjazz.com/fr/artist/1/fats-waller">Fats Waller</a>.', 'fr', 'Biographie de Ralph Sutton. Transcriptions pour piano. Partitions de piano stride', 'ralph sutton, piano stride, partition, harlem stride, ragtime, stride, transcription, piano jazz, mp3, blueblackjazz', 'Ralph Sutton | BlueBlackJazz'),
(41, 18, '1916 - 1983', '<img style="width: 200px;float: right" src="http://www.blueblackjazz.com/media/images/don_ewell.jpg" />\r\n\r\nA native of Baltimore, Don Ewell began his career in the 30''s. Primarily influenced by  Jelly Roll Morton and <a href="http://www.blueblackjazz.com/en/artist/21/earl-hines">Earl Hines</a>, his playing evolved toward the stride style of <a href="http://blueblackjazz.com/en/artist/1/fats-waller">Fats Waller</a> and <a href="http://blueblackjazz.com/en/artist/3/james-p-johnson">James P. Johnson</a>. He gave performances as a soloist, and with Sidney Bechet, Kid Ory and Jack Teagarden. He also played in duet with <a href="http://www.blueblackjazz.com/en/artist/2/willie-the-lion-smith">Willie "The Lions" Smith</a> during the 60''s.\r\n\r\nWith a high piano technique, Don Ewell nevertheless focused on the respect of tradition and the value of melody. He was an inspired pianist who perpetuated the New-Orleans jazz tradition, with a great musical personality.', 'en', 'Don Ewell biography. Transcriptions for piano. Stride piano sheet music', 'don ewell, stride piano, sheet music, harlem stride, ragtime, stride, transcription, jazz piano, mp3, blueblackjazz', 'Don Ewell | BlueBlackJazz'),
(42, 18, '1916 - 1983', '<img style="width: 200px;float: right" src="http://www.blueblackjazz.com/media/images/don_ewell.jpg" />\r\n\r\nOriginaire de Baltimore, Don Ewell débute sa carrière dans les années 30. D''abord influencé par Jelly Roll Morton et <a href="http://www.blueblackjazz.com/fr/artist/21/earl-hines">Earl Hines</a>, le jeu de Don Ewell évolue par la suite vers le style stride de <a href="http://blueblackjazz.com/fr/artist/1/fats-waller">Fats Waller</a> et <a href="http://blueblackjazz.com/fr/artist/3/james-p-johnson">James P. Johnson</a>. Il se produit en solo, et accompagne notamment Sidney Bechet, Kid Ory et Jack Teagarden. Il joua également en duo avec <a href="http://www.blueblackjazz.com/fr/artist/2/willie-the-lion-smith">Willie "The Lions" Smith</a> dans les années 60.\r\n\r\nDoté d''une grande technique, Don Ewell s''attachait néanmoins au respect de la tradition, et à la valeur des mélodies. Pianiste très inspiré, il a su perpétuer l''esprit des anciens tout en y apportant sa personnalité musicale.', 'fr', 'Biographie de Don Ewell. Transcriptions pour piano. Partitions de piano stride', 'don ewell, piano stride, partition, harlem stride, ragtime, stride, transcription, piano jazz, mp3, blueblackjazz', 'Don Ewell | BlueBlackJazz'),
(43, 19, '1898 - 1952', '<img style="width: 250px;float: right" src="http://www.blueblackjazz.com/media/images/fletcher_henderson.jpg" />\r\n\r\nGreat conductor and precursor of the Big Bands era, Fletcher Henderson first learned the piano. In 1920, he moved to New-York to complete his studies in chemistry which he soon gave up for music. He starts his own group in 1924 and, with the growing success, begin to recruit the best soloist of that time : <a href="http://blueblackjazz.com/en/artist/1/fats-waller">Fats Waller</a>, Louis Armstrong, Coleman Hawkins et Benny Carter among others started their career under the direction of Fletcher Henderson. During many years, the orchestra is successful and gave performance in the most prestigious institutions.\r\n\r\nFletcher Henderson has defined the architecture of big orchestras, opening the way for the masters of swing era, especially Duke Ellington and Benny Goodman.', 'en', 'Fletcher Henderson biography. Transcriptions for piano. Jazz piano sheet music', 'Fletcher Henderson, stride piano, sheet music, blues, stride, transcription, jazz piano, mp3, blueblackjazz', 'Fletcher Henderson | BlueBlackJazz'),
(44, 19, '1898 - 1952', '<img style="width: 250px;float: right" src="http://www.blueblackjazz.com/media/images/fletcher_henderson.jpg" />\r\n\r\nGrand chef d''orchestre précurseur de l''ère des Big Bands, Fletcher Henderson apprend d''abord le piano. En 1920, il part à New-York pour finir ses études de chimie qu''il va rapidement délaisser au profit de la musique. Il monte son propre orchestre en 1924 et, le succès grandissant, s''entoure des meilleurs solistes de l''époque : <a href="http://blueblackjazz.com/fr/artist/1/fats-waller">Fats Waller</a>, Louis Armstrong, Coleman Hawkins et Benny Carter entre autres, ont fait leurs armes sous la direction de Fletcher Henderson. Le succès sera au rendez-vous pendant de longues années durant lesquelles l''orchestre se produit dans les plus prestigieux établissements.\r\n\r\nFletcher Henderson aura mis au point l''architecture des grands orchestre en ouvrant la voie aux maîtres de l''ère du swing, notamment Duke Ellington et Benny Goodman.', 'fr', 'Biographie de Fletcher Henderson. Transcriptions pour piano. Partitions de piano jazz', 'Fletcher Henderson, piano stride, partition, blues, stride, transcription, piano jazz, mp3, blueblackjazz', 'Fletcher Henderson | BlueBlackJazz');

-- --------------------------------------------------------

--
-- Structure de la table `Book`
--

CREATE TABLE IF NOT EXISTS `Book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `artist_id` int(11) DEFAULT NULL,
  `style_id` int(11) DEFAULT NULL,
  `priceEuro` decimal(10,2) NOT NULL,
  `item_number` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pdf` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_cover` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_other` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_table` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_6BD70C0F10696A9B` (`item_number`),
  KEY `IDX_6BD70C0FB7970CF8` (`artist_id`),
  KEY `IDX_6BD70C0FBACD6074` (`style_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Contenu de la table `Book`
--

INSERT INTO `Book` (`id`, `artist_id`, `style_id`, `priceEuro`, `item_number`, `title`, `pdf`, `img_cover`, `img_other`, `img_table`, `slug`) VALUES
(1, 1, 2, '44.95', 1001, '17 Solos For Piano Volume 1', 'FF3ekfJi98nq34JX/_waller_vol1.pdf', 'vol1.jpg', NULL, 'vol1_table.jpg', 'fats-waller-17-solos-for-piano-volume-1'),
(2, 1, 2, '44.95', 1002, '17 Solos For Piano Volume 2', 'FF3ekfJi98nq34JX/_waller_vol2.pdf', 'vol2.jpg', NULL, 'vol2_table.jpg', 'fats-waller-17-solos-for-piano-volume-2'),
(3, 1, 2, '44.95', 1003, '18 Solos For Piano Volume 3', 'FF3ekfJi98nq34JX/_waller_vol3.pdf', 'vol3.jpg', NULL, 'vol3_table_sm.jpg', 'fats-waller-18-solos-for-piano-volume-3'),
(4, 3, 2, '44.95', 1004, '17 Solos For Piano Volume 1', 'FF3ekfJi98nq34JX/_johnson.pdf', 'johnson_couv.jpg', NULL, 'johnson_table_sm.jpg', 'james-p-johnson-17-solos-for-piano-volume-1'),
(5, 3, 2, '44.95', 1009, '17 Solos For Piano Volume 2', 'FF3ekfJi98nq34JX/_johnson_vol2.pdf', 'johnson2_couv.jpg', NULL, 'johnson2_table_sm.jpg', 'james-p-johnson-17-solos-for-piano-volume-2'),
(6, 3, 2, '44.95', 1008, 'The Piano Rolls - 17 Piano Solos', 'FF3ekfJi98nq34JX/_johnson_rolls.pdf', 'johnson-roll_couv.jpg', NULL, 'johnson-roll_table.jpg', 'james-p-johnson-the-piano-rolls-17-piano-solos'),
(7, 2, 2, '44.95', 1005, '16 Solos For Piano', 'FF3ekfJi98nq34JX/_lion.pdf', 'partitions_lion.jpg', NULL, 'lion_table_sm.jpg', 'willie-the-lion-smith-16-solos-for-piano'),
(8, 4, 2, '44.95', 1006, '15 Solos For Piano', 'FF3ekfJi98nq34JX/_lambert.pdf', 'lambert_cov.jpg', NULL, 'lambert_table_sm.jpg', 'donald-lambert-15-solos-for-piano'),
(9, NULL, 1, '44.95', 1007, '17 Boogie-woogie & Blues Piano Solos Vol1', 'FF3ekfJi98nq34JX/_boogie-woogie.pdf', 'boogie-woogie.jpg', NULL, 'boogie_table_sm.jpg', '17-boogie-woogie-blues-piano-solos-vol1'),
(10, NULL, 1, '44.95', NULL, '17 Boogie-woogie & Blues Piano Solos Vol2', 'FF3ekfJi98nq34JX/boogie_vol2.pdf', 'boogie-woogie_vol2.jpg', NULL, 'boogie_table_sm.jpg', '17-boogie-woogie-blues-piano-solos-vol2');

-- --------------------------------------------------------

--
-- Structure de la table `BookTranslation`
--

CREATE TABLE IF NOT EXISTS `BookTranslation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seoDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seoKeywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seoTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_46D64EED2C2AC5D34180C698` (`translatable_id`,`locale`),
  KEY `IDX_46D64EED2C2AC5D3` (`translatable_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Contenu de la table `BookTranslation`
--

INSERT INTO `BookTranslation` (`id`, `translatable_id`, `description`, `locale`, `seoDescription`, `seoKeywords`, `seoTitle`) VALUES
(1, 1, '17 transcriptions pour piano de Fats Waller d''après les enregistrements originaux - Transcrit par Paul Marcorelles - 108 pages', 'fr', NULL, NULL, 'Fats Waller - 17 Piano solos Vol.1 | BlueBlackJazz'),
(2, 1, '17 Fats Waller piano transcriptions from original recordings - Transcribed by Paul Marcorelles - 108 pages', 'en', NULL, NULL, 'Fats Waller - 17 Piano solos Vol.1 | BlueBlackJazz'),
(3, 2, '17 transcriptions pour piano de Fats Waller d''après les enregistrements originaux - Transcrit par Paul Marcorelles - 94 pages', 'fr', NULL, NULL, 'Fats Waller - 17 Piano solos Vol.2 | BlueBlackJazz'),
(4, 2, 'Book of 17 accurate piano transcriptions from Fats Waller original recordings.Transcribed by Paul Marcorelles - 94 pages', 'en', NULL, NULL, 'Fats Waller - 17 Piano solos Vol.2 | BlueBlackJazz'),
(5, 3, '18 transcriptions pour piano de Fats Waller d''après les enregistrements originaux - Transcrit par Paul Marcorelles - 104 pages', 'fr', NULL, NULL, 'Fats Waller - 18 Piano solos Vol.3 | BlueBlackJazz'),
(6, 3, 'Book of 18 accurate piano transcriptions from Fats Waller original recordings.Transcribed by Paul Marcorelles - 104 pages', 'en', NULL, NULL, 'Fats Waller - 18 Piano solos Vol.3 | BlueBlackJazz'),
(7, 4, '17 transcriptions pour piano de James P. Johnson d''après les enregistrements originaux - Transcrit par Paul Marcorelles - 131 pages', 'fr', NULL, NULL, 'James P. Johnson - 17 Piano solos Vol.1 | BlueBlackJazz'),
(8, 4, 'Book of 17 accurate piano transcriptions from James P. Johnson original recordings.Transcribed by Paul Marcorelles - 131 pages', 'en', NULL, NULL, 'James P. Johnson - 17 Piano solos Vol.1 | BlueBlackJazz'),
(9, 5, '17 transcriptions pour piano de James P. Johnson d''après les enregistrements originaux - Transcrit par Paul Marcorelles - 120 pages', 'fr', NULL, NULL, 'James P. Johnson - 17 Piano solos Vol.2 | BlueBlackJazz'),
(10, 5, 'Book of 17 accurate piano transcriptions from James P. Johnson original recordings.Transcribed by Paul Marcorelles - 120 pages', 'en', NULL, NULL, 'James P. Johnson - 17 Piano solos Vol.2 | BlueBlackJazz'),
(11, 6, '17 transcriptions pour piano de James P. Johnson d''après les rouleaux de piano originaux - Transcrit par Paul Marcorelles - 137 pages', 'fr', NULL, NULL, 'James P. Johnson - 17 Piano Rolls transcriptions | BlueBlackJazz'),
(12, 6, 'Book of 17 accurate piano transcriptions from James P. Johnson original piano rolls.Transcribed by Paul Marcorelles - 137 pages', 'en', NULL, NULL, 'James P. Johnson - 17 Piano Rolls transcriptions | BlueBlackJazz'),
(13, 7, '17 transcriptions pour piano de Willie "The Lion" Smith d''après les enregistrements originaux. Transcrit par Paul Marcorelles - 118 pages', 'fr', NULL, NULL, 'Willie "The Lion" Smith - 16 Piano Solos | BlueBlackJazz'),
(14, 7, 'Book of 17 accurate piano transcriptions from Willie "The Lion" Smith original recordings. Transcribed by Paul Marcorelles - 118 pages', 'en', NULL, NULL, 'Willie "The Lion" Smith - 16 Piano Solos | BlueBlackJazz'),
(15, 8, '17 transcriptions pour piano de Donald Lambert d''après les enregistrements originaux. Transcrit par Paul Marcorelles - 104 pages', 'fr', NULL, NULL, 'Donald Lambert - 15 Piano Solos | BlueBlackJazz'),
(16, 8, 'Book of 17 accurate piano transcriptions from Donald Lambert original recordings. Transcribed by Paul Marcorelles - 104 pages', 'en', NULL, NULL, 'Donald Lambert - 15 Piano Solos | BlueBlackJazz'),
(17, 9, '17 transcriptions originales de boogie-woogie d''après les enregistrements originaux. Transcrit par Paul Marcorelles - 119 pages', 'fr', NULL, NULL, 'Boogie Woogie - 17 Piano Solos Vol.1 | BlueBlackJazz'),
(18, 9, 'Book of 17 original boogie-woogie transcriptions from original recordings. Transcribed by Paul Marcorelles - 119 pages', 'en', NULL, NULL, 'Boogie Woogie - 17 Piano Solos Vol.1 | BlueBlackJazz'),
(19, 10, 'Book of 17 original boogie-woogie transcriptions from original recordings. Transcribed by Paul Marcorelles - 122 pages', 'en', NULL, NULL, 'Boogie Woogie - 17 Piano Solos Vol.2 | BlueBlackJazz'),
(20, 10, '17 transcriptions originales de boogie-woogie d''après les enregistrements originaux. Transcrit par Paul Marcorelles - 122 pages', 'fr', NULL, NULL, 'Boogie Woogie - 17 Piano Solos Vol.2 | BlueBlackJazz');

-- --------------------------------------------------------

--
-- Structure de la table `credits`
--

CREATE TABLE IF NOT EXISTS `credits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_instruction_id` int(11) NOT NULL,
  `payment_id` int(11) DEFAULT NULL,
  `attention_required` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `credited_amount` decimal(10,5) NOT NULL,
  `crediting_amount` decimal(10,5) NOT NULL,
  `reversing_amount` decimal(10,5) NOT NULL,
  `state` smallint(6) NOT NULL,
  `target_amount` decimal(10,5) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4117D17E8789B572` (`payment_instruction_id`),
  KEY `IDX_4117D17E4C3A3BB` (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `financial_transactions`
--

CREATE TABLE IF NOT EXISTS `financial_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `credit_id` int(11) DEFAULT NULL,
  `payment_id` int(11) DEFAULT NULL,
  `extended_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:extended_payment_data)',
  `processed_amount` decimal(10,5) NOT NULL,
  `reason_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reference_number` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `requested_amount` decimal(10,5) NOT NULL,
  `response_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` smallint(6) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `tracking_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transaction_type` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1353F2D9CE062FF9` (`credit_id`),
  KEY `IDX_1353F2D94C3A3BB` (`payment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=38 ;

--
-- Contenu de la table `financial_transactions`
--

INSERT INTO `financial_transactions` (`id`, `credit_id`, `payment_id`, `extended_data`, `processed_amount`, `reason_code`, `reference_number`, `requested_amount`, `response_code`, `state`, `created_at`, `updated_at`, `tracking_id`, `transaction_type`) VALUES
(1, NULL, 1, NULL, '0.00000', 'action_required', '8H1800758C211120D', '194.80000', 'pending', 4, '2014-06-07 16:42:11', '2014-06-07 16:42:44', NULL, 2),
(2, NULL, 2, NULL, '0.00000', 'action_required', '77404439H2406340W', '104.90000', 'pending', 4, '2014-06-07 16:44:59', '2014-06-07 16:45:24', NULL, 2),
(3, NULL, 3, NULL, '0.00000', 'action_required', '6XF62636A4537921B', '176.85000', 'pending', 4, '2014-06-08 15:33:07', '2014-06-08 15:33:51', NULL, 2),
(4, NULL, 4, NULL, '0.00000', 'action_required', '8S012480G44539446', '402.80000', 'pending', 4, '2014-06-08 15:38:20', '2014-06-08 15:38:56', NULL, 2),
(5, NULL, 5, NULL, '0.00000', 'action_required', NULL, '59.95000', 'pending', 4, '2014-06-27 00:02:47', '2014-06-27 00:02:49', NULL, 2),
(6, NULL, 6, NULL, '0.00000', 'action_required', '1L755596WD056314U', '59.95000', 'pending', 4, '2014-06-27 00:03:49', '2014-06-27 00:04:35', NULL, 2),
(7, NULL, 7, NULL, '0.00000', 'action_required', NULL, '52.45000', 'pending', 4, '2014-07-08 17:10:45', '2014-07-08 17:10:47', NULL, 2),
(8, NULL, 8, NULL, '0.00000', 'action_required', '9VM7586184198734F', '44.95000', 'pending', 4, '2014-07-10 18:26:56', '2014-07-10 18:28:15', NULL, 2),
(9, NULL, 9, NULL, '0.00000', '10525', NULL, '0.00000', 'Failure', 2, '2014-07-10 23:13:29', '2014-07-10 23:13:30', NULL, 2),
(10, NULL, 10, NULL, '0.00000', 'action_required', NULL, '7.50000', 'pending', 4, '2014-07-10 23:13:56', '2014-07-10 23:13:57', NULL, 2),
(11, NULL, 11, NULL, '0.00000', '10525', NULL, '0.00000', 'Failure', 2, '2014-07-23 16:44:52', '2014-07-23 16:44:53', NULL, 2),
(12, NULL, 12, NULL, '0.00000', '10525', NULL, '0.00000', 'Failure', 2, '2014-07-23 16:44:54', '2014-07-23 16:44:55', NULL, 2),
(13, NULL, 13, NULL, '0.00000', '10525', NULL, '0.00000', 'Failure', 2, '2014-07-23 16:44:56', '2014-07-23 16:44:57', NULL, 2),
(14, NULL, 14, NULL, '0.00000', '10525', NULL, '0.00000', 'Failure', 2, '2014-07-23 16:45:06', '2014-07-23 16:45:07', NULL, 2),
(15, NULL, 15, NULL, '0.00000', '10525', NULL, '0.00000', 'Failure', 2, '2014-07-23 16:45:11', '2014-07-23 16:45:12', NULL, 2),
(16, NULL, 16, NULL, '0.00000', '10525', NULL, '0.00000', 'Failure', 2, '2014-07-23 16:45:13', '2014-07-23 16:45:14', NULL, 2),
(17, NULL, 17, NULL, '0.00000', 'action_required', '2BE66607749871002', '89.95000', 'pending', 4, '2014-08-04 20:32:58', '2014-08-04 20:34:46', NULL, 2),
(18, NULL, 18, NULL, '0.00000', 'action_required', NULL, '89.90000', 'pending', 4, '2014-08-04 20:38:46', '2014-08-04 20:38:48', NULL, 2),
(19, NULL, 19, NULL, '183.96000', 'none', '6035804868712083C', '183.96000', 'success', 5, '2014-08-04 20:39:26', '2014-08-04 20:40:32', NULL, 2),
(20, NULL, 20, NULL, '10.23000', 'none', '23018024ME9483448', '10.23000', 'success', 5, '2014-09-05 06:35:04', '2014-09-05 06:43:51', NULL, 2),
(21, NULL, 21, NULL, '0.00000', 'action_required', '9YY65929FP1676845', '7.50000', 'pending', 4, '2014-09-28 23:00:40', '2014-09-28 23:08:35', NULL, 2),
(22, NULL, 22, NULL, '0.00000', 'action_required', NULL, '51.95000', 'pending', 4, '2014-09-30 17:13:08', '2014-09-30 17:13:09', NULL, 2),
(23, NULL, 23, NULL, '0.00000', 'action_required', NULL, '51.95000', 'pending', 4, '2014-09-30 17:13:12', '2014-09-30 17:13:13', NULL, 2),
(24, NULL, 24, NULL, '0.00000', 'action_required', NULL, '51.95000', 'pending', 4, '2014-09-30 17:13:55', '2014-09-30 17:13:55', NULL, 2),
(25, NULL, 25, NULL, '0.00000', 'action_required', '14145558M3794482A', '51.95000', 'pending', 4, '2014-09-30 17:15:39', '2014-09-30 17:18:39', NULL, 2),
(26, NULL, 26, NULL, '0.00000', 'action_required', NULL, '44.95000', 'pending', 4, '2014-10-01 14:44:36', '2014-10-01 14:44:37', NULL, 2),
(27, NULL, 27, NULL, '0.00000', 'action_required', NULL, '44.95000', 'pending', 4, '2014-10-01 14:52:20', '2014-10-01 14:52:21', NULL, 2),
(28, NULL, 28, NULL, '0.00000', 'action_required', NULL, '44.95000', 'pending', 4, '2014-10-01 14:52:53', '2014-10-01 14:52:54', NULL, 2),
(29, NULL, 29, NULL, '0.00000', 'action_required', '4XJ44073KM405074V', '44.95000', 'pending', 4, '2014-10-01 14:53:38', '2014-10-01 14:55:11', NULL, 2),
(30, NULL, 30, NULL, '0.00000', 'action_required', NULL, '44.95000', 'pending', 4, '2014-10-02 17:40:23', '2014-10-02 17:40:24', NULL, 2),
(31, NULL, 31, NULL, '0.00000', 'action_required', NULL, '44.95000', 'pending', 4, '2014-10-02 21:06:12', '2014-10-02 21:06:13', NULL, 2),
(32, NULL, 32, NULL, '0.00000', 'action_required', NULL, '44.95000', 'pending', 4, '2014-10-02 21:09:22', '2014-10-02 21:09:24', NULL, 2),
(33, NULL, 33, NULL, '0.00000', 'action_required', NULL, '7.50000', 'pending', 4, '2014-10-02 21:13:32', '2014-10-02 21:13:33', NULL, 2),
(34, NULL, 34, NULL, '0.00000', '10002', NULL, '7.50000', 'Failure', 2, '2014-10-02 21:15:38', '2014-10-02 21:15:39', NULL, 2),
(35, NULL, 35, NULL, '0.00000', 'action_required', NULL, '7.50000', 'pending', 4, '2014-10-02 21:17:41', '2014-10-02 21:17:42', NULL, 2),
(36, NULL, 36, NULL, '0.00000', 'action_required', NULL, '7.50000', 'pending', 4, '2014-10-02 21:17:54', '2014-10-02 21:17:55', NULL, 2),
(37, NULL, 37, NULL, '0.50000', 'none', '72D71176EP7989428', '0.50000', 'success', 5, '2014-10-02 21:20:14', '2014-10-02 21:23:15', NULL, 2);

-- --------------------------------------------------------

--
-- Structure de la table `lexik_currency`
--

CREATE TABLE IF NOT EXISTS `lexik_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `rate` decimal(10,4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Contenu de la table `lexik_currency`
--

INSERT INTO `lexik_currency` (`id`, `code`, `rate`) VALUES
(1, 'EUR', '1.0000'),
(2, 'USD', '1.3642');

-- --------------------------------------------------------

--
-- Structure de la table `MyOrder`
--

CREATE TABLE IF NOT EXISTS `MyOrder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nDownloads` int(11) NOT NULL,
  `expiresAt` datetime DEFAULT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payerId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `shipping` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `shippingCost` decimal(10,2) NOT NULL,
  `cancelled` tinyint(1) NOT NULL,
  `confirmed` tinyint(1) NOT NULL,
  `paymentInstruction_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_6657DC4F8FFBE0F7` (`salt`),
  UNIQUE KEY `UNIQ_6657DC4FFD913E4D` (`paymentInstruction_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=34 ;

--
-- Contenu de la table `MyOrder`
--

INSERT INTO `MyOrder` (`id`, `nDownloads`, `expiresAt`, `salt`, `payerId`, `currency`, `shipping`, `email`, `amount`, `shippingCost`, `cancelled`, `confirmed`, `paymentInstruction_id`) VALUES
(1, 0, '2014-06-09 16:42:44', 'b8f5bdac54b6e11fca561661f44325902e344b68d47b052307173290bacf5df8', 'LMALJMSNP2SHJ', 'EUR', 'PDF', 'pierroweb@gmail.com', '194.80', '0.00', 0, 1, 1),
(2, 0, '2014-06-09 16:45:24', '8ff8e7536592a818e80fd9f048c7ffc7eee69c5dc19a7cde049e986b719c49fc', 'LMALJMSNP2SHJ', 'EUR', 'PDF', 'pierroweb@gmail.com', '104.90', '0.00', 0, 1, 2),
(3, 0, '2014-06-10 15:33:51', '7b9b9f8f83a5b373e63dfd527ea865a45dbe27eb810eb3cd5cb33c05300801ea', 'LMALJMSNP2SHJ', 'EUR', 'WORLD', 'paul.marcorelles@gmail.com', '176.85', '27.00', 0, 1, 3),
(4, 0, '2014-06-10 15:38:57', '5989ccc452f7f339602305cfe999693245c0b83bde7aea1cea9bf6b81220eaad', 'LMALJMSNP2SHJ', 'EUR', 'EUROPE', 'paul.marcorelles@gmail.com', '402.80', '28.00', 0, 1, 4),
(5, 0, NULL, '379976b61497f5f024871aafd7681ad06cb20e5bf2fe07619e153e6dd6c8db26', NULL, 'EUR', 'PDF', 'toto@helloworld.fr', '59.95', '0.00', 0, 0, 5),
(6, 2, '2014-06-29 00:04:35', 'f8c7b176a179068a94c4f05cf260abfa580bf47a5520f6b9b597ccf68f617ccd', 'LMALJMSNP2SHJ', 'EUR', 'PDF', 'antoine.fouchier@gmail.com', '59.95', '0.00', 0, 1, 6),
(7, 0, NULL, 'aae5a1b0d3f6a22422266461cc56eac0a4ea352ed3ca526d7587e5f122723d9e', NULL, 'EUR', 'PDF', 'morela@zulauf.net', '52.45', '0.00', 0, 0, 7),
(8, 1, '2014-07-12 18:28:15', '2e66da3def9a7815ecf1860b13f1cd249d43b4954c967bec569589a4b91d19c3', 'LMALJMSNP2SHJ', 'EUR', 'PDF', 'pierroweb@gmail.com', '44.95', '0.00', 0, 1, 8),
(9, 0, NULL, 'f47ea6d0263ec3abca45ec0ada75167dc46c7ec024850e7303d7dfdf8aa65349', NULL, 'EUR', 'FRANCE', 'kj@jo.c', '0.00', '0.00', 0, 0, 9),
(10, 0, NULL, 'd920b5f6302e2936c32afb5ea70b9d89120771bf580a623c6ab6b606ebb68109', NULL, 'EUR', 'FRANCE', 'zef@zef.f', '7.50', '0.00', 0, 0, 10),
(11, 0, NULL, 'b842d921fbe0c0282463504f69f60e8284935dd3321c3cd092d67334e4ed183e', NULL, 'EUR', 'PDF', 'ezfdze@ef.fr', '0.00', '0.00', 0, 0, 11),
(12, 0, NULL, 'ec26066987dd0146574bb38c8d14f8105401c56da6c8258b8203a3e775a9fa5d', NULL, 'USD', 'WORLD', 'ezfdze@ef.fr', '0.00', '0.00', 0, 0, 12),
(13, 0, NULL, 'fb51e9ae24b7f0db7767aae321003046b4a90d4dd8cde36d812b0d734b29752f', NULL, 'USD', 'PDF', 'frfer@ef.fr', '0.00', '0.00', 0, 0, 13),
(14, 2, '2014-08-06 20:34:46', 'f8a84eb6f35062d3bf72ac548623f8d58da8c782733cbc97e36837caf7e21037', 'LMALJMSNP2SHJ', 'EUR', 'PDF', 'paul.marcorelles@gmail.com', '89.95', '0.00', 0, 1, 14),
(15, 0, NULL, 'a7417ed2b103dc0a3eadfac0522a9829c5416a1051f3d606beb33c7d566d5468', NULL, 'EUR', 'PDF', 'paul.marcorelles@gmail.com', '89.90', '0.00', 0, 0, 15),
(16, 6, '2014-08-06 20:40:32', 'd9cfa514fe708c48ed7d241c5cac69505a05bb9649eb3c0ac5ad95c64e5c91a3', 'LMALJMSNP2SHJ', 'USD', 'PDF', 'paul.marcorelles@gmail.com', '183.96', '0.00', 0, 1, 16),
(17, 0, '2014-09-07 06:43:51', '174a2a83ab4d2b16ada848c9e7514d60aea68fc4d9933163434a3091dd1aece2', '76AFWSSZE2GLA', 'USD', 'PDF', 'hrachhov@hotmail.com', '10.23', '0.00', 0, 1, 17),
(18, 0, '2014-09-30 23:08:35', '4ecda372d887a3b149a971ec75c9388a28cd479eac58da3419116adb17888a75', 'ZFGSJDDJMJBKN', 'EUR', 'PDF', 'donphelan@gmail.com', '7.50', '0.00', 0, 1, 18),
(19, 0, NULL, '98e0e4ddc0e451d52a317ab06e2abc9fe460895e927642727aed1d8d0a10f8ae', NULL, 'EUR', 'EUROPE', 'cbadams%@yahoo.com', '51.95', '7.00', 0, 0, 19),
(20, 0, NULL, 'ae9ef7cb160a3ce0f95faf00d25379ded39a9e05aa1bd4736f11bb60eac8e9f8', NULL, 'EUR', 'EUROPE', 'cbadams5@yahoo.com', '51.95', '7.00', 0, 0, 20),
(21, 0, NULL, '25aa086e9f16887be2cd9514e967cfcdf402ffda27d24eb0d92aa6f988556b79', NULL, 'EUR', 'EUROPE', 'cbadams5@yahoo.com', '51.95', '7.00', 1, 0, 21),
(22, 0, '2014-10-02 17:18:39', '00ebd1b560e8b0ee98119d477a33395f6fb796eb8670ae71365275d2ce303f41', 'PGP6NZMWNPZNN', 'EUR', 'EUROPE', 'cbadams5@yahoo.com', '51.95', '7.00', 0, 1, 22),
(23, 0, NULL, '9378e9fcc247d2e6ae7f256a4890e23d6a8c7a07998eb76ec95efab64404373d', NULL, 'EUR', 'PDF', 'pierre.bellec@ensae.fr', '44.95', '0.00', 0, 0, 23),
(24, 0, NULL, '95c0718b30515dd910de7f5617686921a94e8a1e7cf5d4b6e200769939c1f95e', NULL, 'EUR', 'PDF', 'pierre.bellec@ensae.fr', '44.95', '0.00', 0, 0, 24),
(25, 0, NULL, '46daee40665aadcfbf2c7781359a13acc7ad7c05b642adacce57019b9a9ea52b', NULL, 'EUR', 'PDF', 'pierre.bellec@ensae.fr', '44.95', '0.00', 1, 0, 25),
(26, 3, '2014-10-03 14:55:11', '132b21ee933758cdfb7934f51f08cd7ea48e4fc5376bc4276320052173c4203e', 'LMALJMSNP2SHJ', 'EUR', 'PDF', 'pierre.bellec@ensae.fr', '44.95', '0.00', 0, 1, 26),
(27, 0, NULL, '69ba0ed036a7d8bfa8d1731fb4fe07ada7e4de3dcead0df16f5cd92cb2ebb6dd', NULL, 'EUR', 'PDF', 'pierre.bellec@ensae.fr', '44.95', '0.00', 0, 0, 27),
(28, 0, NULL, '35c4f37c74a21042ef4e037763c39dfaa4ed529311cdf94ab48f4bd6d7641735', NULL, 'EUR', 'PDF', 'pierre.bellec@ensae.fr', '44.95', '0.00', 0, 0, 28),
(29, 0, NULL, '6f025c6699ff3769054eb17b06bc23ed425a70c4a47d4ccd14b8e4d24b3d976c', NULL, 'EUR', 'PDF', 'pierre.bellec@ensae.fr', '44.95', '0.00', 0, 0, 29),
(30, 0, NULL, 'bf6af394b2519b3f31b7fef23f68803db9743503201dacf9051e53a6c3b579f6', NULL, 'EUR', 'PDF', 'paul.marcorelles@gmail.com', '7.50', '0.00', 0, 0, 30),
(31, 0, NULL, '47d654db17eccb64832114b46fc9c7f960ffb351180925f1e621e515efdca7d8', NULL, 'EUR', 'PDF', 'paul.marcorelles@gmail.com', '7.50', '0.00', 0, 0, 31),
(32, 0, NULL, 'ea250d87a6cfe729f91db39297bba0c70ac55d2f0c962cec9091071ad54cb5b6', NULL, 'EUR', 'PDF', 'paul.marcorelles@gmail.com', '7.50', '0.00', 1, 0, 32),
(33, 0, '2014-10-04 21:23:15', 'f1c4a78dad5a65bd7d1fe4798da9c739d82aeb4076065e6b9dfad19001281c49', 'JMQEZJFUXFJ9A', 'EUR', 'PDF', 'paul.marcorelles@gmail.com', '0.50', '0.00', 0, 1, 33);

-- --------------------------------------------------------

--
-- Structure de la table `myorder_book`
--

CREATE TABLE IF NOT EXISTS `myorder_book` (
  `myorder_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  PRIMARY KEY (`myorder_id`,`book_id`),
  KEY `IDX_2DD936AF732E2069` (`myorder_id`),
  KEY `IDX_2DD936AF16A2B381` (`book_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `myorder_book`
--

INSERT INTO `myorder_book` (`myorder_id`, `book_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 8),
(2, 1),
(2, 2),
(3, 2),
(3, 5),
(3, 8),
(4, 6),
(4, 7),
(4, 8),
(4, 9),
(5, 1),
(6, 1),
(7, 2),
(8, 7),
(14, 10),
(15, 1),
(15, 7),
(16, 1),
(16, 7),
(16, 10),
(19, 9),
(20, 9),
(21, 9),
(22, 9),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 2),
(29, 1);

-- --------------------------------------------------------

--
-- Structure de la table `myorder_transcription`
--

CREATE TABLE IF NOT EXISTS `myorder_transcription` (
  `myorder_id` int(11) NOT NULL,
  `transcription_id` int(11) NOT NULL,
  PRIMARY KEY (`myorder_id`,`transcription_id`),
  KEY `IDX_F875FD04732E2069` (`myorder_id`),
  KEY `IDX_F875FD04F678E194` (`transcription_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `myorder_transcription`
--

INSERT INTO `myorder_transcription` (`myorder_id`, `transcription_id`) VALUES
(1, 2),
(1, 120),
(2, 120),
(2, 122),
(3, 48),
(3, 149),
(4, 1),
(4, 2),
(4, 6),
(4, 7),
(4, 10),
(4, 13),
(4, 15),
(4, 27),
(4, 35),
(4, 39),
(4, 41),
(4, 44),
(4, 46),
(4, 47),
(4, 49),
(4, 51),
(4, 52),
(4, 55),
(4, 57),
(4, 60),
(4, 62),
(4, 69),
(4, 88),
(4, 89),
(4, 98),
(4, 100),
(5, 1),
(5, 2),
(6, 1),
(6, 2),
(7, 22),
(10, 82),
(14, 1),
(14, 4),
(14, 163),
(14, 166),
(14, 168),
(14, 169),
(17, 147),
(18, 164),
(30, 1),
(31, 2),
(32, 1),
(33, 1);

-- --------------------------------------------------------

--
-- Structure de la table `Page`
--

CREATE TABLE IF NOT EXISTS `Page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B438191E5E237E06` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Contenu de la table `Page`
--

INSERT INTO `Page` (`id`, `name`, `slug`) VALUES
(1, 'services', 'services'),
(2, 'how-to-order', 'how-to-order'),
(3, 'contacts', 'contacts'),
(4, 'testimonials', 'testimonials'),
(5, 'homepage', 'homepage'),
(6, 'all', 'all'),
(7, 'books', 'books');

-- --------------------------------------------------------

--
-- Structure de la table `PageTranslation`
--

CREATE TABLE IF NOT EXISTS `PageTranslation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `markdown` longtext COLLATE utf8_unicode_ci,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seoDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seoKeywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seoTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_D29B35C02C2AC5D34180C698` (`translatable_id`,`locale`),
  KEY `IDX_D29B35C02C2AC5D3` (`translatable_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Contenu de la table `PageTranslation`
--

INSERT INTO `PageTranslation` (`id`, `translatable_id`, `title`, `markdown`, `locale`, `seoDescription`, `seoKeywords`, `seoTitle`) VALUES
(1, 4, 'Testimonials', '\n> The transcriptions you have done are accurate and overall amazing\n>\n> <footer> -- <cite>William P., Pianist, USA</cite></footer>\n\n* * *\n\n\n> Congratulations on your great work \n>\n> <footer> -- <cite>David A., USA</cite></footer>\n\n* * *\n\n\n> Your books of transcriptions are the answer to my prayers! I find your transcriptions to be the most accurate I''ve come across. \n>\n> <footer> -- <cite>Andrew C., USA</cite></footer>\n\n* * *\n\n\n> Your sense of earing and your ability to understand this music is just phenomenal\n>\n> <footer> -- <cite>Daniel P., pianist, France</cite></footer>\n\n* * *\n\n\n> It''s just fantastic! It sounds exactly like the recording. You have a rare and great talent for transcribing music! \n>\n> <footer> -- <cite>Luigi R., Italy</cite></footer>\n\n* * *\n\n\n> Great job with these wonderful stride piano transcriptions. \n>\n> <footer> -- <cite>Chris D., pianist, USA</cite></footer>\n\n* * *\n\n\n> With your transcriptions there are those moments of pure happiness felt by listening the original recordings\n>\n> <footer> -- <cite>Pilippe G., France</cite></footer>\n\n* * *\n\n\n> Thanks for the transcription that I''ve been waiting 40 years!\n>\n> <footer> -- <cite>Bernard C., France</cite></footer>\n\n* * *\n\n\n> Thank you so much for transcribing these great Jazz piano solos that would otherwise be lost and never available for us piano players. I have purchased every folio that you have published and have never been dissappointed. Keep up the good work... \n>\n> <footer> -- <cite>Steve S., USA</cite></footer>\n\n* * *\n\n\n> I think your transcriptions are fantastic and very accurate. These are the third set i have bought \n>\n> <footer> -- <cite>Jon W., U.K</cite></footer>\n\n* * *\n\n\n> Many thanks, this is great work you''re doing!! It''s great that you are making these arrangements accessible \n>\n> <footer> -- <cite>Shoshana M., USA</cite></footer>\n\n* * *\n\n\n> I''m a great fan of yours, you write excellent transcriptions! \n>\n> <footer> -- <cite>Eldad S., IsraÃ«l</cite></footer>\n\n* * *\n\n\n> Thank you again for making your wonderfully accurate transcriptions! \n>\n> <footer> -- <cite>Dave J., USA</cite></footer>\n\n* * *\n\n\n> The extraordinary side of your work is that it''s perfect!\n>\n> <footer> -- <cite>Mike S., Switzerland</cite></footer>\n\n* * *\n\n\n> Keep up the great work! \n>\n> <footer> -- <cite>Ari K., USA</cite></footer>\n\n* * *\n\n\n> You are doing an amazing job keeping all this fantastic music alive \n>\n> <footer> -- <cite>Nicholas C. U-K</cite></footer>\n\n* * *\n\n\n> Thank you for making your wonderful transcriptions! I get enjoyment from practicing them every day \n>\n> <footer> -- <cite>Dave J., USA</cite></footer>\n\n* * *\n\n\n> I am very impressed with your ability to transcribe these wonderful but exclusive pieces \n>\n> <footer> -- <cite>Thomas M., USA</cite></footer>\n\n* * *\n\n\n> I am really very happy because of Your transcriptions \n>\n> <footer> -- <cite>Norbert W., Austria</cite></footer>\n\n* * *\n\n\n> Your transcriptions are wonderful! Very much appreciated. \n>\n> <footer> -- <cite>Cliff M., USA</cite></footer>\n\n* * *\n\n\n> Thanks once again for your great transcriptions! They are a real treasure. \n>\n> <footer> -- <cite>Chris S., USA</cite></footer>\n\n* * *\n\n', 'en', NULL, NULL, NULL),
(2, 4, 'Témoignages', '\n> The transcriptions you have done are accurate and overall amazing\n>\n> <footer> -- <cite>William P., Pianist, USA</cite></footer>\n\n* * *\n\n\n> Congratulations on your great work \n>\n> <footer> -- <cite>David A., USA</cite></footer>\n\n* * *\n\n\n> Your books of transcriptions are the answer to my prayers! I find your transcriptions to be the most accurate I''ve come across. \n>\n> <footer> -- <cite>Andrew C., USA</cite></footer>\n\n* * *\n\n\n> Your sense of earing and your ability to understand this music is just phenomenal\n>\n> <footer> -- <cite>Daniel P., pianist, France</cite></footer>\n\n* * *\n\n\n> It''s just fantastic! It sounds exactly like the recording. You have a rare and great talent for transcribing music! \n>\n> <footer> -- <cite>Luigi R., Italy</cite></footer>\n\n* * *\n\n\n> Great job with these wonderful stride piano transcriptions. \n>\n> <footer> -- <cite>Chris D., pianist, USA</cite></footer>\n\n* * *\n\n\n> With your transcriptions there are those moments of pure happiness felt by listening the original recordings\n>\n> <footer> -- <cite>Pilippe G., France</cite></footer>\n\n* * *\n\n\n> Thanks for the transcription that I''ve been waiting 40 years!\n>\n> <footer> -- <cite>Bernard C., France</cite></footer>\n\n* * *\n\n\n> Thank you so much for transcribing these great Jazz piano solos that would otherwise be lost and never available for us piano players. I have purchased every folio that you have published and have never been dissappointed. Keep up the good work... \n>\n> <footer> -- <cite>Steve S., USA</cite></footer>\n\n* * *\n\n\n> I think your transcriptions are fantastic and very accurate. These are the third set i have bought \n>\n> <footer> -- <cite>Jon W., U.K</cite></footer>\n\n* * *\n\n\n> Many thanks, this is great work you''re doing!! It''s great that you are making these arrangements accessible \n>\n> <footer> -- <cite>Shoshana M., USA</cite></footer>\n\n* * *\n\n\n> I''m a great fan of yours, you write excellent transcriptions! \n>\n> <footer> -- <cite>Eldad S., IsraÃ«l</cite></footer>\n\n* * *\n\n\n> Thank you again for making your wonderfully accurate transcriptions! \n>\n> <footer> -- <cite>Dave J., USA</cite></footer>\n\n* * *\n\n\n> The extraordinary side of your work is that it''s perfect!\n>\n> <footer> -- <cite>Mike S., Switzerland</cite></footer>\n\n* * *\n\n\n> Keep up the great work! \n>\n> <footer> -- <cite>Ari K., USA</cite></footer>\n\n* * *\n\n\n> You are doing an amazing job keeping all this fantastic music alive \n>\n> <footer> -- <cite>Nicholas C. U-K</cite></footer>\n\n* * *\n\n\n> Thank you for making your wonderful transcriptions! I get enjoyment from practicing them every day \n>\n> <footer> -- <cite>Dave J., USA</cite></footer>\n\n* * *\n\n\n> I am very impressed with your ability to transcribe these wonderful but exclusive pieces \n>\n> <footer> -- <cite>Thomas M., USA</cite></footer>\n\n* * *\n\n\n> I am really very happy because of Your transcriptions \n>\n> <footer> -- <cite>Norbert W., Austria</cite></footer>\n\n* * *\n\n\n> Your transcriptions are wonderful! Very much appreciated. \n>\n> <footer> -- <cite>Cliff M., USA</cite></footer>\n\n* * *\n\n\n> Thanks once again for your great transcriptions! They are a real treasure. \n>\n> <footer> -- <cite>Chris S., USA</cite></footer>\n\n* * *\n\n', 'fr', NULL, NULL, NULL),
(3, 5, 'BlueBlackJazz', 'Around 180 jazz piano transcriptions available, transcribed from original recordings. \r\n<br>\r\n<br>\r\n\r\n<i>"Learning early jazz piano quite difficult because it is a very visual style of music which has nearly disappeared today. Even if a natural ear for music allows to immerse in the sounds we hear on records, a sheet music is very useful to learn the basis of Stride piano and play it properly. There are very few transcriptions of the great jazz pianists and this is a lack in the musical edition and for people who want to learn this style of music and keep it alive.\r\n\r\nThese transcriptions will help you to understand many characteristics of the classic jazz piano styles but they are not supposed to be played with a great accuracy : Every piece must be performed with the genuineness and the freshness that jazz music requires. In our own way, we can give a second life to these pieces. The left hand playing requires a great skill that you get with practice : it is useless to play tenth with your left hand (especially in quick tempo) if your technical level or your morphology do not allow it. Play only the low note then. Do not forget that stride piano and boogie woogie are part of jazz and is mainly improvised."</i>\r\n\r\n<div style="text-align:right">Paul Marcorelles </div>', 'en', 'Jazz piano sheet music : Stride piano, boogie woogie, blues, swing piano. Download accurate transcriptions from original recordings.', 'jazz piano, transcription, sheet music, stride, stride piano, harlem stride, boogie woogie, blues, jazz, ragtime, fats waller, james p. johnson, donald lambert, lion, willie the lion smith, blueblackjazz, mp3, instant download, pdf', 'BlueBlackJazz | Jazz piano transcriptions'),
(4, 5, NULL, 'L''apprentissage du piano stride et du boogie-woogie n''est pas de toute facilité, d''autant que ce sont des styles très visuels et qui ont tendance à se perdre aujourd''hui. Une bonne oreille permet de s''imprégner de ce que l''on entend sur les disques, mais un support papier peut s''avérer d''une grande utilité afin de cerner les bases et de jouer ce style avec justesse.\r\n\r\nTrès peu de transcriptions fidèles existent des grands pianistes des débuts du jazz ce qui est fort regrettable, surtout pour ceux qui désirent apprendre et faire perdurer ce style. J''ai ainsi transcris un certain nombre de morceaux, le plus fidèlement possible, d''aprés les enregistrements d''époque des pianistes de stride et boogie-woogie. Pour la quasi totalité de ces morceaux, aucune musique écrite ou transcriptions n''a été publiées à ce jour.\r\n\r\nEnviron 160 transcriptions sont proposées, disponibles individuellements ou regroupées en 9 recueils. Téléchargement au format PDF ou envoi par courrier au format papier.', 'fr', 'Partitions de piano jazz : Piano stride, boogie woogie, blues, piano swing. Téléchargement de transcriptions fidèles d''après les enregistrements originaux.', 'piano jazz, transcription, partition, stride, piano stride, harlem stride, boogie woogie, blues, jazz, ragtime, fats waller, james p. johnson, donald lambert, lion, willie the lion smith, blueblackjazz, mp3, telechargement, pdf', 'BlueBlackJazz | Transcriptions de piano jazz'),
(5, 2, 'How to order ?', '<br>\r\n<p>\r\n    BlueBlackJazz offers piano solo transcriptions of great jazz pianists. These note-for-note transcriptions are made from original recordings. Most of the pieces availables are also included in transcriptions books.\r\n</p>\r\n<hr style="width: 40%; height: 1px;" noshade="noshade">\r\n<p>\r\n    <strong></strong>\r\n</p>\r\n<p>\r\n    <strong>How to order ?</strong>\r\n</p>\r\n<p>\r\n    Each product (solo transcription or transcription book) can be ordered by adding it in cart. In order to finalize your order, an e-mail address is requested for the PDF delivery.\r\n</p>\r\n<br>\r\n    <strong>Payment :</strong>\r\n</p>\r\n<p>\r\n    Payment is processed with Paypal. It''s possible to pay with credit card, or directly with your Paypal account if you have one.\r\n</p>\r\n<a href="javascript:void(0)" onclick="javascript:window.open(''https://www.paypal.com/uk/cgi-bin/webscr?cmd=xpt/Marketing/popup/OLCWhatIsPayPal-outside'',''olcwhatispaypal'',''toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=400, height=350'');"><img src="http://www.blueblackjazz.com/media/images/horizontal_solution_PP.gif" alt="Logos de solution PayPal" border="0"></a>\r\n<br>\r\nIf you live in metropolitan France, you can order by cheque by submitting this <a href="http://www.blueblackjazz.com/media/pdf/Bon-waller.pdf">order form</a>.\r\n</p>\r\n<br>\r\n<p>\r\n    <strong>Delivery mode :</strong>\r\n</p>\r\n<p>\r\n    All sheet music on this website are available in PDF format. Transcriptions books can also be ordered in hard copy format (printed book). The default delivery mode is PDF (instant delivery). Delivery mode can be changed in the cart.\r\n</p>\r\n<br>\r\n<p>\r\n    <strong>Order reception :</strong>\r\n</p>\r\n<p>\r\n    PDF delivery occurs immediately after the order : once your order finished, you''ll receive a download link by e-mail at the address provided during the order.\r\n<p>\r\n</p>\r\n    Printed books are generally shipped within 7 days according to availability. Delivery time of printed books never exceeds 4 weeks. .\r\n</p>\r\n<br>\r\n<p>\r\n    <strong>How to read PDF ?</strong>\r\n</p>\r\n<p>\r\n    PDF sheet music can be read with <a href="http://get.adobe.com/reader/">Acrobat Reader</a>.\r\n</p>\r\n\r\n<img style="float: right" src="http://www.blueblackjazz.com/media/images/blueblackjazz.bmp" />', 'en', 'Order PDF sheet music on BlueBlackJazz - Download Sheet music - Payment - Delivery', 'pdf, sheet music, jazz, jazz piano, transcription, stride, boogie, stride piano, boogie woogie, delivery, paypal, bueblackjazz', 'How to order? | BlueBlackJazz'),
(6, 2, 'Comment commander ?', '<br>\r\n<p>\r\n    BlueBlackJazz propose des transcriptions de piano solos des grands pianistes de jazz. Ces transcriptions note-pour-note sont issues des enregistrements\r\n    originaux. La plupart des morceaux proposés figurent également dans les recueils de partitions.\r\n</p>\r\n<hr style="width: 40%; height: 1px;" noshade="noshade">\r\n<p>\r\n    <strong></strong>\r\n</p>\r\n<p>\r\n    <strong>Comment commander ?</strong>\r\n</p>\r\n<p>\r\n    Chaque article (partition ou recueil de partitions) peut être commandé en ajoutant l''article au panier. Afin de finaliser la commande, une adresse e-mail est demandée pour l’envoie instantané des partitions.\r\n</p>\r\n<br>\r\n    <strong>Modes de paiements :</strong>\r\n</p>\r\n<p>\r\n    Le règlement des commandes est effectué via Paypal. Il est donc possible de régler pas carte bancaire, ou directement via votre compte Paypal si vous en avez un.\r\n</p>\r\n<a href="javascript:void(0)" onclick="javascript:window.open(''https://www.paypal.com/fr/cgi-bin/webscr?cmd=xpt/Marketing/popup/OLCWhatIsPayPal-outside'',''olcwhatispaypal'',''toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=400, height=350'');"><img src="http://www.blueblackjazz.com/media/images/horizontal_solution_PP.gif" alt="Logos de solution PayPal" border="0"></a>\r\n<br>\r\nSi vous résidez en France métropolitaine, il est possible de régler par chèque en retournant ce <a href="http://www.blueblackjazz.com/media/pdf/Bon-waller.pdf">formulaire de commande</a>.\r\n</p>\r\n<br>\r\n<p>\r\n    <strong>Mode de livraison :</strong>\r\n</p>\r\n<p>\r\n    L''ensemble des partitions présentes sur le site sont disponibles au format PDF. Les recueils de partitions sont également disponibles au format papier\r\n    (livre imprimé). Le mode de livraison par défaut est la livraison électronique instantanée au format PDF. Le mode livraison est modifiable au niveau du\r\n    panier.\r\n</p>\r\n<br>\r\n<p>\r\n    <strong>Réception de la commande :</strong>\r\n</p>\r\n<p>\r\n    La livraison des partitions PDF est instantanée : une fois la commande finalisée, un e-mail contenant les liens de téléchargement des partitions PDF\r\n    commandées vous est envoyé à l''adresse indiquée lors de la commande.\r\n<p>\r\n</p>\r\n    Les recueils commandés au format papier sont généralement expédiés sous 7 jours selon les disponibilités, le délai de livraison n''excédant jamais 4 semaines.\r\n</p>\r\n<br>\r\n<p>\r\n    <strong>Comment lire les partitions PDF ?</strong>\r\n</p>\r\n<p>\r\n    Les partitions PDF sont lisibles à l''aide de l''outil <a href="http://get.adobe.com/fr/reader/">Acrobat Reader</a>.\r\n</p>\r\n\r\n<img style="float: right" src="http://www.blueblackjazz.com/media/images/blueblackjazz.bmp" />', 'fr', 'Commander des partitions PDF sur BlueBlackJazz - Partitons à télécharger - Paiement - Livraison', 'pdf, partition, jazz, piano jazz, transcription, stride, boogie, piano stride, boogie woogie, livraison, paypal, bueblackjazz', 'Comment commander? | BlueBlackJazz'),
(7, 3, 'Contacts', '<br>\r\n    Any question, suggestion, testimonial ?\r\n</p>\r\n<p>\r\n    <strong>paul.marcorelles@blueblackjazz.com</strong>\r\n</p>\r\n<div>\r\n    <hr size="1" width="251" noshade="" align="left"/>\r\n</div>\r\n<br>\r\n    BlueBlackJazz\r\n<br>\r\n    81 rue du Cherche-Midi\r\n<br>\r\n    75006 Paris\r\n<br>\r\n    France\r\n</p>\r\n\r\n<img style="width: 150px;float: right" src="http://www.blueblackjazz.com/media/images/blueblackjazz.jpg" />', 'en', NULL, NULL, 'Contacts | BlueBlackJazz'),
(8, 3, 'Contacts', '<br>\r\n    Une question, suggestion, temoignage ?\r\n</p>\r\n<p>\r\n    <strong>paul.marcorelles@blueblackjazz.com</strong>\r\n</p>\r\n<div>\r\n    <hr size="1" width="251" noshade="" align="left"/>\r\n</div>\r\n<br>\r\n    BlueBlackJazz\r\n<br>\r\n    81 rue du Cherche-Midi\r\n<br>\r\n    75006 Paris\r\n<br>\r\n    France\r\n</p>\r\n\r\n<img style="width: 150px;float: right" src="http://www.blueblackjazz.com/media/images/blueblackjazz.jpg" />', 'fr', NULL, NULL, 'Contacts | BlueBlackJazz'),
(9, 1, 'Transcription service', NULL, 'en', 'Jazz piano transcription service. BlueBlackJazz offers an accurate and professional experience of jazz piano transcription.', 'jazz piano, transcription, jazz, ragtime, transcription service, on demand, mp3, sheet music, accurate, jazz transcription,', 'Transcription service | BlueBlackJazz'),
(10, 1, 'Service de transcription', NULL, 'fr', 'Service de transcription de piano jazz. BlueBlackJazz propose un service de transcription à la demande.', 'piano jazz, transcription, jazz, ragtime, service de transcription, sur demande, mp3, sheet music, jazz transcription, oreille, audio', 'Service de transcription | BlueBlackJazz'),
(11, 6, 'All transcriptions', NULL, 'en', NULL, NULL, 'Jazz piano transcriptions | BlueBlackJazz'),
(12, 6, 'Toutes les transcriptions', NULL, 'fr', NULL, NULL, 'Piano jazz transcriptions | BlueBlackJazz'),
(13, 7, NULL, NULL, 'en', NULL, NULL, 'Transcriptions books | BlueBlackJazz'),
(14, 7, NULL, NULL, 'fr', NULL, NULL, 'Recueils de transcriptions | BlueBlackJazz');

-- --------------------------------------------------------

--
-- Structure de la table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_instruction_id` int(11) NOT NULL,
  `approved_amount` decimal(10,5) NOT NULL,
  `approving_amount` decimal(10,5) NOT NULL,
  `credited_amount` decimal(10,5) NOT NULL,
  `crediting_amount` decimal(10,5) NOT NULL,
  `deposited_amount` decimal(10,5) NOT NULL,
  `depositing_amount` decimal(10,5) NOT NULL,
  `expiration_date` datetime DEFAULT NULL,
  `reversing_approved_amount` decimal(10,5) NOT NULL,
  `reversing_credited_amount` decimal(10,5) NOT NULL,
  `reversing_deposited_amount` decimal(10,5) NOT NULL,
  `state` smallint(6) NOT NULL,
  `target_amount` decimal(10,5) NOT NULL,
  `attention_required` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_65D29B328789B572` (`payment_instruction_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=38 ;

--
-- Contenu de la table `payments`
--

INSERT INTO `payments` (`id`, `payment_instruction_id`, `approved_amount`, `approving_amount`, `credited_amount`, `crediting_amount`, `deposited_amount`, `depositing_amount`, `expiration_date`, `reversing_approved_amount`, `reversing_credited_amount`, `reversing_deposited_amount`, `state`, `target_amount`, `attention_required`, `expired`, `created_at`, `updated_at`) VALUES
(1, 1, '0.00000', '194.80000', '0.00000', '0.00000', '0.00000', '194.80000', NULL, '0.00000', '0.00000', '0.00000', 2, '194.80000', 0, 0, '2014-06-07 16:42:11', '2014-06-07 16:42:12'),
(2, 2, '0.00000', '104.90000', '0.00000', '0.00000', '0.00000', '104.90000', NULL, '0.00000', '0.00000', '0.00000', 2, '104.90000', 0, 0, '2014-06-07 16:44:59', '2014-06-07 16:45:00'),
(3, 3, '0.00000', '176.85000', '0.00000', '0.00000', '0.00000', '176.85000', NULL, '0.00000', '0.00000', '0.00000', 2, '176.85000', 0, 0, '2014-06-08 15:33:07', '2014-06-08 15:33:08'),
(4, 4, '0.00000', '402.80000', '0.00000', '0.00000', '0.00000', '402.80000', NULL, '0.00000', '0.00000', '0.00000', 2, '402.80000', 0, 0, '2014-06-08 15:38:20', '2014-06-08 15:38:21'),
(5, 5, '0.00000', '59.95000', '0.00000', '0.00000', '0.00000', '59.95000', NULL, '0.00000', '0.00000', '0.00000', 2, '59.95000', 0, 0, '2014-06-27 00:02:47', '2014-06-27 00:02:49'),
(6, 6, '0.00000', '59.95000', '0.00000', '0.00000', '0.00000', '59.95000', NULL, '0.00000', '0.00000', '0.00000', 2, '59.95000', 0, 0, '2014-06-27 00:03:49', '2014-06-27 00:03:51'),
(7, 7, '0.00000', '52.45000', '0.00000', '0.00000', '0.00000', '52.45000', NULL, '0.00000', '0.00000', '0.00000', 2, '52.45000', 0, 0, '2014-07-08 17:10:45', '2014-07-08 17:10:47'),
(8, 8, '0.00000', '44.95000', '0.00000', '0.00000', '0.00000', '44.95000', NULL, '0.00000', '0.00000', '0.00000', 2, '44.95000', 0, 0, '2014-07-10 18:26:56', '2014-07-10 18:26:57'),
(9, 9, '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', NULL, '0.00000', '0.00000', '0.00000', 5, '0.00000', 0, 0, '2014-07-10 23:13:29', '2014-07-10 23:13:30'),
(10, 10, '0.00000', '7.50000', '0.00000', '0.00000', '0.00000', '7.50000', NULL, '0.00000', '0.00000', '0.00000', 2, '7.50000', 0, 0, '2014-07-10 23:13:56', '2014-07-10 23:13:57'),
(11, 11, '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', NULL, '0.00000', '0.00000', '0.00000', 5, '0.00000', 0, 0, '2014-07-23 16:44:52', '2014-07-23 16:44:53'),
(12, 12, '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', NULL, '0.00000', '0.00000', '0.00000', 5, '0.00000', 0, 0, '2014-07-23 16:44:54', '2014-07-23 16:44:55'),
(13, 12, '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', NULL, '0.00000', '0.00000', '0.00000', 5, '0.00000', 0, 0, '2014-07-23 16:44:56', '2014-07-23 16:44:57'),
(14, 12, '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', NULL, '0.00000', '0.00000', '0.00000', 5, '0.00000', 0, 0, '2014-07-23 16:45:06', '2014-07-23 16:45:07'),
(15, 13, '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', NULL, '0.00000', '0.00000', '0.00000', 5, '0.00000', 0, 0, '2014-07-23 16:45:11', '2014-07-23 16:45:12'),
(16, 13, '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', NULL, '0.00000', '0.00000', '0.00000', 5, '0.00000', 0, 0, '2014-07-23 16:45:13', '2014-07-23 16:45:14'),
(17, 14, '0.00000', '89.95000', '0.00000', '0.00000', '0.00000', '89.95000', NULL, '0.00000', '0.00000', '0.00000', 2, '89.95000', 0, 0, '2014-08-04 20:32:58', '2014-08-04 20:33:00'),
(18, 15, '0.00000', '89.90000', '0.00000', '0.00000', '0.00000', '89.90000', NULL, '0.00000', '0.00000', '0.00000', 2, '89.90000', 0, 0, '2014-08-04 20:38:46', '2014-08-04 20:38:48'),
(19, 16, '183.96000', '0.00000', '0.00000', '0.00000', '183.96000', '0.00000', NULL, '0.00000', '0.00000', '0.00000', 8, '183.96000', 0, 0, '2014-08-04 20:39:26', '2014-08-04 20:40:32'),
(20, 17, '10.23000', '0.00000', '0.00000', '0.00000', '10.23000', '0.00000', NULL, '0.00000', '0.00000', '0.00000', 8, '10.23000', 0, 0, '2014-09-05 06:35:03', '2014-09-05 06:43:51'),
(21, 18, '0.00000', '7.50000', '0.00000', '0.00000', '0.00000', '7.50000', NULL, '0.00000', '0.00000', '0.00000', 2, '7.50000', 0, 0, '2014-09-28 23:00:39', '2014-09-28 23:00:41'),
(22, 19, '0.00000', '51.95000', '0.00000', '0.00000', '0.00000', '51.95000', NULL, '0.00000', '0.00000', '0.00000', 2, '51.95000', 0, 0, '2014-09-30 17:13:08', '2014-09-30 17:13:09'),
(23, 20, '0.00000', '51.95000', '0.00000', '0.00000', '0.00000', '51.95000', NULL, '0.00000', '0.00000', '0.00000', 2, '51.95000', 0, 0, '2014-09-30 17:13:12', '2014-09-30 17:13:13'),
(24, 21, '0.00000', '51.95000', '0.00000', '0.00000', '0.00000', '51.95000', NULL, '0.00000', '0.00000', '0.00000', 2, '51.95000', 0, 0, '2014-09-30 17:13:55', '2014-09-30 17:13:55'),
(25, 22, '0.00000', '51.95000', '0.00000', '0.00000', '0.00000', '51.95000', NULL, '0.00000', '0.00000', '0.00000', 2, '51.95000', 0, 0, '2014-09-30 17:15:39', '2014-09-30 17:15:40'),
(26, 23, '0.00000', '44.95000', '0.00000', '0.00000', '0.00000', '44.95000', NULL, '0.00000', '0.00000', '0.00000', 2, '44.95000', 0, 0, '2014-10-01 14:44:36', '2014-10-01 14:44:37'),
(27, 24, '0.00000', '44.95000', '0.00000', '0.00000', '0.00000', '44.95000', NULL, '0.00000', '0.00000', '0.00000', 2, '44.95000', 0, 0, '2014-10-01 14:52:20', '2014-10-01 14:52:21'),
(28, 25, '0.00000', '44.95000', '0.00000', '0.00000', '0.00000', '44.95000', NULL, '0.00000', '0.00000', '0.00000', 2, '44.95000', 0, 0, '2014-10-01 14:52:53', '2014-10-01 14:52:54'),
(29, 26, '0.00000', '44.95000', '0.00000', '0.00000', '0.00000', '44.95000', NULL, '0.00000', '0.00000', '0.00000', 2, '44.95000', 0, 0, '2014-10-01 14:53:38', '2014-10-01 14:53:39'),
(30, 27, '0.00000', '44.95000', '0.00000', '0.00000', '0.00000', '44.95000', NULL, '0.00000', '0.00000', '0.00000', 2, '44.95000', 0, 0, '2014-10-02 17:40:23', '2014-10-02 17:40:24'),
(31, 28, '0.00000', '44.95000', '0.00000', '0.00000', '0.00000', '44.95000', NULL, '0.00000', '0.00000', '0.00000', 2, '44.95000', 0, 0, '2014-10-02 21:06:12', '2014-10-02 21:06:13'),
(32, 29, '0.00000', '44.95000', '0.00000', '0.00000', '0.00000', '44.95000', NULL, '0.00000', '0.00000', '0.00000', 2, '44.95000', 0, 0, '2014-10-02 21:09:22', '2014-10-02 21:09:24'),
(33, 30, '0.00000', '7.50000', '0.00000', '0.00000', '0.00000', '7.50000', NULL, '0.00000', '0.00000', '0.00000', 2, '7.50000', 0, 0, '2014-10-02 21:13:32', '2014-10-02 21:13:33'),
(34, 31, '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', '0.00000', NULL, '0.00000', '0.00000', '0.00000', 5, '7.50000', 0, 0, '2014-10-02 21:15:38', '2014-10-02 21:15:39'),
(35, 31, '0.00000', '7.50000', '0.00000', '0.00000', '0.00000', '7.50000', NULL, '0.00000', '0.00000', '0.00000', 2, '7.50000', 0, 0, '2014-10-02 21:17:41', '2014-10-02 21:17:42'),
(36, 32, '0.00000', '7.50000', '0.00000', '0.00000', '0.00000', '7.50000', NULL, '0.00000', '0.00000', '0.00000', 2, '7.50000', 0, 0, '2014-10-02 21:17:54', '2014-10-02 21:17:55'),
(37, 33, '0.50000', '0.00000', '0.00000', '0.00000', '0.50000', '0.00000', NULL, '0.00000', '0.00000', '0.00000', 8, '0.50000', 0, 0, '2014-10-02 21:20:14', '2014-10-02 21:23:15');

-- --------------------------------------------------------

--
-- Structure de la table `payment_instructions`
--

CREATE TABLE IF NOT EXISTS `payment_instructions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` decimal(10,5) NOT NULL,
  `approved_amount` decimal(10,5) NOT NULL,
  `approving_amount` decimal(10,5) NOT NULL,
  `created_at` datetime NOT NULL,
  `credited_amount` decimal(10,5) NOT NULL,
  `crediting_amount` decimal(10,5) NOT NULL,
  `currency` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `deposited_amount` decimal(10,5) NOT NULL,
  `depositing_amount` decimal(10,5) NOT NULL,
  `extended_data` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:extended_payment_data)',
  `payment_system_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `reversing_approved_amount` decimal(10,5) NOT NULL,
  `reversing_credited_amount` decimal(10,5) NOT NULL,
  `reversing_deposited_amount` decimal(10,5) NOT NULL,
  `state` smallint(6) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=34 ;

--
-- Contenu de la table `payment_instructions`
--

INSERT INTO `payment_instructions` (`id`, `amount`, `approved_amount`, `approving_amount`, `created_at`, `credited_amount`, `crediting_amount`, `currency`, `deposited_amount`, `depositing_amount`, `extended_data`, `payment_system_name`, `reversing_approved_amount`, `reversing_credited_amount`, `reversing_deposited_amount`, `state`, `updated_at`) VALUES
(1, '194.80000', '0.00000', '194.80000', '2014-06-07 16:42:11', '0.00000', '0.00000', 'EUR', '0.00000', '194.80000', 'a:4:{s:10:"return_url";a:3:{i:0;s:192:"/uAExjDaYzviEznRzXATKorbYJ272MudRj0iUF32eNmjo37h+itgrjd0Kv9QyfBGOG4L28TwOXkIvbkWUaHJdGxedGYWS2+x4tYwhU+kbNnoYJF8g6OUpDagPT3YoYej+dUL+Ql61jHYeHO4UiKC4UMWmJJvdBuX2x+epdlhAND9pVXB8/hk4UM3e+4PZlWm";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:192:"29VOnqzj6YN8tJ9jn4LHGutBmdkEAPggSGOzZ5G960k566zPd0oQalhcX+/rAX/zjHZghs2zrGf/DJId1hLX0nz06zDcLn59JyMm0Hp2Mf5rwNvGiKeFcrasQQBZZumymUfJZlHa58cwqbH/4oSkUpH8vAoSoKrZpnjgCYm21oOPc4TN7rCm4Gp6BUddZg==";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:1928:"tHNGM0V6XB/+LRQK4BjZOAvqjE5abIwvwLbVcwG4wraF+DKgBABWsw+HsixPtS7BXLOEv7ub8nMcS9b09MphY62/PtqrCXX3eWrj2gSbtTa0a3ZPMTLQBITShijXK6ol+VKT9aZeYrRx90eS0O19OgKwcFTDerPIwg5kZFgUZs9h+yAuCLqv8UPYlHHZtLMZAgeZg1ADiiN+Qmkj2TXpk2UsWxyyuiPAZH7LDP4kdEzn5xEplEIkduXUYCNDhAUOTSIR5A2vqMssMIbcsoyMq/9PtrajzWuacLEvyrjpkzimgd2aqQelvuv5dt6xGXGZElSGI/vDG3B5M0vg6LNpvI0+zLD1Jje/emqHRcNULAYdBzb19y5mUbPR8Dy3QK2EmAlMO2Z2/ui8ycmSS9QmvUo413pvYpdvI4UWEwiWVU9SQt+hA8OxHDUeZceujZp2GehNIt95UI4jzuOzRfa9Lxz6ZdJii8FuIzSytyttq0IBcEGdWBRHZaUFT8Ab+doetwpavWaF0V0Yq3F6ICTxxwj1hiySJar3nG7fGCjAxFhTRQeHGiTV2HhsaCCgYmuk2aOBZGw66sdI1J8A07CL5AAVaGl4t7hWmiqr4vy9lV3s9HtppRtyEpMkiCHCcfcdEdalS6gh/EoP5e+C7HF25vsL0JqmIT5jsXuAR34zIiLT2RoBYv+zY8fPiHRfed+nHSb8YqhOu7knFEzXoK31vjhgysMivUhHn85L3rCY1p2hXH2R6NSfNdzhaFEMfmvPZ+FqWpVkT4+0PqhKgYfLUKwSD5/jLrdeXQVKanj59X/bW99POPT/yGWmD0yx0g6QfECu8c84oAxffHXlDdbL9XRe7BvH22p9YPsZwhMj4jqokP5cuFAWS+e3AR69dUWSpCoNG16QqMjZt+Pgx0hNdwkZL1wTchWSfIM+RaQUbdHkJPgUEdtgDXn5H8dZqF/VV73y8CFPs26C0OA0KRC+52J90hz8Lvi2AkU+KKdmQDv8oMgemIuCgYvg6s4yJszf7J6waZpSkFeWXphAaNvsM9FZwljChuwxcVkrwZk4y8VIbo8WZRo89isxBZb12uyX2ph0e1smsj5dxAOoYu9j8uAWRsbp/AkJ3fgD0NYkSiaGr/P6a4M64meL2hovL33fXkEb00QdXL4vc3/A4thIbGhW0GJhiwNuBJcuCA0XfcVQ76ubnn9IvvZboaZFCBnBQ8KPu6NQxWlr8g0FVZhyAoUeVhnDZwKehCoM+ECSfgQBETmxAYFADTaCYvlgMc/YG83kvCN2uzWZblWIeBnY3I13OJpfztKYoPB4LKgPuKycpcmGQY/wN+zS000v+8/E/MAYwWFyFiRhvJZeba1KSPu29hadb6R8ZereE3/POI3pgy4ax4oE0RFE8StUrqi3qA3V5sGIfA9LKBG7Zl33jpgr9qX8+RPUtU9OAkS/U5X2bdNgCeNmDjzUGL7mKSoU54zXd6Sm3iFPDVNCAzOCIpK5PKNYhgGCM1vwxoTKlnHZwrkYaLMrBGhAhUgb0n+izjhzmlVDua+a8yylzp2KYvXdmvKcdc4Dbgu57juAjKlmim3o4ImMK61hXxNKWB0Fgs1hDnTRM/mmcHnoHROMFOQlPP5euXDK6lk4X3Unstrc/sXdGf1ucIE2ddT9XnWocNcJLXvnNehsEBteFovCQ9L3nGybxNygrB1nm6AIpe7w6plq9dopSOP/pSnfHM+1RSmccxdl1cP+9u5GQ1Wwr8WMBMaj7wWt+8/O1InoyWlAyG1AjgM5lK10fygjxlGZsgAEj0dMlWcLtH5oBgTaMY3f/ksHQ/7yOOmKbA/1HqrK/vk4Qc1fDwtHpf854neDe3tEXUO4Vg5rO8ANlmR18gDtyEc/ChozD2JLvJqmqyWpNoBRsQjeo1E8xLRTeH46tEgfUqY=";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"pfM2ex1dsdby1qmPTMj6l5eADfeq+CCSXDhncEnELw8ObRvpaDLT5/WrXsWSm9U5fg1vurCqS7Mu5+87";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-06-07 16:42:12'),
(2, '104.90000', '0.00000', '104.90000', '2014-06-07 16:44:59', '0.00000', '0.00000', 'EUR', '0.00000', '104.90000', 'a:4:{s:10:"return_url";a:3:{i:0;s:192:"szmhWFpa83xSQ7ppK+xgybtdsJqKt1yNtzJhF5Ls5XzQMqS/LtSJw6ktx08iRKhbin3y7aC7QiSV65QRbkk1GCq7SqbD3PPd+oRLIa7TDSvHqTV9A9tKm5KK+wcA/P5NXWGXKNOcnjdyFCLNGNLah08043sh15Cn3esh+7rtZ0b0r3+YeNp1CB8vlM2l5HgZ";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:192:"3KqhMA6sPtsZyikd8cX172oncoomHwYPX/qeZPOrW/j4uSNe34e7cM+BInWGBctHWTUT60pk083vVtbi3mAXrJPTHn6VSw/Rjg+J0rCYJTPBBF7ZYo0I/sXyfUandYGkiXyf1rImulFkJGnFnWsEHMTcqlCcx1bOYCIKtbmtv0OrLEnvOLUTlG6hnWMtvQ==";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:1260:"yUSyt8ThPbL2sgB4npElgJIEx2VQNHo9BhnVEf1ZAEVbhaEfzfN9WXmTEBiRgyvWYhfU9ofYgT8yoWDJJNrMjG+MHuFGV/XEkW3S0/mn2QBYJrVYCHs3F0N9zi3n7hWMRNeSr4mCO8rAiTAFOiWhhO6IQsQIieWz2hwfi9Rotizjvq8JZbLRAR8IiDnRFR1ZQl+bCa/hn63LPyyX9B/0PE+No4MOoMSXIOICIIogrm0zwcfUGRGgzt/Ph5o2iaX/YY13jSslEO+EyBt6/DNqMtmjQmgIAhxPqovfapamNLKk/Rs2J0w6Y++5J6kdeR+LnEAXMhD+dyZPAm5YVISxuFLRrpzXoT8I9YK8tnFeUapIUw/aPCCgP7ghIySOXp6Zj3r16AgzwUXsgSdbp+uvQUANteeRyNy7/cXa/CynFV94C1xZXKr2HaJsU4pItiHEhcVbWFoManF6k0sQfL2IIK75aSU0bOFtN+uBg+x37+onq5Q8u3xi75Udwh/cWVXC7e1MMhsJsk2g5OULGSJ0F8RpEyB2xkkB7alUE7L66R9sY0OOZBBq1/vadbyft9Qlv67CBPwUyF0ZERg8tkItD+ZSipF+Dis1k2g+lNGuTFq20Dp05zDCKwcCjEYjK6HNArQpaFgVrITS8flsxmFW75ks9/eCbrm6G8SEflQr189DxZqKKAm8yhuCS42qJzBhvFniwXYYbv0SZTn/zrzl5U/7ustKBzzr4/KjjIqCgGQbznbr64DuxfUoXhKhT8MiEK/OFxwOQhwOR/2hVdCrwfiGIM94E/pLsr75EFP5BlHQ40xkginpfZ69vO7wm8tgDXis6y/o7BW3TuuU1rBkLdiueaenK5t2QJ2IgROGrullXa/R0Dv8rypigO+4yPEeZW0fH1RTKgaw2hNLC0bDXAvHu0IVibhepCantX8bf+WJ1UR/ZQ5vi8MzTXqk+EZLKQZwMKsBDwkepWcwfifTCzJV9ZdEIdeEsvSML3NFUSkXnkGnwHcWy5cNm7jpCvgWa+FSwL4b11iuhzzfe5L6EIP7T/WahykmhVFXwWI21y48gmjDeP+tuweOJIDaFi4wdf4Q1qWMhBaJjjwkzZzrKLyk3DPx5weyCMQXEkjyUOhSRRrLXO9XrTQF/2W/p8z3bxmuuEHfNFXYe/Oa/m6LEZrkuBd/hyTrxokUrs+zbYw+5cuLNmF36WFFpYcViJveaAQin/+VDGFW9kCnaOKfSVhHAso7ZmhPm3dzD3n/Cg==";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"6XKcg9MBXctafCo/dVan2KU2S2ns+7Umya7Urk9u9bhDCJoWYrLCzfVbeyBacsuJ0O+XXehHABUD5ySL";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-06-07 16:45:00'),
(3, '176.85000', '0.00000', '176.85000', '2014-06-08 15:33:07', '0.00000', '0.00000', 'EUR', '0.00000', '176.85000', 'a:4:{s:10:"return_url";a:3:{i:0;s:192:"JycaGmrGzCEYmXLbnbM4SIPJWymGqoNJOkuNOtPpSM+2tclhRGpWt/8B2aeDVI/x9xemPVlh9EFKhNNxOy4xlMvq/2Il4IU+TiWmobl2SONJqaDlopJ+kH1gL/FIHwtTDl9UuoikPg/TeCvEXMPA60lakmbecvQC24Po5psuEAgNZ0kNseaJAEfNZGtXoJKs";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:192:"ISAHpNj8gc3d7pqIEg0jYolhFeH/7izxcardtgkRgS2iL8YGiSA/XofH/HQE5nShgi8LnjNKLnViiS3Qjb4/OPvtEvwKb8fhaWcxjNWYhM8dEd0gX8wb8cOYh0kryejGmKHHTgROB7vrcOaXeYTmiFNvWpqVcKuRQrst/CNhHiDldGfT0rq62s+9KUL4gA==";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:1896:"IeUMF98TdLyHVaCX8HlMm6YOFDSN8mF7dlYyz/85NMw6mUMsTM71ApkGuWctpfT0gKeKaY3ty/wvO/2xyDAacUrgzPWCQsLsyVOVZlSHYR6x0gRFkJUaDlx4TL7SmMUWnozmxsPRrFVXMKTYUzOABiLwU80HUSocnnlLtYgVuczC1dN3b8xYPkWZw7pAUWp40wfrTKDz8Ly4E/5r4Gdhwl2A9FuZLixvPRl+V9Rs8vRg/N3x3p13zZudDC4YKavUqREwrKRZ8RQKx5D3WfqWNgpBOBvhcS0557Ln3X0QFkuNIouo8JokRpdewkN4mtpnyaOQGbcaXqBGQFTx+PsxdaOHdaY9ZgbHPFRXGDPdu6E/UDm7fYZknf8f/7+ZLYE6s0WDYh4oCWbYxNrqU/MuMiua8Llz9leKs2xLa5K7RNg4LXUu0V07YCvEtrbh2dVNRnuy1/SSc3SXeCI3IM63GGvEWqIf32YRUsjB8e6Q1pGAMtt9zXMekyZtfP2RamAnZ6KuIaIsufrgXNc5q3yAns/swSYhXg8VSG9g1+Bbnn38HHAMbhldpdcIQ72LbTktpKFdiJZsgGnVDpFEOT8c3unpf4mtwMO569bhBf6otA5izP67Y0MnDkg6qGhBlJ0ZoWvievIwsjrc4ibfGOL5jdjPIWtJRTJAn14cG9o+JklwxTU/8ZkQ2hGTAvKiIVwpQnMmsj9MVSFK59ipPxmlejF+yOkdkK5mkDlyrACj1L1cADMC/l7JkXE712hA6kB0+F2AW6UXQpKmsH7g5Dx5869u6vPxIJwUxChR9w8UfIVyF08dv96Kt9WaHrh17SJAkcvGOj6Ng1D6qY0bTFHc16REghrUQ+yI72aEOeF6mHlV2Fg/QvaNnJp3atyf/B+4xvlTjioGtn/5557hrw36C3QuialRYqIzUt4WwmwMnWU1Bat2c+XB4kKVqxauEikqvtmZBF7HpDPL3wpkF0nkvGpV3yVXFFsauv69sF2FKshvW+ojhr0tXx2gjrNCyuQHTcrtte6eoBcA1DR7sY7tblv2TwO2cq7IZ2el4NNB+qlHeaK9eO6Y57OXkqBtmsRQp2tie803g3Aft0k9batIaeFswhTVWxY/aC5woMOG8D+fVjYpiQJNbyRp98X7SeqDn8oBDT5NhcXehqKPSgGGnAyri/e/0gH2Z8oDIaBnaPfn1KBc6ueREW7D4ijQBJWHoveUMJEY2m4XeQ3xj6qKYqEJs06yVAPuR73uGuJ54AyOQ0THVsjjS7ysGzJckOU7DytrBSajx3ZB36U98O8wRqRuLMbp+TtV1a3FMAJnjMYCWJfMFXhw0KQNoqAojvB+dkkolKcBAuU2pjrq/fSlnkeE68A1SCKYTDjIaQ1izJQss+8WuQKVjUFwvVTcSpU69zY/rkPRtvtY8/ucomR5DIYvY+Xkz/o/593Ey7y5/aKucS8aPcyXWHpMT0/VRIJSs3Z5XqBuNuosk6AFK1wJVPsR5t9wpVx2oAJ/ugQdv9n1zM3RG28OlrH1pQhkYVDMzBJ4Bv5qtuW1+HF1Ky8c1D99dvd0QmSP1yXlYRtOhdejI0CfrrWOWm5MGqIk08klqfrdE8gdQvtNYtis5UkF4tTrqPFWWIN2SlThCHUU6lTKjZiT03jRNQvpKFBvQrDY6r4QIes7/arAK6q64BFbOqDrLUCsBdeGBIfgwvq46v8gSi473LBXsCI40hHHO0A1GggDiAmlVJa86hvkjeQAIabCsmoybNoRJGZFF+RsCvHkTOZJr3NiUjKN7QH+TpHayGNtc+9h96OmD8P1ekMJJBK6Rnpn+DXmzh+n84fbn7luiyEB3QAwAzNh+JzWQnXx/BL1WiVUzJz5BX5MXEwmfhRqmgeUqaB9EWJltA==";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"2NZnR51aecC5yYX/tG6naGrrqp4T9khh9HpJqWcMNVGfidrw+YLYppDuZb7mdu9Mhlcf5L8cSG9t0QuZ";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-06-08 15:33:08'),
(4, '402.80000', '0.00000', '402.80000', '2014-06-08 15:38:19', '0.00000', '0.00000', 'EUR', '0.00000', '402.80000', 'a:4:{s:10:"return_url";a:3:{i:0;s:192:"SCihWnQkVeTvAxEf5VUD9fEJeod0igu1rRN8RibdKCE/hC2WMdHenc8qVzbnDRheb0aJJLlciuBfzqwM0P0c2Q02Q61nZlsM3IsnbCmBpZgYoHiKDS65A6bs2OAnpziWDWe8GXDotUKR1/REByxZuVirnvinicL6cuszxH87o54LNdScog5LI9i6XV32ib1r";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:192:"KnFPXWjvJ9zDMNBT7m3SuJq0b79PMv9AllC13+dDJHjNVKeT2Wokk2ImgNi0EhMRO8Ej5BzvyqZonpC0zkg8Wv5IofGuyY/b2+lgajH1Dy2QF2QpIoIqTWyKi1yJL5N//6QySYXDMSvqOV8VWe7YiGjGPpaOtBWvl4oXe5UvpmKpFP8nGIZab3t7Y8JwsA==";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:8104:"qe5o2VlX3IQFVG7UhY7xnq5b8pNY6H+Zd+stvh2LJYNzHjv7jWCkxnI0drCdiy546aj+4AQxuzOFgLLACXlnC+0Mw2GAiYTYvt2xl/w/bz4m3p5wddaeVOYaXULWHk5SRVK5XjKtvBg4CF83eL2itIhQNJyNH7khMcIHSOqN/IToLDu5602KoHPP3fw3fDGwCM6eCq97MVKtajnRjF2qEh5Rwt0z1d4R77/SBSEkj8uaYis4N8Xw0re+nvegkb8HcbeUAI31w4ZnSCAn8HQuNf+RjwoLv8QxuXoxFD6zuehJ1b5EE1TyNskf1q/JzB/vwhOpG/jYRBBiHyCXye5xXdt9iVLzhKtqd+pF52K/ekOCK95VpoGiQMz4doPwAc5Sfu8Q38lEuVFGYaagGdHvJjk8jrv8jQ6iIa3DuSal18NACLLoQEqJ+FaB5YvSAMJB6a+YWK7kowAVKdqflbBzt4iXRU1bm8K6TsVR5c1s1ZAugFSvtWCtLH27erauMKMG1ILDdmnUUnFj8OoePI2L4B4M0SGqdTkjfVSh8pu0JR6pzLcNSUnDRCMqsdOHkcVTSKkPguznwH5otrPEoMUj2+4awbKnlCp6Q5R7zpqc3q0ASRFw/SNQo+sk6yhtzLLrt4KofWJ0oEBIBATDr5jrZ3oN4Z5W09hsOwILPmP1+2Eo4rrSKFrDlU1QZfg7naAViTKbnbHMAWl8NQ24++jp2CclW7xPUhKKbR64gGeOJhqCbyQJL213C3OofBY70PQd3uJdiiiPZo2lf9C/yhRpU72pZs9eG395N4GsSpIZV/ZXysodwwYaAVeyqMgeMqOfJ3EikXax2T98C/Xa/i02tMwor1LoyBrvsKlnr0cbah+Ll2SyPhAHYgWNxfTFq5TVfBZQhEVYh2dGjGVmdPOuVJ++oevip1F+twWrBaKfebKSGbCELQAOm0if1FBnyo+DCZcIzWu4dIkwptLP5L4qZqDHYJYV0qQ8vm5cH6ztwROXT/O93kptBaky+8fgB8W3t7wPkYLUbP2/dn45LdD6Fpk7MgjCVD1OT+VkzgYq0waKcGRnIAtvihsIrkUVx3Uw/0i8Rt95dV3T1B1Lzg1/OSa8VD9GVT3Y6plhZSat7mN52HUJl7ixRjxXAEzTmYJFstm6NKtE2DVOHH1kSQ/P3yrFU3ZhCnQEzzlIStD2qxPdGQVvu6HMa1UV9D4srmQ8kkxZeveVG/Z00pXBnUE8r3iXRyu38Eitav0hCEDu74SNj2FI8Jy70XRFI/xLQK9PSX0eTXUCLaoPILovi6+yCcqIKR4NnnOpGaazFU0xNh2nx+fnwChsqaPqnfugh2kP03Bc9iuOngY+TcPfs7/ssEnUxtv/PB5MnN8//+Y21z3lJzbBRWVxttks1rhL0axPSH9f9xp6Dt62M22S7kd7fWX3Y6OIQUx3rN9LahW3Fcexyc+G4ftQeZ3WQEXnL4dP1tJlN+bdG5IMURf5e6BfpxZHBVHRzz2OPgFoCljiH6/clHKg0aA6r3UAgQdyviS2ARRT4TGcLM1EttBHfuhhPYEBu8HknwzcstEOxjTcQ7oSiX706Op7M4r3wxx7D61mttf3DysVpFEBCETGAYrepQT6akMU2r1Q74uZr+1PI1E7fMUh5MnFRfoBgGqe4CMRM9AhObmY7YAtp+ztUjG1oqxaaG1GmToE1zUjxttnes44xJ9AqXZMkf1LofYanBAfyu87yyjfya9wyFXTuNl2e/ZMfs+wgbJxHiNiYloN0fuX0QTbcYT1bIQrUVS5WwwOZGA86SqIMKE52b0uFugtG5ZjWX0gLihy3cFwgXIIMFV0QzmRi8LC/Ms8ynQs/z0FOUGAS4gNCgjZazkptCnLKT+I4M1kYosuTlPaTeJAFGxf3KOV61qAuxh+VgDJfVABXzujlVfATeDPP44/R2lTAILIMglsn4keoWWkr6yyPZGF6cDmcnbd384+3Rhhl0HFFI4MZmJ60did62qe4IAnT0KckH6z1GEUjASQic64d5BrGvxhcLVOZdBNtGqrFZ5xGLaK/f1bBtWDpfk0yDxVXZLBNe9vmC9hvHngzG127okXbw0Pdbkot/0sTmDx9voP0+wGC81xKIRsC11F7TegyiJBXnsb0PRFBFQOpUjG3rvKXe3cHJRPHMdh5Ds07nOd5ZkMGTQudL88XFNgDr/9a/pv3ED3EDcJOhVJuH28633088OrtEeCCH6kQzYRz1SgP4mrTr6u9vNT4nFwat/wfEs7SzgAmyFyOOJTvoi+DL4tpvAuedEQDwqiNhR67P1Lo4YUHzvVAqgeF6SkzRwPXiKK6Rkr7GLb3tjib1hzg1zT9ykJpRb+w2fUxb9IT+an4Fg95ltM3tZUn4ECWcfQLAYjgG2zjW3ULSKGmgkchp8DgKeBgdrDNbE63RQDq5d2X00kub4cnZ3aQ8DySu2EA66LbzO7RUC450OOrDXj735KZjJWnLA9oXSI7sj6pcoSe3JCSHuE4L9pNLDDEe9PC+C3ayJI07kiPUCHWQe4pC7QiqNUqplzSrlFhKxMV7iufauE2EVv7TjaDy9JIAPBltYCHFOvaZo6HkJAFO6th3q4m3jcGYarARt5XNtNbcrK5HRUth1WzDADd0fC7SdZZdWf6xYHqJ6lQUdhTC4UeVKd6ZrNWkKyB9pCouU1gLn4ifqSA0jSRWOFUFBgWLfCCGeeUv77D9FesuNsgoet0OVWCsYYSEH2LfBzORPOBGePBjNiyX44gUT9MmRA2K/8ghr1XK7MGsCN7N0FtbRnpdPZwF2yoyWqT1kOcLl9OWiFYfXbvCDPDYsU5H86TPVaUmG59vUur0BQO2eKzTtvO6fzE4x2wTbRbOrXER7+un9kdYZfdP9+EcMBfT3mK8wBdLlgAWrbsii2G6wSiZACt+9/i0L8uSbHYbKdnLz68Ogf6XXh0L0EZ6+pgK9jpFkxhL+O06GvdlyRO3+qAx4z5jY0r3CqKM7tyRxm9VFuCeZKo9GCcEh0K1x86EP367UuSfeG9AbsS9g15Jdbbku9vVo2nYTaq27TUmsChOAODjkVTsmemEn/JY1P0cdptGfSe5a/bwtM4F0x9X0nDcZ2wGlIoCKPcAFfV0iOEQ7joxOmfexY4gG9sDyzGHU6smkHlPZRWKssUmqWF3nYoNtiaCFRYqmnuGtabgerWi9M95uKqbXWybnEpNY7Qs390hr47kXtQ3CtMmC/8k+8j/d+AIuFq8w7tiPs5rOyJKVehnQgTyw97s8zd1BUORTkg2Csj+S3IQb+DLZSwW8otHx9WBKuyoXnEMBrQe4k8d/trHv5shZ/48wO68XGpC7cP7sgJnHiawEyYs6p1HCcXXhnMNO4qF2WEX7R02WctRR7HEyeFE8LcvzkFtcMVUfuOqzPPsR5vrmhXOD0MM04ftyRxsAV7/jKZ4ZYByPcD/G5yWDHjUMr0j4XEkSBzjCM2k7WTzstcb2JLYMJEaPbgAPHrrJvTz8x2e3h06N3ZS0FZCm9iF1u/oAzgs9VgFxuBR+B4qcvQ/NABXs+he1OZyq+T9NKidLkFizXgzhEynxXotdhbJuuU0hT3srxcNw4grNpjLrMEyESxMZ8bxdtGHPok6p7RlmZAbWfsdvT2pHomhbegYnQj5a2Xn5/CqiS32TQnwRZctpSNzUmriG36YYsUhYR2TGsgUdNmxaiAkGu/haqOMEcg+PzOlwTmH9XB6KpovaaWW6+Nt8LS8wMX/szG7pTAHnw92fWRTWqY9QMf/5ykpkQZkfcSE/3JDjzGV7DQbuIVnhSWc6up2KKTVsF4IIbFN2QUsA6REEAlIKZNYfics5vYKMPqD2GLeTj++Kdm0j+6Ki8x3UcliwABr1Gn86CLMJxP+mjJDlG4tb4D7T80V7BzJQahy7q95MZnAXMJGA3jSag3EHLdg+elpikhu5mFq4Qxc1DqiCAjpozCzb8KLAqpxhO3dg7r8wjZakExE/JLjqkdmvTXefQar8we9Z3F2QPsWGaqEAOl19itFdwHIrHw1Ppj3mejf1i43njRdCBEpkqUrS3vTeKKhiOj3c0lMbQPybRSY4OJWrXxO8345N8MuqJ+eWeI8mrWv9dVSLkHtD4Y2uBwqw9sLJxitAKMOhY0q1uKya/cXjEu5++CQgKQUpo4KiAxS0lNl0YVobjDZiETh5ivB6YvY+E6IrSIZQJN9MgFNCOcQkgM1yVse/pP3O4Iuj2PryjR9ry6zVk5wzqXsFadpQgVRWSBMAbxaAu7TVRUdo4edUB1txFQiaOytuz1fQyoFsFo0VISDH6pN0kc7EjXvsnyiDAoi7F7N9YMS0n4OOyGB1gB4wY9Iy9S0wPWrbN16Ok5dzbPh4gDiiGa9rn/qn/1k7sfGjNpn2WIRCrS7GTd//SdHN2IlPDsLC16ibDHIj7hXaVhUpmi9tf08BSM3kGqkr46bCrn3t8Y0gZrWfjl6i7hzVtfyhYXK6dKyE+hfHF2Ic+3fnudGTXwYJPbr77fAa77rxwJZpdKTL/uzv5TG9HNtesuzPfYU4IJY1zfBT88sGmKU1BGz/arqtGCXpM6V7svq6rIATk0fn7p7L6IqNSBJHiWD79w+IPxye/+KZFidlnBel40Vz69JLEr3g7UO22cDLCZYM3monHp8EAtc2wMnGEKDqimZPgqYqOighOkDwnrS6OBdUJSAHEUwfuPgM2gs395M+SwWQyFo8GK2lMq6GCJirKBnREKCTWvsc83fvomX7l3ZB2pygaHiq9KtzB+hyiouCsmA7u4o5sgvnaw5TvurI5BnNHApvIS6eOwbKiAL83JB29mdXvn28N0PHsWklEN6BympZDZ+ZKl0YTkrKL8MCR3urkRZjuu3z9jHCg225UQQqnaN5lKcvEUAb/vfzepdAojrGTLcq0Z6qSmQnRyPvyHIslIOl/5J4OSobqBeqFIY/37hWXUTRWcF5jpw0rUonFMpTh07krSY4uBngWCVuK0prebdp3NFahQEp1403815H0Il0hj3nHrxT1EYmdXUoDFiuiTW97VUiW8g0QqXCUr8GTS4t2/rC/VaePXQTRV/vGCf9FhaJRLqU2FDanwUyhZ8H/RVvjN7uja9a/Y+onOF7Dwnpq1TpR2fbwZ1e7Kx8b43vuBM1xwsaGViK+s7juWIM6TZhdHJTDMr9NhJaxElnrkImW8yNOLKdLP9/npf9IsWeL2HghGpJOEdPF+PsYTSALYhirbfI/Ovx6pC9bK11qKzYVE0rmzTlcszNsOe1ciognyBdIQC+AQEAs4n4UtrAUV/5waf7ehhr9fkuZRDSlv/58P4adoZl/fE9knTgPEWVVTy4JI+Q5OVNt3gxix2vOujyntTeCFXh4bjt0sizNs3w33p9s25rPdDq6ALU7wyQRidEyVTz33lFisOuSiaTcGkSYbh8SrsrMlzXDfAPCfXrmPJK1eNhALNM94q1cWTLMxHOlEF9zBVaSThR1tIaxUBJvxjMjq38Paalx625Pcz0MDZJOIFcmd84tzLxly5XZkd6NE+1YYjouVeVFWyvHDnDRmQYXu6NtBLRLfuXC0qgQcKXIDlOePWVLTQwQyLKDe60NQUpNKb/Lutj66Gl3YKQzsHO/6+8utQg14bbPb0U+NHIdDGwXYzowcl5NAx2v3jwP0o0PodGNFgTCAg46zAqJYjHThOIrwg6NUKr6HmFSkoQOUd/8WKCeDOTe0/5XJHQua0nA9X5YBpiKv/FAFPI4zt1O4jmmi91sdrwO7lwwbFYzeXjLjyjr1imheDG9SbbBaIw0RA4DOxrgvdM6XKu96+ASUNXlJRdvKUtpGX+NC8ec+QmVAKFZWnULjkyiMcCJGZAO47ekACNGEjKd9d3PmDF7wEREIqqmtklVG8dBK2nXW2P+QiAZ/S7C48fEF30hFdgP9YTNyTvUNxHnrOpYpP/vjkSFL7Jgu3T2mdT0jkP8E5xH64p+gC5SagEAa7Q1j96N0hh4JGrzBF0BPQdJTWUKteDmrIK6PTm3oBbg6JudZ7qIS7mM0JxlDid1vq4A3gBHNjsfTjrU+ExTm1krw1RvZz4GEKPvHIXoInUuLPxG6Cf8gCZh4hdj16fE9+GrNkL/zAWWwo4aakfv7X+gCPGQIN5sEJ6+eaoBFp2Z2AGna9Yvf2f+Mxoxtre0xRMakgYbBQDVHXl/2+jwL9NhuLjwmWMAlLCEaj3v+eeCEEi5kZlSr6tUCAh9spq7Hcx8KiMKIZ+1jwUaz8j7KE0iiYRBXUV3qXtqt+bXM6orXocZoBCeGiQy+BtEy4QS5sWJzogXB9kq0uluSLcTT1PC5abAnnMQ51/WTyVpu0Z9GHRZqg4TJQkWRCDJwlvECuD/YbC/+Fr9XzDH/psiBt7DvT7KpPSDEnKB3vpH3i0AFvMlTyP25mZTcYvXV4nkXkuNtcUznezuQf4k+omDAGcYwwFG87AEIh47OuPRjC+950BKlOh/+tdYWjQY9jZH881bj5krO8TvrowS7l/E1dpttaJD6HkCf0hSP3q2KXK3kE5B7zTlrjxTddlpRo5widqbXMy83IawaOxsYLv9MGWXp9NlA3nakIGZ+npeN8ySJ5ed49JM6P51xuiG4MET06rwuyn+lf+z+xAWl6Wx6ie5YQEN1kavtHwLWx3ExejtYLNC2M5IpiQpGI9sLF11LVaqxC2rIiulZceBzFumKI3CVmpTlUEmNx2yYKHAuV4bStvCVpIGdlBtznszGztp85y4YX9i3PoHOd5RAclVV9p2tTlHgN1U0/hBMGR+XCIO97XCU8SKOU0cecSLsGBJvJrZKwv0w9aZmbp/Z2LZ2E1WCKtCvObitR2vbwisLPUp6ZYjL0//J+gAA2gncoCgqMgD2cILn8LyoeQ/k3Ok0EJt8hKvjwjTlTmXhDZyPSX8XCYlvnMaIHs3vHSUCfXqfofmmBwJHXeuZE/K97uTP1wgyhUGa4It8mxdVP3NqaBu7gWlLzlZmxVJHGj+Ne8gMSG18dVzjh3sKY4soRR7BizknsLEm87kOdm27K1S1OnEEctw8B10od8eV0FYz6VY0zw63gzNsyhBjnAj7BByGPX5BKkTa0A6z/1ojPS1Z562GU1j1LumuhJazj1SEJT2mtaYvWAu8sVji8ISPwqdaUGeEv4CoVkDK1FUDgRvytK935lQuq0OVzJH2iNR0dDfQaMuynBm1orduYzPt6uI8fG4fqv5DNx6Cxx4VVDQo6ODo7WWZTqXVCMNRNOQOXS8TTl+96vlVK0loKiJ5xbr+K6jcJw/mKYGvfsspHHjy5uA+fGHaufKF5tsqJA7GMTwZGJSHoJFYgKVhukwNE98AwqUy8sL42gDnRe2WV/OiOpnh82X84z5p9kKUkhrWhDoOVFEiPGGrUf5EitmSkrfuT3dLc7ATwRT+w3xlmam3+IN3tQE81YJoAB0FRYmq9IJbmibIx/osPLa+WAAxv6s2tavzHj8vqggGUjyBP/Fc35Z8nhrkKRiMhvx8rKZ+wtDLocMBkrqXTo50Ah27TGQYSp3PZthi3wLObK8kQyqceKmFUlnhA/BTElCGhwlsZF8/85n68JbSkyiLlZgDMS5wAEG1Nd2tiZBbf9r+B8q4SfWfm1bbO2sh06ltvX6QOUnLtY+5Wt3zGzVlQPXb+ojxup8g6g6jdlaLTMa8xK7wP7OPAb/JF8UdlABIxt3Gond82wyX7du7DBEE0IQcEM1d0+BWruhE+OPeUJ2WL4RuX3X8a3Gv7rBnxFYHqXfRuxpa/tx2kRviEH32W6+6al7BcgE6G65xTZjB8vgpLlM3mMZ/T3QSuqPrbpDUMIn1HWPFVtSwN/Pf6h2lchB/Ws2xuydaiNyKY75AGsxYyH4vFmlNiTyF8K4rLybUDlEPfrSVxEaKl2ON+7s8y5RtrAlsoBGCbQbrQz3E4sjeXQRQheKsuEsGCHM/4X3hYph3IkPy2AdLF7tet0FeHhVeAjo2Oh5T4XRJB7raAqVVi71Pyc6KLgkRb/hz3OmsUNrMv6YEXKhWWqPf4fQYUZdJh1nKqiD/25c0qZC/R66+zrDBehbBo9/Jzhj9Eez7reQWMSnEDv8Lg==";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"qrgzc9qBhO38iJwOdHP6bhJ5TDBF13uaOiMaziX8ra23Wcz5NQXqHjWSetQrPMYdGVWOB5WOQEVgQfaJ";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-06-08 15:38:21'),
(5, '59.95000', '0.00000', '59.95000', '2014-06-27 00:02:46', '0.00000', '0.00000', 'EUR', '0.00000', '59.95000', 'a:4:{s:10:"return_url";a:3:{i:0;s:196:"hUkgI9ermZpuA1hAXPUEV8cVNkxQHzNGil11E4cmwzW5nPQoVjkpm3QRDDMr1CXRWjtcUjA5cAJd+OVb9mRVJkHjwtIK84eBVVNWeZbTqJe8HTJlDJKiHoAxwnS+3UygeAn5MzvNt9W+voalsYT09xSoNLTfM5T/Xyz8HgANzd6HrfY77VW/bYqQJUhIhkR0DQnx";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:196:"uW3tHffVX1/k27sf/mwLJdc3iiAZsNv2IGBW30GqxxRB+Whc8DKDdFD6T+3RuVveqheb2v3tXjasQgkpOJCvmg6oPZAJg27KKdmUUZwK398RYRuwLbQO9qyy3gJYql4178v/XDpvJc/hRN0jCwdnpY+cKnspi31/R6r6waluYh+abmHTFxs1oO9Dbaw3L2tBjg==";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:956:"VV0tF17zAcfHCUN9ElnoRU63WvBeVNYDbBZCehHRkFE6N25jO0xVaFZN6K5XhhLQgSZGhk5BXrkTmaPTTwoYVzLj6QRX8QEkqurwMhpm740ryzDzP88CRhP0fAFiO9ao7gITwLFfpIw9q0EkwM9KEeO+DbqBTPv2hjnSV/WNrhNYA78W3lWkDriQKqiDNnn/AsVkh8/QAOk4D68WFiuHlg+nhyseJn7M9RHKVu9cNejt8c5KiTpblIJ7bahJvGPQF4YlLv6K/0+g248jazUnVMLvhos4CHhtrSOSCMSZ9wygAaL3XiPfObqNdSoQAxdHhERVtA00G+r+Xbwu+WaeedjA3D/GCpV71w8lfwOMNeYpvUzTzGzcwS6D+qdcNc0EKL45hoNjMi7aek0J8qC/j0HeNtgeK4ptCWSn0BYVbj4taZzySoT2ucucr6eRHc3GlT1VNxFgqvTb4bjvU6xkqyDxagqL6dFdTYueP+axkAtatj5shRqiSpeK45szPcaRK0cMOti5vHEGbAs/JrLUutXPiFoLBnoyS4KpORXyKjb6LZu+yurNtjvUtJbzxN5jxpE6H+Ouzutl4OUJjy318tfAI0uezvnJZk/rb5d2Rlt863JB9LnnJS8NuTDIqC7zhk6UwAlVZZy25Xlz+AXotBoFjkuSMOU0mpC1em43gtFGRL7l+7Z1k/qtVaJTBYW2PCHOEr57NTThWnx7VO66qfpxHlUYtNqPhBIWLhdzzlLn8QfHsiuojmf66VQ/vPOOS5CpcAMIvZ3m9tJbrHhGUqEhBiSOFHGRuT7AUq4+L2Yk+1soc7n5vCA0d9gQwakI1Do3ljlf6pqY4b2cb/BC/5aLGmFHDxccP0EFzIB63XwquJJpGmx2iVb4hXakH3+4zHt03NWhc1DYbhNRboyklYfb3d0mlUKAPwQPQwfwn5ZnRA/Piiouah/6nBU=";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"a1gG6E0xXMh+tTuMSc9A/wvg5SWzwtboTZdO7dVwD6SNvfZlhjF9yIO+m4TkRI/oJLiFfol04lwX89FM";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-06-27 00:02:49'),
(6, '59.95000', '0.00000', '59.95000', '2014-06-27 00:03:49', '0.00000', '0.00000', 'EUR', '0.00000', '59.95000', 'a:4:{s:10:"return_url";a:3:{i:0;s:196:"nzlKEfyAyvdWwGs/grFvDBFS9eXCKApVaOKaQoAAkRecYkPQI9vaC/W+5KUOA3vs6WVRRwKIOY4BJeAqhuF1zbuoKRw0Q9GMPKtKkYf4vL+Kxw9/dejV3eN8II0V2mlFxwJkPAUiuVLbRg6fUQqYSR69mcaJ+0yRsutvdBdl8v5tNxf9g/cyq5aDuj4a4EqWNwug";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:196:"+Qq4WNHDnu1cTdLF5I9Jd5R35wPLH9k6eB+dhN2SstI7M5WPU5HmG2YaHOMU91FDbRwUxlgd9EUJn9Tw6927W5Un+oX8GcS8YRenFlQcbS7u/bwxsdnT8pnkqFETK+JOs/bxTs8CDBpvKdhjb3Xk7x4wF4O5MBSyAB1IAtsXAb/zcBFFvC44/oq3sJ5yenXx0Q==";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:956:"I9LtVK8FGZxypoHXQA4MsH7vDPj+m4dAf1Iu1XVZ/bkQWtjbtRxpMqL7lRgqSsM0cM7B5EcSDvY+xSxJujB/sNrG0DTS9YB11EupBKYFsBb9uIpMiucoFdZrforOesjwVkKU39jFj+x0qtaSMZBFs02KxhkRIqTKBiUzs3w0XNJk5bBK1WDPNuBdDTE3z6jG43mpCVath6YUi9kYaptiAnZpKy9Xo7lpTZWLgji1SOaHgGETZZBU6158vQXLfPLF4JBcmr/Bl6YYEtmEr7JLI+jpAmTkUhHCCZjwOZwUYTADKOUFFTxcraQZbEkio9JdlM0NnqXSXhIUaNAn7Sb73JVXptV34s6wW46O9Nq2Kn+VLJXW+o9SazpYpFcNdzim6/DMyvQjoZ3FUHuzE3cTCiLDzZPEf7R7XrWave+MLUpXZ9o+zDakjUPWwn/p1UfWPadmC1sXEBl4vEtU98lL4XMHWcaeHdoZGL2UmRWy6QyFT4N+UvoH3+F7P0O7brhUAJ72uCCvyELJIZ9tgeq9Yl+4hTjnvlEXU7KBVyWgGYoUZk6TXgYLGdQROJkouOcRKXI2q40fjcn1dF8CVCfRfC9BIo5fc5KoxR3B0ohtH0roQPLRa7MpSDEXx1tRq6xWK+zOhu8b0okqhTY2Pbaypqsb3chGMjYdg9NbLmh9Y+t2t0oRt5zvhOgMZzfBv9/G+bgAAxmq928uYTLebCg9iChkglg5MC+aK7hJW2IrhR5CS3qRt68OjUdCiV4lOtJQ2VFz+d7jS9YXhNjv+C1O49rS0agirn4Gjkp8fVKBfj+dpLxz+2mTy+jIEXZhmghacV4aIa4tBEr0olb+fHVmqcVonMEx7WeNOVUM54MCDreNSeMalWSP8Ij6sZYJyVpxqCCBKD80nEFT1zZY6YAdDhCL5GX4+gmaZZsjVkV9Nh1QKE11oaQvVRYnIPk=";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"Mq/jWT/QfDbx7Z2EqFrTxlRx8ZWVMSJsDeocouyezaCmndlJDmt3C/MRQizZpA8ONJUxp5f0CXpKsnVF";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-06-27 00:03:51'),
(7, '52.45000', '0.00000', '52.45000', '2014-07-08 17:10:45', '0.00000', '0.00000', 'EUR', '0.00000', '52.45000', 'a:4:{s:10:"return_url";a:3:{i:0;s:196:"FaGnGWfRk1Hx6P+13nsBKdwy96OKD08BB6uQ2/+bEWMXlETgrJZWTpXJbGgbvKygRmb6T9GkYhZidUD2dSRhMtbU0Z1xqWyLrmMs/NHPIRY6PBpdRCiUUNS+ENhDXbEnmT6NuDBoYIdsvwuxofymy5+LRjb/+L5sMfiiNtHO4b91ACjGcnx1ZXjgcFTxkMWBf0Eq";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:196:"TQbvsPjiWbxClwIX8OiVrI5TZw+X0yWlwR0qQMkKbR8cZ4ucxFqOWNPri12VahF2xG4hMVBYpvomva144EQE+dxdaibVfooTmurercTICVYtcngK4RwN6gEXjlMUZFmcEZbHN0q/Jyl2EwVE0K2vhDXG/eKx1QfWjOqGF9cZNPONxo2MUkhgsgPKytSFoGnl8w==";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:724:"3DnSYLqRNCIfji7QrRg6M8h3pLuBZq8/ugDX7ytUk3Cf50kN9PU6jvnBA/zxq2cnDdTIoOrnBky8Qx2wP2IeKZQZ91VAkc1gDzvHxEbVR72/kFEFkSOyR0W8SC4mTXI/RkBxnoQARjzeDaSeZQACpoKm3TzaxULB6VqnR2IJdxj29GcL8cUQTsLtnrNIsDxvG4T55cEdIf9rxgn61O3S1HdyV7RpDAht9XOctRnnECgZ7uTKxRicqUQ9g/kE79I0v7clMvqgmfs/lbECUbJcAZ8ZnGCbOv7HnZio+MMkC6FZjBT6ZdQCyMN3FL8nBbxX+qR/3ZZ1q5h/QuxPV/RE62KDIsOsD9aVYZuRqR9cEx3C/bvdEJwzv8fqCNZRQwmor1eQKKVMWLFIWGNnFGV4JgaR2oDlt/qcdSfDIHNnXs6Vo0RXXawyaZ3wOdxSZwBmWfw70g7k9h2zhzkxQS9QXUkBRzZg415v9Aih3aotoTouBZIMfERPeDegnhT8HivHc0HLNZ1fTDDcL7J7Hdy2vxUCV94Xv2VhPTufAcy75DCFPj+lanRZeKnVhuyXy/yyvRg8gSz9PIBM0TOyVXCxeapofWe7yv+J03xnwthXligb7bchTKUrEM80IJNMJ9KOlTQg8sX5aqSme3SAlbyYtIZR4xKEHCKLZz4fnd6xuVlcUcS4ZSFtqzUi1KXsyXjxccJTcI4Lv8HWj4rM7L4=";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"W366UeFZHcBnVV3oWj+rpvxLBqNRWmAmFoKt6SKlrL5LZ6ZUowqjaCLsDs3Jw9fjf/ceQkxRGEDjywHX";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-07-08 17:10:47'),
(8, '44.95000', '0.00000', '44.95000', '2014-07-10 18:26:54', '0.00000', '0.00000', 'EUR', '0.00000', '44.95000', 'a:4:{s:10:"return_url";a:3:{i:0;s:196:"hHF4BOC2Pd+lWQiVZX1ohzbARyxJUki+706FTWsMIop4hfM5DfQEpECqxTbrkQCSRZTU+6RW/GRmRjqbcD+GPinEuRUyaJxeZy4v/d8Y6aTJOMRUUqLP8LbyiplGQoBMZH7QKywWVYO1gyV//PuokCpiAM/BGFj3R3GWg6vhum/jWEQnybcmzB3uK/6ljHpB/sCw";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:196:"JTEFZO5/jyzXHOQVbznaD53ktqmCJKjhGcU7mlmhCwyGhZEzMn3iUny99U0YyGReMsPYSOlVvoGu/8nob0Cudgjda4QW59PGaqa5WsHR62kse5U6KHxahnKLSHvun33f0ZNzjv8kJXCJt08VQYEv9BGQ0oYX0cEVgUidh7+daE9PpzotNlBRJIYNrZONozozWA==";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:476:"Z+Akibf0Bc+aFpnbSWMvePbsslK1j0vVk3Zj/8KOIfgUfWHnpsN1pTACfczjpiMMVFXanaOo5zjtOHVoByQVVxC9iDFq7/iQIH1aNY1j1kfyIpmdv5njKw6cWnMuz5U0Fla7iBixvVejP+/0OK2+4mRxLDf8xPuFkbjvelS/tek5uinY5stkJROvJ8XkhjPOYQ+/X8ZBXtdt+yxK9lA2WsFr+OU2w+PS2MKBQ3LxKH8ZXN9r6Sg/56WtdQzC4c3GNqyOAdNHG7P2SkgxQZAkDjVStY/4HT1EKdW/5PBhFOK/OTxlC3fP0Umq69BYqPCU+Gx05Ck3CRX/tK2uSVTMqUUCvMizt7b3bQE53Wja30PBpzcrCLOEKhtywWZx6yflMDCOyrycK3l8tP9pFlN3dpGb8n1kvvSl5uc2+gE+JOL7+dN7dl/3dZNBImIEp5A/Xvh+/1qbOt+nz30GiUUFMQ2v0O0H";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"KgWurpuKiJZBqB+TWR9HH59yJO7m4WicG0u8O1H3BcnjcL5ire7gQsKARYezl3S+8xVzTftEuddHZA43";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-07-10 18:26:57'),
(9, '0.00000', '0.00000', '0.00000', '2014-07-10 23:13:28', '0.00000', '0.00000', 'EUR', '0.00000', '0.00000', 'a:3:{s:10:"return_url";a:3:{i:0;s:196:"0XwPVi9Af3kYS/4vHd14VwS5x5KBNW+mOY3E4DClfQ4yaHGUVmOnMoaL/NpEj9z7qI7lkaYKg3BgYnKrXOOeyDUt5EIkFmQHOpAPQ1QpxB2Y/a9iOJNJznFkngMvJohvAxL7nG3XHqf/LVz8ZJpcXUSC10VT1wUXddtJBf2f1IU+vqbG8wx76g6l1Fxt/pKzSXIt";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:196:"c5tCBsesqqYGbjUpqstUdhYNvUsyXa3zwnrksj1/0ETKky2E9j0ln1yV1kkhAkUsfvBPtCm5cMHdSPj3ps+RKft05DkCvsrA0MP6si3joXF4AwjSReiE22Bdu3OeTEWbJp55UY0V4cBQ/D/4I+7Om48+V611ao/WmtOd8vlYEIel7BwUV5G0ksQqQE2SJePJDw==";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:372:"2aizEgB/ymS6s7JTyHwlIpgzkFWlMxRwuH0DULobfVz/mz+G3UompKdjG5442lc6ZENbI9JJWGJmPNy3RoorTr+Z32dJlTdREJm+H3SUFXhhKVWYGtvfxTzl9UBHNS5ldpdhxWb6U7rELqeb2srLwKG9/frn5Ye4DfODhwFnT7LJBAsh1QmG/jxxBHTZuQtdINois3NRXq3BNKiSOioW0P/GQPoWYuCL79zI7SwIi/7o6fSYzbuzwQr7vedJQ9KQThXMbG8UwPx5qCoPWl7VSgtn7pLSXOFZe+VtI61EjU/EmyQ09879W2m5QFf8FPXIIs9rWeC2/c9JJjySS0T9l6b1fT/yHtk6484X2BWD6PYYf0zREnoT";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-07-10 23:13:30'),
(10, '7.50000', '0.00000', '7.50000', '2014-07-10 23:13:55', '0.00000', '0.00000', 'EUR', '0.00000', '7.50000', 'a:4:{s:10:"return_url";a:3:{i:0;s:196:"veQ2a/iUucWJJDZ/OKvWSFCVPnbclrWUCUCsapKKbfPhZSbdedvbFoC2PiBNDI4/7IFuHe1Wve/EVK7VINZ5p1qjHJFFDRjl/PlqRJ6jbM2WRn2v07mke4QFN+sE7FVWS9pyWWogFqSF6MRejDc4YO2MH7xjjcyTgp2OUD/Ii7dqvPPzLRHRnXU0PhbgNOlM/9Gp";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:196:"zVn4ADzyXxOiLgs1N3BHA1ipGrtAkBQXEUcnQ4i3mK9X66Ly5lwZSo7tkscnQWNREuQmLKlFkNFP2vG7UtaQpuLrRlYD3+iCpaaiOXY0ptMP6TLTERJ9kU7XZSXeO6HFbe0iRkWQBuWGdSPxGB9Su4cIOsosVQe1FlAr/4nrZN9uGayzjjO1spctDB2OSz5CEQ==";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:608:"3og6QKu6kfvjvmQQ244XKc0G6YmjzrcsYTL5SgTuBC2jPgCIywymmNLGvdmnbdvdHt7alkzx7fr10aebX112lprtCwQEExawlJ5A/VNQepRmF6+A3+2rpeBOKztmH1jtFrQiDtp1IHRv2wyxtGcp7Nofp12th662O0vL5O4DxXgpVOsKwqT4Zu5RCaywJ5ngXxBLNi4YeczY8tHIo3ektzY+QMVHktcYUrTJLMfIRMBzKxrw5ldtB04XuWwzaPisPDS7wVGOaHSeyR25BPEYCD6J0aR3JM0Rzf2kX+dB3lNOKPQlgayroosMkKBaEv4U7RAMuKlTTE7on19e8NeUJ7ym1ni1a0bZymDVLZFHYT2XPDXqO5fkA0FCFnkiwroPsQap2yik6DiQM8fAqYGf4JqRvopLXOmNLc4nXav39YRAdTdEgGuX+wsHNJ0SWUqxfTeOd3+YG5R0StJCXmSlZoqtKInkfBldo9LRsp4I2DHcWXdsqKgG+2NuDvcqK5W1/irRSkVSaRtkwExv6cu93SaCZetSMcOMDL2L2U8jdLZMd+KswSzCnDvRbfl+B+Hweiteb3Mp5yhcEOPq5c8F/YRRmmUbE+Qy";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"Mb3Wz4osP2pr0s+cwA+EMXmlJLeeoNgDyA6lrT4qEGjPLlGly9DoCjHKp/PyMXuyvIS9YEcf0l3IQ9xM";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-07-10 23:13:57'),
(11, '0.00000', '0.00000', '0.00000', '2014-07-23 16:44:51', '0.00000', '0.00000', 'EUR', '0.00000', '0.00000', 'a:3:{s:10:"return_url";a:3:{i:0;s:204:"j4OgSW0BblUvc1U1RejYax9af7S2Qk+YQcMIKyUnBfagGOFcVn/w5E8+NESeDqESN13sa3b/ygGI7sscqk97tzN4eRZ0ykBYyMkc7kAELGnJKbBQfZh0E5yi8gr65/d1/M38KFWcUVes6TxEPo3dJHPLtFI+IeKtJvZNH7wXUU7n6woNxnuE4LfpbOhd6+dsIKZ916le6A==";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:200:"/DJgXaakPhZ2Cc8nIxAIuXjP6TGsAB5pKCWC/9pDxs/o71yaIpjWpzPxhoUgDky/951PT+HxU76DuFewVVMCS8r5t22V2lRaKBEI61e+lmDLeTBFCkrBgYUEIv06Z7nvQULiSyjEFc9iIHDHnsqJzaty9ELp94EWE9ilUE/1bK1Jj73oqBQWUCsjndCUOYXbfGdOHxA=";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:136:"J+N25O8DfJC5d9dAxM1++1DYxhKpMJraQeS1KI3FKKsHlLXSZT1xKNjKms0uLzn8VqmO6axElPJWCnCsQC9nkO0QHnWYH8D+Y14r7Oo7bYY0zVPGF0EbbNKe4OZznq3BlbyYEqC4";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-07-23 16:44:53'),
(12, '0.00000', '0.00000', '0.00000', '2014-07-23 16:44:54', '0.00000', '0.00000', 'USD', '0.00000', '0.00000', 'a:3:{s:10:"return_url";a:3:{i:0;s:204:"ISSJQo1Gxvf69GMCtq/lDOSXi3CAOnc/m0nJxQeHtzjQzIgCW8l0L1Su2ovNScTmRuX5IAqNSVlkr2ZQH6p7kmIveE+6j8mBQuMC28ShM/VUoQyNLYgq9PsQktboaVoI58PMq5J1L4ZNCTbgVWDR3oNrOfo2l8mxz+cAoij55ZMgtm1mHmbeBq7Sbss5j2/9c/Dts0vDsw==";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:200:"YuhDttiwrf5b/eocDYk6cgG4LFkcRPrtkRRD3yzLW5VHTcbXQ+Jw186PUp1MbNW5IEj1onf+TWZz5oEBAkvJLki4yP4FPEiONFpR6pkMIKVg45/0ocH9LbCxDVx7nK6Ooj3DTX0UsVmKL/Iz1clH+n+DjlzfpdjBj4LZXQCkXovbTbfIUSn6sT0wpKiP5Lh1VvlcYFQ=";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:372:"eOM9Bigw1IsJigJK70DRAkTCaWaQm0pSblwWSVpluHbxKsaYKU80r26WuTFKvcZb/qODoT6dj96m2ON3xW9f4CWg3bVGpmbOdsc+a+h3OWG7r6nbh1+2HreD9eQclHDmJQy19eQ0wJrCweCCeRVErP8P0e9Uh9KAwjZFJujQK5ZiHq+4fO0/fS+0v/eCBdQlBavbT6jWoSt788OgaPFu6OcY6do0jS9xh4DxmT6ltaheKaorEs31hfuk98biDdx6lsmak84vb+0kLHwr44CU6Xxno3gk4Kh+5BqEhfVGrZzDpUYKfRF0L/GXzqFFaVcBhFL31uxhp1henjaj34/YrGyttVDcXWrbQT8vM9kZd9jAPfqENt24";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-07-23 16:45:07'),
(13, '0.00000', '0.00000', '0.00000', '2014-07-23 16:45:10', '0.00000', '0.00000', 'USD', '0.00000', '0.00000', 'a:3:{s:10:"return_url";a:3:{i:0;s:204:"l2vccdxIde0y6482PRPcjpFYvHo6DprdskjM+DH+JRpDpF3IXMPAtMuFa/usGpQr62Z/ISZd2GD+nfTrUMF0DOKKNYcCvZQgZKADo+vMyduxelOJY6rH9+zgA/x47aXGdK4sUrhTp/lpaVSgfslEBPfdoBzJ+YJ/hNvrtoAZh2nodmDt55HUqkH20+NxJegx0Y1Qg1ZYIQ==";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:200:"Q5Krqo08X9nQJNu0o19C5XBIWDqmQPmgWs1rJqLUzhSW3k98K3IMOkP26wdcdm17bp10eQ3ESUutw+Zfo9TrhwXPyCy7eXiskT/VoHFE1eX9Gyio81pSsxArEI7DTbc4LBDgVDACkNGl7/B/n5AdZtAVMa71VFVZsni9cNGn+WbKSjE9Y60BvlMOILQ8ycrWL+ZN6/Y=";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:136:"RXHFPi1dUdaDvK77HK84GMVUdKalx2lSPbKeHSBSYXtD6RM3urI0Zut4n6dcjcE0xZ2i5RdrK4Ayqc/6YaJPU6a5ryrwxLv/e1HcaU8PIHyqqZhk7NsffBc74A7+fYe+519uTLJ6";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-07-23 16:45:14'),
(14, '89.95000', '0.00000', '89.95000', '2014-08-04 20:32:57', '0.00000', '0.00000', 'EUR', '0.00000', '89.95000', 'a:4:{s:10:"return_url";a:3:{i:0;s:196:"Cpj0c+xhQ94eHW3jFANmHpvFQTURYVPwHJxC0aaej5SUIoNNgqqMQLuLcksvP1PI4msMUye31HjkJvsOA7ZluyhuFIPl2D1McVsLcP1FdSDvosOwGHxdw8zlhWFFqhwsy3JIZNshCDc4p9Bi5fSgoVzVdNwIS/9F4EMu4IzJ79tABW4mhkshmCXaA3Tqqk9a4gqe";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:196:"8b+uTe/FgxcdvwFQXwwXdUow4SgNSc+z3Rvc313JW8lEJOrLpmpX1m0TgKNHHIfoquwUTQ+cq5DdyWfJnfzoLpk8EM3fnJ7+sMIp+dbdKGmEEbwF8hZ7A1aEsMVjI011CcFn/3N1WHSaO8PzzCeIVdeqMZB0sMKlJ3XCAab57tRw28bpxI9S9LJde0GBI7C/AA==";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:1992:"fQkYRSuwuVH3HKGB09MwNCMJ7b6f7PBcAGFIJDysH5gcDM6uzfzVRB2tB7rW82mxRAGBWmIvmvKkStKu6SsbqOY7h6O1879cKpAcLnTHcG7QhbmUATRUay/LDHrzKFQ0AEgEDjKmyYAuRcn46n9fE/MdxrAru4b+jTjvFi7yU+DRXuveOpPsRKTw14dZ3PeAlPAP7i+6coEDMdAOmW0mHSjI54cEJhukjWreWvyEqO3WcHWx9UujqZ14oiC5Ms3Gzep3l/BO3xoR2mYW1eLxYk7uq4QY27XmCnONk+SclMNoI39Jka+vPhV0FN524lSEiIwlaMTVTUBdNTrjpvx3+4A+Z+gZuSEie5RXL3cVPHt9tUQKAQWei26L9arnLwkG5QXmYD/YQ8qauEgmqqi6gOPaBJcEwkYCWTbiPYGEIHZjp//M1oJPNJ7gsStEgEnLe0l/gDTsjfnlBUm4X4H2xyYeUZdL79zdA8UfhbBgMT/NiA34QeTxefeQ2P5Sj8b89n7eJFep6eBbF9pFn0/8AwTtP9+MgJoeJPrHgSW9Y+rwSxiZ1XBghU4zgbG4Rzk/Ic7JAjL+x1NEovhZvAMkKWW3BmPh1LoSyR4uN6lGsK/rYltvacueQssWjPKBmcFKtLAYETVPEkaLv/eHRp84EQDGWZPho2Fz8nGO+KF2L2J6FaNCBwDbpUSBPrp7/vwX3plQjKqbX+mtqq9SVcfV6LKxLowQQsnIHVz2W+W0zRF2J0oe8U7HfyLA0JjIGWBhHDksoFuePIG9xAaf6FGWyFhZ3LVyVOsWwPmCW/xKIHHMFn0JRLzlfFkPygbUYip5ZfQooQem6jkjcRCiVKj2lZ0uM2+xeLJTLOH6N0dwl1gcUG4qDCZfjErvjbmhvU1E2DGIBcAt3C72gSblho8DEi+DXu9c/x7vte7xJY+aymoAt4AWMpRzDuqZEIMkzbSiOKc/hsa3jofa+Mh2mnD7a/mh3RTLInqI15A0pEtw0O1Y19/yvXqaxxeaqRYjpHxTGmL4ci01DrjcgJs5qB29KWhvkYo3TQcoRSZI0O8ni4uRcAMImga65rAYl+1aQtet8VlCfJBxAPnxJaLUJTQJW2qXw50AVCepxceg9yECc1WO0SqH/qaQxJmP0o1wj8N1at7DxoLHOuh9A1SLMNDGMoyT5iVXUxHsbjYcm+Z5fHOfvn2h0yTL53LB0AhOexDf8ZtutELYl07HXMwLAe7f6Yg95olzEV39VbqsVVHiu0o8Re0/Ay3NmIkB7r6PRVaEvFYQ0PlIQ0W2RKkj9Fe09Ue8f58zXqIeHpwzPEda65wfyszYMX9d+ChvNm/Tny12UMhv8dQ4bQqKVIh01+U/gTJdnklQhZuUl2Jh7UEhQ8JaTadCXf/2nVUWzOnyVpDp4+25LZSXWKdZRou9ik6CxTyCQ7Ilmhji5ChCRHOL50C4KRnAIR271xMvZ8S7QJPN9y/7BmjCf379YIljCWMW0zczclw0kKM6Bc+vG17z5LpfgpG8fN6RIkBNzquoHAQYk/SOyOSAd9s6sppBCPN6uqaARrr/XwJ0ftc0IxzSLr9mdMle3i8JYCXCf+BQ+bco4sHUVZE93cuYl+j5kOQo7daXMOeSIf7g/+n9aowBRajv9p79AQy28xu2w7UlYIYAOE7Fx//dZ/XcFO7F/yqPZm8GkK7I7hBi+in322NKaNruTkEJ5/rTLabYTLlsyS2tT+t/u6dogidFOhwBwaUl/pOdRm21JEF6XLvweimc0so317/dNSroN6y2ER5PyjdQLTQv85ug/weIVEbxOI9aCi2GlEj0KrXwW9rywxPhDS6490zAvBsMymyjDOeOyUvfPZhI2xMLm5V/MXaq8xiJEpFZ2GBl00N04QQ8K7OmJJ8vCU+BHfZTyzK0gLO1GFTwQdyqtV06rpVg/7FCfAV5qbkIeU2NvQUhhAL97E5dDOXllWIdJUM4Ud/AfgirsiPZ8lEN9A==";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"dgs1GjIAF1Y//mwKO6Q8IN1g39eJ9nvhEolOjOeEMM0bG8ep8Hk+vZgacljMpe18PKbo/fcOThWP6+U2";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-08-04 20:33:00'),
(15, '89.90000', '0.00000', '89.90000', '2014-08-04 20:38:46', '0.00000', '0.00000', 'EUR', '0.00000', '89.90000', 'a:4:{s:10:"return_url";a:3:{i:0;s:196:"vIADrfnnBtpvMiBVBjL4nECpmX5zEpBD85RGs7l0UvDi+jBPjlTkqHWyUHhSeIXF/C6jSA6djMtUQANZp07aFtpL4RqmmwTHd2fgFsGu8x2wCzyCedKz7nMpo2nadEMEGw2BAZQ730fKV2PIKcM7kn2Ey63uZpR3Q9uQp12ROmrRSk0Y1T/TD+Z8I7MwfWzPc244";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:196:"+0RxrTvC9pYiarPjNK6JkrYjz0lrFmLGulFcpgNeOUltKvWWCxhxwy/Jy0f/CyaOZSTDPXfsLsbBEfH4/0nPe6Hgny/iiiC8NulU8VtU5efliyCOSWulMuojtYJkX8XauyEh0EIP/8pb3igrfHAVkKD1/c1vhZ63fRbzGPYJYC0nDUndpCVpSxwTZJhv0UU3AA==";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:812:"vdfQBrxht93j61zrPOncdBaIyE2br0GpZAZqKZl2SZpunuo3B820PZOkgNU5+ny77XroIwW0/3Ro0g03mYf24JoiIToqV0hcN9oHrRUo7MqoRo0wjKdLkCKVyZXEp9aT3BlhUGlrlnRZ8GTmkCgVPeCjJNmzK905i6idngGnDhi3wMLhC3Tqu9aO9GzXDHj0MZ0yxuz/95PxDjtNdwbni2yPSSheRurky7F6S3cO25KlWlJnr+VW88PFy48G7BprKzBimsuA6eLzcTbOO2ZWHedqYbNO3yIXnOZbxGwlTfMtilw3PK/Ll+/R7yimZ2+lFPOaYMrJzf1jRaubew4z2XAeG3Q2G/zKAzCZFPGZkn+Q5WsIAUJWCDzKvbQhlokHvPJ3LHvfjfWIWgyKS4u6RrOziHgLJJenLN/q0NX8fjL5KBC4pfjmJz2fq75VT6mpYkbxKyiy1GJfGaAXbSI7930c+CyWQEV26ELwp9aQqmQIPFW2qbppgYl0QW97WRO9LaKGTRHCGIxWuN20XBEaQz/fTFhosnNpyrt/UaPE1ZuVTL92aUGGxxwTVMrogjtuQJdEyiZFt8IWPFuljI/R2WDQvOFSWv/Zkom6E8JUPsolMVnvBvSp3t+rl1tXe0+NrtpxKV7+t72VfFbz+t946DQH78n1Ma3JCt5g4bui5JAGXtRCeglozefTTyGsTr7KddMQnn/HH3WP7QhmIuDLkv3T7XXMa7wIqKvXssJZAtlc/xCAAJTW4FupuDOvX5ZGEixTXLABnCzXeizaQpAXH1ugyd6q6LpvHHD7CaERYg==";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"mkrxA9P5YVi/HPjp30mz8an/WKTI/Sd0zIYtcKRzgrruVQk2S16rA9aQCk/DfZDqDXuCcyY+I7rsO/0z";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-08-04 20:38:48'),
(16, '183.96000', '183.96000', '0.00000', '2014-08-04 20:39:25', '0.00000', '0.00000', 'USD', '183.96000', '0.00000', 'a:5:{s:10:"return_url";a:3:{i:0;s:196:"DOlalQQMHpgb2iUZv6RVS7nqwzzF8sZMKSGy7LqWLWfSsc4tMBhMz3LVpmprZhhBX06O4JrOOPgtBhF23bgue3E+tBJXxpJVojnh6JDqxPJOAbTR+0w1RCMvjsLdNfG+HR7egD8bJdTAGXg8fIHWmTL4D6BIGqOgKaQNKQQNv4HLLQPhtpQuKHCDZX0uw9HGZOOY";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:196:"vY9/VeXIis/Pi1VfwRDKmG330IZkprLN2LZYViF9UCKBI+8iO+SlBcgDBAF1IhWtHUnT1z4OS/7k1i2eE4zNY5lo7m5QDMlUhSjZOu+bDJzPR2BjPon4MTFyt6NUN/Xi8ECjb1G8Rxgo2f550f/3FrslBzXxcsZy75RTv2SpEkfG1NZmLa2ZZk5SJxufedBcJg==";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:1116:"xBuzxPBiOyL5UmDtM66fT2/7kdbRSganDsa2GjiAk3v+S3MLbR8xe4k/38uEq0lI5Pz1pEukGZj4FvymqcXrr4RRyytXNJKTD8Y+rKb5h++Eu8hg/jOpW/trQ+CSlz5rlML98LHn6w/Y1/rNkXmvNubxBz+zK4nFEReuB/HDOh6gOSR8VsYQ7dULNwwSOJ45yBolyCtlDX1s89qjq/DZp6r5B2f/M+oycg5qUNJRhFtmL3rYIre/mGCxz/CaXSZNC1o+ss6o9Nf2fMm1qkMvWGH614G1vDypBSDraiBvlDcAifOJTz/jViPwUMyznNqJmlsvA9FBTWau3lJ5Nzr3FfECl+VotNWfaNuF6Jhz1H5iPOhVgMKYff2uOuPE+qXT+NLe6xXrJWT5QPLaUAg8rK8APonSob4mqU3lh2GqFA1SI4UVgCga1RtnwSy1VSc7Cv6BDiY6FYHACW5Oq3NEQ4AJOez6LF3be11/lNKNsx3jn54SgWh6ewXx+YocjQ3kDrTTbPQ7y6r+IZMsKfgX/maSHJfMCLojiuim7ub4Hj9siY+zzS4PnaWg+u47BVVi1NwdxZmTibp5I717Az1E1aemt4qXthvQVjvyJ3uxmudxyhbg54QpU6GEk9/wRMQ2hxQoxJW4LTY0e6URTZdRstu94aVGixtIuUa0UPt+K4j7tstts8YtCS1YwPQExiAGoMJoh+sl3mzJRWTeXlatOEe8yWC5WrhiC4mdc+d09MyIssL3QykdvNfgJrcsaqQ74/6T8dkpTdbmm8kvccJk23GQfOu/TP2Nk/CoEE3AZkY4zL5R5hg1Bp7KsDN408P+kwiZJagCJowA1JyC1veWWW+/TkieMScRnQuwNKEhjnCWd0Bw/BmHYUEURRV3kjBJ7Ds8sLv/V8dg71atFvqufItM8NzFEzL6ASGey2cJcEvFTeAd5MR4HCUWdYI2+bj4D9H5L+BtkMUtCiFvqGZ0AJSXoYWylBqyomprPW8AJCXCiGxKTU1M18uyjOQuqetKDUxAYJk3fZJeZ+b14yJN0rwnogxSifuYdeG7adKurr57R7E+Z0JVgjIRHXADvJRMx0aHx6pOUFeD8ikTBztziTiklw==";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"ivQe1SKAmWJELhHSNti+xVqN4czufMZdtIi3IgMcWZjHTk0wKQ2jl3w0+UP9/s7xtyCcWudJpixIWRWM";i:1;b:1;i:2;b:1;}s:15:"paypal_payer_id";a:3:{i:0;s:72:"43UM6rRNQdO5LLGtW6B4FII6XKbKywqJgtycBOwh8Lt0qwYImsF06b/eVwMFoq+8paJ2fi4=";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-08-04 20:40:32'),
(17, '10.23000', '10.23000', '0.00000', '2014-09-05 06:35:02', '0.00000', '0.00000', 'USD', '10.23000', '0.00000', 'a:5:{s:10:"return_url";a:3:{i:0;s:204:"qsnJOCUO0NDkH1sYxoz3YyxJKbq9eb+kWqeey+1hXMOHlc0zAukKhmaZJZhQUy8o8K6xIp0Ui/GETqxDr4frT17UZCk1LRM0i7It4OC8DwgLflWywJJvIQtVDLVxypgUaQIpU/MWNYfTr8wiZtUcrmYzBA8a1Bmy2pN7uZ61wHnrRb7rudfw3xq017j8g3yRRPjF1hXdVA==";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:200:"RjOGNjGbfPdEkZeHXxqR0q9nycxlYANEW5gnZXPojF2f0jc8eC1pAqsCJM4FaoPntNiwisg7fQwweJraadz/m6Nt2asr/NjBgbxXSKh8ZLTClhlussxllOTcMoYev2ESBSpoI5pgmu/vZM0TC3xN3OM6oid5hEwUstsHPevSi33hmX9JZVTW7lN6jwqwhRurXaAgkSs=";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:388:"mnS8lP2XEb1P8dfN/0Xjp6uRl2BLob9rqdrSdj6dYQAgEER2eutNJvuJnN08GuDZomws5sl09KyRMkploYiVF0cbZBmhmqUDUNDCAx3QfgkVpzuWc0FfPch5O9N7PMA5IhJ8CLtUhnB9NMgQ5sBPvSYakRZYjmEdwkUbVfSDfkO7S/GiXGmXqnB2NUQPks6bXWaNMuYiX/rQKEdLiZh9KHZuIF4Ekcd1gtoCh0JdZd2oT93kU6CUTb/M+1UgAWEYQ2hD2LyG3Z3wdQitxJvqWE4tj2tV3l8DOj4rXAi69qOowW+1qGwLHTzmPONEr8/9vyiFzwEId9nc/xrKZgg+rW1djt3SX7plcE1upjQYZwNtKNpWIGO0aZuceRqNA9zc0XE=";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"NmfH61s/X2NAgnpKxlildDajBalvw8JSsIxeBUlZCBGmNowqzYH54FpgCLzg1AfAfqfOdKTK+DHz3x/f";i:1;b:1;i:2;b:1;}s:15:"paypal_payer_id";a:3:{i:0;s:72:"dn3B7Q46dTzjTPx27aXy6HR/YEqEk5kDNBAmyjma3hhhnYjlhjE6io3ehm+sGLwLYMsiO1E=";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-09-05 06:43:51'),
(18, '7.50000', '0.00000', '7.50000', '2014-09-28 23:00:38', '0.00000', '0.00000', 'EUR', '0.00000', '7.50000', 'a:4:{s:10:"return_url";a:3:{i:0;s:204:"pEPzob0yAAdBNyLAWElQ8RHcDBb7tYxGX+as72hZztuRgAfGcsZxiQHpo384OQsAKMExl9OoJ9tD42Lugrh2jXlDJz6f+yXJtB4w0EaLqqvTPBwL9G3UcXGQLw42OOO317/MflMNFAdeFKtQSCIv68HyneufLHvcLoMq/DlPzlZzY0OTdq0MMayXuMJr6CwEYvtg1PYusQ==";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:200:"zZjPMhzG85CvDtjRDTb23La1NKf6sRb8H5GGJ8/L5MRv5U1nrgSrlSBZXs+VLOaFZKbjUMZ3Wxzhzl5NuVWSut86NAUmkf6JcvKIDfKR0XitWhY4UZBzkSIA4mCZtXvpVxlYTugltIOhAs5XYwnjgu4RkoFRTuqySuZED6e0jxF6l98TP/pQa3LOxFWcI+varTkK1Mg=";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:372:"wZ5RVFPopMYJoVYNe1Oe0MTk9zUAll92bINVgv46UU0U5KQxI1Xp64bHFp3yTgmcZ5VAEwFuJJL3IwohcBEffFJoVya2UTvhlrdkB9vEAnfDndPW0ByOyKXqGVGgSqzsRuB8OXUM/qau2FrxLHHYGzm2Dm7MnO4A/5K2jWaiEl9hf/sobq/NUWS3u/SvN7zch9+Pr6DH+P0nWg/qyUuKYYUqlwya48IhcsFo8aTyxXeZITVZtSYris9D+PotF+jyMcP6UwGhuNrIR7ITkEhe3oEcX0W7G5J+NvRoL98UQIuq2Kt08aVq/fz4AFdEbaMjiea66EyYWQ49V1z4FzdGWGyGhFaxINRU68OOohzxp8RRkx7HrzA=";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"XdSU+ev17HayDWbm4hf/+DN4KDzVGaa5flxxDS6TUTe7/uzaEf5XcQ0DyNGzAR00iYWyCCoPYWzu60de";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-09-28 23:00:41'),
(19, '51.95000', '0.00000', '51.95000', '2014-09-30 17:13:06', '0.00000', '0.00000', 'EUR', '0.00000', '51.95000', 'a:4:{s:10:"return_url";a:3:{i:0;s:204:"mLYpKteqYF712veo2STMyfbT+iNOYpyFq3ivhJIgEjkdARK0ofUbD0iArgUiiwsbEJrqQD7lEmG31F/jPoCPVeG8twkLxY5cg2hq9kVgiU9VJfK3jiSQl0LljYYRdPsPOgpmANU7J5ry4UgMFeKKwy6fYN3B9nDPPQExj/br7/Jiy2OQ6hU7dGwYaJdVFLAZlSI5/p/mJA==";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:200:"6d5FbeEwAxagr3I3GhAVvlFvsdRdbr4Ertv6gCSuhcv87oXjXUVOs7XNEKBqR4A5nQ57zvEA5/EPpeMHOSVUqG5bnERNXvCzHeFg6BU12F33yRrZH8fiW71UWRwPNqA8SpVt9OWbUwnzNi3h4n95M4rCxAAF/KKwRjgSu/wZjW4VShOe1Xj9QSmyYPQkBubOnHAJD7c=";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:708:"gdQfcBg+IKNBRmxlOHF3Zw53DTxeeJTbkjLp4ft6AR6qlD9ekHODbf5jB91FExsvSlRqhYtndUwxe0jHt0qdV8WxHIFaNRwa+/5lf/EPer8rBy7YvgKOLBiIG2Lxe5GTwmBdDUiRgx5cL3F2LsF/tMhctpvd/n32Q9cAjjEdCEeDCYTUMP+s+ISG23gUwzY+ALm3i9oP0KDqCB5s/njo5oR4AUKYAt+N9MLJaqkGx+zHlJpHVoc3jAuYMHhcLi4x/W0kx4t7PjV+vTkCs3bIFXiF4HsYXb0k3kLLZVjmIx2y96oMWSEoImu7W3AcsXAdL8Osh6hqau7W47JYE20sGgXwfbTvNVbAb5PLYfinlFkUtUvcfCI6iAka6ZTLwqi+ZWS4Ai7Ggsxw0o+px6NFeyy1+eD/1WFQznoR7DX4kBiRwt3dHbo6o8JSf2HtZposjjJsHAZQ9OnvI6o+g9tqdTvYMrtqnkfaPAWttWNlBjVT359NTRLwKijS4XZynLezBR4Htpp1kMwJ77AuKVEjwv7+HhfVHx/bpQ3N+P+dS6KAe1HAz2QJU+rCt41GP3XV0peFHncc4Rr5JL8Yrz8nnbpfqfYmYJqZ4Ze67t78pI1L2gcUqxe9GxXgyeQISI8u+zrn4E+VmTgr/ftlsWDh2iMb64zVCH3BigwDoHcuVPeUbVtoj1iB3zZusZFwEGquwg==";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"imBAXMrvsKNNz2gu4iLmvxQG0kYn+Z5O4OHlDmvOZI9/fAiHUrqVwzgAtRMHNtBNefLRd4/oQ2FT2y/t";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-09-30 17:13:09'),
(20, '51.95000', '0.00000', '51.95000', '2014-09-30 17:13:11', '0.00000', '0.00000', 'EUR', '0.00000', '51.95000', 'a:4:{s:10:"return_url";a:3:{i:0;s:204:"CJoHSMryAyzAGEd0e9uJ/0VltK3k7QxiSdUREaVs02Roee76xJW7qdLl8z73fKuE+gWLfAvL+CnHU5E/Z6sajMBQSzdImGRqt5lTS2ievvvPbxaTiMyWi4FxiEkVd/gHdl4jfhD19jM+03fzYdZaAta6E6Q/a6b1qLK3cy28yayuR57TRy4L3AveHZOUrrwLRXYxJN3NXQ==";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:200:"2NOj/6VLh/1ytTCjM4GjDDfwxaxX/82FSL2Sm90b9HnfsOxIy8wtgdrJ95h4APEni0OBDoqlWdlRe7DSRl6WN9Y+5Udz8uaCRr4ij1nBrMRNEB5l4LyQGsLtI8qtKeI/at8TdakzU6J6x9kPCS4J6TMcdAQYrh1PHHz8X8vcjItkF4Xoez0h3yrWo3LsqdBjSQhkKVQ=";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:708:"+86IlZxZIRNknOAW/6xePbGv73N6zLxum7U6Sjanqlj9v5cf5ng1jdRGIySChxKD5iAXh7HEo2oWqU4cJlQfOUvj0fjD3rY5SAodoTgsZ+hwb+jxXDY8ZKITvOUXhcT6RHCRt8AGU/9pnwFPsDihbnlsHV6lSvEi2zMyNlaWzel9QgYjXDIKXdp23nnhCb+QrI+D1lBOm8a2MJmFEBi7fcryJj9RDwrskhywnp9Fy0w846JeSNDQaVf00cvtWq52eKf9TweyQJC3xUW0Cf7IApHujHOJ18W0k0TWWNsa68jkL0Ltsi27nOEErGEcIETZlfQLJ0cIig6fhJZQyq/3D1j/Tg+7p3nOFJavf9g5eE3AClNw1BwH1M4qCBTc6gaN4CVYYQarUjgsVm+gHpw/BvZnb+OAaU2df5P9LzP/TtEg/eb9B+tJyEpnATUVobYGq3i+k5sTc681pstWwkZP5ip69FNZ4dYuJ8kLUA5w6bNX0fGXpe4p3t5qbjqftP6jPQaEiYOCqepS8S8GOReCUZzavt26bfhLSjqa4dyDxB4nPaoi374eF2T64nHuK8gmW4GIX2lEAG1+8NaIGt/quaC5d+DDkN/RoMXb2lehF6yhbluXRCxdh6sB6VE/WhBShotWsWYnEeLVGmLzUmTkfd50OusN6U2zW5B07cpvDFmXSoHOBvWiqb0x1FdSrDamXQ==";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"7XW+jfVKFOd9CJ8mw1EQzQ3bpvF9W6irz9DB1e5EHQd5bP6IcK0LJjJdpwJHwiNVlNH5XQGV500onpEl";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-09-30 17:13:13'),
(21, '51.95000', '0.00000', '51.95000', '2014-09-30 17:13:54', '0.00000', '0.00000', 'EUR', '0.00000', '51.95000', 'a:4:{s:10:"return_url";a:3:{i:0;s:204:"Y5KpozWRzs2Hblxz+BkyLp2Og0Kek36vjbWVA6TyJvPnQItUhjgzgvlV2PUhyDlizcYHwzfOa6y2P4MxXeP8RZC/4hiP3dRUNwdNQ+TT0vUTWyzfYoPv+z9digotlXqOMEuBf8oajqXZJbpcUDgohaVVPLNzLNkFE3Oipg4KwYkI7tzq8T0ILNudcisOoOHchUcVWeCDjg==";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:200:"AQaZRl26FG8i3gYm09nlKS292PysAGp+4fMe8vTaY+QezcI/SnE5CT8oxBL/jdhloO874dMiQQxjGX6BK0WPm0+GUiw/TDQq6rJr+KErEZY7zviRBI2in+EWvdGTKxyIQf3MmegYUgGwFgphmxqlDL3s0hlzqp+tbwGr9m2PKY9kq2zKV1tDgkZdZLUzbPStlN8TFTM=";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:708:"fWE1y2RkZ6qb7Iyg/lamLBT0J32qprBi3KeYmrrMZmPvNDWAn33NrRVq4SpXXMwU7sm4ubrYqCosH8hMyOES2Zl8UqDEdVqqDkdTqS2cOQCuM7QFAULjVlEThUV5tltZUmfB+dGTHebK05K5JSoA4CiEpaCNVYE4+U6DkWGAsk/g0a7gVpfe7QQOuzDvioYVFHlpcZJ7HvIAZRk2PIThMTCpcT5zgVNzmbVofUIx8yKOwDl8paaK8/QbUWukjZ84Z4mRXtAS+igbh8unC58wUDo7p5seWPZPud7mnNVedKRzdb3rS0NbiWWE6axgYAORJO7jrUcjP3nGfTDDA9BVVOap204Nw1suEi1bQASozWtEGtzrFfi4+r8FNGvXjE/+MYhKnwHG5uwW3I/9LrZ4pvsCOda+/j3g7xj8DRHXoVGForqX231h8plxnnMYh1s/7OY95LhrT+y9TPJgG5+Yka8DwolDrpL3CoXNrN21GmlFh3P5/0CzTofAH2ISivYIth++vjCIacC4G3170qsD9P6nfpwSL5qrA/0sM1EGBHCUkMn347P7ZzdWBL32aaKzankaLnRFIT9TNxfc6/kLYTIJ2/UZA/wvl1+/vWMIyMlVSyowcHy51nbvkuA2Kfvm5uKprzifjVYCDJePOGLcon7lMUUTk04sDChUXkLCyxVhP9V9OdUfUVuXpI4pPirxgg==";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"metIDivEh4lYUq7qFzBSvHZR5wOJCO6q18t+Q4RPQ+tc5JIH+w7zW0K5eH1AJ4zlgu/NT96pRK+3Jmig";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-09-30 17:13:55'),
(22, '51.95000', '0.00000', '51.95000', '2014-09-30 17:15:38', '0.00000', '0.00000', 'EUR', '0.00000', '51.95000', 'a:4:{s:10:"return_url";a:3:{i:0;s:204:"+DS3zNAXNAOWXJSw9jND2PR9xHGuLDKLJJNX+iCyViYJsMerWH2Ua3juY/LlMIRsMZ5y3fW3TfTdFDkp6Jv46BpBPk09o9nDARTp8o4NLk9FTcO+q/09SE/XNN65iVtAd3IFD59+emj/sAwl93e7J82IdtSNECoYGyZbM3pJRXUY70dogUFHbquEUmblDaoCemMRKg55fQ==";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:200:"j2HYY/HJvU6T7SBAO16AYA2fl5a/d9E4DUbRhR4bN4N8tz5HMtHA8aHufCvzgV0OsouXZTG1h246m/zn1BwQPJU12vRz2vFvoEl14o7XhGs3vQ6FMhgDGnYjGiwoF/NAvgHAKZj2TksVP/HglYxnrnr/hV3vGDtMNdsRsrCTgJJVQXKTAdGU07b0RP1SPHBGR4aCcW0=";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:708:"F82EoE8CbdCTYa/X6QnW1xPN+SzCGZLw7zVpApwSA7Zt/RQ8A6NrRIsj16+6ojHvzSbqkavsEMftsyuueIJgQiQdI85XhwP74Ecq407v5c+WeaH4/3T0yN3hYVDNlyv39I7HMjfQuL3tDl3Cs7b7vbAJKmN2RTLHXF+WIuY15XSTJjeintrkq1fRxIU53LhCdldC/YuYzNFR9RnewVFvY6KlvcDw+xi4QOhQpVmA773u3T6cZal4ma1hijnzReml1bneELNYBKkgOPunC+wXN9SIvt766QWbmafSDlHqfzkHN6TJHPZ1Sbl7I/PcjQWwNPxcKLtg3uvrvzdYS1aMJxvk2qmdKOVYmKPvwnnXUavmqUIpZNaao5sa2z6eTuHVtqvF+aZuVLcIn/pEt9hVd8C6yID4r/rM7YLy+GwSOZ/rKou4ZqLURbeyb9gMwX7Yrk/T4KTmmvW8MFb0EgMjU63vkpZWfIiz/KKDxexTyLhLmNsqc9CRYLZvbjas783N7XhQ0Sf7ch+ej6MJcNXD4Jlucxgz0b1jJwrf983wcFSNKbu9U2jvHaThP4hhpDWHisSKUSpUgr3UZFM77EPXwgKmKwHI91KGWIHSgJZeu70tU5CA8/SS3En8L/waV2Tur/bDHQ/cSVFErBHcPb3fuBKKv00+C5YH8H6lier1ml7zAGXzq/DF9BuCSi/9ymItuQ==";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"cq2ycj1KuNSenAKRcw9+zhPeI5JZNYtsa1Q7prKUAFPjDgEOQ3R+fvAgn1fu3fOcWrw5x3sNty1S9AZX";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-09-30 17:15:40'),
(23, '44.95000', '0.00000', '44.95000', '2014-10-01 14:44:34', '0.00000', '0.00000', 'EUR', '0.00000', '44.95000', 'a:4:{s:10:"return_url";a:3:{i:0;s:196:"qJvz2U7/zlsIQgWrRjUhinGVZfUSGv80nmU6WaMugPVuGZcBkP3GBAPLxFBgL4qPx25VdiNFgCHIYEW9r8vyLF4lzeARAmM9qJxsQh5do0OTIXfpDArpO3Gqw3GbepWUvOljsvT8CtlY0u12l/yFXPqcIPuz5BNOKfm8PCIrkH01c86jVjdK76FFMNDUZtYo4N+y";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:196:"HQNF/YGSHak69js5ne24ueYE+/TwUjpZZY9bA7k6fW8iqHkTPVKKx5s8BIzF1QdjkizO0OANhCg5ID6OR/jKqPEftneODjs6SEOI+RcUB2zjAbEE5Cb47TgARjhr4hVROWtlAhA7qBOcoUMEvySgk1pSraQodKvLhZj0CqTvYXoV8fsy7Omt9VUzmjv5g9oOzQ==";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:468:"ZNGuJcTKBtXP84DKXMh1hqbxNxUpQMgJos0pYzjLwK9GWj6hL1vzD4tbbTJt5yyO2B7YvCGfdJhjBh4rv6CDpj0VLLM3v5YupQ5X2xeE6xjPB8nkEwN4g9Njef4F86DsNZP2UF25xmkChtvCr25c6mz0MF/Cu/HDQ23ka9JpysxSEMZdAqDhUCBY82yO0MW503U1Eu1RMdbop37eLR9KTLNzdQLBX+n8Ex3nA5z4DO5wt5pyqBPYpubFmqEIoNUR8o2eNYzg7BlBxoKLEm0yl35sZCoa2my4zV8JDDnuRxX0gezj00dRqPKcEaa/Pn5TP5MqXhfvPPpdqxUrZCvz3kzUD5ISfeyfivAC0yLAb3Wp6wIMkEb9f5zgn3eYgIEb7HLOl4QtgVyf/3u4iJM+oMbHUcHGsDdI4K4/qWNpwBJQtT34z4UlKA+BjR98P4mLWU6ZU68LpZ4+fl3AMZYn";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"IrnN64kYqRj0pM/VDBKi9IOqkVr66moVlDL+oiEVcegyqexYXETyLO67v4GPUU/FTWeNsH/iVKDIKaP3";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-10-01 14:44:37'),
(24, '44.95000', '0.00000', '44.95000', '2014-10-01 14:52:20', '0.00000', '0.00000', 'EUR', '0.00000', '44.95000', 'a:4:{s:10:"return_url";a:3:{i:0;s:204:"LFtqVkddga/WdzC4jTICSUft5y9lXcM2gnwsHHCTMc/SGzPaVWnsCTQKcfn4WODvSJrCtn7l1sRVe30UfOAshxgopNq9ctssNtJMpEQO40/ZSH84LCQIzrRgJ3TtVPEJPv4cX/aFCYRzK+PY2djOwEByShIOTypKZxMdNPcAXtVKN3yH7hSq6zvGbN5UKRTB7tyaTIbRpg==";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:200:"w0ZwtAyEOCVCXTzg5ktBWS+z21rvaRjkFPr+0uLjgcm4GNh1LK8xc/tTehhThMI0D0t1vaa5m065HHWMpchnT3ja/gMtUy5Q4sEoQl37iJO0GCDJZR9TuAYcdkbR9yXG7K18Z/RprazYM37S7TExivdTz/TK4G7pFspltfiX/q3UZ1OcfHN2Kaw4MiO0QzxtVWBr11Q=";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:468:"cd65TZOFygGx/rVotqPpXB3uQvDgIL3ZrkWN4kHxroempa8mmbx2jJFzP6HUvZvXADRc1JUo4NdJhqgP0LJZNEnF2dy6Ah9dbyvT3EvDWJ8xnSyZbvDgXy5nOdi/YlQ92FjQrXPNMVogC9UIvpJwWhKqu4fVo/8uJeKs0yLoze0u73DFbGA8+lLY4pUgT6LnzJGOjihFjDyenIXXoaYnjAcLxnY8uEWwQohjGq88AnHUTY1wdwCODdetFbIzJzOw04AR9w9LoeuNIpP2dYRG4wtGbBYLGrf1EpSibYpGBIe8Vg7qZPuPD8kMidCxf89bpRduVhfwaUpEwdMuA6k8/843AEwD5U+EsmjKSZNDVCse34AzU1uuswrBj8vXNov+14jpu/yvHH5TI5X4cZBLWi1Rg2mL6dUDg12TOgHfz9E0s+1RPnOkkrnjl/zZxoBE250ImB7OYzVU2ObMgNQp";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"jN75Kh9nz35fF9O2ykHV6pUXe7W8eo6QSfONj9OAxkuevwUmgzPc6Kx9zLNjBaJNqncIhdqO9uVxxkvb";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-10-01 14:52:21'),
(25, '44.95000', '0.00000', '44.95000', '2014-10-01 14:52:52', '0.00000', '0.00000', 'EUR', '0.00000', '44.95000', 'a:4:{s:10:"return_url";a:3:{i:0;s:204:"DfRBqJNyVBVVJ6kOGtoy4oCyHVCWZZDPohZxl5bRiG0njr3x9lMH1zOFuFJsh6CfAkavUjJwEHnUlcYsBdqnG7Gz54LU+IfDfl2X+X/u76nLre9DT0nKlRLUw7BI62r3cRPLK2oMcIk1EZlrrsrq9x1owpG9rHcTblPkUnmvnQZFsbhBWQ6k6d2/CAziecLaBzrJZjj2QQ==";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:200:"O9gzwUlqNX8XA+AsP8UcbDe9oyTTs9xrYBTar7orEfolvPVFqMHunjyeMdq11zaHGAg9zLiT3xbv5GlTedaHxbmFQoL1ePxkyXuSRfKgDpP8dXkCvW4wMm+iUnm8Ph+cCsvWZQ5JRPdj8eg5BbVsae/WabSWNebPwVYrqKv41CEBwPNT44oZVwGfC+/toFQSIlqZlcI=";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:468:"ZZwV9YosyiO04A1lfQiuL/SGg+g5FIvgAtCc2nbauTRPoGj/E7iZlPMot/3hhfZyBhZJLGD1pgsByQO0lKQUt+VAnZkiIUPgh2C9bvMSYXIyP6Ls5qxtv2wwL/uZSGaG8+1n8hR+5iraaftl/S8sAQMExIgDhiP5lATQZzvElq+gdM7By3gZgr3nrYWbwFLFHHZ08LU9ZLXG0mceeFTKl04ENEXgrGq4QmbhirP6SyVAzdTAlqhXbId5vVnZMINBdKE3Qjsj1QTK+FdNRxn3m3KzRwXkak7iGDF1UETSv3grUK2PPrfL4GJRqFSMTdDju+GdrYxejIUi4k1KjTSi1o6xQeL/TlpCWvsScWtY1efl8ADgxK4uFP5qgCQfUhnpkaV/53jLs4v9qI4LeFtx8nrjA2GSDFbktZTZpAk49ziqp3gPi9SiFkHoS0l01xBQlr36zE2J12hlEvsL55TR";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"Ia0e5mqbBgn2M9YklVNQ0NC9LaMAvYxRXtPvRqpXIRKGEuj/J9I56JYgRR1kcE0sIYbgtA3v+/yaK8sB";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-10-01 14:52:54');
INSERT INTO `payment_instructions` (`id`, `amount`, `approved_amount`, `approving_amount`, `created_at`, `credited_amount`, `crediting_amount`, `currency`, `deposited_amount`, `depositing_amount`, `extended_data`, `payment_system_name`, `reversing_approved_amount`, `reversing_credited_amount`, `reversing_deposited_amount`, `state`, `updated_at`) VALUES
(26, '44.95000', '0.00000', '44.95000', '2014-10-01 14:53:38', '0.00000', '0.00000', 'EUR', '0.00000', '44.95000', 'a:4:{s:10:"return_url";a:3:{i:0;s:204:"cD/Im9X9HNl6jDzeL6xU7LPdjeUwWHI5C2mlBHuYSjVnY4T/JdH8RGiJYibINrJjMbiEAUyrVKMEo1PeAg4srgaCkL5q7AQC/mUmbii2XPFAA8Oo/pi2Ll+M3yYbNVDoj/MZyIjFHqon4OD2U+72IWF+ucm7FKfmyJ7QKFeJACsGNFSeef4z3jqi6sTD3ibW/XzsPQgiPA==";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:200:"YDDr8NVC7uRaSuA7N5USMwfnNdhuy5JwD691qbCPZ9u9emF9JK0RZoCANY8dOhusgigenNMC0CSDgCdD7fA3QHTP6dmM+BnFf9CoDZHsDUY4KHhwdjnaBybvgQgs2qxi984nQ+F/bW48GKA30/R3KxGJ59bzUrsGO8OLRxDh7YI/6L3pR9DaVFqaok2sd2sJw3wMyBc=";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:468:"8xW1+m7aR1yGHJZAedqTm3I79k651uJBsn0T3lEl+ajDzzNEdKGtm53OP1Llq6Nb/a1JEr+htDkGZCZNGu80hxJ0XQOhjL5TYXWWrjxW2a4AqsvKFg3kXis1nD9Qz71UkfPabKQBJ0dLavFAqTVhVxZJwxZQdjNHwtDlKxwoVWA1pR0reI7NIDVHntTsz4uh1/V9FGUzivUvF2Sf9YcRib2+lAUQKM8OUxlskjWn9BuxjtUGTSyx3uYMg7QBapeZ3/1HWSgNLrMiQvwU8UJ/RVnMkbM8T2+Gs1qmk7CvXTF6XpI1OOvdZ1OLPTALoGCn2cSqLig1OZDJTfF/l07xkpdDjp3O+RGxqajqs5dwYyfO8PAjybrj+5apdxkCrw1Mvv+7rz/c3GpOQaDIIXPFiTR4e/gA065RpW75LzT0jPg1FigMdnwo3Jyz0lGyP3nXGmuoxqwP3bBoqLN3MZQ/";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"ufqaj6RAeS3r95oYUWiOT0HSIqWzq5KvyqUBoobB6LyHN2RlnHsOIXt6sI76yTtxaM1JbmGPpFdSejqF";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-10-01 14:53:39'),
(27, '44.95000', '0.00000', '44.95000', '2014-10-02 17:40:22', '0.00000', '0.00000', 'EUR', '0.00000', '44.95000', 'a:4:{s:10:"return_url";a:3:{i:0;s:196:"55lNdo8/EIKsOFRBqmkjcBiOXIa8jg3dI6e9sdAppeTFh7QXCVpivc/mMsU8xUVuMI9OJWdrItCWLUbFgcdMmcLuNWjSDbCfcciMjmoWxDOXG5ZmuBl295DMiN6QAPq8K0Qt+QbkRX0/zoOkUMMsML6M+z+MHqF3Tu0cwaaAne4/eoh3xb5dOi+xWm3tALxo6UmP";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:196:"4DUYWwINIcWCDn7ZypY3ZGXRv/NVjktTpEiJKDAeHDpvXq15MuPl26xXY5OD7QUoMmK8vYZVK8jxkqrbF4QC2iquiSVs9IE3V2hYxLmDzk7xZ1GHRor11JfDYlI04w33/tpFtWpilVOftxJeG6CbRa60Wbyclv0woXtEzcu0hgqVxXZMcPEqurt8VNUgq2ry7Q==";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:468:"XtOtnSuX+fvSXyEucJKs5nFJ/AN7VcEtZxNjv7/zqDWDIooMcezY5iaBuLeG82hIIGqvVNlsl8Z48Lfci+BN6pXah/BijSXAeuxHREAgjNxKOoFWMBt8X0+pVvIs+lKvxspeu0tYtDRksc/2nfbEx+6blREVB29zUs6eTpB67x8gzk5Dw8tPmM9tLAjK4HFdtJCNsAeUM4mCKcpDkLJ8ZCmF61uel2kBJ97b1uTJMPZAXVjb9h3xS0uBQDO95FB/TCJd8x5GX4TmgkHKMyEHJbjK+oHKvqPb9tEHTwag9h6MHetoYp3JM4gmgDAeub3wyF21P8Ivjc9tnQlrWf/CTCklXEQKU+fWgx9NEu0gYjFondF3ryNTUxcrupUQaRp2/wEy9Ah83gLEFM6MPyChRYilFVWqhQkcwTxIFzt5ERPtZNLyDuIN8xWoAhE/x21k4IqHmMFaGqmayz7yAO+s";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"ud1/TXbaud2G7Kc1iQi0g230ofT3DR60DRgktFqFlIEvMcsu5ZPkPKbcVNFgWQhZWrOrjwlkr5j1DASD";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-10-02 17:40:24'),
(28, '44.95000', '0.00000', '44.95000', '2014-10-02 21:06:11', '0.00000', '0.00000', 'EUR', '0.00000', '44.95000', 'a:4:{s:10:"return_url";a:3:{i:0;s:196:"/dYIpq/DTdm6Kog0f6R9OvpsDodONAS7QEivowlYeXotMNUTygo8+BGV+CFK/upAtxO++PUbkMBdkBtMQcQMAOh5HhSYgDZQOH5CvPlm5rc3VDCuxxvqGN6GIrv8za38tB8ntMYeZ38EJ4Ix4KWUoZg6ZYvmYOMF+dzJhWuauwIWM96U7Le5hlmpLPvAnE+1aWEV";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:196:"6xvEvL5T6Z7yLGmtHBJM77s2Xhz6FUmLTVUFOTY3yJCS/ikf5JP27s5nSYK+AG2VviIdv+fFMS6nr4uikHMyM0dK0gphFSBm4Jn+SGibSt7BWAGbxfy6BSrNpGzWGmaruUJV1E+Hef5QcIwqO8tnMyTg9S715GLcLjkhfUN2mgR7EtQ7lv1gpTbjMzZxuPUmGg==";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:468:"lkP4EI7WnrQj3H3JrGyMagCbzoc/1KgJ8MXkTIJuWFatNcY55pban26R8YBjwaqg3b13JRvSsMSLzVa0Sf60kDUncLfHUNpo7pjQabYD9rxzyln5HqaXO5+KUniiHhJIFm4iSiCkg2giE9rc6YIj6mkV0V67hU18Lfwj9UYu3aVu5upamwhLIiEYPC9rIfDTWYI59F8cDn7wo0BExUbTkYb6tkbtRHypNLliOfGctgjDKrOGkYtgWgyxjRdb1lTFcqlAmyTAdVSD5iI2eGmwINWm6pQJ88GyCNT6l+1OB/Wr5X03TygP4/Z7nWjSaimUuEJCFc9lwDYvkNDfgFFwuH9QyzR6WkSOliGO7HJbAI7hUTmn2ajq4TFOgX3UpDGDGDZJ3e37o3AbmS2p0J6/f3UtEIkGCfaCKhQ7y7rFKoSEWmvR2JA7xFHXRro9mfEtFcfHyNFzgE5XZ2cdz5Vn";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"na5oap1ZwcmITOKQD99WwrJPa2DHV+lWlVEdy+kKwX4qU+nMkjfYEJ3zMmWSyZBcwm4wYQNOKxpS6zm7";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-10-02 21:06:13'),
(29, '44.95000', '0.00000', '44.95000', '2014-10-02 21:09:22', '0.00000', '0.00000', 'EUR', '0.00000', '44.95000', 'a:4:{s:10:"return_url";a:3:{i:0;s:196:"TFSPrOZK9hrW8mMsbe3FCiOvdyK5xmpOLnNu/AQHFiccamQrw2wp+rG6Pc8D8KSgHvVAe+y1iNqdkcNsnYbQi6DBCMZY4F81fTf6gco2vW71a6cAMLUnDKIIActuxx48txY0i77rTk/u21u2feC1SJuT/qZcWlrsCj2gOm387pzHjSHIZCU2wdHF4JJ+Q9o1al6D";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:196:"a5oB/SNiyou/dC0C6G78F5k6OM7QsU7dTdCHIFLx0eZq/9VrjlRYSLdLRnIO5odSpdbJjZJ3ixQfsEbhbeaLIAE5B0Yf/2fiEjOuwa5ru50MUD2BK42WGH3g40Pmc+N4BGtnGtOM2J7nm3PwaCownNnG301G66KDbRxhadYlREixCgiTKYN9l2i3nw9NvZkfUA==";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:468:"OG+S3LfL5FyEoEi7TNdGOvt75dtQ89ep4zxo5wa4O/Ya/UqaZeg3o3wOhGOrV4+FFBHdMVTlMG1vjUi9H3ifPvRKHmsvMFHyEPL2SmqVzeAuj2zLiPuDH849D/5n2G70+11pVf/YX7WxcHpULuLCoC3RtP2dqJa/M6axKGNpa6wjmw2DPMmsR8x5XutSEJARBMoTcljA7ll9pu3olXQ22361/7ZQ1pWmhA4lM/S7fuTTstHS+xLv2BPy+hOATx0yNkucTcpbRvu4H8gcfjjn0BBAkkOM2V8hTWDOSdwm6jpzTwqtnUtgAykSD+zq7T6BT7yE9FsVqsv4gGnOmB4ODbIkT6YWBPdxX+JDsZ6s0DJzeladlkfcX6HxT5Lqxa4PHQ0rznrfX+SdKmQVRdaUZkIDwLKq4cqXlRRS+8IpF1u5fO6OSkmqwA4De1D4+T7LLBRGQs7+DlQdvNWfQRGr";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"yD45iCv7zaENvLKwxpGlQwBB7cfR1DTW67HXW06b8hnZwxvj+lcaK00o/m2ih1G8VnTqdkzc2pdK4QOL";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-10-02 21:09:24'),
(30, '7.50000', '0.00000', '7.50000', '2014-10-02 21:13:32', '0.00000', '0.00000', 'EUR', '0.00000', '7.50000', 'a:4:{s:10:"return_url";a:3:{i:0;s:196:"nU+RuuF0i4pjZY9Rb2kbZ9Eyts+Qir8jek31HNmw2Otgy8em42IDSIEZDbutICkJWk+nMydGbHj+LAb+zqj16qRFMLp4h2zSDZMkNAvdZ+3GUeILdBsiAIpaGRvrGqbdP7n4X+jRXmSZIbRJq1U6PP+jcfnj9AeNO2E2f9qOkbyjnTfAmq41qaXP479juZC/98wX";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:196:"t2QQ8x14V897b48Yi3dPDvu9vm6m6QOBPHsOZ+ho/wyvFY5aOioobdUTQo78Ww5yGRekX67xqVFebPZP2Ne4fpDpuZpvd0va35aLtpPkkb1TxGhnrEHK03uj9YHUUEKmXDMb3q8GkBzdKF750zaBAFB1BEv0bDmvQAzTAXeI/W9vPIns8fYbAz3z2HD6arBA3Q==";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:384:"8QV+R5E5AUOnFgX8GexrstXHsPhgPK/QZ5yA/RKglD0GoI2NwsWB/by6nuKN7LGGxkfEus2sSIwWsP6uyoYPCdlakUcJh/4dB0DSTXSeYfceOnSalTTgMZjj6qv7fXb4FJ2+hmSREtD/AyUvFpAu5uGkBltS58pOyw152SOkMlOXm0Y1bFoRTHQUPpPoy3PtlENA+FXAFgauEeTDRZ+/zA6OTdgmkuovetoCKm4ybB7qNZWjQjdzWH7f0hOw+M1G+DKjvKhYaShG9IllNraA/Z5TESZVO6xgSEStvo7VVzLN2zV4wqicy7LYYD+hjAd3oaO9Prwi/kNiobOdOMJXFKgIBhkDUsB0y9Lpfavs0F1Hq6SeZAKoasTMpcoT8GCK";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"eFQUoD81deOQPOv50iGdL/5hr0PG+NDRJd6axzU7FK4E+R7fDq2agf5AkRU2o1AwTobRDPSHsNV9C3dl";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-10-02 21:13:33'),
(31, '7.50000', '0.00000', '7.50000', '2014-10-02 21:15:37', '0.00000', '0.00000', 'EUR', '0.00000', '7.50000', 'a:4:{s:10:"return_url";a:3:{i:0;s:196:"xRfUzx9ax6Zt0JqY/F7rws4zfWZrXYgg3H+NZcznfgWsONJ8DPyHqsw0h0IYJn4sjYcyNyXz1xk2Pb4GKAwn7kvbwDDkBV+gaJe0DPmoRtxZgn1PY8NLgN4+M9ZEii98Jt1obTOc5LZU7qip61NBCNHr8YSde8L1SuRxbw2kRQpFrlnVXeCWnkvfO5NY81bUmGud";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:196:"dAVZ7pzLffq8p6UKxX2osv7r/ml1zzcYM9k29vRTXrgTSoQ6vlzbdeoyoFcX0mhj3A6876Ka6TxnpZBprEmJ6e50+ZNWqcgRtgwCB2FvuZW9VloK3APFrka+c9ghJJmX/otKs7T0suie5XUBNgDHqb1TdhQl6GVOvAnJdxpY8qIUNXBY31Z8Roe6hjQXa7QSsA==";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:376:"0IUIbg7jEIx2TaB9ScK4Lj1b3kDF40Xt+1ePpWj2bVSQ4wR5qQgavhgTrImK2X5m1bFNvzIK0TfHWg1RavB3ySxrruZ7tDCHEbVKIB/X5lcI2s+dY8acPKglu6Bf1tj084U7Uyj52AEguoWX7fietK7ykvCBHxROukdHLkSXhTq5Tjg8mPr16NpHusfZfyNzIzOB1XuJ0KinIhLDDHaWSYk0OcngB98FdOZc54F6F9QyIQL7v1QNxP9UVXSzUtpoo277q5FEQLEcv7D3EHumzACFPx99DakRWF9C+0Q5YPYhU403M3E+rceJz6F1qO5ZPAYOr/aQH2Es/u1nFTE0RlhTUkHVQ2y2ApGcmhx6JpkAFNfrmMYQNA==";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"CzepVvYawoTCk+AeswXYA0AY02F0G4yAqJpD/PDANp+XZJUx94DgWEyQRaMHcfIV4byOVZ76D0LQkdc3";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-10-02 21:17:42'),
(32, '7.50000', '0.00000', '7.50000', '2014-10-02 21:17:54', '0.00000', '0.00000', 'EUR', '0.00000', '7.50000', 'a:4:{s:10:"return_url";a:3:{i:0;s:196:"RkpoHWxeED4/1tKDiceeD1Lgq8c9tpXB+43cv3yURpP9WG5PwH7dozD/2hcCP35MUohbiNqC+W12Oo7BMb0+9IqLO0wHxQbBqImXoaegAYx69ozKlZ1PPWGraR4P3VZkHvmRy/6WwHekvsFB4NTZeCP6HW9PFgmypgh6X0TjGHtQK7D/EhcJZLLlecYYJFOambFQ";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:196:"yEz9ebGlPjT2biCDTNF09yMx0norM3aCMuSR0F6Xs7mJ3COr3CH3hCOmIaOtlNwUeKV+hGPjfsCGvzOjrCHlPaKzQZ787lUzGUVSfkNH1DR88LbDedFM/vzG/6wsbF465usB9oH++JwUXP2EH+2TKSpJS7fl8E3Xs6JAG5vEZ++ChCEr+o70CM9UtqGUOx+qnA==";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:384:"KNuXwshUNj3k6WVNv5LspsVx+n5v+tAnvr7heNV24zc4qG5ayz8MZU/hFPcjGIZf6aIpEgwryI/vD2Yswi7XbDufBeOiJ5d4Uli6xve/ZwopsohUEgi5xhuKgA+Rdc5fLad2eRXM9pCo9+PbMH98G1Qja3S5AFamEx/XtSljAwFF35zjHyQD2jWSUB3my7gUAOh6DJ5z1THUKh1afm3YyScrYxBYReidA+5Br8YLTakaPqP44gzvieyFL8Eb7IzbdbIkTgTjPFcMLh7vcAv9XFmVwg70J1cueJD9flr6iIaksscabNHeid5Xr/7ruqCBLmt/zsi5NmnYPTXy3OVPQj9w1YBgex8jlsinYCDXGC2rZBPrN7dNwdC1omwkf/A2";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"Vb8JEf88GE0WpRvLDg1+fGoQot10agiVIP7LIeSu9zNr8Lqhq6n5MIWdhT/rdsCgXz4xm6vzfEdL4Ikz";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-10-02 21:17:55'),
(33, '0.50000', '0.50000', '0.00000', '2014-10-02 21:20:14', '0.00000', '0.00000', 'EUR', '0.50000', '0.00000', 'a:5:{s:10:"return_url";a:3:{i:0;s:196:"EWYv1p/o5XJPMWxO2pwMR2Z6BQwZ0WGyAJRoRMVqXsvkvBaw/X+Amjp2pAE3SvuftJ53rvG2k8FB8Pf3Tx23lmwZSsO90U7AG2yFCnbh8TlbG5gxG6//eGTHRj7EaKKJo/fHN2yxPPa1eP6LKFbP/HJbUT1TeckUfSu3vdpToOvoVHF8Tq0l57G/UeJj9FUXoYAg";i:1;b:1;i:2;b:1;}s:10:"cancel_url";a:3:{i:0;s:196:"ftLZuCOZGuPKmh0/Q5frMOM1Gp/5kEIS+0/3YYIkBXcBdQG4M3loAd3kgrIchnxBoQHmxhU5247ZTi33R9eYSgRQfLEaxWv8mTfH2exHHjhXefxzniedw5i2BQj4dykosgL/U+Bf3dRtecj4vV6JWv3UKBH/hQrFXexFRdePLP5m5Tgx9JeTL08bXYbZJWaHIQ==";i:1;b:1;i:2;b:1;}s:15:"checkout_params";a:3:{i:0;s:384:"z4bzOR/BiET0lHK9hvHzTLz+ij9FIb55bmzX+zbsybORJ44MPyKfQGz41WCYHenXsVO+dM7SPAqzqXm38D2G1wvXlJQfIR/1dH1FD1IHcUyuG3PEsrfKg5d2rsEkpGTgvqzgUUeM2CVX7AnHsXl3rGqXSY+s1rhl5alZ0/Xwp51qx1/bnHN6t7tSWs1CFppMj1jZjMjcT+Xv2PBZYlm6b9TB0TUgpaQxdcNGPt+917ccuE0LDIwI9gvopToY/5Ono5TwtXtYAS07HScjbCUwtPP7tOu7//ZI+e3Hik+J1lDJCc7/LGV6ucsn04+VqPGiMBzz1fegQupNWl+qQ1ggO3uJr/W1W3EyC2zxoJfsb36qog6FWV49Cv3PRQAxl6RK";i:1;b:1;i:2;b:1;}s:22:"express_checkout_token";a:3:{i:0;s:80:"a02mqWVtzTYxrUthg+ForGjdrqfZnrT5nhs16iCb0I6Wvfn2T2nbcSpXUpTgs0kMUodBRQ1ZOEis9EgJ";i:1;b:1;i:2;b:1;}s:15:"paypal_payer_id";a:3:{i:0;s:72:"PatiXEX1DW8xQkz0PoRfpu3PcK37DN4aN5t9PC7QBRn2DhsHPB9dLmILpCijfub/EXQOcEw=";i:1;b:1;i:2;b:1;}}', 'paypal_express_checkout', '0.00000', '0.00000', '0.00000', 4, '2014-10-02 21:23:15');

-- --------------------------------------------------------

--
-- Structure de la table `Redirect`
--

CREATE TABLE IF NOT EXISTS `Redirect` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `route` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `oldPath` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parameters` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_3A7AFC7D793406F3` (`oldPath`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4941 ;

--
-- Contenu de la table `Redirect`
--

INSERT INTO `Redirect` (`id`, `route`, `oldPath`, `parameters`) VALUES
(4179, 'transcription', 'fr/transcription/1-fats-waller-blue-black-bottom', 'a:3:{s:2:"id";i:1;s:4:"slug";s:31:"1-fats-waller-blue-black-bottom";s:7:"_locale";s:2:"en";}'),
(4180, 'transcription', 'part/blueblack_en', 'a:3:{s:2:"id";i:1;s:4:"slug";s:31:"1-fats-waller-blue-black-bottom";s:7:"_locale";s:2:"en";}'),
(4181, 'transcription', 'en/transcription/1-fats-waller-blue-black-bottom', 'a:3:{s:2:"id";i:1;s:4:"slug";s:31:"1-fats-waller-blue-black-bottom";s:7:"_locale";s:2:"fr";}'),
(4182, 'transcription', 'part/blueblack', 'a:3:{s:2:"id";i:1;s:4:"slug";s:31:"1-fats-waller-blue-black-bottom";s:7:"_locale";s:2:"fr";}'),
(4183, 'transcription', 'fr/transcription/2-fats-waller-numb-fumblin', 'a:3:{s:2:"id";i:2;s:4:"slug";s:26:"2-fats-waller-numb-fumblin";s:7:"_locale";s:2:"en";}'),
(4184, 'transcription', 'part/numb_en', 'a:3:{s:2:"id";i:2;s:4:"slug";s:26:"2-fats-waller-numb-fumblin";s:7:"_locale";s:2:"en";}'),
(4185, 'transcription', 'en/transcription/2-fats-waller-numb-fumblin', 'a:3:{s:2:"id";i:2;s:4:"slug";s:26:"2-fats-waller-numb-fumblin";s:7:"_locale";s:2:"fr";}'),
(4186, 'transcription', 'part/numb', 'a:3:{s:2:"id";i:2;s:4:"slug";s:26:"2-fats-waller-numb-fumblin";s:7:"_locale";s:2:"fr";}'),
(4187, 'transcription', 'fr/transcription/3-fats-waller-love-me-or-leave-me', 'a:3:{s:2:"id";i:3;s:4:"slug";s:33:"3-fats-waller-love-me-or-leave-me";s:7:"_locale";s:2:"en";}'),
(4188, 'transcription', 'part/loveme_en', 'a:3:{s:2:"id";i:3;s:4:"slug";s:33:"3-fats-waller-love-me-or-leave-me";s:7:"_locale";s:2:"en";}'),
(4189, 'transcription', 'en/transcription/3-fats-waller-love-me-or-leave-me', 'a:3:{s:2:"id";i:3;s:4:"slug";s:33:"3-fats-waller-love-me-or-leave-me";s:7:"_locale";s:2:"fr";}'),
(4190, 'transcription', 'part/loveme', 'a:3:{s:2:"id";i:3;s:4:"slug";s:33:"3-fats-waller-love-me-or-leave-me";s:7:"_locale";s:2:"fr";}'),
(4191, 'transcription', 'fr/transcription/4-fats-waller-valentine-stomp', 'a:3:{s:2:"id";i:4;s:4:"slug";s:29:"4-fats-waller-valentine-stomp";s:7:"_locale";s:2:"en";}'),
(4192, 'transcription', 'part/valentine_en', 'a:3:{s:2:"id";i:4;s:4:"slug";s:29:"4-fats-waller-valentine-stomp";s:7:"_locale";s:2:"en";}'),
(4193, 'transcription', 'en/transcription/4-fats-waller-valentine-stomp', 'a:3:{s:2:"id";i:4;s:4:"slug";s:29:"4-fats-waller-valentine-stomp";s:7:"_locale";s:2:"fr";}'),
(4194, 'transcription', 'part/valentine', 'a:3:{s:2:"id";i:4;s:4:"slug";s:29:"4-fats-waller-valentine-stomp";s:7:"_locale";s:2:"fr";}'),
(4195, 'transcription', 'fr/transcription/5-fats-waller-ive-got-a-feeling-im-falling', 'a:3:{s:2:"id";i:5;s:4:"slug";s:42:"5-fats-waller-ive-got-a-feeling-im-falling";s:7:"_locale";s:2:"en";}'),
(4196, 'transcription', 'part/falling_en', 'a:3:{s:2:"id";i:5;s:4:"slug";s:42:"5-fats-waller-ive-got-a-feeling-im-falling";s:7:"_locale";s:2:"en";}'),
(4197, 'transcription', 'en/transcription/5-fats-waller-ive-got-a-feeling-im-falling', 'a:3:{s:2:"id";i:5;s:4:"slug";s:42:"5-fats-waller-ive-got-a-feeling-im-falling";s:7:"_locale";s:2:"fr";}'),
(4198, 'transcription', 'part/falling', 'a:3:{s:2:"id";i:5;s:4:"slug";s:42:"5-fats-waller-ive-got-a-feeling-im-falling";s:7:"_locale";s:2:"fr";}'),
(4199, 'transcription', 'fr/transcription/6-fats-waller-smashing-thirds', 'a:3:{s:2:"id";i:6;s:4:"slug";s:29:"6-fats-waller-smashing-thirds";s:7:"_locale";s:2:"en";}'),
(4200, 'transcription', 'part/smashing_en', 'a:3:{s:2:"id";i:6;s:4:"slug";s:29:"6-fats-waller-smashing-thirds";s:7:"_locale";s:2:"en";}'),
(4201, 'transcription', 'en/transcription/6-fats-waller-smashing-thirds', 'a:3:{s:2:"id";i:6;s:4:"slug";s:29:"6-fats-waller-smashing-thirds";s:7:"_locale";s:2:"fr";}'),
(4202, 'transcription', 'part/smashing', 'a:3:{s:2:"id";i:6;s:4:"slug";s:29:"6-fats-waller-smashing-thirds";s:7:"_locale";s:2:"fr";}'),
(4203, 'transcription', 'fr/transcription/7-fats-waller-turn-on-the-heat', 'a:3:{s:2:"id";i:7;s:4:"slug";s:30:"7-fats-waller-turn-on-the-heat";s:7:"_locale";s:2:"en";}'),
(4204, 'transcription', 'part/turnon_en', 'a:3:{s:2:"id";i:7;s:4:"slug";s:30:"7-fats-waller-turn-on-the-heat";s:7:"_locale";s:2:"en";}'),
(4205, 'transcription', 'en/transcription/7-fats-waller-turn-on-the-heat', 'a:3:{s:2:"id";i:7;s:4:"slug";s:30:"7-fats-waller-turn-on-the-heat";s:7:"_locale";s:2:"fr";}'),
(4206, 'transcription', 'part/turnon', 'a:3:{s:2:"id";i:7;s:4:"slug";s:30:"7-fats-waller-turn-on-the-heat";s:7:"_locale";s:2:"fr";}'),
(4207, 'transcription', 'fr/transcription/8-fats-waller-my-fate-is-in-your-hands', 'a:3:{s:2:"id";i:8;s:4:"slug";s:38:"8-fats-waller-my-fate-is-in-your-hands";s:7:"_locale";s:2:"en";}'),
(4208, 'transcription', 'part/myfate_en', 'a:3:{s:2:"id";i:8;s:4:"slug";s:38:"8-fats-waller-my-fate-is-in-your-hands";s:7:"_locale";s:2:"en";}'),
(4209, 'transcription', 'en/transcription/8-fats-waller-my-fate-is-in-your-hands', 'a:3:{s:2:"id";i:8;s:4:"slug";s:38:"8-fats-waller-my-fate-is-in-your-hands";s:7:"_locale";s:2:"fr";}'),
(4210, 'transcription', 'part/myfate', 'a:3:{s:2:"id";i:8;s:4:"slug";s:38:"8-fats-waller-my-fate-is-in-your-hands";s:7:"_locale";s:2:"fr";}'),
(4211, 'transcription', 'fr/transcription/9-fats-waller-african-ripples', 'a:3:{s:2:"id";i:9;s:4:"slug";s:29:"9-fats-waller-african-ripples";s:7:"_locale";s:2:"en";}'),
(4212, 'transcription', 'part/african_en', 'a:3:{s:2:"id";i:9;s:4:"slug";s:29:"9-fats-waller-african-ripples";s:7:"_locale";s:2:"en";}'),
(4213, 'transcription', 'en/transcription/9-fats-waller-african-ripples', 'a:3:{s:2:"id";i:9;s:4:"slug";s:29:"9-fats-waller-african-ripples";s:7:"_locale";s:2:"fr";}'),
(4214, 'transcription', 'part/african', 'a:3:{s:2:"id";i:9;s:4:"slug";s:29:"9-fats-waller-african-ripples";s:7:"_locale";s:2:"fr";}'),
(4215, 'transcription', 'fr/transcription/10-fats-waller-hallelujah', 'a:3:{s:2:"id";i:10;s:4:"slug";s:25:"10-fats-waller-hallelujah";s:7:"_locale";s:2:"en";}'),
(4216, 'transcription', 'part/hallelujah_en', 'a:3:{s:2:"id";i:10;s:4:"slug";s:25:"10-fats-waller-hallelujah";s:7:"_locale";s:2:"en";}'),
(4217, 'transcription', 'en/transcription/10-fats-waller-hallelujah', 'a:3:{s:2:"id";i:10;s:4:"slug";s:25:"10-fats-waller-hallelujah";s:7:"_locale";s:2:"fr";}'),
(4218, 'transcription', 'part/hallelujah', 'a:3:{s:2:"id";i:10;s:4:"slug";s:25:"10-fats-waller-hallelujah";s:7:"_locale";s:2:"fr";}'),
(4219, 'transcription', 'fr/transcription/11-fats-waller-california-here-i-come', 'a:3:{s:2:"id";i:11;s:4:"slug";s:37:"11-fats-waller-california-here-i-come";s:7:"_locale";s:2:"en";}'),
(4220, 'transcription', 'part/california_en', 'a:3:{s:2:"id";i:11;s:4:"slug";s:37:"11-fats-waller-california-here-i-come";s:7:"_locale";s:2:"en";}'),
(4221, 'transcription', 'en/transcription/11-fats-waller-california-here-i-come', 'a:3:{s:2:"id";i:11;s:4:"slug";s:37:"11-fats-waller-california-here-i-come";s:7:"_locale";s:2:"fr";}'),
(4222, 'transcription', 'part/california', 'a:3:{s:2:"id";i:11;s:4:"slug";s:37:"11-fats-waller-california-here-i-come";s:7:"_locale";s:2:"fr";}'),
(4223, 'transcription', 'fr/transcription/12-fats-waller-youre-the-top', 'a:3:{s:2:"id";i:12;s:4:"slug";s:28:"12-fats-waller-youre-the-top";s:7:"_locale";s:2:"en";}'),
(4224, 'transcription', 'part/top_en', 'a:3:{s:2:"id";i:12;s:4:"slug";s:28:"12-fats-waller-youre-the-top";s:7:"_locale";s:2:"en";}'),
(4225, 'transcription', 'en/transcription/12-fats-waller-youre-the-top', 'a:3:{s:2:"id";i:12;s:4:"slug";s:28:"12-fats-waller-youre-the-top";s:7:"_locale";s:2:"fr";}'),
(4226, 'transcription', 'part/top', 'a:3:{s:2:"id";i:12;s:4:"slug";s:28:"12-fats-waller-youre-the-top";s:7:"_locale";s:2:"fr";}'),
(4227, 'transcription', 'fr/transcription/13-fats-waller-because-of-once-upon-a-time', 'a:3:{s:2:"id";i:13;s:4:"slug";s:42:"13-fats-waller-because-of-once-upon-a-time";s:7:"_locale";s:2:"en";}'),
(4228, 'transcription', 'part/onceupon_en', 'a:3:{s:2:"id";i:13;s:4:"slug";s:42:"13-fats-waller-because-of-once-upon-a-time";s:7:"_locale";s:2:"en";}'),
(4229, 'transcription', 'en/transcription/13-fats-waller-because-of-once-upon-a-time', 'a:3:{s:2:"id";i:13;s:4:"slug";s:42:"13-fats-waller-because-of-once-upon-a-time";s:7:"_locale";s:2:"fr";}'),
(4230, 'transcription', 'part/onceupon', 'a:3:{s:2:"id";i:13;s:4:"slug";s:42:"13-fats-waller-because-of-once-upon-a-time";s:7:"_locale";s:2:"fr";}'),
(4231, 'transcription', 'fr/transcription/14-fats-waller-swaltzing-with-faust', 'a:3:{s:2:"id";i:14;s:4:"slug";s:35:"14-fats-waller-swaltzing-with-faust";s:7:"_locale";s:2:"en";}'),
(4232, 'transcription', 'part/faust_en', 'a:3:{s:2:"id";i:14;s:4:"slug";s:35:"14-fats-waller-swaltzing-with-faust";s:7:"_locale";s:2:"en";}'),
(4233, 'transcription', 'en/transcription/14-fats-waller-swaltzing-with-faust', 'a:3:{s:2:"id";i:14;s:4:"slug";s:35:"14-fats-waller-swaltzing-with-faust";s:7:"_locale";s:2:"fr";}'),
(4234, 'transcription', 'part/faust', 'a:3:{s:2:"id";i:14;s:4:"slug";s:35:"14-fats-waller-swaltzing-with-faust";s:7:"_locale";s:2:"fr";}'),
(4235, 'transcription', 'fr/transcription/15-fats-waller-intermezzo', 'a:3:{s:2:"id";i:15;s:4:"slug";s:25:"15-fats-waller-intermezzo";s:7:"_locale";s:2:"en";}'),
(4236, 'transcription', 'part/intermezzo_en', 'a:3:{s:2:"id";i:15;s:4:"slug";s:25:"15-fats-waller-intermezzo";s:7:"_locale";s:2:"en";}'),
(4237, 'transcription', 'en/transcription/15-fats-waller-intermezzo', 'a:3:{s:2:"id";i:15;s:4:"slug";s:25:"15-fats-waller-intermezzo";s:7:"_locale";s:2:"fr";}'),
(4238, 'transcription', 'part/intermezzo', 'a:3:{s:2:"id";i:15;s:4:"slug";s:25:"15-fats-waller-intermezzo";s:7:"_locale";s:2:"fr";}'),
(4239, 'transcription', 'fr/transcription/16-fats-waller-carolina-shout', 'a:3:{s:2:"id";i:16;s:4:"slug";s:29:"16-fats-waller-carolina-shout";s:7:"_locale";s:2:"en";}'),
(4240, 'transcription', 'part/carolina_en', 'a:3:{s:2:"id";i:16;s:4:"slug";s:29:"16-fats-waller-carolina-shout";s:7:"_locale";s:2:"en";}'),
(4241, 'transcription', 'en/transcription/16-fats-waller-carolina-shout', 'a:3:{s:2:"id";i:16;s:4:"slug";s:29:"16-fats-waller-carolina-shout";s:7:"_locale";s:2:"fr";}'),
(4242, 'transcription', 'part/carolina', 'a:3:{s:2:"id";i:16;s:4:"slug";s:29:"16-fats-waller-carolina-shout";s:7:"_locale";s:2:"fr";}'),
(4243, 'transcription', 'fr/transcription/17-fats-waller-honeysuckle-rose', 'a:3:{s:2:"id";i:17;s:4:"slug";s:31:"17-fats-waller-honeysuckle-rose";s:7:"_locale";s:2:"en";}'),
(4244, 'transcription', 'part/honeysuckle_en', 'a:3:{s:2:"id";i:17;s:4:"slug";s:31:"17-fats-waller-honeysuckle-rose";s:7:"_locale";s:2:"en";}'),
(4245, 'transcription', 'en/transcription/17-fats-waller-honeysuckle-rose', 'a:3:{s:2:"id";i:17;s:4:"slug";s:31:"17-fats-waller-honeysuckle-rose";s:7:"_locale";s:2:"fr";}'),
(4246, 'transcription', 'part/honeysuckle', 'a:3:{s:2:"id";i:17;s:4:"slug";s:31:"17-fats-waller-honeysuckle-rose";s:7:"_locale";s:2:"fr";}'),
(4247, 'transcription', 'fr/transcription/18-fats-waller-muscle-shoals-blues', 'a:3:{s:2:"id";i:18;s:4:"slug";s:34:"18-fats-waller-muscle-shoals-blues";s:7:"_locale";s:2:"en";}'),
(4248, 'transcription', 'part/muscleshoals_en', 'a:3:{s:2:"id";i:18;s:4:"slug";s:34:"18-fats-waller-muscle-shoals-blues";s:7:"_locale";s:2:"en";}'),
(4249, 'transcription', 'en/transcription/18-fats-waller-muscle-shoals-blues', 'a:3:{s:2:"id";i:18;s:4:"slug";s:34:"18-fats-waller-muscle-shoals-blues";s:7:"_locale";s:2:"fr";}'),
(4250, 'transcription', 'part/muscleshoals', 'a:3:{s:2:"id";i:18;s:4:"slug";s:34:"18-fats-waller-muscle-shoals-blues";s:7:"_locale";s:2:"fr";}'),
(4251, 'transcription', 'fr/transcription/19-fats-waller-birmingham-blues', 'a:3:{s:2:"id";i:19;s:4:"slug";s:31:"19-fats-waller-birmingham-blues";s:7:"_locale";s:2:"en";}'),
(4252, 'transcription', 'part/birmingham_en', 'a:3:{s:2:"id";i:19;s:4:"slug";s:31:"19-fats-waller-birmingham-blues";s:7:"_locale";s:2:"en";}'),
(4253, 'transcription', 'en/transcription/19-fats-waller-birmingham-blues', 'a:3:{s:2:"id";i:19;s:4:"slug";s:31:"19-fats-waller-birmingham-blues";s:7:"_locale";s:2:"fr";}'),
(4254, 'transcription', 'part/birmingham', 'a:3:{s:2:"id";i:19;s:4:"slug";s:31:"19-fats-waller-birmingham-blues";s:7:"_locale";s:2:"fr";}'),
(4255, 'transcription', 'fr/transcription/20-fats-waller-handful-of-keys', 'a:3:{s:2:"id";i:20;s:4:"slug";s:30:"20-fats-waller-handful-of-keys";s:7:"_locale";s:2:"en";}'),
(4256, 'transcription', 'part/handful_en', 'a:3:{s:2:"id";i:20;s:4:"slug";s:30:"20-fats-waller-handful-of-keys";s:7:"_locale";s:2:"en";}'),
(4257, 'transcription', 'en/transcription/20-fats-waller-handful-of-keys', 'a:3:{s:2:"id";i:20;s:4:"slug";s:30:"20-fats-waller-handful-of-keys";s:7:"_locale";s:2:"fr";}'),
(4258, 'transcription', 'part/handful', 'a:3:{s:2:"id";i:20;s:4:"slug";s:30:"20-fats-waller-handful-of-keys";s:7:"_locale";s:2:"fr";}'),
(4259, 'transcription', 'fr/transcription/21-fats-waller-baby-oh-where-can-you-be-', 'a:3:{s:2:"id";i:21;s:4:"slug";s:40:"21-fats-waller-baby-oh-where-can-you-be-";s:7:"_locale";s:2:"en";}'),
(4260, 'transcription', 'part/babyoh_en', 'a:3:{s:2:"id";i:21;s:4:"slug";s:40:"21-fats-waller-baby-oh-where-can-you-be-";s:7:"_locale";s:2:"en";}'),
(4261, 'transcription', 'en/transcription/21-fats-waller-baby-oh-where-can-you-be-', 'a:3:{s:2:"id";i:21;s:4:"slug";s:40:"21-fats-waller-baby-oh-where-can-you-be-";s:7:"_locale";s:2:"fr";}'),
(4262, 'transcription', 'part/babyoh', 'a:3:{s:2:"id";i:21;s:4:"slug";s:40:"21-fats-waller-baby-oh-where-can-you-be-";s:7:"_locale";s:2:"fr";}'),
(4263, 'transcription', 'fr/transcription/22-fats-waller-sweet-savannah-sue', 'a:3:{s:2:"id";i:22;s:4:"slug";s:33:"22-fats-waller-sweet-savannah-sue";s:7:"_locale";s:2:"en";}'),
(4264, 'transcription', 'part/savannah_en', 'a:3:{s:2:"id";i:22;s:4:"slug";s:33:"22-fats-waller-sweet-savannah-sue";s:7:"_locale";s:2:"en";}'),
(4265, 'transcription', 'en/transcription/22-fats-waller-sweet-savannah-sue', 'a:3:{s:2:"id";i:22;s:4:"slug";s:33:"22-fats-waller-sweet-savannah-sue";s:7:"_locale";s:2:"fr";}'),
(4266, 'transcription', 'part/savannah', 'a:3:{s:2:"id";i:22;s:4:"slug";s:33:"22-fats-waller-sweet-savannah-sue";s:7:"_locale";s:2:"fr";}'),
(4267, 'transcription', 'fr/transcription/23-fats-waller-vipers-drag', 'a:3:{s:2:"id";i:23;s:4:"slug";s:26:"23-fats-waller-vipers-drag";s:7:"_locale";s:2:"en";}'),
(4268, 'transcription', 'part/viper_en', 'a:3:{s:2:"id";i:23;s:4:"slug";s:26:"23-fats-waller-vipers-drag";s:7:"_locale";s:2:"en";}'),
(4269, 'transcription', 'en/transcription/23-fats-waller-vipers-drag', 'a:3:{s:2:"id";i:23;s:4:"slug";s:26:"23-fats-waller-vipers-drag";s:7:"_locale";s:2:"fr";}'),
(4270, 'transcription', 'part/viper', 'a:3:{s:2:"id";i:23;s:4:"slug";s:26:"23-fats-waller-vipers-drag";s:7:"_locale";s:2:"fr";}'),
(4271, 'transcription', 'fr/transcription/24-fats-waller-alligator-crawl', 'a:3:{s:2:"id";i:24;s:4:"slug";s:30:"24-fats-waller-alligator-crawl";s:7:"_locale";s:2:"en";}'),
(4272, 'transcription', 'part/alligator35_en', 'a:3:{s:2:"id";i:24;s:4:"slug";s:30:"24-fats-waller-alligator-crawl";s:7:"_locale";s:2:"en";}'),
(4273, 'transcription', 'en/transcription/24-fats-waller-alligator-crawl', 'a:3:{s:2:"id";i:24;s:4:"slug";s:30:"24-fats-waller-alligator-crawl";s:7:"_locale";s:2:"fr";}'),
(4274, 'transcription', 'part/alligator35', 'a:3:{s:2:"id";i:24;s:4:"slug";s:30:"24-fats-waller-alligator-crawl";s:7:"_locale";s:2:"fr";}'),
(4275, 'transcription', 'fr/transcription/25-fats-waller-keepin-out-of-mischief-now', 'a:3:{s:2:"id";i:25;s:4:"slug";s:41:"25-fats-waller-keepin-out-of-mischief-now";s:7:"_locale";s:2:"en";}'),
(4276, 'transcription', 'part/mischief_en', 'a:3:{s:2:"id";i:25;s:4:"slug";s:41:"25-fats-waller-keepin-out-of-mischief-now";s:7:"_locale";s:2:"en";}'),
(4277, 'transcription', 'en/transcription/25-fats-waller-keepin-out-of-mischief-now', 'a:3:{s:2:"id";i:25;s:4:"slug";s:41:"25-fats-waller-keepin-out-of-mischief-now";s:7:"_locale";s:2:"fr";}'),
(4278, 'transcription', 'part/mischief', 'a:3:{s:2:"id";i:25;s:4:"slug";s:41:"25-fats-waller-keepin-out-of-mischief-now";s:7:"_locale";s:2:"fr";}'),
(4279, 'transcription', 'fr/transcription/26-fats-waller-tea-for-two', 'a:3:{s:2:"id";i:26;s:4:"slug";s:26:"26-fats-waller-tea-for-two";s:7:"_locale";s:2:"en";}'),
(4280, 'transcription', 'part/teafortwo_en', 'a:3:{s:2:"id";i:26;s:4:"slug";s:26:"26-fats-waller-tea-for-two";s:7:"_locale";s:2:"en";}'),
(4281, 'transcription', 'en/transcription/26-fats-waller-tea-for-two', 'a:3:{s:2:"id";i:26;s:4:"slug";s:26:"26-fats-waller-tea-for-two";s:7:"_locale";s:2:"fr";}'),
(4282, 'transcription', 'part/teafortwo', 'a:3:{s:2:"id";i:26;s:4:"slug";s:26:"26-fats-waller-tea-for-two";s:7:"_locale";s:2:"fr";}'),
(4283, 'transcription', 'fr/transcription/27-fats-waller-london-suite-i-piccadilly', 'a:3:{s:2:"id";i:27;s:4:"slug";s:40:"27-fats-waller-london-suite-i-piccadilly";s:7:"_locale";s:2:"en";}'),
(4284, 'transcription', 'part/piccadilly_en', 'a:3:{s:2:"id";i:27;s:4:"slug";s:40:"27-fats-waller-london-suite-i-piccadilly";s:7:"_locale";s:2:"en";}'),
(4285, 'transcription', 'en/transcription/27-fats-waller-london-suite-i-piccadilly', 'a:3:{s:2:"id";i:27;s:4:"slug";s:40:"27-fats-waller-london-suite-i-piccadilly";s:7:"_locale";s:2:"fr";}'),
(4286, 'transcription', 'part/piccadilly', 'a:3:{s:2:"id";i:27;s:4:"slug";s:40:"27-fats-waller-london-suite-i-piccadilly";s:7:"_locale";s:2:"fr";}'),
(4287, 'transcription', 'fr/transcription/28-fats-waller-london-suite-ii-chelsea', 'a:3:{s:2:"id";i:28;s:4:"slug";s:38:"28-fats-waller-london-suite-ii-chelsea";s:7:"_locale";s:2:"en";}'),
(4288, 'transcription', 'part/chelsea_en', 'a:3:{s:2:"id";i:28;s:4:"slug";s:38:"28-fats-waller-london-suite-ii-chelsea";s:7:"_locale";s:2:"en";}'),
(4289, 'transcription', 'en/transcription/28-fats-waller-london-suite-ii-chelsea', 'a:3:{s:2:"id";i:28;s:4:"slug";s:38:"28-fats-waller-london-suite-ii-chelsea";s:7:"_locale";s:2:"fr";}'),
(4290, 'transcription', 'part/chelsea', 'a:3:{s:2:"id";i:28;s:4:"slug";s:38:"28-fats-waller-london-suite-ii-chelsea";s:7:"_locale";s:2:"fr";}'),
(4291, 'transcription', 'fr/transcription/29-fats-waller-london-suite-iii-soho', 'a:3:{s:2:"id";i:29;s:4:"slug";s:36:"29-fats-waller-london-suite-iii-soho";s:7:"_locale";s:2:"en";}'),
(4292, 'transcription', 'part/soho_en', 'a:3:{s:2:"id";i:29;s:4:"slug";s:36:"29-fats-waller-london-suite-iii-soho";s:7:"_locale";s:2:"en";}'),
(4293, 'transcription', 'en/transcription/29-fats-waller-london-suite-iii-soho', 'a:3:{s:2:"id";i:29;s:4:"slug";s:36:"29-fats-waller-london-suite-iii-soho";s:7:"_locale";s:2:"fr";}'),
(4294, 'transcription', 'part/soho', 'a:3:{s:2:"id";i:29;s:4:"slug";s:36:"29-fats-waller-london-suite-iii-soho";s:7:"_locale";s:2:"fr";}'),
(4295, 'transcription', 'fr/transcription/30-fats-waller-london-suite-iv-bond-street', 'a:3:{s:2:"id";i:30;s:4:"slug";s:42:"30-fats-waller-london-suite-iv-bond-street";s:7:"_locale";s:2:"en";}'),
(4296, 'transcription', 'part/bondstreet_en', 'a:3:{s:2:"id";i:30;s:4:"slug";s:42:"30-fats-waller-london-suite-iv-bond-street";s:7:"_locale";s:2:"en";}'),
(4297, 'transcription', 'en/transcription/30-fats-waller-london-suite-iv-bond-street', 'a:3:{s:2:"id";i:30;s:4:"slug";s:42:"30-fats-waller-london-suite-iv-bond-street";s:7:"_locale";s:2:"fr";}'),
(4298, 'transcription', 'part/bondstreet', 'a:3:{s:2:"id";i:30;s:4:"slug";s:42:"30-fats-waller-london-suite-iv-bond-street";s:7:"_locale";s:2:"fr";}'),
(4299, 'transcription', 'fr/transcription/31-fats-waller-london-suite-v-limehouse', 'a:3:{s:2:"id";i:31;s:4:"slug";s:39:"31-fats-waller-london-suite-v-limehouse";s:7:"_locale";s:2:"en";}'),
(4300, 'transcription', 'part/limehouse_en', 'a:3:{s:2:"id";i:31;s:4:"slug";s:39:"31-fats-waller-london-suite-v-limehouse";s:7:"_locale";s:2:"en";}'),
(4301, 'transcription', 'en/transcription/31-fats-waller-london-suite-v-limehouse', 'a:3:{s:2:"id";i:31;s:4:"slug";s:39:"31-fats-waller-london-suite-v-limehouse";s:7:"_locale";s:2:"fr";}'),
(4302, 'transcription', 'part/limehouse', 'a:3:{s:2:"id";i:31;s:4:"slug";s:39:"31-fats-waller-london-suite-v-limehouse";s:7:"_locale";s:2:"fr";}'),
(4303, 'transcription', 'fr/transcription/32-fats-waller-london-suite-vi-whitechapel', 'a:3:{s:2:"id";i:32;s:4:"slug";s:42:"32-fats-waller-london-suite-vi-whitechapel";s:7:"_locale";s:2:"en";}'),
(4304, 'transcription', 'part/whitechapel_en', 'a:3:{s:2:"id";i:32;s:4:"slug";s:42:"32-fats-waller-london-suite-vi-whitechapel";s:7:"_locale";s:2:"en";}'),
(4305, 'transcription', 'en/transcription/32-fats-waller-london-suite-vi-whitechapel', 'a:3:{s:2:"id";i:32;s:4:"slug";s:42:"32-fats-waller-london-suite-vi-whitechapel";s:7:"_locale";s:2:"fr";}'),
(4306, 'transcription', 'part/whitechapel', 'a:3:{s:2:"id";i:32;s:4:"slug";s:42:"32-fats-waller-london-suite-vi-whitechapel";s:7:"_locale";s:2:"fr";}'),
(4307, 'transcription', 'fr/transcription/33-fats-waller-rockin-chair', 'a:3:{s:2:"id";i:33;s:4:"slug";s:27:"33-fats-waller-rockin-chair";s:7:"_locale";s:2:"en";}'),
(4308, 'transcription', 'part/rockin_en', 'a:3:{s:2:"id";i:33;s:4:"slug";s:27:"33-fats-waller-rockin-chair";s:7:"_locale";s:2:"en";}'),
(4309, 'transcription', 'en/transcription/33-fats-waller-rockin-chair', 'a:3:{s:2:"id";i:33;s:4:"slug";s:27:"33-fats-waller-rockin-chair";s:7:"_locale";s:2:"fr";}'),
(4310, 'transcription', 'part/rockin', 'a:3:{s:2:"id";i:33;s:4:"slug";s:27:"33-fats-waller-rockin-chair";s:7:"_locale";s:2:"fr";}'),
(4311, 'transcription', 'fr/transcription/34-fats-waller-ring-dem-bells', 'a:3:{s:2:"id";i:34;s:4:"slug";s:29:"34-fats-waller-ring-dem-bells";s:7:"_locale";s:2:"en";}'),
(4312, 'transcription', 'part/ringdem_en', 'a:3:{s:2:"id";i:34;s:4:"slug";s:29:"34-fats-waller-ring-dem-bells";s:7:"_locale";s:2:"en";}'),
(4313, 'transcription', 'en/transcription/34-fats-waller-ring-dem-bells', 'a:3:{s:2:"id";i:34;s:4:"slug";s:29:"34-fats-waller-ring-dem-bells";s:7:"_locale";s:2:"fr";}'),
(4314, 'transcription', 'part/ringdem', 'a:3:{s:2:"id";i:34;s:4:"slug";s:29:"34-fats-waller-ring-dem-bells";s:7:"_locale";s:2:"fr";}'),
(4315, 'transcription', 'fr/transcription/35-fats-waller-aint-misbehavin', 'a:3:{s:2:"id";i:35;s:4:"slug";s:30:"35-fats-waller-aint-misbehavin";s:7:"_locale";s:2:"en";}'),
(4316, 'transcription', 'part/misbehavin_en', 'a:3:{s:2:"id";i:35;s:4:"slug";s:30:"35-fats-waller-aint-misbehavin";s:7:"_locale";s:2:"en";}'),
(4317, 'transcription', 'en/transcription/35-fats-waller-aint-misbehavin', 'a:3:{s:2:"id";i:35;s:4:"slug";s:30:"35-fats-waller-aint-misbehavin";s:7:"_locale";s:2:"fr";}'),
(4318, 'transcription', 'part/misbehavin', 'a:3:{s:2:"id";i:35;s:4:"slug";s:30:"35-fats-waller-aint-misbehavin";s:7:"_locale";s:2:"fr";}'),
(4319, 'transcription', 'fr/transcription/36-fats-waller-gladyse', 'a:3:{s:2:"id";i:36;s:4:"slug";s:22:"36-fats-waller-gladyse";s:7:"_locale";s:2:"en";}'),
(4320, 'transcription', 'part/gladyse_en', 'a:3:{s:2:"id";i:36;s:4:"slug";s:22:"36-fats-waller-gladyse";s:7:"_locale";s:2:"en";}'),
(4321, 'transcription', 'en/transcription/36-fats-waller-gladyse', 'a:3:{s:2:"id";i:36;s:4:"slug";s:22:"36-fats-waller-gladyse";s:7:"_locale";s:2:"fr";}'),
(4322, 'transcription', 'part/gladyse', 'a:3:{s:2:"id";i:36;s:4:"slug";s:22:"36-fats-waller-gladyse";s:7:"_locale";s:2:"fr";}'),
(4323, 'transcription', 'fr/transcription/37-fats-waller-waiting-at-the-end-of-the-road', 'a:3:{s:2:"id";i:37;s:4:"slug";s:45:"37-fats-waller-waiting-at-the-end-of-the-road";s:7:"_locale";s:2:"en";}'),
(4324, 'transcription', 'part/waiting_en', 'a:3:{s:2:"id";i:37;s:4:"slug";s:45:"37-fats-waller-waiting-at-the-end-of-the-road";s:7:"_locale";s:2:"en";}'),
(4325, 'transcription', 'en/transcription/37-fats-waller-waiting-at-the-end-of-the-road', 'a:3:{s:2:"id";i:37;s:4:"slug";s:45:"37-fats-waller-waiting-at-the-end-of-the-road";s:7:"_locale";s:2:"fr";}'),
(4326, 'transcription', 'part/waiting', 'a:3:{s:2:"id";i:37;s:4:"slug";s:45:"37-fats-waller-waiting-at-the-end-of-the-road";s:7:"_locale";s:2:"fr";}'),
(4327, 'transcription', 'fr/transcription/38-fats-waller-goin-about', 'a:3:{s:2:"id";i:38;s:4:"slug";s:25:"38-fats-waller-goin-about";s:7:"_locale";s:2:"en";}'),
(4328, 'transcription', 'part/goinabout_en', 'a:3:{s:2:"id";i:38;s:4:"slug";s:25:"38-fats-waller-goin-about";s:7:"_locale";s:2:"en";}'),
(4329, 'transcription', 'en/transcription/38-fats-waller-goin-about', 'a:3:{s:2:"id";i:38;s:4:"slug";s:25:"38-fats-waller-goin-about";s:7:"_locale";s:2:"fr";}'),
(4330, 'transcription', 'part/goinabout', 'a:3:{s:2:"id";i:38;s:4:"slug";s:25:"38-fats-waller-goin-about";s:7:"_locale";s:2:"fr";}'),
(4331, 'transcription', 'fr/transcription/39-fats-waller-my-feelings-are-hurt', 'a:3:{s:2:"id";i:39;s:4:"slug";s:35:"39-fats-waller-my-feelings-are-hurt";s:7:"_locale";s:2:"en";}'),
(4332, 'transcription', 'part/myfeelings_en', 'a:3:{s:2:"id";i:39;s:4:"slug";s:35:"39-fats-waller-my-feelings-are-hurt";s:7:"_locale";s:2:"en";}'),
(4333, 'transcription', 'en/transcription/39-fats-waller-my-feelings-are-hurt', 'a:3:{s:2:"id";i:39;s:4:"slug";s:35:"39-fats-waller-my-feelings-are-hurt";s:7:"_locale";s:2:"fr";}'),
(4334, 'transcription', 'part/myfeelings', 'a:3:{s:2:"id";i:39;s:4:"slug";s:35:"39-fats-waller-my-feelings-are-hurt";s:7:"_locale";s:2:"fr";}'),
(4335, 'transcription', 'fr/transcription/40-fats-waller-clothesline-ballet', 'a:3:{s:2:"id";i:40;s:4:"slug";s:33:"40-fats-waller-clothesline-ballet";s:7:"_locale";s:2:"en";}'),
(4336, 'transcription', 'part/clothesline_en', 'a:3:{s:2:"id";i:40;s:4:"slug";s:33:"40-fats-waller-clothesline-ballet";s:7:"_locale";s:2:"en";}'),
(4337, 'transcription', 'en/transcription/40-fats-waller-clothesline-ballet', 'a:3:{s:2:"id";i:40;s:4:"slug";s:33:"40-fats-waller-clothesline-ballet";s:7:"_locale";s:2:"fr";}'),
(4338, 'transcription', 'part/clothesline', 'a:3:{s:2:"id";i:40;s:4:"slug";s:33:"40-fats-waller-clothesline-ballet";s:7:"_locale";s:2:"fr";}'),
(4339, 'transcription', 'fr/transcription/41-fats-waller-alligator-crawl', 'a:3:{s:2:"id";i:41;s:4:"slug";s:30:"41-fats-waller-alligator-crawl";s:7:"_locale";s:2:"en";}'),
(4340, 'transcription', 'part/alligator34_en', 'a:3:{s:2:"id";i:41;s:4:"slug";s:30:"41-fats-waller-alligator-crawl";s:7:"_locale";s:2:"en";}'),
(4341, 'transcription', 'en/transcription/41-fats-waller-alligator-crawl', 'a:3:{s:2:"id";i:41;s:4:"slug";s:30:"41-fats-waller-alligator-crawl";s:7:"_locale";s:2:"fr";}'),
(4342, 'transcription', 'part/alligator34', 'a:3:{s:2:"id";i:41;s:4:"slug";s:30:"41-fats-waller-alligator-crawl";s:7:"_locale";s:2:"fr";}'),
(4343, 'transcription', 'fr/transcription/42-fats-waller-e-flat-blues', 'a:3:{s:2:"id";i:42;s:4:"slug";s:27:"42-fats-waller-e-flat-blues";s:7:"_locale";s:2:"en";}'),
(4344, 'transcription', 'part/flatblues_en', 'a:3:{s:2:"id";i:42;s:4:"slug";s:27:"42-fats-waller-e-flat-blues";s:7:"_locale";s:2:"en";}'),
(4345, 'transcription', 'en/transcription/42-fats-waller-e-flat-blues', 'a:3:{s:2:"id";i:42;s:4:"slug";s:27:"42-fats-waller-e-flat-blues";s:7:"_locale";s:2:"fr";}'),
(4346, 'transcription', 'part/flatblues', 'a:3:{s:2:"id";i:42;s:4:"slug";s:27:"42-fats-waller-e-flat-blues";s:7:"_locale";s:2:"fr";}'),
(4347, 'transcription', 'fr/transcription/43-fats-waller-zonky', 'a:3:{s:2:"id";i:43;s:4:"slug";s:20:"43-fats-waller-zonky";s:7:"_locale";s:2:"en";}'),
(4348, 'transcription', 'part/zonky_en', 'a:3:{s:2:"id";i:43;s:4:"slug";s:20:"43-fats-waller-zonky";s:7:"_locale";s:2:"en";}'),
(4349, 'transcription', 'en/transcription/43-fats-waller-zonky', 'a:3:{s:2:"id";i:43;s:4:"slug";s:20:"43-fats-waller-zonky";s:7:"_locale";s:2:"fr";}'),
(4350, 'transcription', 'part/zonky', 'a:3:{s:2:"id";i:43;s:4:"slug";s:20:"43-fats-waller-zonky";s:7:"_locale";s:2:"fr";}'),
(4351, 'transcription', 'fr/transcription/44-fats-waller-russian-fantasy', 'a:3:{s:2:"id";i:44;s:4:"slug";s:30:"44-fats-waller-russian-fantasy";s:7:"_locale";s:2:"en";}'),
(4352, 'transcription', 'part/fantasy_en', 'a:3:{s:2:"id";i:44;s:4:"slug";s:30:"44-fats-waller-russian-fantasy";s:7:"_locale";s:2:"en";}'),
(4353, 'transcription', 'en/transcription/44-fats-waller-russian-fantasy', 'a:3:{s:2:"id";i:44;s:4:"slug";s:30:"44-fats-waller-russian-fantasy";s:7:"_locale";s:2:"fr";}'),
(4354, 'transcription', 'part/fantasy', 'a:3:{s:2:"id";i:44;s:4:"slug";s:30:"44-fats-waller-russian-fantasy";s:7:"_locale";s:2:"fr";}'),
(4355, 'transcription', 'fr/transcription/45-fats-waller-basin-street-blues', 'a:3:{s:2:"id";i:45;s:4:"slug";s:33:"45-fats-waller-basin-street-blues";s:7:"_locale";s:2:"en";}'),
(4356, 'transcription', 'part/basin_en', 'a:3:{s:2:"id";i:45;s:4:"slug";s:33:"45-fats-waller-basin-street-blues";s:7:"_locale";s:2:"en";}'),
(4357, 'transcription', 'en/transcription/45-fats-waller-basin-street-blues', 'a:3:{s:2:"id";i:45;s:4:"slug";s:33:"45-fats-waller-basin-street-blues";s:7:"_locale";s:2:"fr";}'),
(4358, 'transcription', 'part/basin', 'a:3:{s:2:"id";i:45;s:4:"slug";s:33:"45-fats-waller-basin-street-blues";s:7:"_locale";s:2:"fr";}'),
(4359, 'transcription', 'fr/transcription/46-fats-waller-stardust', 'a:3:{s:2:"id";i:46;s:4:"slug";s:23:"46-fats-waller-stardust";s:7:"_locale";s:2:"en";}'),
(4360, 'transcription', 'part/stardust_en', 'a:3:{s:2:"id";i:46;s:4:"slug";s:23:"46-fats-waller-stardust";s:7:"_locale";s:2:"en";}'),
(4361, 'transcription', 'en/transcription/46-fats-waller-stardust', 'a:3:{s:2:"id";i:46;s:4:"slug";s:23:"46-fats-waller-stardust";s:7:"_locale";s:2:"fr";}'),
(4362, 'transcription', 'part/stardust', 'a:3:{s:2:"id";i:46;s:4:"slug";s:23:"46-fats-waller-stardust";s:7:"_locale";s:2:"fr";}'),
(4363, 'transcription', 'fr/transcription/47-fats-waller-i-aint-got-nobody', 'a:3:{s:2:"id";i:47;s:4:"slug";s:32:"47-fats-waller-i-aint-got-nobody";s:7:"_locale";s:2:"en";}'),
(4364, 'transcription', 'part/nobody_en', 'a:3:{s:2:"id";i:47;s:4:"slug";s:32:"47-fats-waller-i-aint-got-nobody";s:7:"_locale";s:2:"en";}'),
(4365, 'transcription', 'en/transcription/47-fats-waller-i-aint-got-nobody', 'a:3:{s:2:"id";i:47;s:4:"slug";s:32:"47-fats-waller-i-aint-got-nobody";s:7:"_locale";s:2:"fr";}'),
(4366, 'transcription', 'part/nobody', 'a:3:{s:2:"id";i:47;s:4:"slug";s:32:"47-fats-waller-i-aint-got-nobody";s:7:"_locale";s:2:"fr";}'),
(4367, 'transcription', 'fr/transcription/48-fats-waller-hallelujah-yacht-club-version', 'a:3:{s:2:"id";i:48;s:4:"slug";s:44:"48-fats-waller-hallelujah-yacht-club-version";s:7:"_locale";s:2:"en";}'),
(4368, 'transcription', 'part/hallelujah38_en', 'a:3:{s:2:"id";i:48;s:4:"slug";s:44:"48-fats-waller-hallelujah-yacht-club-version";s:7:"_locale";s:2:"en";}'),
(4369, 'transcription', 'en/transcription/48-fats-waller-hallelujah-yacht-club-version', 'a:3:{s:2:"id";i:48;s:4:"slug";s:44:"48-fats-waller-hallelujah-yacht-club-version";s:7:"_locale";s:2:"fr";}'),
(4370, 'transcription', 'part/hallelujah38', 'a:3:{s:2:"id";i:48;s:4:"slug";s:44:"48-fats-waller-hallelujah-yacht-club-version";s:7:"_locale";s:2:"fr";}'),
(4371, 'transcription', 'fr/transcription/49-fats-waller-saint-louis-blues', 'a:3:{s:2:"id";i:49;s:4:"slug";s:32:"49-fats-waller-saint-louis-blues";s:7:"_locale";s:2:"en";}'),
(4372, 'transcription', 'part/louisblues_en', 'a:3:{s:2:"id";i:49;s:4:"slug";s:32:"49-fats-waller-saint-louis-blues";s:7:"_locale";s:2:"en";}'),
(4373, 'transcription', 'en/transcription/49-fats-waller-saint-louis-blues', 'a:3:{s:2:"id";i:49;s:4:"slug";s:32:"49-fats-waller-saint-louis-blues";s:7:"_locale";s:2:"fr";}'),
(4374, 'transcription', 'part/louisblues', 'a:3:{s:2:"id";i:49;s:4:"slug";s:32:"49-fats-waller-saint-louis-blues";s:7:"_locale";s:2:"fr";}'),
(4375, 'transcription', 'fr/transcription/50-fats-waller-then-youll-remember-me', 'a:3:{s:2:"id";i:50;s:4:"slug";s:37:"50-fats-waller-then-youll-remember-me";s:7:"_locale";s:2:"en";}'),
(4376, 'transcription', 'part/remember_en', 'a:3:{s:2:"id";i:50;s:4:"slug";s:37:"50-fats-waller-then-youll-remember-me";s:7:"_locale";s:2:"en";}'),
(4377, 'transcription', 'en/transcription/50-fats-waller-then-youll-remember-me', 'a:3:{s:2:"id";i:50;s:4:"slug";s:37:"50-fats-waller-then-youll-remember-me";s:7:"_locale";s:2:"fr";}'),
(4378, 'transcription', 'part/remember', 'a:3:{s:2:"id";i:50;s:4:"slug";s:37:"50-fats-waller-then-youll-remember-me";s:7:"_locale";s:2:"fr";}'),
(4379, 'transcription', 'fr/transcription/51-fats-waller-georgia-on-my-mind', 'a:3:{s:2:"id";i:51;s:4:"slug";s:33:"51-fats-waller-georgia-on-my-mind";s:7:"_locale";s:2:"en";}'),
(4380, 'transcription', 'part/georgia_en', 'a:3:{s:2:"id";i:51;s:4:"slug";s:33:"51-fats-waller-georgia-on-my-mind";s:7:"_locale";s:2:"en";}'),
(4381, 'transcription', 'en/transcription/51-fats-waller-georgia-on-my-mind', 'a:3:{s:2:"id";i:51;s:4:"slug";s:33:"51-fats-waller-georgia-on-my-mind";s:7:"_locale";s:2:"fr";}'),
(4382, 'transcription', 'part/georgia', 'a:3:{s:2:"id";i:51;s:4:"slug";s:33:"51-fats-waller-georgia-on-my-mind";s:7:"_locale";s:2:"fr";}'),
(4383, 'transcription', 'fr/transcription/52-fats-waller-martinique', 'a:3:{s:2:"id";i:52;s:4:"slug";s:25:"52-fats-waller-martinique";s:7:"_locale";s:2:"en";}'),
(4384, 'transcription', 'part/martinique_en', 'a:3:{s:2:"id";i:52;s:4:"slug";s:25:"52-fats-waller-martinique";s:7:"_locale";s:2:"en";}'),
(4385, 'transcription', 'en/transcription/52-fats-waller-martinique', 'a:3:{s:2:"id";i:52;s:4:"slug";s:25:"52-fats-waller-martinique";s:7:"_locale";s:2:"fr";}'),
(4386, 'transcription', 'part/martinique', 'a:3:{s:2:"id";i:52;s:4:"slug";s:25:"52-fats-waller-martinique";s:7:"_locale";s:2:"fr";}'),
(4387, 'transcription', 'fr/transcription/53-james-p-johnson-harlem-strut', 'a:3:{s:2:"id";i:53;s:4:"slug";s:31:"53-james-p-johnson-harlem-strut";s:7:"_locale";s:2:"en";}'),
(4388, 'transcription', 'part/strut_en', 'a:3:{s:2:"id";i:53;s:4:"slug";s:31:"53-james-p-johnson-harlem-strut";s:7:"_locale";s:2:"en";}'),
(4389, 'transcription', 'en/transcription/53-james-p-johnson-harlem-strut', 'a:3:{s:2:"id";i:53;s:4:"slug";s:31:"53-james-p-johnson-harlem-strut";s:7:"_locale";s:2:"fr";}'),
(4390, 'transcription', 'part/strut', 'a:3:{s:2:"id";i:53;s:4:"slug";s:31:"53-james-p-johnson-harlem-strut";s:7:"_locale";s:2:"fr";}'),
(4391, 'transcription', 'fr/transcription/54-james-p-johnson-carolina-shout', 'a:3:{s:2:"id";i:54;s:4:"slug";s:33:"54-james-p-johnson-carolina-shout";s:7:"_locale";s:2:"en";}'),
(4392, 'transcription', 'part/carolina_johnson_en', 'a:3:{s:2:"id";i:54;s:4:"slug";s:33:"54-james-p-johnson-carolina-shout";s:7:"_locale";s:2:"en";}'),
(4393, 'transcription', 'en/transcription/54-james-p-johnson-carolina-shout', 'a:3:{s:2:"id";i:54;s:4:"slug";s:33:"54-james-p-johnson-carolina-shout";s:7:"_locale";s:2:"fr";}'),
(4394, 'transcription', 'part/carolina_johnson', 'a:3:{s:2:"id";i:54;s:4:"slug";s:33:"54-james-p-johnson-carolina-shout";s:7:"_locale";s:2:"fr";}'),
(4395, 'transcription', 'fr/transcription/55-james-p-johnson-riffs', 'a:3:{s:2:"id";i:55;s:4:"slug";s:24:"55-james-p-johnson-riffs";s:7:"_locale";s:2:"en";}'),
(4396, 'transcription', 'part/riffs_en', 'a:3:{s:2:"id";i:55;s:4:"slug";s:24:"55-james-p-johnson-riffs";s:7:"_locale";s:2:"en";}'),
(4397, 'transcription', 'en/transcription/55-james-p-johnson-riffs', 'a:3:{s:2:"id";i:55;s:4:"slug";s:24:"55-james-p-johnson-riffs";s:7:"_locale";s:2:"fr";}'),
(4398, 'transcription', 'part/riffs', 'a:3:{s:2:"id";i:55;s:4:"slug";s:24:"55-james-p-johnson-riffs";s:7:"_locale";s:2:"fr";}'),
(4399, 'transcription', 'fr/transcription/56-james-p-johnson-feeling-blues', 'a:3:{s:2:"id";i:56;s:4:"slug";s:32:"56-james-p-johnson-feeling-blues";s:7:"_locale";s:2:"en";}'),
(4400, 'transcription', 'part/feeling_en', 'a:3:{s:2:"id";i:56;s:4:"slug";s:32:"56-james-p-johnson-feeling-blues";s:7:"_locale";s:2:"en";}'),
(4401, 'transcription', 'en/transcription/56-james-p-johnson-feeling-blues', 'a:3:{s:2:"id";i:56;s:4:"slug";s:32:"56-james-p-johnson-feeling-blues";s:7:"_locale";s:2:"fr";}'),
(4402, 'transcription', 'part/feeling', 'a:3:{s:2:"id";i:56;s:4:"slug";s:32:"56-james-p-johnson-feeling-blues";s:7:"_locale";s:2:"fr";}'),
(4403, 'transcription', 'fr/transcription/57-james-p-johnson-jingles', 'a:3:{s:2:"id";i:57;s:4:"slug";s:26:"57-james-p-johnson-jingles";s:7:"_locale";s:2:"en";}'),
(4404, 'transcription', 'part/jingles_en', 'a:3:{s:2:"id";i:57;s:4:"slug";s:26:"57-james-p-johnson-jingles";s:7:"_locale";s:2:"en";}'),
(4405, 'transcription', 'en/transcription/57-james-p-johnson-jingles', 'a:3:{s:2:"id";i:57;s:4:"slug";s:26:"57-james-p-johnson-jingles";s:7:"_locale";s:2:"fr";}'),
(4406, 'transcription', 'part/jingles', 'a:3:{s:2:"id";i:57;s:4:"slug";s:26:"57-james-p-johnson-jingles";s:7:"_locale";s:2:"fr";}'),
(4407, 'transcription', 'fr/transcription/58-james-p-johnson-crying-for-the-carolines', 'a:3:{s:2:"id";i:58;s:4:"slug";s:43:"58-james-p-johnson-crying-for-the-carolines";s:7:"_locale";s:2:"en";}'),
(4408, 'transcription', 'part/crying_en', 'a:3:{s:2:"id";i:58;s:4:"slug";s:43:"58-james-p-johnson-crying-for-the-carolines";s:7:"_locale";s:2:"en";}'),
(4409, 'transcription', 'en/transcription/58-james-p-johnson-crying-for-the-carolines', 'a:3:{s:2:"id";i:58;s:4:"slug";s:43:"58-james-p-johnson-crying-for-the-carolines";s:7:"_locale";s:2:"fr";}'),
(4410, 'transcription', 'part/crying', 'a:3:{s:2:"id";i:58;s:4:"slug";s:43:"58-james-p-johnson-crying-for-the-carolines";s:7:"_locale";s:2:"fr";}'),
(4411, 'transcription', 'fr/transcription/59-james-p-johnson-modernistic', 'a:3:{s:2:"id";i:59;s:4:"slug";s:30:"59-james-p-johnson-modernistic";s:7:"_locale";s:2:"en";}'),
(4412, 'transcription', 'part/modernistic_en', 'a:3:{s:2:"id";i:59;s:4:"slug";s:30:"59-james-p-johnson-modernistic";s:7:"_locale";s:2:"en";}'),
(4413, 'transcription', 'en/transcription/59-james-p-johnson-modernistic', 'a:3:{s:2:"id";i:59;s:4:"slug";s:30:"59-james-p-johnson-modernistic";s:7:"_locale";s:2:"fr";}'),
(4414, 'transcription', 'part/modernistic', 'a:3:{s:2:"id";i:59;s:4:"slug";s:30:"59-james-p-johnson-modernistic";s:7:"_locale";s:2:"fr";}'),
(4415, 'transcription', 'fr/transcription/60-james-p-johnson-if-dreams-come-true', 'a:3:{s:2:"id";i:60;s:4:"slug";s:38:"60-james-p-johnson-if-dreams-come-true";s:7:"_locale";s:2:"en";}'),
(4416, 'transcription', 'part/ifdreams_en', 'a:3:{s:2:"id";i:60;s:4:"slug";s:38:"60-james-p-johnson-if-dreams-come-true";s:7:"_locale";s:2:"en";}'),
(4417, 'transcription', 'en/transcription/60-james-p-johnson-if-dreams-come-true', 'a:3:{s:2:"id";i:60;s:4:"slug";s:38:"60-james-p-johnson-if-dreams-come-true";s:7:"_locale";s:2:"fr";}'),
(4418, 'transcription', 'part/ifdreams', 'a:3:{s:2:"id";i:60;s:4:"slug";s:38:"60-james-p-johnson-if-dreams-come-true";s:7:"_locale";s:2:"fr";}'),
(4419, 'transcription', 'fr/transcription/61-james-p-johnson-mule-walk-stomp', 'a:3:{s:2:"id";i:61;s:4:"slug";s:34:"61-james-p-johnson-mule-walk-stomp";s:7:"_locale";s:2:"en";}'),
(4420, 'transcription', 'part/mule_en', 'a:3:{s:2:"id";i:61;s:4:"slug";s:34:"61-james-p-johnson-mule-walk-stomp";s:7:"_locale";s:2:"en";}'),
(4421, 'transcription', 'en/transcription/61-james-p-johnson-mule-walk-stomp', 'a:3:{s:2:"id";i:61;s:4:"slug";s:34:"61-james-p-johnson-mule-walk-stomp";s:7:"_locale";s:2:"fr";}'),
(4422, 'transcription', 'part/mule', 'a:3:{s:2:"id";i:61;s:4:"slug";s:34:"61-james-p-johnson-mule-walk-stomp";s:7:"_locale";s:2:"fr";}'),
(4423, 'transcription', 'fr/transcription/62-james-p-johnson-a-flat-dream', 'a:3:{s:2:"id";i:62;s:4:"slug";s:31:"62-james-p-johnson-a-flat-dream";s:7:"_locale";s:2:"en";}'),
(4424, 'transcription', 'part/flatdream_en', 'a:3:{s:2:"id";i:62;s:4:"slug";s:31:"62-james-p-johnson-a-flat-dream";s:7:"_locale";s:2:"en";}'),
(4425, 'transcription', 'en/transcription/62-james-p-johnson-a-flat-dream', 'a:3:{s:2:"id";i:62;s:4:"slug";s:31:"62-james-p-johnson-a-flat-dream";s:7:"_locale";s:2:"fr";}'),
(4426, 'transcription', 'part/flatdream', 'a:3:{s:2:"id";i:62;s:4:"slug";s:31:"62-james-p-johnson-a-flat-dream";s:7:"_locale";s:2:"fr";}'),
(4427, 'transcription', 'fr/transcription/63-james-p-johnson-daintiness-rag', 'a:3:{s:2:"id";i:63;s:4:"slug";s:33:"63-james-p-johnson-daintiness-rag";s:7:"_locale";s:2:"en";}'),
(4428, 'transcription', 'part/daintiness_en', 'a:3:{s:2:"id";i:63;s:4:"slug";s:33:"63-james-p-johnson-daintiness-rag";s:7:"_locale";s:2:"en";}'),
(4429, 'transcription', 'en/transcription/63-james-p-johnson-daintiness-rag', 'a:3:{s:2:"id";i:63;s:4:"slug";s:33:"63-james-p-johnson-daintiness-rag";s:7:"_locale";s:2:"fr";}'),
(4430, 'transcription', 'part/daintiness', 'a:3:{s:2:"id";i:63;s:4:"slug";s:33:"63-james-p-johnson-daintiness-rag";s:7:"_locale";s:2:"fr";}'),
(4431, 'transcription', 'fr/transcription/64-james-p-johnson-im-gonna-sit-right-down', 'a:3:{s:2:"id";i:64;s:4:"slug";s:42:"64-james-p-johnson-im-gonna-sit-right-down";s:7:"_locale";s:2:"en";}'),
(4432, 'transcription', 'part/letter_en', 'a:3:{s:2:"id";i:64;s:4:"slug";s:42:"64-james-p-johnson-im-gonna-sit-right-down";s:7:"_locale";s:2:"en";}'),
(4433, 'transcription', 'en/transcription/64-james-p-johnson-im-gonna-sit-right-down', 'a:3:{s:2:"id";i:64;s:4:"slug";s:42:"64-james-p-johnson-im-gonna-sit-right-down";s:7:"_locale";s:2:"fr";}'),
(4434, 'transcription', 'part/letter', 'a:3:{s:2:"id";i:64;s:4:"slug";s:42:"64-james-p-johnson-im-gonna-sit-right-down";s:7:"_locale";s:2:"fr";}'),
(4435, 'transcription', 'fr/transcription/65-james-p-johnson-keep-off-the-grass', 'a:3:{s:2:"id";i:65;s:4:"slug";s:37:"65-james-p-johnson-keep-off-the-grass";s:7:"_locale";s:2:"en";}'),
(4436, 'transcription', 'part/keepoff_en', 'a:3:{s:2:"id";i:65;s:4:"slug";s:37:"65-james-p-johnson-keep-off-the-grass";s:7:"_locale";s:2:"en";}'),
(4437, 'transcription', 'en/transcription/65-james-p-johnson-keep-off-the-grass', 'a:3:{s:2:"id";i:65;s:4:"slug";s:37:"65-james-p-johnson-keep-off-the-grass";s:7:"_locale";s:2:"fr";}'),
(4438, 'transcription', 'part/keepoff', 'a:3:{s:2:"id";i:65;s:4:"slug";s:37:"65-james-p-johnson-keep-off-the-grass";s:7:"_locale";s:2:"fr";}'),
(4439, 'transcription', 'fr/transcription/66-james-p-johnson-im-crazy-bout-my-baby', 'a:3:{s:2:"id";i:66;s:4:"slug";s:40:"66-james-p-johnson-im-crazy-bout-my-baby";s:7:"_locale";s:2:"en";}'),
(4440, 'transcription', 'part/crazy_en', 'a:3:{s:2:"id";i:66;s:4:"slug";s:40:"66-james-p-johnson-im-crazy-bout-my-baby";s:7:"_locale";s:2:"en";}'),
(4441, 'transcription', 'en/transcription/66-james-p-johnson-im-crazy-bout-my-baby', 'a:3:{s:2:"id";i:66;s:4:"slug";s:40:"66-james-p-johnson-im-crazy-bout-my-baby";s:7:"_locale";s:2:"fr";}'),
(4442, 'transcription', 'part/crazy', 'a:3:{s:2:"id";i:66;s:4:"slug";s:40:"66-james-p-johnson-im-crazy-bout-my-baby";s:7:"_locale";s:2:"fr";}'),
(4443, 'transcription', 'fr/transcription/67-james-p-johnson-twilight-rag', 'a:3:{s:2:"id";i:67;s:4:"slug";s:31:"67-james-p-johnson-twilight-rag";s:7:"_locale";s:2:"en";}'),
(4444, 'transcription', 'part/twilight_en', 'a:3:{s:2:"id";i:67;s:4:"slug";s:31:"67-james-p-johnson-twilight-rag";s:7:"_locale";s:2:"en";}'),
(4445, 'transcription', 'en/transcription/67-james-p-johnson-twilight-rag', 'a:3:{s:2:"id";i:67;s:4:"slug";s:31:"67-james-p-johnson-twilight-rag";s:7:"_locale";s:2:"fr";}'),
(4446, 'transcription', 'part/twilight', 'a:3:{s:2:"id";i:67;s:4:"slug";s:31:"67-james-p-johnson-twilight-rag";s:7:"_locale";s:2:"fr";}'),
(4447, 'transcription', 'fr/transcription/68-james-p-johnson-jersey-sweet', 'a:3:{s:2:"id";i:68;s:4:"slug";s:31:"68-james-p-johnson-jersey-sweet";s:7:"_locale";s:2:"en";}'),
(4448, 'transcription', 'part/jersey_en', 'a:3:{s:2:"id";i:68;s:4:"slug";s:31:"68-james-p-johnson-jersey-sweet";s:7:"_locale";s:2:"en";}'),
(4449, 'transcription', 'en/transcription/68-james-p-johnson-jersey-sweet', 'a:3:{s:2:"id";i:68;s:4:"slug";s:31:"68-james-p-johnson-jersey-sweet";s:7:"_locale";s:2:"fr";}'),
(4450, 'transcription', 'part/jersey', 'a:3:{s:2:"id";i:68;s:4:"slug";s:31:"68-james-p-johnson-jersey-sweet";s:7:"_locale";s:2:"fr";}'),
(4451, 'transcription', 'fr/transcription/69-james-p-johnson-liza', 'a:3:{s:2:"id";i:69;s:4:"slug";s:23:"69-james-p-johnson-liza";s:7:"_locale";s:2:"en";}'),
(4452, 'transcription', 'part/liza_en', 'a:3:{s:2:"id";i:69;s:4:"slug";s:23:"69-james-p-johnson-liza";s:7:"_locale";s:2:"en";}'),
(4453, 'transcription', 'en/transcription/69-james-p-johnson-liza', 'a:3:{s:2:"id";i:69;s:4:"slug";s:23:"69-james-p-johnson-liza";s:7:"_locale";s:2:"fr";}'),
(4454, 'transcription', 'part/liza', 'a:3:{s:2:"id";i:69;s:4:"slug";s:23:"69-james-p-johnson-liza";s:7:"_locale";s:2:"fr";}'),
(4455, 'transcription', 'fr/transcription/70-james-p-johnson-mamas-blues', 'a:3:{s:2:"id";i:70;s:4:"slug";s:30:"70-james-p-johnson-mamas-blues";s:7:"_locale";s:2:"en";}'),
(4456, 'transcription', 'part/roll-mamasblues_en', 'a:3:{s:2:"id";i:70;s:4:"slug";s:30:"70-james-p-johnson-mamas-blues";s:7:"_locale";s:2:"en";}'),
(4457, 'transcription', 'en/transcription/70-james-p-johnson-mamas-blues', 'a:3:{s:2:"id";i:70;s:4:"slug";s:30:"70-james-p-johnson-mamas-blues";s:7:"_locale";s:2:"fr";}'),
(4458, 'transcription', 'part/roll-mamasblues', 'a:3:{s:2:"id";i:70;s:4:"slug";s:30:"70-james-p-johnson-mamas-blues";s:7:"_locale";s:2:"fr";}'),
(4459, 'transcription', 'fr/transcription/71-james-p-johnson-caprice-rag', 'a:3:{s:2:"id";i:71;s:4:"slug";s:30:"71-james-p-johnson-caprice-rag";s:7:"_locale";s:2:"en";}'),
(4460, 'transcription', 'part/roll-capricerag_en', 'a:3:{s:2:"id";i:71;s:4:"slug";s:30:"71-james-p-johnson-caprice-rag";s:7:"_locale";s:2:"en";}'),
(4461, 'transcription', 'en/transcription/71-james-p-johnson-caprice-rag', 'a:3:{s:2:"id";i:71;s:4:"slug";s:30:"71-james-p-johnson-caprice-rag";s:7:"_locale";s:2:"fr";}'),
(4462, 'transcription', 'part/roll-capricerag', 'a:3:{s:2:"id";i:71;s:4:"slug";s:30:"71-james-p-johnson-caprice-rag";s:7:"_locale";s:2:"fr";}'),
(4463, 'transcription', 'fr/transcription/72-james-p-johnson-steeplechase-rag', 'a:3:{s:2:"id";i:72;s:4:"slug";s:35:"72-james-p-johnson-steeplechase-rag";s:7:"_locale";s:2:"en";}'),
(4464, 'transcription', 'part/roll-steeplechase_en', 'a:3:{s:2:"id";i:72;s:4:"slug";s:35:"72-james-p-johnson-steeplechase-rag";s:7:"_locale";s:2:"en";}'),
(4465, 'transcription', 'en/transcription/72-james-p-johnson-steeplechase-rag', 'a:3:{s:2:"id";i:72;s:4:"slug";s:35:"72-james-p-johnson-steeplechase-rag";s:7:"_locale";s:2:"fr";}'),
(4466, 'transcription', 'part/roll-steeplechase', 'a:3:{s:2:"id";i:72;s:4:"slug";s:35:"72-james-p-johnson-steeplechase-rag";s:7:"_locale";s:2:"fr";}'),
(4467, 'transcription', 'fr/transcription/73-james-p-johnson-stop-it', 'a:3:{s:2:"id";i:73;s:4:"slug";s:26:"73-james-p-johnson-stop-it";s:7:"_locale";s:2:"en";}'),
(4468, 'transcription', 'part/roll-stopit_en', 'a:3:{s:2:"id";i:73;s:4:"slug";s:26:"73-james-p-johnson-stop-it";s:7:"_locale";s:2:"en";}'),
(4469, 'transcription', 'en/transcription/73-james-p-johnson-stop-it', 'a:3:{s:2:"id";i:73;s:4:"slug";s:26:"73-james-p-johnson-stop-it";s:7:"_locale";s:2:"fr";}'),
(4470, 'transcription', 'part/roll-stopit', 'a:3:{s:2:"id";i:73;s:4:"slug";s:26:"73-james-p-johnson-stop-it";s:7:"_locale";s:2:"fr";}'),
(4471, 'transcription', 'fr/transcription/74-james-p-johnson-carolina-shout', 'a:3:{s:2:"id";i:74;s:4:"slug";s:33:"74-james-p-johnson-carolina-shout";s:7:"_locale";s:2:"en";}'),
(4472, 'transcription', 'part/roll-carolinashout_en', 'a:3:{s:2:"id";i:74;s:4:"slug";s:33:"74-james-p-johnson-carolina-shout";s:7:"_locale";s:2:"en";}'),
(4473, 'transcription', 'en/transcription/74-james-p-johnson-carolina-shout', 'a:3:{s:2:"id";i:74;s:4:"slug";s:33:"74-james-p-johnson-carolina-shout";s:7:"_locale";s:2:"fr";}'),
(4474, 'transcription', 'part/roll-carolinashout', 'a:3:{s:2:"id";i:74;s:4:"slug";s:33:"74-james-p-johnson-carolina-shout";s:7:"_locale";s:2:"fr";}'),
(4475, 'transcription', 'fr/transcription/75-james-p-johnson-eccentricity', 'a:3:{s:2:"id";i:75;s:4:"slug";s:31:"75-james-p-johnson-eccentricity";s:7:"_locale";s:2:"en";}'),
(4476, 'transcription', 'part/roll-eccentricity_en', 'a:3:{s:2:"id";i:75;s:4:"slug";s:31:"75-james-p-johnson-eccentricity";s:7:"_locale";s:2:"en";}'),
(4477, 'transcription', 'en/transcription/75-james-p-johnson-eccentricity', 'a:3:{s:2:"id";i:75;s:4:"slug";s:31:"75-james-p-johnson-eccentricity";s:7:"_locale";s:2:"fr";}'),
(4478, 'transcription', 'part/roll-eccentricity', 'a:3:{s:2:"id";i:75;s:4:"slug";s:31:"75-james-p-johnson-eccentricity";s:7:"_locale";s:2:"fr";}'),
(4479, 'transcription', 'fr/transcription/76-james-p-johnson-it-takes-love-to-cure-the-hearts-disease', 'a:3:{s:2:"id";i:76;s:4:"slug";s:59:"76-james-p-johnson-it-takes-love-to-cure-the-hearts-disease";s:7:"_locale";s:2:"en";}'),
(4480, 'transcription', 'part/roll-ittakeslove_en', 'a:3:{s:2:"id";i:76;s:4:"slug";s:59:"76-james-p-johnson-it-takes-love-to-cure-the-hearts-disease";s:7:"_locale";s:2:"en";}'),
(4481, 'transcription', 'en/transcription/76-james-p-johnson-it-takes-love-to-cure-the-hearts-disease', 'a:3:{s:2:"id";i:76;s:4:"slug";s:59:"76-james-p-johnson-it-takes-love-to-cure-the-hearts-disease";s:7:"_locale";s:2:"fr";}'),
(4482, 'transcription', 'part/roll-ittakeslove', 'a:3:{s:2:"id";i:76;s:4:"slug";s:59:"76-james-p-johnson-it-takes-love-to-cure-the-hearts-disease";s:7:"_locale";s:2:"fr";}'),
(4483, 'transcription', 'fr/transcription/77-james-p-johnson-dr-jazzes-raz-ma-taz', 'a:3:{s:2:"id";i:77;s:4:"slug";s:39:"77-james-p-johnson-dr-jazzes-raz-ma-taz";s:7:"_locale";s:2:"en";}'),
(4484, 'transcription', 'part/roll-drjazz_en', 'a:3:{s:2:"id";i:77;s:4:"slug";s:39:"77-james-p-johnson-dr-jazzes-raz-ma-taz";s:7:"_locale";s:2:"en";}'),
(4485, 'transcription', 'en/transcription/77-james-p-johnson-dr-jazzes-raz-ma-taz', 'a:3:{s:2:"id";i:77;s:4:"slug";s:39:"77-james-p-johnson-dr-jazzes-raz-ma-taz";s:7:"_locale";s:2:"fr";}'),
(4486, 'transcription', 'part/roll-drjazz', 'a:3:{s:2:"id";i:77;s:4:"slug";s:39:"77-james-p-johnson-dr-jazzes-raz-ma-taz";s:7:"_locale";s:2:"fr";}'),
(4487, 'transcription', 'fr/transcription/78-james-p-johnson-roumania', 'a:3:{s:2:"id";i:78;s:4:"slug";s:27:"78-james-p-johnson-roumania";s:7:"_locale";s:2:"en";}'),
(4488, 'transcription', 'part/roll-roumania_en', 'a:3:{s:2:"id";i:78;s:4:"slug";s:27:"78-james-p-johnson-roumania";s:7:"_locale";s:2:"en";}'),
(4489, 'transcription', 'en/transcription/78-james-p-johnson-roumania', 'a:3:{s:2:"id";i:78;s:4:"slug";s:27:"78-james-p-johnson-roumania";s:7:"_locale";s:2:"fr";}'),
(4490, 'transcription', 'part/roll-roumania', 'a:3:{s:2:"id";i:78;s:4:"slug";s:27:"78-james-p-johnson-roumania";s:7:"_locale";s:2:"fr";}'),
(4491, 'transcription', 'fr/transcription/79-james-p-johnson-arkansas-blues', 'a:3:{s:2:"id";i:79;s:4:"slug";s:33:"79-james-p-johnson-arkansas-blues";s:7:"_locale";s:2:"en";}'),
(4492, 'transcription', 'part/roll-arkansas_en', 'a:3:{s:2:"id";i:79;s:4:"slug";s:33:"79-james-p-johnson-arkansas-blues";s:7:"_locale";s:2:"en";}'),
(4493, 'transcription', 'en/transcription/79-james-p-johnson-arkansas-blues', 'a:3:{s:2:"id";i:79;s:4:"slug";s:33:"79-james-p-johnson-arkansas-blues";s:7:"_locale";s:2:"fr";}'),
(4494, 'transcription', 'part/roll-arkansas', 'a:3:{s:2:"id";i:79;s:4:"slug";s:33:"79-james-p-johnson-arkansas-blues";s:7:"_locale";s:2:"fr";}');
INSERT INTO `Redirect` (`id`, `route`, `oldPath`, `parameters`) VALUES
(4495, 'transcription', 'fr/transcription/80-james-p-johnson-joe-turner-blues', 'a:3:{s:2:"id";i:80;s:4:"slug";s:35:"80-james-p-johnson-joe-turner-blues";s:7:"_locale";s:2:"en";}'),
(4496, 'transcription', 'part/roll-joeturnerblues_en', 'a:3:{s:2:"id";i:80;s:4:"slug";s:35:"80-james-p-johnson-joe-turner-blues";s:7:"_locale";s:2:"en";}'),
(4497, 'transcription', 'en/transcription/80-james-p-johnson-joe-turner-blues', 'a:3:{s:2:"id";i:80;s:4:"slug";s:35:"80-james-p-johnson-joe-turner-blues";s:7:"_locale";s:2:"fr";}'),
(4498, 'transcription', 'part/roll-joeturnerblues', 'a:3:{s:2:"id";i:80;s:4:"slug";s:35:"80-james-p-johnson-joe-turner-blues";s:7:"_locale";s:2:"fr";}'),
(4499, 'transcription', 'fr/transcription/81-james-p-johnson-harlem-strut', 'a:3:{s:2:"id";i:81;s:4:"slug";s:31:"81-james-p-johnson-harlem-strut";s:7:"_locale";s:2:"en";}'),
(4500, 'transcription', 'part/roll-harlemstrut_en', 'a:3:{s:2:"id";i:81;s:4:"slug";s:31:"81-james-p-johnson-harlem-strut";s:7:"_locale";s:2:"en";}'),
(4501, 'transcription', 'en/transcription/81-james-p-johnson-harlem-strut', 'a:3:{s:2:"id";i:81;s:4:"slug";s:31:"81-james-p-johnson-harlem-strut";s:7:"_locale";s:2:"fr";}'),
(4502, 'transcription', 'part/roll-harlemstrut', 'a:3:{s:2:"id";i:81;s:4:"slug";s:31:"81-james-p-johnson-harlem-strut";s:7:"_locale";s:2:"fr";}'),
(4503, 'transcription', 'fr/transcription/82-james-p-johnson-railroad-man', 'a:3:{s:2:"id";i:82;s:4:"slug";s:31:"82-james-p-johnson-railroad-man";s:7:"_locale";s:2:"en";}'),
(4504, 'transcription', 'part/roll-railroadman_en', 'a:3:{s:2:"id";i:82;s:4:"slug";s:31:"82-james-p-johnson-railroad-man";s:7:"_locale";s:2:"en";}'),
(4505, 'transcription', 'en/transcription/82-james-p-johnson-railroad-man', 'a:3:{s:2:"id";i:82;s:4:"slug";s:31:"82-james-p-johnson-railroad-man";s:7:"_locale";s:2:"fr";}'),
(4506, 'transcription', 'part/roll-railroadman', 'a:3:{s:2:"id";i:82;s:4:"slug";s:31:"82-james-p-johnson-railroad-man";s:7:"_locale";s:2:"fr";}'),
(4507, 'transcription', 'fr/transcription/83-james-p-johnson-black-man', 'a:3:{s:2:"id";i:83;s:4:"slug";s:28:"83-james-p-johnson-black-man";s:7:"_locale";s:2:"en";}'),
(4508, 'transcription', 'part/roll-blackman_en', 'a:3:{s:2:"id";i:83;s:4:"slug";s:28:"83-james-p-johnson-black-man";s:7:"_locale";s:2:"en";}'),
(4509, 'transcription', 'en/transcription/83-james-p-johnson-black-man', 'a:3:{s:2:"id";i:83;s:4:"slug";s:28:"83-james-p-johnson-black-man";s:7:"_locale";s:2:"fr";}'),
(4510, 'transcription', 'part/roll-blackman', 'a:3:{s:2:"id";i:83;s:4:"slug";s:28:"83-james-p-johnson-black-man";s:7:"_locale";s:2:"fr";}'),
(4511, 'transcription', 'fr/transcription/84-james-p-johnson-charleston', 'a:3:{s:2:"id";i:84;s:4:"slug";s:29:"84-james-p-johnson-charleston";s:7:"_locale";s:2:"en";}'),
(4512, 'transcription', 'part/roll-charleston_en', 'a:3:{s:2:"id";i:84;s:4:"slug";s:29:"84-james-p-johnson-charleston";s:7:"_locale";s:2:"en";}'),
(4513, 'transcription', 'en/transcription/84-james-p-johnson-charleston', 'a:3:{s:2:"id";i:84;s:4:"slug";s:29:"84-james-p-johnson-charleston";s:7:"_locale";s:2:"fr";}'),
(4514, 'transcription', 'part/roll-charleston', 'a:3:{s:2:"id";i:84;s:4:"slug";s:29:"84-james-p-johnson-charleston";s:7:"_locale";s:2:"fr";}'),
(4515, 'transcription', 'fr/transcription/85-james-p-johnson-harlem-choclate-babies-on-parade', 'a:3:{s:2:"id";i:85;s:4:"slug";s:51:"85-james-p-johnson-harlem-choclate-babies-on-parade";s:7:"_locale";s:2:"en";}'),
(4516, 'transcription', 'part/roll-harlemchoclate_en', 'a:3:{s:2:"id";i:85;s:4:"slug";s:51:"85-james-p-johnson-harlem-choclate-babies-on-parade";s:7:"_locale";s:2:"en";}'),
(4517, 'transcription', 'en/transcription/85-james-p-johnson-harlem-choclate-babies-on-parade', 'a:3:{s:2:"id";i:85;s:4:"slug";s:51:"85-james-p-johnson-harlem-choclate-babies-on-parade";s:7:"_locale";s:2:"fr";}'),
(4518, 'transcription', 'part/harlemchoclate', 'a:3:{s:2:"id";i:85;s:4:"slug";s:51:"85-james-p-johnson-harlem-choclate-babies-on-parade";s:7:"_locale";s:2:"fr";}'),
(4519, 'transcription', 'fr/transcription/86-james-p-johnson-sugar', 'a:3:{s:2:"id";i:86;s:4:"slug";s:24:"86-james-p-johnson-sugar";s:7:"_locale";s:2:"en";}'),
(4520, 'transcription', 'part/roll-sugar_en', 'a:3:{s:2:"id";i:86;s:4:"slug";s:24:"86-james-p-johnson-sugar";s:7:"_locale";s:2:"en";}'),
(4521, 'transcription', 'en/transcription/86-james-p-johnson-sugar', 'a:3:{s:2:"id";i:86;s:4:"slug";s:24:"86-james-p-johnson-sugar";s:7:"_locale";s:2:"fr";}'),
(4522, 'transcription', 'part/roll-sugar', 'a:3:{s:2:"id";i:86;s:4:"slug";s:24:"86-james-p-johnson-sugar";s:7:"_locale";s:2:"fr";}'),
(4523, 'transcription', 'fr/transcription/87-james-p-johnson-arkansas-blues', 'a:3:{s:2:"id";i:87;s:4:"slug";s:33:"87-james-p-johnson-arkansas-blues";s:7:"_locale";s:2:"en";}'),
(4524, 'transcription', 'part/arkansas_blues_en', 'a:3:{s:2:"id";i:87;s:4:"slug";s:33:"87-james-p-johnson-arkansas-blues";s:7:"_locale";s:2:"en";}'),
(4525, 'transcription', 'en/transcription/87-james-p-johnson-arkansas-blues', 'a:3:{s:2:"id";i:87;s:4:"slug";s:33:"87-james-p-johnson-arkansas-blues";s:7:"_locale";s:2:"fr";}'),
(4526, 'transcription', 'part/arkansas_blues', 'a:3:{s:2:"id";i:87;s:4:"slug";s:33:"87-james-p-johnson-arkansas-blues";s:7:"_locale";s:2:"fr";}'),
(4527, 'transcription', 'fr/transcription/88-james-p-johnson-aint-cha-got-music', 'a:3:{s:2:"id";i:88;s:4:"slug";s:37:"88-james-p-johnson-aint-cha-got-music";s:7:"_locale";s:2:"en";}'),
(4528, 'transcription', 'part/aint_cha_got_music_en', 'a:3:{s:2:"id";i:88;s:4:"slug";s:37:"88-james-p-johnson-aint-cha-got-music";s:7:"_locale";s:2:"en";}'),
(4529, 'transcription', 'en/transcription/88-james-p-johnson-aint-cha-got-music', 'a:3:{s:2:"id";i:88;s:4:"slug";s:37:"88-james-p-johnson-aint-cha-got-music";s:7:"_locale";s:2:"fr";}'),
(4530, 'transcription', 'part/aint_cha_got_music', 'a:3:{s:2:"id";i:88;s:4:"slug";s:37:"88-james-p-johnson-aint-cha-got-music";s:7:"_locale";s:2:"fr";}'),
(4531, 'transcription', 'fr/transcription/89-james-p-johnson-bleeding-hearted-blues', 'a:3:{s:2:"id";i:89;s:4:"slug";s:41:"89-james-p-johnson-bleeding-hearted-blues";s:7:"_locale";s:2:"en";}'),
(4532, 'transcription', 'part/bleeding-hearted_blues_en', 'a:3:{s:2:"id";i:89;s:4:"slug";s:41:"89-james-p-johnson-bleeding-hearted-blues";s:7:"_locale";s:2:"en";}'),
(4533, 'transcription', 'en/transcription/89-james-p-johnson-bleeding-hearted-blues', 'a:3:{s:2:"id";i:89;s:4:"slug";s:41:"89-james-p-johnson-bleeding-hearted-blues";s:7:"_locale";s:2:"fr";}'),
(4534, 'transcription', 'part/bleeding-hearted_blues', 'a:3:{s:2:"id";i:89;s:4:"slug";s:41:"89-james-p-johnson-bleeding-hearted-blues";s:7:"_locale";s:2:"fr";}'),
(4535, 'transcription', 'fr/transcription/90-james-p-johnson-blueberry-rhyme', 'a:3:{s:2:"id";i:90;s:4:"slug";s:34:"90-james-p-johnson-blueberry-rhyme";s:7:"_locale";s:2:"en";}'),
(4536, 'transcription', 'part/blueberrye_rhyme_en', 'a:3:{s:2:"id";i:90;s:4:"slug";s:34:"90-james-p-johnson-blueberry-rhyme";s:7:"_locale";s:2:"en";}'),
(4537, 'transcription', 'en/transcription/90-james-p-johnson-blueberry-rhyme', 'a:3:{s:2:"id";i:90;s:4:"slug";s:34:"90-james-p-johnson-blueberry-rhyme";s:7:"_locale";s:2:"fr";}'),
(4538, 'transcription', 'part/blueberrye_rhyme', 'a:3:{s:2:"id";i:90;s:4:"slug";s:34:"90-james-p-johnson-blueberry-rhyme";s:7:"_locale";s:2:"fr";}'),
(4539, 'transcription', 'fr/transcription/91-james-p-johnson-carolina-balmoral', 'a:3:{s:2:"id";i:91;s:4:"slug";s:36:"91-james-p-johnson-carolina-balmoral";s:7:"_locale";s:2:"en";}'),
(4540, 'transcription', 'part/carolina_balmoral_en', 'a:3:{s:2:"id";i:91;s:4:"slug";s:36:"91-james-p-johnson-carolina-balmoral";s:7:"_locale";s:2:"en";}'),
(4541, 'transcription', 'en/transcription/91-james-p-johnson-carolina-balmoral', 'a:3:{s:2:"id";i:91;s:4:"slug";s:36:"91-james-p-johnson-carolina-balmoral";s:7:"_locale";s:2:"fr";}'),
(4542, 'transcription', 'part/carolina_balmoral', 'a:3:{s:2:"id";i:91;s:4:"slug";s:36:"91-james-p-johnson-carolina-balmoral";s:7:"_locale";s:2:"fr";}'),
(4543, 'transcription', 'fr/transcription/92-james-p-johnson-concerto-jazz-a-mine', 'a:3:{s:2:"id";i:92;s:4:"slug";s:39:"92-james-p-johnson-concerto-jazz-a-mine";s:7:"_locale";s:2:"en";}'),
(4544, 'transcription', 'part/jazz-a-mine_en', 'a:3:{s:2:"id";i:92;s:4:"slug";s:39:"92-james-p-johnson-concerto-jazz-a-mine";s:7:"_locale";s:2:"en";}'),
(4545, 'transcription', 'en/transcription/92-james-p-johnson-concerto-jazz-a-mine', 'a:3:{s:2:"id";i:92;s:4:"slug";s:39:"92-james-p-johnson-concerto-jazz-a-mine";s:7:"_locale";s:2:"fr";}'),
(4546, 'transcription', 'part/jazz-a-mine', 'a:3:{s:2:"id";i:92;s:4:"slug";s:39:"92-james-p-johnson-concerto-jazz-a-mine";s:7:"_locale";s:2:"fr";}'),
(4547, 'transcription', 'fr/transcription/93-james-p-johnson-fascination', 'a:3:{s:2:"id";i:93;s:4:"slug";s:30:"93-james-p-johnson-fascination";s:7:"_locale";s:2:"en";}'),
(4548, 'transcription', 'part/fascination_en', 'a:3:{s:2:"id";i:93;s:4:"slug";s:30:"93-james-p-johnson-fascination";s:7:"_locale";s:2:"en";}'),
(4549, 'transcription', 'en/transcription/93-james-p-johnson-fascination', 'a:3:{s:2:"id";i:93;s:4:"slug";s:30:"93-james-p-johnson-fascination";s:7:"_locale";s:2:"fr";}'),
(4550, 'transcription', 'part/fascination', 'a:3:{s:2:"id";i:93;s:4:"slug";s:30:"93-james-p-johnson-fascination";s:7:"_locale";s:2:"fr";}'),
(4551, 'transcription', 'fr/transcription/94-james-p-johnson-gut-stomp', 'a:3:{s:2:"id";i:94;s:4:"slug";s:28:"94-james-p-johnson-gut-stomp";s:7:"_locale";s:2:"en";}'),
(4552, 'transcription', 'part/gut_stomp_en', 'a:3:{s:2:"id";i:94;s:4:"slug";s:28:"94-james-p-johnson-gut-stomp";s:7:"_locale";s:2:"en";}'),
(4553, 'transcription', 'en/transcription/94-james-p-johnson-gut-stomp', 'a:3:{s:2:"id";i:94;s:4:"slug";s:28:"94-james-p-johnson-gut-stomp";s:7:"_locale";s:2:"fr";}'),
(4554, 'transcription', 'part/gut_stomp', 'a:3:{s:2:"id";i:94;s:4:"slug";s:28:"94-james-p-johnson-gut-stomp";s:7:"_locale";s:2:"fr";}'),
(4555, 'transcription', 'fr/transcription/95-james-p-johnson-honeysuckle-rose', 'a:3:{s:2:"id";i:95;s:4:"slug";s:35:"95-james-p-johnson-honeysuckle-rose";s:7:"_locale";s:2:"en";}'),
(4556, 'transcription', 'part/honeysuckle_rose_en', 'a:3:{s:2:"id";i:95;s:4:"slug";s:35:"95-james-p-johnson-honeysuckle-rose";s:7:"_locale";s:2:"en";}'),
(4557, 'transcription', 'en/transcription/95-james-p-johnson-honeysuckle-rose', 'a:3:{s:2:"id";i:95;s:4:"slug";s:35:"95-james-p-johnson-honeysuckle-rose";s:7:"_locale";s:2:"fr";}'),
(4558, 'transcription', 'part/honeysuckle_rose', 'a:3:{s:2:"id";i:95;s:4:"slug";s:35:"95-james-p-johnson-honeysuckle-rose";s:7:"_locale";s:2:"fr";}'),
(4559, 'transcription', 'fr/transcription/96-james-p-johnson-keep-movin', 'a:3:{s:2:"id";i:96;s:4:"slug";s:29:"96-james-p-johnson-keep-movin";s:7:"_locale";s:2:"en";}'),
(4560, 'transcription', 'part/keep_movin_en', 'a:3:{s:2:"id";i:96;s:4:"slug";s:29:"96-james-p-johnson-keep-movin";s:7:"_locale";s:2:"en";}'),
(4561, 'transcription', 'en/transcription/96-james-p-johnson-keep-movin', 'a:3:{s:2:"id";i:96;s:4:"slug";s:29:"96-james-p-johnson-keep-movin";s:7:"_locale";s:2:"fr";}'),
(4562, 'transcription', 'part/keep_movin', 'a:3:{s:2:"id";i:96;s:4:"slug";s:29:"96-james-p-johnson-keep-movin";s:7:"_locale";s:2:"fr";}'),
(4563, 'transcription', 'fr/transcription/97-james-p-johnson-old-fashioned-love', 'a:3:{s:2:"id";i:97;s:4:"slug";s:37:"97-james-p-johnson-old-fashioned-love";s:7:"_locale";s:2:"en";}'),
(4564, 'transcription', 'part/old_fashioned_love_en', 'a:3:{s:2:"id";i:97;s:4:"slug";s:37:"97-james-p-johnson-old-fashioned-love";s:7:"_locale";s:2:"en";}'),
(4565, 'transcription', 'en/transcription/97-james-p-johnson-old-fashioned-love', 'a:3:{s:2:"id";i:97;s:4:"slug";s:37:"97-james-p-johnson-old-fashioned-love";s:7:"_locale";s:2:"fr";}'),
(4566, 'transcription', 'part/old_fashioned_love', 'a:3:{s:2:"id";i:97;s:4:"slug";s:37:"97-james-p-johnson-old-fashioned-love";s:7:"_locale";s:2:"fr";}'),
(4567, 'transcription', 'fr/transcription/98-james-p-johnson-scouting-around', 'a:3:{s:2:"id";i:98;s:4:"slug";s:34:"98-james-p-johnson-scouting-around";s:7:"_locale";s:2:"en";}'),
(4568, 'transcription', 'part/scouting_around_en', 'a:3:{s:2:"id";i:98;s:4:"slug";s:34:"98-james-p-johnson-scouting-around";s:7:"_locale";s:2:"en";}'),
(4569, 'transcription', 'en/transcription/98-james-p-johnson-scouting-around', 'a:3:{s:2:"id";i:98;s:4:"slug";s:34:"98-james-p-johnson-scouting-around";s:7:"_locale";s:2:"fr";}'),
(4570, 'transcription', 'part/scouting_around', 'a:3:{s:2:"id";i:98;s:4:"slug";s:34:"98-james-p-johnson-scouting-around";s:7:"_locale";s:2:"fr";}'),
(4571, 'transcription', 'fr/transcription/99-james-p-johnson-snowy-morning-blues', 'a:3:{s:2:"id";i:99;s:4:"slug";s:38:"99-james-p-johnson-snowy-morning-blues";s:7:"_locale";s:2:"en";}'),
(4572, 'transcription', 'part/snowy_morning_blues_en', 'a:3:{s:2:"id";i:99;s:4:"slug";s:38:"99-james-p-johnson-snowy-morning-blues";s:7:"_locale";s:2:"en";}'),
(4573, 'transcription', 'en/transcription/99-james-p-johnson-snowy-morning-blues', 'a:3:{s:2:"id";i:99;s:4:"slug";s:38:"99-james-p-johnson-snowy-morning-blues";s:7:"_locale";s:2:"fr";}'),
(4574, 'transcription', 'part/snowy_morning_blues', 'a:3:{s:2:"id";i:99;s:4:"slug";s:38:"99-james-p-johnson-snowy-morning-blues";s:7:"_locale";s:2:"fr";}'),
(4575, 'transcription', 'fr/transcription/100-james-p-johnson-squeeze-me', 'a:3:{s:2:"id";i:100;s:4:"slug";s:30:"100-james-p-johnson-squeeze-me";s:7:"_locale";s:2:"en";}'),
(4576, 'transcription', 'part/squeeze_me_en', 'a:3:{s:2:"id";i:100;s:4:"slug";s:30:"100-james-p-johnson-squeeze-me";s:7:"_locale";s:2:"en";}'),
(4577, 'transcription', 'en/transcription/100-james-p-johnson-squeeze-me', 'a:3:{s:2:"id";i:100;s:4:"slug";s:30:"100-james-p-johnson-squeeze-me";s:7:"_locale";s:2:"fr";}'),
(4578, 'transcription', 'part/squeeze_me', 'a:3:{s:2:"id";i:100;s:4:"slug";s:30:"100-james-p-johnson-squeeze-me";s:7:"_locale";s:2:"fr";}'),
(4579, 'transcription', 'fr/transcription/101-james-p-johnson-toddlin', 'a:3:{s:2:"id";i:101;s:4:"slug";s:27:"101-james-p-johnson-toddlin";s:7:"_locale";s:2:"en";}'),
(4580, 'transcription', 'part/toddlin_en', 'a:3:{s:2:"id";i:101;s:4:"slug";s:27:"101-james-p-johnson-toddlin";s:7:"_locale";s:2:"en";}'),
(4581, 'transcription', 'en/transcription/101-james-p-johnson-toddlin', 'a:3:{s:2:"id";i:101;s:4:"slug";s:27:"101-james-p-johnson-toddlin";s:7:"_locale";s:2:"fr";}'),
(4582, 'transcription', 'part/toddlin', 'a:3:{s:2:"id";i:101;s:4:"slug";s:27:"101-james-p-johnson-toddlin";s:7:"_locale";s:2:"fr";}'),
(4583, 'transcription', 'fr/transcription/102-james-p-johnson-what-is-this-thing-called-love', 'a:3:{s:2:"id";i:102;s:4:"slug";s:50:"102-james-p-johnson-what-is-this-thing-called-love";s:7:"_locale";s:2:"en";}'),
(4584, 'transcription', 'part/what_is_this_thing_called_love_en', 'a:3:{s:2:"id";i:102;s:4:"slug";s:50:"102-james-p-johnson-what-is-this-thing-called-love";s:7:"_locale";s:2:"en";}'),
(4585, 'transcription', 'en/transcription/102-james-p-johnson-what-is-this-thing-called-love', 'a:3:{s:2:"id";i:102;s:4:"slug";s:50:"102-james-p-johnson-what-is-this-thing-called-love";s:7:"_locale";s:2:"fr";}'),
(4586, 'transcription', 'part/what_is_this_thing_called_love', 'a:3:{s:2:"id";i:102;s:4:"slug";s:50:"102-james-p-johnson-what-is-this-thing-called-love";s:7:"_locale";s:2:"fr";}'),
(4587, 'transcription', 'fr/transcription/103-james-p-johnson-you-cant-do-what-my-last-man-did', 'a:3:{s:2:"id";i:103;s:4:"slug";s:52:"103-james-p-johnson-you-cant-do-what-my-last-man-did";s:7:"_locale";s:2:"en";}'),
(4588, 'transcription', 'part/you_cant_do_en', 'a:3:{s:2:"id";i:103;s:4:"slug";s:52:"103-james-p-johnson-you-cant-do-what-my-last-man-did";s:7:"_locale";s:2:"en";}'),
(4589, 'transcription', 'en/transcription/103-james-p-johnson-you-cant-do-what-my-last-man-did', 'a:3:{s:2:"id";i:103;s:4:"slug";s:52:"103-james-p-johnson-you-cant-do-what-my-last-man-did";s:7:"_locale";s:2:"fr";}'),
(4590, 'transcription', 'part/you_cant_do', 'a:3:{s:2:"id";i:103;s:4:"slug";s:52:"103-james-p-johnson-you-cant-do-what-my-last-man-did";s:7:"_locale";s:2:"fr";}'),
(4591, 'transcription', 'fr/transcription/104-willie-the-lion-smith-concentratin', 'a:3:{s:2:"id";i:104;s:4:"slug";s:38:"104-willie-the-lion-smith-concentratin";s:7:"_locale";s:2:"en";}'),
(4592, 'transcription', 'part/concentratin_en', 'a:3:{s:2:"id";i:104;s:4:"slug";s:38:"104-willie-the-lion-smith-concentratin";s:7:"_locale";s:2:"en";}'),
(4593, 'transcription', 'en/transcription/104-willie-the-lion-smith-concentratin', 'a:3:{s:2:"id";i:104;s:4:"slug";s:38:"104-willie-the-lion-smith-concentratin";s:7:"_locale";s:2:"fr";}'),
(4594, 'transcription', 'part/concentratin', 'a:3:{s:2:"id";i:104;s:4:"slug";s:38:"104-willie-the-lion-smith-concentratin";s:7:"_locale";s:2:"fr";}'),
(4595, 'transcription', 'fr/transcription/105-willie-the-lion-smith-sneakaway', 'a:3:{s:2:"id";i:105;s:4:"slug";s:35:"105-willie-the-lion-smith-sneakaway";s:7:"_locale";s:2:"en";}'),
(4596, 'transcription', 'part/sneakaway_en', 'a:3:{s:2:"id";i:105;s:4:"slug";s:35:"105-willie-the-lion-smith-sneakaway";s:7:"_locale";s:2:"en";}'),
(4597, 'transcription', 'en/transcription/105-willie-the-lion-smith-sneakaway', 'a:3:{s:2:"id";i:105;s:4:"slug";s:35:"105-willie-the-lion-smith-sneakaway";s:7:"_locale";s:2:"fr";}'),
(4598, 'transcription', 'part/sneakaway', 'a:3:{s:2:"id";i:105;s:4:"slug";s:35:"105-willie-the-lion-smith-sneakaway";s:7:"_locale";s:2:"fr";}'),
(4599, 'transcription', 'fr/transcription/106-willie-the-lion-smith-echoes-of-spring', 'a:3:{s:2:"id";i:106;s:4:"slug";s:42:"106-willie-the-lion-smith-echoes-of-spring";s:7:"_locale";s:2:"en";}'),
(4600, 'transcription', 'part/echoes_en', 'a:3:{s:2:"id";i:106;s:4:"slug";s:42:"106-willie-the-lion-smith-echoes-of-spring";s:7:"_locale";s:2:"en";}'),
(4601, 'transcription', 'en/transcription/106-willie-the-lion-smith-echoes-of-spring', 'a:3:{s:2:"id";i:106;s:4:"slug";s:42:"106-willie-the-lion-smith-echoes-of-spring";s:7:"_locale";s:2:"fr";}'),
(4602, 'transcription', 'part/echoes', 'a:3:{s:2:"id";i:106;s:4:"slug";s:42:"106-willie-the-lion-smith-echoes-of-spring";s:7:"_locale";s:2:"fr";}'),
(4603, 'transcription', 'fr/transcription/107-willie-the-lion-smith-morning-air', 'a:3:{s:2:"id";i:107;s:4:"slug";s:37:"107-willie-the-lion-smith-morning-air";s:7:"_locale";s:2:"en";}'),
(4604, 'transcription', 'part/morning_en', 'a:3:{s:2:"id";i:107;s:4:"slug";s:37:"107-willie-the-lion-smith-morning-air";s:7:"_locale";s:2:"en";}'),
(4605, 'transcription', 'en/transcription/107-willie-the-lion-smith-morning-air', 'a:3:{s:2:"id";i:107;s:4:"slug";s:37:"107-willie-the-lion-smith-morning-air";s:7:"_locale";s:2:"fr";}'),
(4606, 'transcription', 'part/morning', 'a:3:{s:2:"id";i:107;s:4:"slug";s:37:"107-willie-the-lion-smith-morning-air";s:7:"_locale";s:2:"fr";}'),
(4607, 'transcription', 'fr/transcription/108-willie-the-lion-smith-finger-buster', 'a:3:{s:2:"id";i:108;s:4:"slug";s:39:"108-willie-the-lion-smith-finger-buster";s:7:"_locale";s:2:"en";}'),
(4608, 'transcription', 'part/finger_en', 'a:3:{s:2:"id";i:108;s:4:"slug";s:39:"108-willie-the-lion-smith-finger-buster";s:7:"_locale";s:2:"en";}'),
(4609, 'transcription', 'en/transcription/108-willie-the-lion-smith-finger-buster', 'a:3:{s:2:"id";i:108;s:4:"slug";s:39:"108-willie-the-lion-smith-finger-buster";s:7:"_locale";s:2:"fr";}'),
(4610, 'transcription', 'part/finger', 'a:3:{s:2:"id";i:108;s:4:"slug";s:39:"108-willie-the-lion-smith-finger-buster";s:7:"_locale";s:2:"fr";}'),
(4611, 'transcription', 'fr/transcription/109-willie-the-lion-smith-fading-star', 'a:3:{s:2:"id";i:109;s:4:"slug";s:37:"109-willie-the-lion-smith-fading-star";s:7:"_locale";s:2:"en";}'),
(4612, 'transcription', 'part/fading_en', 'a:3:{s:2:"id";i:109;s:4:"slug";s:37:"109-willie-the-lion-smith-fading-star";s:7:"_locale";s:2:"en";}'),
(4613, 'transcription', 'en/transcription/109-willie-the-lion-smith-fading-star', 'a:3:{s:2:"id";i:109;s:4:"slug";s:37:"109-willie-the-lion-smith-fading-star";s:7:"_locale";s:2:"fr";}'),
(4614, 'transcription', 'part/fading', 'a:3:{s:2:"id";i:109;s:4:"slug";s:37:"109-willie-the-lion-smith-fading-star";s:7:"_locale";s:2:"fr";}'),
(4615, 'transcription', 'fr/transcription/110-willie-the-lion-smith-rippling-waters', 'a:3:{s:2:"id";i:110;s:4:"slug";s:41:"110-willie-the-lion-smith-rippling-waters";s:7:"_locale";s:2:"en";}'),
(4616, 'transcription', 'part/rippling_en', 'a:3:{s:2:"id";i:110;s:4:"slug";s:41:"110-willie-the-lion-smith-rippling-waters";s:7:"_locale";s:2:"en";}'),
(4617, 'transcription', 'en/transcription/110-willie-the-lion-smith-rippling-waters', 'a:3:{s:2:"id";i:110;s:4:"slug";s:41:"110-willie-the-lion-smith-rippling-waters";s:7:"_locale";s:2:"fr";}'),
(4618, 'transcription', 'part/rippling', 'a:3:{s:2:"id";i:110;s:4:"slug";s:41:"110-willie-the-lion-smith-rippling-waters";s:7:"_locale";s:2:"fr";}'),
(4619, 'transcription', 'fr/transcription/111-willie-the-lion-smith-stormy-weather', 'a:3:{s:2:"id";i:111;s:4:"slug";s:40:"111-willie-the-lion-smith-stormy-weather";s:7:"_locale";s:2:"en";}'),
(4620, 'transcription', 'part/stormy_en', 'a:3:{s:2:"id";i:111;s:4:"slug";s:40:"111-willie-the-lion-smith-stormy-weather";s:7:"_locale";s:2:"en";}'),
(4621, 'transcription', 'en/transcription/111-willie-the-lion-smith-stormy-weather', 'a:3:{s:2:"id";i:111;s:4:"slug";s:40:"111-willie-the-lion-smith-stormy-weather";s:7:"_locale";s:2:"fr";}'),
(4622, 'transcription', 'part/stormy', 'a:3:{s:2:"id";i:111;s:4:"slug";s:40:"111-willie-the-lion-smith-stormy-weather";s:7:"_locale";s:2:"fr";}'),
(4623, 'transcription', 'fr/transcription/112-willie-the-lion-smith-ill-follow-you', 'a:3:{s:2:"id";i:112;s:4:"slug";s:40:"112-willie-the-lion-smith-ill-follow-you";s:7:"_locale";s:2:"en";}'),
(4624, 'transcription', 'part/follow_en', 'a:3:{s:2:"id";i:112;s:4:"slug";s:40:"112-willie-the-lion-smith-ill-follow-you";s:7:"_locale";s:2:"en";}'),
(4625, 'transcription', 'en/transcription/112-willie-the-lion-smith-ill-follow-you', 'a:3:{s:2:"id";i:112;s:4:"slug";s:40:"112-willie-the-lion-smith-ill-follow-you";s:7:"_locale";s:2:"fr";}'),
(4626, 'transcription', 'part/follow', 'a:3:{s:2:"id";i:112;s:4:"slug";s:40:"112-willie-the-lion-smith-ill-follow-you";s:7:"_locale";s:2:"fr";}'),
(4627, 'transcription', 'fr/transcription/113-willie-the-lion-smith-passionette', 'a:3:{s:2:"id";i:113;s:4:"slug";s:37:"113-willie-the-lion-smith-passionette";s:7:"_locale";s:2:"en";}'),
(4628, 'transcription', 'part/passionette_en', 'a:3:{s:2:"id";i:113;s:4:"slug";s:37:"113-willie-the-lion-smith-passionette";s:7:"_locale";s:2:"en";}'),
(4629, 'transcription', 'en/transcription/113-willie-the-lion-smith-passionette', 'a:3:{s:2:"id";i:113;s:4:"slug";s:37:"113-willie-the-lion-smith-passionette";s:7:"_locale";s:2:"fr";}'),
(4630, 'transcription', 'part/passionette', 'a:3:{s:2:"id";i:113;s:4:"slug";s:37:"113-willie-the-lion-smith-passionette";s:7:"_locale";s:2:"fr";}'),
(4631, 'transcription', 'fr/transcription/114-willie-the-lion-smith-what-is-there-to-say', 'a:3:{s:2:"id";i:114;s:4:"slug";s:46:"114-willie-the-lion-smith-what-is-there-to-say";s:7:"_locale";s:2:"en";}'),
(4632, 'transcription', 'part/whatisthere_en', 'a:3:{s:2:"id";i:114;s:4:"slug";s:46:"114-willie-the-lion-smith-what-is-there-to-say";s:7:"_locale";s:2:"en";}'),
(4633, 'transcription', 'en/transcription/114-willie-the-lion-smith-what-is-there-to-say', 'a:3:{s:2:"id";i:114;s:4:"slug";s:46:"114-willie-the-lion-smith-what-is-there-to-say";s:7:"_locale";s:2:"fr";}'),
(4634, 'transcription', 'part/whatisthere', 'a:3:{s:2:"id";i:114;s:4:"slug";s:46:"114-willie-the-lion-smith-what-is-there-to-say";s:7:"_locale";s:2:"fr";}'),
(4635, 'transcription', 'fr/transcription/115-willie-the-lion-smith-here-come-the-band', 'a:3:{s:2:"id";i:115;s:4:"slug";s:44:"115-willie-the-lion-smith-here-come-the-band";s:7:"_locale";s:2:"en";}'),
(4636, 'transcription', 'part/theband_en', 'a:3:{s:2:"id";i:115;s:4:"slug";s:44:"115-willie-the-lion-smith-here-come-the-band";s:7:"_locale";s:2:"en";}'),
(4637, 'transcription', 'en/transcription/115-willie-the-lion-smith-here-come-the-band', 'a:3:{s:2:"id";i:115;s:4:"slug";s:44:"115-willie-the-lion-smith-here-come-the-band";s:7:"_locale";s:2:"fr";}'),
(4638, 'transcription', 'part/theband', 'a:3:{s:2:"id";i:115;s:4:"slug";s:44:"115-willie-the-lion-smith-here-come-the-band";s:7:"_locale";s:2:"fr";}'),
(4639, 'transcription', 'fr/transcription/116-willie-the-lion-smith-cuttin-out', 'a:3:{s:2:"id";i:116;s:4:"slug";s:36:"116-willie-the-lion-smith-cuttin-out";s:7:"_locale";s:2:"en";}'),
(4640, 'transcription', 'part/cuttinout_en', 'a:3:{s:2:"id";i:116;s:4:"slug";s:36:"116-willie-the-lion-smith-cuttin-out";s:7:"_locale";s:2:"en";}'),
(4641, 'transcription', 'en/transcription/116-willie-the-lion-smith-cuttin-out', 'a:3:{s:2:"id";i:116;s:4:"slug";s:36:"116-willie-the-lion-smith-cuttin-out";s:7:"_locale";s:2:"fr";}'),
(4642, 'transcription', 'part/cuttinout', 'a:3:{s:2:"id";i:116;s:4:"slug";s:36:"116-willie-the-lion-smith-cuttin-out";s:7:"_locale";s:2:"fr";}'),
(4643, 'transcription', 'fr/transcription/117-willie-the-lion-smith-portrait-of-the-duke', 'a:3:{s:2:"id";i:117;s:4:"slug";s:46:"117-willie-the-lion-smith-portrait-of-the-duke";s:7:"_locale";s:2:"en";}'),
(4644, 'transcription', 'part/portrait_en', 'a:3:{s:2:"id";i:117;s:4:"slug";s:46:"117-willie-the-lion-smith-portrait-of-the-duke";s:7:"_locale";s:2:"en";}'),
(4645, 'transcription', 'en/transcription/117-willie-the-lion-smith-portrait-of-the-duke', 'a:3:{s:2:"id";i:117;s:4:"slug";s:46:"117-willie-the-lion-smith-portrait-of-the-duke";s:7:"_locale";s:2:"fr";}'),
(4646, 'transcription', 'part/portrait', 'a:3:{s:2:"id";i:117;s:4:"slug";s:46:"117-willie-the-lion-smith-portrait-of-the-duke";s:7:"_locale";s:2:"fr";}'),
(4647, 'transcription', 'fr/transcription/118-willie-the-lion-smith-zig-zag', 'a:3:{s:2:"id";i:118;s:4:"slug";s:33:"118-willie-the-lion-smith-zig-zag";s:7:"_locale";s:2:"en";}'),
(4648, 'transcription', 'part/zigzag_en', 'a:3:{s:2:"id";i:118;s:4:"slug";s:33:"118-willie-the-lion-smith-zig-zag";s:7:"_locale";s:2:"en";}'),
(4649, 'transcription', 'en/transcription/118-willie-the-lion-smith-zig-zag', 'a:3:{s:2:"id";i:118;s:4:"slug";s:33:"118-willie-the-lion-smith-zig-zag";s:7:"_locale";s:2:"fr";}'),
(4650, 'transcription', 'part/zigzag', 'a:3:{s:2:"id";i:118;s:4:"slug";s:33:"118-willie-the-lion-smith-zig-zag";s:7:"_locale";s:2:"fr";}'),
(4651, 'transcription', 'fr/transcription/119-willie-the-lion-smith-contrary-motion', 'a:3:{s:2:"id";i:119;s:4:"slug";s:41:"119-willie-the-lion-smith-contrary-motion";s:7:"_locale";s:2:"en";}'),
(4652, 'transcription', 'part/motion_en', 'a:3:{s:2:"id";i:119;s:4:"slug";s:41:"119-willie-the-lion-smith-contrary-motion";s:7:"_locale";s:2:"en";}'),
(4653, 'transcription', 'en/transcription/119-willie-the-lion-smith-contrary-motion', 'a:3:{s:2:"id";i:119;s:4:"slug";s:41:"119-willie-the-lion-smith-contrary-motion";s:7:"_locale";s:2:"fr";}'),
(4654, 'transcription', 'part/motion', 'a:3:{s:2:"id";i:119;s:4:"slug";s:41:"119-willie-the-lion-smith-contrary-motion";s:7:"_locale";s:2:"fr";}'),
(4655, 'transcription', 'fr/transcription/120-donald-lambert-anitras-dance', 'a:3:{s:2:"id";i:120;s:4:"slug";s:32:"120-donald-lambert-anitras-dance";s:7:"_locale";s:2:"en";}'),
(4656, 'transcription', 'part/anitra_en', 'a:3:{s:2:"id";i:120;s:4:"slug";s:32:"120-donald-lambert-anitras-dance";s:7:"_locale";s:2:"en";}'),
(4657, 'transcription', 'en/transcription/120-donald-lambert-anitras-dance', 'a:3:{s:2:"id";i:120;s:4:"slug";s:32:"120-donald-lambert-anitras-dance";s:7:"_locale";s:2:"fr";}'),
(4658, 'transcription', 'part/anitra', 'a:3:{s:2:"id";i:120;s:4:"slug";s:32:"120-donald-lambert-anitras-dance";s:7:"_locale";s:2:"fr";}'),
(4659, 'transcription', 'fr/transcription/121-donald-lambert-pilgrims-chorus', 'a:3:{s:2:"id";i:121;s:4:"slug";s:34:"121-donald-lambert-pilgrims-chorus";s:7:"_locale";s:2:"en";}'),
(4660, 'transcription', 'part/pilgrim_en', 'a:3:{s:2:"id";i:121;s:4:"slug";s:34:"121-donald-lambert-pilgrims-chorus";s:7:"_locale";s:2:"en";}'),
(4661, 'transcription', 'en/transcription/121-donald-lambert-pilgrims-chorus', 'a:3:{s:2:"id";i:121;s:4:"slug";s:34:"121-donald-lambert-pilgrims-chorus";s:7:"_locale";s:2:"fr";}'),
(4662, 'transcription', 'part/pilgrim', 'a:3:{s:2:"id";i:121;s:4:"slug";s:34:"121-donald-lambert-pilgrims-chorus";s:7:"_locale";s:2:"fr";}'),
(4663, 'transcription', 'fr/transcription/122-donald-lambert-elegie', 'a:3:{s:2:"id";i:122;s:4:"slug";s:25:"122-donald-lambert-elegie";s:7:"_locale";s:2:"en";}'),
(4664, 'transcription', 'part/elegie_en', 'a:3:{s:2:"id";i:122;s:4:"slug";s:25:"122-donald-lambert-elegie";s:7:"_locale";s:2:"en";}'),
(4665, 'transcription', 'en/transcription/122-donald-lambert-elegie', 'a:3:{s:2:"id";i:122;s:4:"slug";s:25:"122-donald-lambert-elegie";s:7:"_locale";s:2:"fr";}'),
(4666, 'transcription', 'part/elegie', 'a:3:{s:2:"id";i:122;s:4:"slug";s:25:"122-donald-lambert-elegie";s:7:"_locale";s:2:"fr";}'),
(4667, 'transcription', 'fr/transcription/123-donald-lambert-sextet-from-luci-di-lammermoor', 'a:3:{s:2:"id";i:123;s:4:"slug";s:49:"123-donald-lambert-sextet-from-luci-di-lammermoor";s:7:"_locale";s:2:"en";}'),
(4668, 'transcription', 'part/sextet_en', 'a:3:{s:2:"id";i:123;s:4:"slug";s:49:"123-donald-lambert-sextet-from-luci-di-lammermoor";s:7:"_locale";s:2:"en";}'),
(4669, 'transcription', 'en/transcription/123-donald-lambert-sextet-from-luci-di-lammermoor', 'a:3:{s:2:"id";i:123;s:4:"slug";s:49:"123-donald-lambert-sextet-from-luci-di-lammermoor";s:7:"_locale";s:2:"fr";}'),
(4670, 'transcription', 'part/sextet', 'a:3:{s:2:"id";i:123;s:4:"slug";s:49:"123-donald-lambert-sextet-from-luci-di-lammermoor";s:7:"_locale";s:2:"fr";}'),
(4671, 'transcription', 'fr/transcription/124-donald-lambert-russian-lullaby', 'a:3:{s:2:"id";i:124;s:4:"slug";s:34:"124-donald-lambert-russian-lullaby";s:7:"_locale";s:2:"en";}'),
(4672, 'transcription', 'part/lullaby_en', 'a:3:{s:2:"id";i:124;s:4:"slug";s:34:"124-donald-lambert-russian-lullaby";s:7:"_locale";s:2:"en";}'),
(4673, 'transcription', 'en/transcription/124-donald-lambert-russian-lullaby', 'a:3:{s:2:"id";i:124;s:4:"slug";s:34:"124-donald-lambert-russian-lullaby";s:7:"_locale";s:2:"fr";}'),
(4674, 'transcription', 'part/lullaby', 'a:3:{s:2:"id";i:124;s:4:"slug";s:34:"124-donald-lambert-russian-lullaby";s:7:"_locale";s:2:"fr";}'),
(4675, 'transcription', 'fr/transcription/125-donald-lambert-people-will-say-we-are-in-love', 'a:3:{s:2:"id";i:125;s:4:"slug";s:49:"125-donald-lambert-people-will-say-we-are-in-love";s:7:"_locale";s:2:"en";}'),
(4676, 'transcription', 'part/people_en', 'a:3:{s:2:"id";i:125;s:4:"slug";s:49:"125-donald-lambert-people-will-say-we-are-in-love";s:7:"_locale";s:2:"en";}'),
(4677, 'transcription', 'en/transcription/125-donald-lambert-people-will-say-we-are-in-love', 'a:3:{s:2:"id";i:125;s:4:"slug";s:49:"125-donald-lambert-people-will-say-we-are-in-love";s:7:"_locale";s:2:"fr";}'),
(4678, 'transcription', 'part/people', 'a:3:{s:2:"id";i:125;s:4:"slug";s:49:"125-donald-lambert-people-will-say-we-are-in-love";s:7:"_locale";s:2:"fr";}'),
(4679, 'transcription', 'fr/transcription/126-donald-lambert-hold-your-temper', 'a:3:{s:2:"id";i:126;s:4:"slug";s:35:"126-donald-lambert-hold-your-temper";s:7:"_locale";s:2:"en";}'),
(4680, 'transcription', 'part/temper_en', 'a:3:{s:2:"id";i:126;s:4:"slug";s:35:"126-donald-lambert-hold-your-temper";s:7:"_locale";s:2:"en";}'),
(4681, 'transcription', 'en/transcription/126-donald-lambert-hold-your-temper', 'a:3:{s:2:"id";i:126;s:4:"slug";s:35:"126-donald-lambert-hold-your-temper";s:7:"_locale";s:2:"fr";}'),
(4682, 'transcription', 'part/temper', 'a:3:{s:2:"id";i:126;s:4:"slug";s:35:"126-donald-lambert-hold-your-temper";s:7:"_locale";s:2:"fr";}'),
(4683, 'transcription', 'fr/transcription/127-donald-lambert-tea-for-two', 'a:3:{s:2:"id";i:127;s:4:"slug";s:30:"127-donald-lambert-tea-for-two";s:7:"_locale";s:2:"en";}'),
(4684, 'transcription', 'part/teafortwo_lamb_en', 'a:3:{s:2:"id";i:127;s:4:"slug";s:30:"127-donald-lambert-tea-for-two";s:7:"_locale";s:2:"en";}'),
(4685, 'transcription', 'en/transcription/127-donald-lambert-tea-for-two', 'a:3:{s:2:"id";i:127;s:4:"slug";s:30:"127-donald-lambert-tea-for-two";s:7:"_locale";s:2:"fr";}'),
(4686, 'transcription', 'part/teafortwo_lamb', 'a:3:{s:2:"id";i:127;s:4:"slug";s:30:"127-donald-lambert-tea-for-two";s:7:"_locale";s:2:"fr";}'),
(4687, 'transcription', 'fr/transcription/128-donald-lambert-trolley-song', 'a:3:{s:2:"id";i:128;s:4:"slug";s:31:"128-donald-lambert-trolley-song";s:7:"_locale";s:2:"en";}'),
(4688, 'transcription', 'part/trolley_en', 'a:3:{s:2:"id";i:128;s:4:"slug";s:31:"128-donald-lambert-trolley-song";s:7:"_locale";s:2:"en";}'),
(4689, 'transcription', 'en/transcription/128-donald-lambert-trolley-song', 'a:3:{s:2:"id";i:128;s:4:"slug";s:31:"128-donald-lambert-trolley-song";s:7:"_locale";s:2:"fr";}'),
(4690, 'transcription', 'part/trolley', 'a:3:{s:2:"id";i:128;s:4:"slug";s:31:"128-donald-lambert-trolley-song";s:7:"_locale";s:2:"fr";}'),
(4691, 'transcription', 'fr/transcription/129-donald-lambert-russian-rag', 'a:3:{s:2:"id";i:129;s:4:"slug";s:30:"129-donald-lambert-russian-rag";s:7:"_locale";s:2:"en";}'),
(4692, 'transcription', 'part/russian_en', 'a:3:{s:2:"id";i:129;s:4:"slug";s:30:"129-donald-lambert-russian-rag";s:7:"_locale";s:2:"en";}'),
(4693, 'transcription', 'en/transcription/129-donald-lambert-russian-rag', 'a:3:{s:2:"id";i:129;s:4:"slug";s:30:"129-donald-lambert-russian-rag";s:7:"_locale";s:2:"fr";}'),
(4694, 'transcription', 'part/russian', 'a:3:{s:2:"id";i:129;s:4:"slug";s:30:"129-donald-lambert-russian-rag";s:7:"_locale";s:2:"fr";}'),
(4695, 'transcription', 'fr/transcription/130-donald-lambert-save-your-sorrow', 'a:3:{s:2:"id";i:130;s:4:"slug";s:35:"130-donald-lambert-save-your-sorrow";s:7:"_locale";s:2:"en";}'),
(4696, 'transcription', 'part/sorrow_en', 'a:3:{s:2:"id";i:130;s:4:"slug";s:35:"130-donald-lambert-save-your-sorrow";s:7:"_locale";s:2:"en";}'),
(4697, 'transcription', 'en/transcription/130-donald-lambert-save-your-sorrow', 'a:3:{s:2:"id";i:130;s:4:"slug";s:35:"130-donald-lambert-save-your-sorrow";s:7:"_locale";s:2:"fr";}'),
(4698, 'transcription', 'part/sorrow', 'a:3:{s:2:"id";i:130;s:4:"slug";s:35:"130-donald-lambert-save-your-sorrow";s:7:"_locale";s:2:"fr";}'),
(4699, 'transcription', 'fr/transcription/131-donald-lambert-pork-and-beans', 'a:3:{s:2:"id";i:131;s:4:"slug";s:33:"131-donald-lambert-pork-and-beans";s:7:"_locale";s:2:"en";}'),
(4700, 'transcription', 'part/beans_en', 'a:3:{s:2:"id";i:131;s:4:"slug";s:33:"131-donald-lambert-pork-and-beans";s:7:"_locale";s:2:"en";}'),
(4701, 'transcription', 'en/transcription/131-donald-lambert-pork-and-beans', 'a:3:{s:2:"id";i:131;s:4:"slug";s:33:"131-donald-lambert-pork-and-beans";s:7:"_locale";s:2:"fr";}'),
(4702, 'transcription', 'part/beans', 'a:3:{s:2:"id";i:131;s:4:"slug";s:33:"131-donald-lambert-pork-and-beans";s:7:"_locale";s:2:"fr";}'),
(4703, 'transcription', 'fr/transcription/132-donald-lambert-im-just-wild-about-harry', 'a:3:{s:2:"id";i:132;s:4:"slug";s:43:"132-donald-lambert-im-just-wild-about-harry";s:7:"_locale";s:2:"en";}'),
(4704, 'transcription', 'part/harry_en', 'a:3:{s:2:"id";i:132;s:4:"slug";s:43:"132-donald-lambert-im-just-wild-about-harry";s:7:"_locale";s:2:"en";}'),
(4705, 'transcription', 'en/transcription/132-donald-lambert-im-just-wild-about-harry', 'a:3:{s:2:"id";i:132;s:4:"slug";s:43:"132-donald-lambert-im-just-wild-about-harry";s:7:"_locale";s:2:"fr";}'),
(4706, 'transcription', 'part/harry', 'a:3:{s:2:"id";i:132;s:4:"slug";s:43:"132-donald-lambert-im-just-wild-about-harry";s:7:"_locale";s:2:"fr";}'),
(4707, 'transcription', 'fr/transcription/133-donald-lambert-as-time-goes-by', 'a:3:{s:2:"id";i:133;s:4:"slug";s:34:"133-donald-lambert-as-time-goes-by";s:7:"_locale";s:2:"en";}'),
(4708, 'transcription', 'part/astime_en', 'a:3:{s:2:"id";i:133;s:4:"slug";s:34:"133-donald-lambert-as-time-goes-by";s:7:"_locale";s:2:"en";}'),
(4709, 'transcription', 'en/transcription/133-donald-lambert-as-time-goes-by', 'a:3:{s:2:"id";i:133;s:4:"slug";s:34:"133-donald-lambert-as-time-goes-by";s:7:"_locale";s:2:"fr";}'),
(4710, 'transcription', 'part/astime', 'a:3:{s:2:"id";i:133;s:4:"slug";s:34:"133-donald-lambert-as-time-goes-by";s:7:"_locale";s:2:"fr";}'),
(4711, 'transcription', 'fr/transcription/134-donald-lambert-doin-what-i-please', 'a:3:{s:2:"id";i:134;s:4:"slug";s:37:"134-donald-lambert-doin-what-i-please";s:7:"_locale";s:2:"en";}'),
(4712, 'transcription', 'part/jumps_en', 'a:3:{s:2:"id";i:134;s:4:"slug";s:37:"134-donald-lambert-doin-what-i-please";s:7:"_locale";s:2:"en";}'),
(4713, 'transcription', 'en/transcription/134-donald-lambert-doin-what-i-please', 'a:3:{s:2:"id";i:134;s:4:"slug";s:37:"134-donald-lambert-doin-what-i-please";s:7:"_locale";s:2:"fr";}'),
(4714, 'transcription', 'part/jumps', 'a:3:{s:2:"id";i:134;s:4:"slug";s:37:"134-donald-lambert-doin-what-i-please";s:7:"_locale";s:2:"fr";}'),
(4715, 'transcription', 'fr/transcription/135-jimmy-blythe-chicago-stomp', 'a:3:{s:2:"id";i:135;s:4:"slug";s:30:"135-jimmy-blythe-chicago-stomp";s:7:"_locale";s:2:"en";}'),
(4716, 'transcription', 'part/chicago_stomp_en', 'a:3:{s:2:"id";i:135;s:4:"slug";s:30:"135-jimmy-blythe-chicago-stomp";s:7:"_locale";s:2:"en";}'),
(4717, 'transcription', 'en/transcription/135-jimmy-blythe-chicago-stomp', 'a:3:{s:2:"id";i:135;s:4:"slug";s:30:"135-jimmy-blythe-chicago-stomp";s:7:"_locale";s:2:"fr";}'),
(4718, 'transcription', 'part/chicago_stomp', 'a:3:{s:2:"id";i:135;s:4:"slug";s:30:"135-jimmy-blythe-chicago-stomp";s:7:"_locale";s:2:"fr";}'),
(4719, 'transcription', 'fr/transcription/136-pine-top-smith-jump-steady-blues', 'a:3:{s:2:"id";i:136;s:4:"slug";s:36:"136-pine-top-smith-jump-steady-blues";s:7:"_locale";s:2:"en";}'),
(4720, 'transcription', 'part/steadyblues_en', 'a:3:{s:2:"id";i:136;s:4:"slug";s:36:"136-pine-top-smith-jump-steady-blues";s:7:"_locale";s:2:"en";}'),
(4721, 'transcription', 'en/transcription/136-pine-top-smith-jump-steady-blues', 'a:3:{s:2:"id";i:136;s:4:"slug";s:36:"136-pine-top-smith-jump-steady-blues";s:7:"_locale";s:2:"fr";}'),
(4722, 'transcription', 'part/steadyblues', 'a:3:{s:2:"id";i:136;s:4:"slug";s:36:"136-pine-top-smith-jump-steady-blues";s:7:"_locale";s:2:"fr";}'),
(4723, 'transcription', 'fr/transcription/137-jimmy-yancey-yancey-stomp', 'a:3:{s:2:"id";i:137;s:4:"slug";s:29:"137-jimmy-yancey-yancey-stomp";s:7:"_locale";s:2:"en";}'),
(4724, 'transcription', 'part/yancey_stomp_en', 'a:3:{s:2:"id";i:137;s:4:"slug";s:29:"137-jimmy-yancey-yancey-stomp";s:7:"_locale";s:2:"en";}'),
(4725, 'transcription', 'en/transcription/137-jimmy-yancey-yancey-stomp', 'a:3:{s:2:"id";i:137;s:4:"slug";s:29:"137-jimmy-yancey-yancey-stomp";s:7:"_locale";s:2:"fr";}'),
(4726, 'transcription', 'part/yancey_stomp', 'a:3:{s:2:"id";i:137;s:4:"slug";s:29:"137-jimmy-yancey-yancey-stomp";s:7:"_locale";s:2:"fr";}'),
(4727, 'transcription', 'fr/transcription/138-jimmy-yancey-the-mellow-blues', 'a:3:{s:2:"id";i:138;s:4:"slug";s:33:"138-jimmy-yancey-the-mellow-blues";s:7:"_locale";s:2:"en";}'),
(4728, 'transcription', 'part/mellow_blues_en', 'a:3:{s:2:"id";i:138;s:4:"slug";s:33:"138-jimmy-yancey-the-mellow-blues";s:7:"_locale";s:2:"en";}'),
(4729, 'transcription', 'en/transcription/138-jimmy-yancey-the-mellow-blues', 'a:3:{s:2:"id";i:138;s:4:"slug";s:33:"138-jimmy-yancey-the-mellow-blues";s:7:"_locale";s:2:"fr";}'),
(4730, 'transcription', 'part/mellow_blues', 'a:3:{s:2:"id";i:138;s:4:"slug";s:33:"138-jimmy-yancey-the-mellow-blues";s:7:"_locale";s:2:"fr";}'),
(4731, 'transcription', 'fr/transcription/139-jimmy-yancey-yancey-bugle-call', 'a:3:{s:2:"id";i:139;s:4:"slug";s:34:"139-jimmy-yancey-yancey-bugle-call";s:7:"_locale";s:2:"en";}'),
(4732, 'transcription', 'part/bugle_call_en', 'a:3:{s:2:"id";i:139;s:4:"slug";s:34:"139-jimmy-yancey-yancey-bugle-call";s:7:"_locale";s:2:"en";}'),
(4733, 'transcription', 'en/transcription/139-jimmy-yancey-yancey-bugle-call', 'a:3:{s:2:"id";i:139;s:4:"slug";s:34:"139-jimmy-yancey-yancey-bugle-call";s:7:"_locale";s:2:"fr";}'),
(4734, 'transcription', 'part/bugle_call', 'a:3:{s:2:"id";i:139;s:4:"slug";s:34:"139-jimmy-yancey-yancey-bugle-call";s:7:"_locale";s:2:"fr";}'),
(4735, 'transcription', 'fr/transcription/140-albert-ammons-boogie-woogie-stomp', 'a:3:{s:2:"id";i:140;s:4:"slug";s:37:"140-albert-ammons-boogie-woogie-stomp";s:7:"_locale";s:2:"en";}'),
(4736, 'transcription', 'part/bw_stomp_en', 'a:3:{s:2:"id";i:140;s:4:"slug";s:37:"140-albert-ammons-boogie-woogie-stomp";s:7:"_locale";s:2:"en";}'),
(4737, 'transcription', 'en/transcription/140-albert-ammons-boogie-woogie-stomp', 'a:3:{s:2:"id";i:140;s:4:"slug";s:37:"140-albert-ammons-boogie-woogie-stomp";s:7:"_locale";s:2:"fr";}'),
(4738, 'transcription', 'part/bw_stomp', 'a:3:{s:2:"id";i:140;s:4:"slug";s:37:"140-albert-ammons-boogie-woogie-stomp";s:7:"_locale";s:2:"fr";}'),
(4739, 'transcription', 'fr/transcription/141-albert-ammons-suitcase-blues', 'a:3:{s:2:"id";i:141;s:4:"slug";s:32:"141-albert-ammons-suitcase-blues";s:7:"_locale";s:2:"en";}'),
(4740, 'transcription', 'part/suitcase_blues_en', 'a:3:{s:2:"id";i:141;s:4:"slug";s:32:"141-albert-ammons-suitcase-blues";s:7:"_locale";s:2:"en";}'),
(4741, 'transcription', 'en/transcription/141-albert-ammons-suitcase-blues', 'a:3:{s:2:"id";i:141;s:4:"slug";s:32:"141-albert-ammons-suitcase-blues";s:7:"_locale";s:2:"fr";}'),
(4742, 'transcription', 'part/suitcase_blues', 'a:3:{s:2:"id";i:141;s:4:"slug";s:32:"141-albert-ammons-suitcase-blues";s:7:"_locale";s:2:"fr";}'),
(4743, 'transcription', 'fr/transcription/142-albert-ammons-12th-street-boogie', 'a:3:{s:2:"id";i:142;s:4:"slug";s:36:"142-albert-ammons-12th-street-boogie";s:7:"_locale";s:2:"en";}'),
(4744, 'transcription', 'part/12street_boogie_en', 'a:3:{s:2:"id";i:142;s:4:"slug";s:36:"142-albert-ammons-12th-street-boogie";s:7:"_locale";s:2:"en";}'),
(4745, 'transcription', 'en/transcription/142-albert-ammons-12th-street-boogie', 'a:3:{s:2:"id";i:142;s:4:"slug";s:36:"142-albert-ammons-12th-street-boogie";s:7:"_locale";s:2:"fr";}'),
(4746, 'transcription', 'part/12street_boogie', 'a:3:{s:2:"id";i:142;s:4:"slug";s:36:"142-albert-ammons-12th-street-boogie";s:7:"_locale";s:2:"fr";}'),
(4747, 'transcription', 'fr/transcription/143-albert-ammons-mecca-flat-blues', 'a:3:{s:2:"id";i:143;s:4:"slug";s:34:"143-albert-ammons-mecca-flat-blues";s:7:"_locale";s:2:"en";}'),
(4748, 'transcription', 'part/meccaflat_blues_en', 'a:3:{s:2:"id";i:143;s:4:"slug";s:34:"143-albert-ammons-mecca-flat-blues";s:7:"_locale";s:2:"en";}'),
(4749, 'transcription', 'en/transcription/143-albert-ammons-mecca-flat-blues', 'a:3:{s:2:"id";i:143;s:4:"slug";s:34:"143-albert-ammons-mecca-flat-blues";s:7:"_locale";s:2:"fr";}'),
(4750, 'transcription', 'part/meccaflat_blues', 'a:3:{s:2:"id";i:143;s:4:"slug";s:34:"143-albert-ammons-mecca-flat-blues";s:7:"_locale";s:2:"fr";}'),
(4751, 'transcription', 'fr/transcription/144-meade-lux-lewis-yancey-special', 'a:3:{s:2:"id";i:144;s:4:"slug";s:34:"144-meade-lux-lewis-yancey-special";s:7:"_locale";s:2:"en";}'),
(4752, 'transcription', 'part/yancey_special_en', 'a:3:{s:2:"id";i:144;s:4:"slug";s:34:"144-meade-lux-lewis-yancey-special";s:7:"_locale";s:2:"en";}'),
(4753, 'transcription', 'en/transcription/144-meade-lux-lewis-yancey-special', 'a:3:{s:2:"id";i:144;s:4:"slug";s:34:"144-meade-lux-lewis-yancey-special";s:7:"_locale";s:2:"fr";}'),
(4754, 'transcription', 'part/yancey_special', 'a:3:{s:2:"id";i:144;s:4:"slug";s:34:"144-meade-lux-lewis-yancey-special";s:7:"_locale";s:2:"fr";}'),
(4755, 'transcription', 'fr/transcription/145-meade-lux-lewis-honky-tonk-train-blues', 'a:3:{s:2:"id";i:145;s:4:"slug";s:42:"145-meade-lux-lewis-honky-tonk-train-blues";s:7:"_locale";s:2:"en";}'),
(4756, 'transcription', 'part/honkytonk_en', 'a:3:{s:2:"id";i:145;s:4:"slug";s:42:"145-meade-lux-lewis-honky-tonk-train-blues";s:7:"_locale";s:2:"en";}'),
(4757, 'transcription', 'en/transcription/145-meade-lux-lewis-honky-tonk-train-blues', 'a:3:{s:2:"id";i:145;s:4:"slug";s:42:"145-meade-lux-lewis-honky-tonk-train-blues";s:7:"_locale";s:2:"fr";}'),
(4758, 'transcription', 'part/honkytonk', 'a:3:{s:2:"id";i:145;s:4:"slug";s:42:"145-meade-lux-lewis-honky-tonk-train-blues";s:7:"_locale";s:2:"fr";}'),
(4759, 'transcription', 'fr/transcription/146-pete-johnson-answer-to-the-boogie', 'a:3:{s:2:"id";i:146;s:4:"slug";s:37:"146-pete-johnson-answer-to-the-boogie";s:7:"_locale";s:2:"en";}'),
(4760, 'transcription', 'part/answer_en', 'a:3:{s:2:"id";i:146;s:4:"slug";s:37:"146-pete-johnson-answer-to-the-boogie";s:7:"_locale";s:2:"en";}'),
(4761, 'transcription', 'en/transcription/146-pete-johnson-answer-to-the-boogie', 'a:3:{s:2:"id";i:146;s:4:"slug";s:37:"146-pete-johnson-answer-to-the-boogie";s:7:"_locale";s:2:"fr";}'),
(4762, 'transcription', 'part/answer', 'a:3:{s:2:"id";i:146;s:4:"slug";s:37:"146-pete-johnson-answer-to-the-boogie";s:7:"_locale";s:2:"fr";}'),
(4763, 'transcription', 'fr/transcription/147-pete-johnson-bottomland-boogie', 'a:3:{s:2:"id";i:147;s:4:"slug";s:34:"147-pete-johnson-bottomland-boogie";s:7:"_locale";s:2:"en";}'),
(4764, 'transcription', 'part/bottomland_boogie_en', 'a:3:{s:2:"id";i:147;s:4:"slug";s:34:"147-pete-johnson-bottomland-boogie";s:7:"_locale";s:2:"en";}'),
(4765, 'transcription', 'en/transcription/147-pete-johnson-bottomland-boogie', 'a:3:{s:2:"id";i:147;s:4:"slug";s:34:"147-pete-johnson-bottomland-boogie";s:7:"_locale";s:2:"fr";}'),
(4766, 'transcription', 'part/bottomland_boogie', 'a:3:{s:2:"id";i:147;s:4:"slug";s:34:"147-pete-johnson-bottomland-boogie";s:7:"_locale";s:2:"fr";}'),
(4767, 'transcription', 'fr/transcription/148-pete-johnson-mr-freddie-blues', 'a:3:{s:2:"id";i:148;s:4:"slug";s:33:"148-pete-johnson-mr-freddie-blues";s:7:"_locale";s:2:"en";}'),
(4768, 'transcription', 'part/freddie_blues_en', 'a:3:{s:2:"id";i:148;s:4:"slug";s:33:"148-pete-johnson-mr-freddie-blues";s:7:"_locale";s:2:"en";}'),
(4769, 'transcription', 'en/transcription/148-pete-johnson-mr-freddie-blues', 'a:3:{s:2:"id";i:148;s:4:"slug";s:33:"148-pete-johnson-mr-freddie-blues";s:7:"_locale";s:2:"fr";}'),
(4770, 'transcription', 'part/freddie_blues', 'a:3:{s:2:"id";i:148;s:4:"slug";s:33:"148-pete-johnson-mr-freddie-blues";s:7:"_locale";s:2:"fr";}'),
(4771, 'transcription', 'fr/transcription/149-pete-johnson-shuffle-boogie', 'a:3:{s:2:"id";i:149;s:4:"slug";s:31:"149-pete-johnson-shuffle-boogie";s:7:"_locale";s:2:"en";}'),
(4772, 'transcription', 'part/shuffle_boogie_en', 'a:3:{s:2:"id";i:149;s:4:"slug";s:31:"149-pete-johnson-shuffle-boogie";s:7:"_locale";s:2:"en";}'),
(4773, 'transcription', 'en/transcription/149-pete-johnson-shuffle-boogie', 'a:3:{s:2:"id";i:149;s:4:"slug";s:31:"149-pete-johnson-shuffle-boogie";s:7:"_locale";s:2:"fr";}'),
(4774, 'transcription', 'part/shuffle_boogie', 'a:3:{s:2:"id";i:149;s:4:"slug";s:31:"149-pete-johnson-shuffle-boogie";s:7:"_locale";s:2:"fr";}'),
(4775, 'transcription', 'fr/transcription/150-count-basie-boogie-woogie', 'a:3:{s:2:"id";i:150;s:4:"slug";s:29:"150-count-basie-boogie-woogie";s:7:"_locale";s:2:"en";}'),
(4776, 'transcription', 'part/boogie_basie_en', 'a:3:{s:2:"id";i:150;s:4:"slug";s:29:"150-count-basie-boogie-woogie";s:7:"_locale";s:2:"en";}'),
(4777, 'transcription', 'en/transcription/150-count-basie-boogie-woogie', 'a:3:{s:2:"id";i:150;s:4:"slug";s:29:"150-count-basie-boogie-woogie";s:7:"_locale";s:2:"fr";}'),
(4778, 'transcription', 'part/boogie_basie', 'a:3:{s:2:"id";i:150;s:4:"slug";s:29:"150-count-basie-boogie-woogie";s:7:"_locale";s:2:"fr";}'),
(4779, 'transcription', 'fr/transcription/151-mary-lou-williams-marys-boogie', 'a:3:{s:2:"id";i:151;s:4:"slug";s:34:"151-mary-lou-williams-marys-boogie";s:7:"_locale";s:2:"en";}'),
(4780, 'transcription', 'part/mary_boogie_en', 'a:3:{s:2:"id";i:151;s:4:"slug";s:34:"151-mary-lou-williams-marys-boogie";s:7:"_locale";s:2:"en";}'),
(4781, 'transcription', 'en/transcription/151-mary-lou-williams-marys-boogie', 'a:3:{s:2:"id";i:151;s:4:"slug";s:34:"151-mary-lou-williams-marys-boogie";s:7:"_locale";s:2:"fr";}'),
(4782, 'transcription', 'part/mary_boogie', 'a:3:{s:2:"id";i:151;s:4:"slug";s:34:"151-mary-lou-williams-marys-boogie";s:7:"_locale";s:2:"fr";}'),
(4783, 'transcription', 'fr/transcription/152-pat-flowers-eight-mile-boogie', 'a:3:{s:2:"id";i:152;s:4:"slug";s:33:"152-pat-flowers-eight-mile-boogie";s:7:"_locale";s:2:"en";}'),
(4784, 'transcription', 'en/transcription/152-pat-flowers-eight-mile-boogie', 'a:3:{s:2:"id";i:152;s:4:"slug";s:33:"152-pat-flowers-eight-mile-boogie";s:7:"_locale";s:2:"fr";}'),
(4785, 'transcription', 'fr/transcription/earl-hines-rosetta', 'a:3:{s:2:"id";i:153;s:4:"slug";s:18:"earl-hines-rosetta";s:7:"_locale";s:2:"en";}'),
(4786, 'transcription', 'part/earl_hines_rosetta_en', 'a:3:{s:2:"id";i:153;s:4:"slug";s:18:"earl-hines-rosetta";s:7:"_locale";s:2:"en";}'),
(4787, 'transcription', 'en/transcription/earl-hines-rosetta', 'a:3:{s:2:"id";i:153;s:4:"slug";s:18:"earl-hines-rosetta";s:7:"_locale";s:2:"fr";}'),
(4788, 'transcription', 'part/earl_hines_rosetta', 'a:3:{s:2:"id";i:153;s:4:"slug";s:18:"earl-hines-rosetta";s:7:"_locale";s:2:"fr";}'),
(4789, 'transcription', 'fr/transcription/alex-hill-stompin-em-down', 'a:3:{s:2:"id";i:154;s:4:"slug";s:25:"alex-hill-stompin-em-down";s:7:"_locale";s:2:"en";}'),
(4790, 'transcription', 'part/alex_hill_stompin_em_down_en', 'a:3:{s:2:"id";i:154;s:4:"slug";s:25:"alex-hill-stompin-em-down";s:7:"_locale";s:2:"en";}'),
(4791, 'transcription', 'en/transcription/alex-hill-stompin-em-down', 'a:3:{s:2:"id";i:154;s:4:"slug";s:25:"alex-hill-stompin-em-down";s:7:"_locale";s:2:"fr";}'),
(4792, 'transcription', 'part/alex_hill_stompin_em_down', 'a:3:{s:2:"id";i:154;s:4:"slug";s:25:"alex-hill-stompin-em-down";s:7:"_locale";s:2:"fr";}'),
(4793, 'transcription', 'fr/transcription/cliff-jackson-crazy-rhythm', 'a:3:{s:2:"id";i:155;s:4:"slug";s:26:"cliff-jackson-crazy-rhythm";s:7:"_locale";s:2:"en";}'),
(4794, 'transcription', 'part/cliff_jackson_crazy_rhythm_en', 'a:3:{s:2:"id";i:155;s:4:"slug";s:26:"cliff-jackson-crazy-rhythm";s:7:"_locale";s:2:"en";}'),
(4795, 'transcription', 'en/transcription/cliff-jackson-crazy-rhythm', 'a:3:{s:2:"id";i:155;s:4:"slug";s:26:"cliff-jackson-crazy-rhythm";s:7:"_locale";s:2:"fr";}'),
(4796, 'transcription', 'part/cliff_jackson_crazy_rhythm', 'a:3:{s:2:"id";i:155;s:4:"slug";s:26:"cliff-jackson-crazy-rhythm";s:7:"_locale";s:2:"fr";}'),
(4797, 'transcription', 'fr/transcription/156-cliff-jackson-royal-garden-blues', 'a:3:{s:2:"id";i:156;s:4:"slug";s:36:"156-cliff-jackson-royal-garden-blues";s:7:"_locale";s:2:"en";}');
INSERT INTO `Redirect` (`id`, `route`, `oldPath`, `parameters`) VALUES
(4798, 'transcription', 'part/cliff_jackson_royal_garden_blues_en', 'a:3:{s:2:"id";i:156;s:4:"slug";s:36:"156-cliff-jackson-royal-garden-blues";s:7:"_locale";s:2:"en";}'),
(4799, 'transcription', 'en/transcription/156-cliff-jackson-royal-garden-blues', 'a:3:{s:2:"id";i:156;s:4:"slug";s:36:"156-cliff-jackson-royal-garden-blues";s:7:"_locale";s:2:"fr";}'),
(4800, 'transcription', 'part/cliff_jackson_royal_garden_blues', 'a:3:{s:2:"id";i:156;s:4:"slug";s:36:"156-cliff-jackson-royal-garden-blues";s:7:"_locale";s:2:"fr";}'),
(4801, 'transcription', 'fr/transcription/cliff-jackson-who', 'a:3:{s:2:"id";i:157;s:4:"slug";s:17:"cliff-jackson-who";s:7:"_locale";s:2:"en";}'),
(4802, 'transcription', 'part/cliff_jackson_who_en', 'a:3:{s:2:"id";i:157;s:4:"slug";s:17:"cliff-jackson-who";s:7:"_locale";s:2:"en";}'),
(4803, 'transcription', 'en/transcription/cliff-jackson-who', 'a:3:{s:2:"id";i:157;s:4:"slug";s:17:"cliff-jackson-who";s:7:"_locale";s:2:"fr";}'),
(4804, 'transcription', 'part/cliff_jackson_who', 'a:3:{s:2:"id";i:157;s:4:"slug";s:17:"cliff-jackson-who";s:7:"_locale";s:2:"fr";}'),
(4805, 'transcription', 'fr/transcription/cliff-jackson-you-took-advantage-of-me', 'a:3:{s:2:"id";i:158;s:4:"slug";s:38:"cliff-jackson-you-took-advantage-of-me";s:7:"_locale";s:2:"en";}'),
(4806, 'transcription', 'part/cliff_jackson_you_took_advantage_of_me_en', 'a:3:{s:2:"id";i:158;s:4:"slug";s:38:"cliff-jackson-you-took-advantage-of-me";s:7:"_locale";s:2:"en";}'),
(4807, 'transcription', 'en/transcription/cliff-jackson-you-took-advantage-of-me', 'a:3:{s:2:"id";i:158;s:4:"slug";s:38:"cliff-jackson-you-took-advantage-of-me";s:7:"_locale";s:2:"fr";}'),
(4808, 'transcription', 'part/cliff_jackson_you_took_advantage_of_me', 'a:3:{s:2:"id";i:158;s:4:"slug";s:38:"cliff-jackson-you-took-advantage-of-me";s:7:"_locale";s:2:"fr";}'),
(4809, 'transcription', 'fr/transcription/don-ewell-parlor-social', 'a:3:{s:2:"id";i:159;s:4:"slug";s:23:"don-ewell-parlor-social";s:7:"_locale";s:2:"en";}'),
(4810, 'transcription', 'part/don_ewell_parlor_social_en', 'a:3:{s:2:"id";i:159;s:4:"slug";s:23:"don-ewell-parlor-social";s:7:"_locale";s:2:"en";}'),
(4811, 'transcription', 'en/transcription/don-ewell-parlor-social', 'a:3:{s:2:"id";i:159;s:4:"slug";s:23:"don-ewell-parlor-social";s:7:"_locale";s:2:"fr";}'),
(4812, 'transcription', 'part/don_ewell_parlor_social', 'a:3:{s:2:"id";i:159;s:4:"slug";s:23:"don-ewell-parlor-social";s:7:"_locale";s:2:"fr";}'),
(4813, 'transcription', 'fr/transcription/fletcher-henderson-unknown-blues', 'a:3:{s:2:"id";i:160;s:4:"slug";s:32:"fletcher-henderson-unknown-blues";s:7:"_locale";s:2:"en";}'),
(4814, 'transcription', 'part/fletcher_henderson_unknown_blues_en', 'a:3:{s:2:"id";i:160;s:4:"slug";s:32:"fletcher-henderson-unknown-blues";s:7:"_locale";s:2:"en";}'),
(4815, 'transcription', 'en/transcription/fletcher-henderson-unknown-blues', 'a:3:{s:2:"id";i:160;s:4:"slug";s:32:"fletcher-henderson-unknown-blues";s:7:"_locale";s:2:"fr";}'),
(4816, 'transcription', 'part/fletcher_henderson_unknown_blues', 'a:3:{s:2:"id";i:160;s:4:"slug";s:32:"fletcher-henderson-unknown-blues";s:7:"_locale";s:2:"fr";}'),
(4817, 'transcription', 'fr/transcription/ralph-sutton-fussin', 'a:3:{s:2:"id";i:161;s:4:"slug";s:19:"ralph-sutton-fussin";s:7:"_locale";s:2:"en";}'),
(4818, 'transcription', 'part/ralph_sutton_fussin_en', 'a:3:{s:2:"id";i:161;s:4:"slug";s:19:"ralph-sutton-fussin";s:7:"_locale";s:2:"en";}'),
(4819, 'transcription', 'en/transcription/ralph-sutton-fussin', 'a:3:{s:2:"id";i:161;s:4:"slug";s:19:"ralph-sutton-fussin";s:7:"_locale";s:2:"fr";}'),
(4820, 'transcription', 'part/ralph_sutton_fussin', 'a:3:{s:2:"id";i:161;s:4:"slug";s:19:"ralph-sutton-fussin";s:7:"_locale";s:2:"fr";}'),
(4821, 'transcription', 'fr/transcription/162-eubie-blake-troublesome-ivories-a-ragtime-rag', 'a:3:{s:2:"id";i:162;s:4:"slug";s:49:"162-eubie-blake-troublesome-ivories-a-ragtime-rag";s:7:"_locale";s:2:"en";}'),
(4822, 'transcription', 'part/eubie_blake_troublesome_ivories_en', 'a:3:{s:2:"id";i:162;s:4:"slug";s:49:"162-eubie-blake-troublesome-ivories-a-ragtime-rag";s:7:"_locale";s:2:"en";}'),
(4823, 'transcription', 'en/transcription/162-eubie-blake-troublesome-ivories-a-ragtime-rag', 'a:3:{s:2:"id";i:162;s:4:"slug";s:49:"162-eubie-blake-troublesome-ivories-a-ragtime-rag";s:7:"_locale";s:2:"fr";}'),
(4824, 'transcription', 'part/eubie_blake_troublesome_ivories', 'a:3:{s:2:"id";i:162;s:4:"slug";s:49:"162-eubie-blake-troublesome-ivories-a-ragtime-rag";s:7:"_locale";s:2:"fr";}'),
(4825, 'transcription', 'fr/transcription/163-albert-ammons-alberts-special-boogie', 'a:3:{s:2:"id";i:163;s:4:"slug";s:40:"163-albert-ammons-alberts-special-boogie";s:7:"_locale";s:2:"en";}'),
(4826, 'transcription', 'en/transcription/163-albert-ammons-alberts-special-boogie', 'a:3:{s:2:"id";i:163;s:4:"slug";s:40:"163-albert-ammons-alberts-special-boogie";s:7:"_locale";s:2:"fr";}'),
(4827, 'transcription', 'fr/transcription/albert-ammons-bugle-boogie', 'a:3:{s:2:"id";i:164;s:4:"slug";s:26:"albert-ammons-bugle-boogie";s:7:"_locale";s:2:"en";}'),
(4828, 'transcription', 'en/transcription/albert-ammons-bugle-boogie', 'a:3:{s:2:"id";i:164;s:4:"slug";s:26:"albert-ammons-bugle-boogie";s:7:"_locale";s:2:"fr";}'),
(4829, 'transcription', 'fr/transcription/165-albert-ammons-swanee-river-boogie', 'a:3:{s:2:"id";i:165;s:4:"slug";s:37:"165-albert-ammons-swanee-river-boogie";s:7:"_locale";s:2:"en";}'),
(4830, 'transcription', 'en/transcription/165-albert-ammons-swanee-river-boogie', 'a:3:{s:2:"id";i:165;s:4:"slug";s:37:"165-albert-ammons-swanee-river-boogie";s:7:"_locale";s:2:"fr";}'),
(4831, 'transcription', 'fr/transcription/hersal-thomas-hersals-blues', 'a:3:{s:2:"id";i:166;s:4:"slug";s:27:"hersal-thomas-hersals-blues";s:7:"_locale";s:2:"en";}'),
(4832, 'transcription', 'en/transcription/hersal-thomas-hersals-blues', 'a:3:{s:2:"id";i:166;s:4:"slug";s:27:"hersal-thomas-hersals-blues";s:7:"_locale";s:2:"fr";}'),
(4833, 'transcription', 'fr/transcription/jimmy-yancey-five-oclock-blues', 'a:3:{s:2:"id";i:167;s:4:"slug";s:30:"jimmy-yancey-five-oclock-blues";s:7:"_locale";s:2:"en";}'),
(4834, 'transcription', 'en/transcription/jimmy-yancey-five-oclock-blues', 'a:3:{s:2:"id";i:167;s:4:"slug";s:30:"jimmy-yancey-five-oclock-blues";s:7:"_locale";s:2:"fr";}'),
(4835, 'transcription', 'fr/transcription/jimmy-yancey-rolling-the-stone', 'a:3:{s:2:"id";i:168;s:4:"slug";s:30:"jimmy-yancey-rolling-the-stone";s:7:"_locale";s:2:"en";}'),
(4836, 'transcription', 'en/transcription/jimmy-yancey-rolling-the-stone', 'a:3:{s:2:"id";i:168;s:4:"slug";s:30:"jimmy-yancey-rolling-the-stone";s:7:"_locale";s:2:"fr";}'),
(4837, 'transcription', 'fr/transcription/169-jimmy-yancey-slow-and-easy-blues', 'a:3:{s:2:"id";i:169;s:4:"slug";s:36:"169-jimmy-yancey-slow-and-easy-blues";s:7:"_locale";s:2:"en";}'),
(4838, 'transcription', 'en/transcription/169-jimmy-yancey-slow-and-easy-blues', 'a:3:{s:2:"id";i:169;s:4:"slug";s:36:"169-jimmy-yancey-slow-and-easy-blues";s:7:"_locale";s:2:"fr";}'),
(4839, 'transcription', 'fr/transcription/meade-lux-lewis-chicago-flyer', 'a:3:{s:2:"id";i:170;s:4:"slug";s:29:"meade-lux-lewis-chicago-flyer";s:7:"_locale";s:2:"en";}'),
(4840, 'transcription', 'en/transcription/meade-lux-lewis-chicago-flyer', 'a:3:{s:2:"id";i:170;s:4:"slug";s:29:"meade-lux-lewis-chicago-flyer";s:7:"_locale";s:2:"fr";}'),
(4841, 'transcription', 'fr/transcription/meade-lux-lewis-glendale-glide', 'a:3:{s:2:"id";i:171;s:4:"slug";s:30:"meade-lux-lewis-glendale-glide";s:7:"_locale";s:2:"en";}'),
(4842, 'transcription', 'en/transcription/meade-lux-lewis-glendale-glide', 'a:3:{s:2:"id";i:171;s:4:"slug";s:30:"meade-lux-lewis-glendale-glide";s:7:"_locale";s:2:"fr";}'),
(4843, 'transcription', 'fr/transcription/pine-top-smith-pinetops-boogie-woogie', 'a:3:{s:2:"id";i:172;s:4:"slug";s:37:"pine-top-smith-pinetops-boogie-woogie";s:7:"_locale";s:2:"en";}'),
(4844, 'transcription', 'en/transcription/pine-top-smith-pinetops-boogie-woogie', 'a:3:{s:2:"id";i:172;s:4:"slug";s:37:"pine-top-smith-pinetops-boogie-woogie";s:7:"_locale";s:2:"fr";}'),
(4845, 'transcription', 'fr/transcription/sammy-price-133rd-street-boogie', 'a:3:{s:2:"id";i:173;s:4:"slug";s:31:"sammy-price-133rd-street-boogie";s:7:"_locale";s:2:"en";}'),
(4846, 'transcription', 'en/transcription/sammy-price-133rd-street-boogie', 'a:3:{s:2:"id";i:173;s:4:"slug";s:31:"sammy-price-133rd-street-boogie";s:7:"_locale";s:2:"fr";}'),
(4847, 'transcription', 'fr/transcription/sammy-price-cow-cow-blues', 'a:3:{s:2:"id";i:174;s:4:"slug";s:25:"sammy-price-cow-cow-blues";s:7:"_locale";s:2:"en";}'),
(4848, 'transcription', 'en/transcription/sammy-price-cow-cow-blues', 'a:3:{s:2:"id";i:174;s:4:"slug";s:25:"sammy-price-cow-cow-blues";s:7:"_locale";s:2:"fr";}'),
(4849, 'transcription', 'fr/transcription/pete-johnson-climbin-and-screamin', 'a:3:{s:2:"id";i:175;s:4:"slug";s:33:"pete-johnson-climbin-and-screamin";s:7:"_locale";s:2:"en";}'),
(4850, 'transcription', 'en/transcription/pete-johnson-climbin-and-screamin', 'a:3:{s:2:"id";i:175;s:4:"slug";s:33:"pete-johnson-climbin-and-screamin";s:7:"_locale";s:2:"fr";}'),
(4851, 'transcription', 'fr/transcription/pete-johnson-death-ray-boogie', 'a:3:{s:2:"id";i:176;s:4:"slug";s:29:"pete-johnson-death-ray-boogie";s:7:"_locale";s:2:"en";}'),
(4852, 'transcription', 'en/transcription/pete-johnson-death-ray-boogie', 'a:3:{s:2:"id";i:176;s:4:"slug";s:29:"pete-johnson-death-ray-boogie";s:7:"_locale";s:2:"fr";}'),
(4853, 'transcription', 'fr/transcription/pete-johnson-just-for-you', 'a:3:{s:2:"id";i:177;s:4:"slug";s:25:"pete-johnson-just-for-you";s:7:"_locale";s:2:"en";}'),
(4854, 'transcription', 'en/transcription/pete-johnson-just-for-you', 'a:3:{s:2:"id";i:177;s:4:"slug";s:25:"pete-johnson-just-for-you";s:7:"_locale";s:2:"fr";}'),
(4855, 'transcription', 'fr/transcription/pete-johnson-rock-it-boogie', 'a:3:{s:2:"id";i:178;s:4:"slug";s:27:"pete-johnson-rock-it-boogie";s:7:"_locale";s:2:"en";}'),
(4856, 'transcription', 'en/transcription/pete-johnson-rock-it-boogie', 'a:3:{s:2:"id";i:178;s:4:"slug";s:27:"pete-johnson-rock-it-boogie";s:7:"_locale";s:2:"fr";}'),
(4857, 'style', 'partitions_boogie-woogie', 'a:3:{s:7:"_locale";s:2:"fr";s:4:"slug";s:13:"boogie-woogie";s:2:"id";i:1;}'),
(4858, 'style', 'sheetmusic_boogie-woogie', 'a:3:{s:7:"_locale";s:2:"en";s:4:"slug";s:13:"boogie-woogie";s:2:"id";i:1;}'),
(4859, 'style', 'stride_piano', 'a:3:{s:7:"_locale";s:2:"en";s:4:"slug";s:12:"stride-piano";s:2:"id";i:2;}'),
(4860, 'style', 'piano_stride', 'a:3:{s:7:"_locale";s:2:"fr";s:4:"slug";s:12:"stride-piano";s:2:"id";i:2;}'),
(4861, 'all', 'sheetmusic', 'a:1:{s:7:"_locale";s:2:"en";}'),
(4862, 'all', 'transcription_en', 'a:1:{s:7:"_locale";s:2:"en";}'),
(4863, 'all', 'transcription_fr', 'a:1:{s:7:"_locale";s:2:"fr";}'),
(4864, 'all', 'partitions', 'a:1:{s:7:"_locale";s:2:"fr";}'),
(4865, 'artist', 'johnson_fr', 'a:3:{s:7:"_locale";s:2:"fr";s:4:"slug";s:15:"james-p-johnson";s:2:"id";i:3;}'),
(4866, 'artist', 'johnson_en', 'a:3:{s:7:"_locale";s:2:"en";s:4:"slug";s:15:"james-p-johnson";s:2:"id";i:3;}'),
(4867, 'artist', 'lion_fr', 'a:3:{s:7:"_locale";s:2:"fr";s:4:"slug";s:21:"willie-the-lion-smith";s:2:"id";i:2;}'),
(4868, 'artist', 'lion_en', 'a:3:{s:7:"_locale";s:2:"en";s:4:"slug";s:21:"willie-the-lion-smith";s:2:"id";i:2;}'),
(4869, 'artist', 'lambert_fr', 'a:3:{s:7:"_locale";s:2:"fr";s:4:"slug";s:14:"donald-lambert";s:2:"id";i:4;}'),
(4870, 'artist', 'lambert_en', 'a:3:{s:7:"_locale";s:2:"en";s:4:"slug";s:14:"donald-lambert";s:2:"id";i:4;}'),
(4871, 'artist', 'fats_waller_fr', 'a:3:{s:7:"_locale";s:2:"fr";s:4:"slug";s:11:"fats-waller";s:2:"id";i:1;}'),
(4872, 'artist', 'fats_waller_en', 'a:3:{s:7:"_locale";s:2:"en";s:4:"slug";s:11:"fats-waller";s:2:"id";i:1;}'),
(4873, 'book', 'en/book/fats-waller-17-solos-for-piano-volume-1', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:1;s:4:"slug";s:39:"fats-waller-17-solos-for-piano-volume-1";}'),
(4874, 'book', 'en/book/fats-waller-17-solos-for-piano-volume-2', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:2;s:4:"slug";s:39:"fats-waller-17-solos-for-piano-volume-2";}'),
(4875, 'book', 'en/book/fats-waller-18-solos-for-piano-volume-3', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:3;s:4:"slug";s:39:"fats-waller-18-solos-for-piano-volume-3";}'),
(4876, 'book', 'en/book/james-p-johnson-17-solos-for-piano-volume-1', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:4;s:4:"slug";s:43:"james-p-johnson-17-solos-for-piano-volume-1";}'),
(4877, 'book', 'en/book/james-p-johnson-17-solos-for-piano-volume-2', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:5;s:4:"slug";s:43:"james-p-johnson-17-solos-for-piano-volume-2";}'),
(4878, 'book', 'en/book/james-p-johnson-the-piano-rolls-17-piano-solos', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:6;s:4:"slug";s:46:"james-p-johnson-the-piano-rolls-17-piano-solos";}'),
(4879, 'book', 'en/book/willie-the-lion-smith-16-solos-for-piano', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:7;s:4:"slug";s:40:"willie-the-lion-smith-16-solos-for-piano";}'),
(4880, 'book', 'en/book/donald-lambert-15-solos-for-piano', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:8;s:4:"slug";s:33:"donald-lambert-15-solos-for-piano";}'),
(4881, 'book', 'en/book/17-boogie-woogie-blues-piano-solos-vol1', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:9;s:4:"slug";s:39:"17-boogie-woogie-blues-piano-solos-vol1";}'),
(4882, 'book', 'en/book/17-boogie-woogie-blues-piano-solos-vol2', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:10;s:4:"slug";s:39:"17-boogie-woogie-blues-piano-solos-vol2";}'),
(4883, 'book', 'fr/book/fats-waller-17-solos-for-piano-volume-1', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:1;s:4:"slug";s:39:"fats-waller-17-solos-for-piano-volume-1";}'),
(4884, 'book', 'fr/book/fats-waller-17-solos-for-piano-volume-2', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:2;s:4:"slug";s:39:"fats-waller-17-solos-for-piano-volume-2";}'),
(4885, 'book', 'fr/book/fats-waller-18-solos-for-piano-volume-3', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:3;s:4:"slug";s:39:"fats-waller-18-solos-for-piano-volume-3";}'),
(4886, 'book', 'fr/book/james-p-johnson-17-solos-for-piano-volume-1', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:4;s:4:"slug";s:43:"james-p-johnson-17-solos-for-piano-volume-1";}'),
(4887, 'book', 'fr/book/james-p-johnson-17-solos-for-piano-volume-2', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:5;s:4:"slug";s:43:"james-p-johnson-17-solos-for-piano-volume-2";}'),
(4888, 'book', 'fr/book/james-p-johnson-the-piano-rolls-17-piano-solos', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:6;s:4:"slug";s:46:"james-p-johnson-the-piano-rolls-17-piano-solos";}'),
(4889, 'book', 'fr/book/willie-the-lion-smith-16-solos-for-piano', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:7;s:4:"slug";s:40:"willie-the-lion-smith-16-solos-for-piano";}'),
(4890, 'book', 'fr/book/donald-lambert-15-solos-for-piano', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:8;s:4:"slug";s:33:"donald-lambert-15-solos-for-piano";}'),
(4891, 'book', 'fr/book/17-boogie-woogie-blues-piano-solos-vol1', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:9;s:4:"slug";s:39:"17-boogie-woogie-blues-piano-solos-vol1";}'),
(4892, 'book', 'fr/book/17-boogie-woogie-blues-piano-solos-vol2', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:10;s:4:"slug";s:39:"17-boogie-woogie-blues-piano-solos-vol2";}'),
(4893, 'artist', 'en/artist/fats-waller', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:1;s:4:"slug";s:11:"fats-waller";}'),
(4894, 'artist', 'en/artist/willie-the-lion-smith', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:2;s:4:"slug";s:21:"willie-the-lion-smith";}'),
(4895, 'artist', 'en/artist/james-p-johnson', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:3;s:4:"slug";s:15:"james-p-johnson";}'),
(4896, 'artist', 'en/artist/donald-lambert', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:4;s:4:"slug";s:14:"donald-lambert";}'),
(4897, 'artist', 'en/artist/albert-ammons', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:5;s:4:"slug";s:13:"albert-ammons";}'),
(4898, 'artist', 'en/artist/pete-johnson', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:6;s:4:"slug";s:12:"pete-johnson";}'),
(4899, 'artist', 'en/artist/jimmy-yancey', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:7;s:4:"slug";s:12:"jimmy-yancey";}'),
(4900, 'artist', 'en/artist/meade-lux-lewis', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:8;s:4:"slug";s:15:"meade-lux-lewis";}'),
(4901, 'artist', 'en/artist/pine-top-smith', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:9;s:4:"slug";s:14:"pine-top-smith";}'),
(4902, 'artist', 'en/artist/jimmy-blythe', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:10;s:4:"slug";s:12:"jimmy-blythe";}'),
(4903, 'artist', 'en/artist/mary-lou-williams', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:11;s:4:"slug";s:17:"mary-lou-williams";}'),
(4904, 'artist', 'en/artist/count-basie', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:12;s:4:"slug";s:11:"count-basie";}'),
(4905, 'artist', 'en/artist/hersal-thomas', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:13;s:4:"slug";s:13:"hersal-thomas";}'),
(4906, 'artist', 'en/artist/pat-flowers', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:14;s:4:"slug";s:11:"pat-flowers";}'),
(4907, 'artist', 'en/artist/sammy-price', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:15;s:4:"slug";s:11:"sammy-price";}'),
(4908, 'artist', 'en/artist/cliff-jackson', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:16;s:4:"slug";s:13:"cliff-jackson";}'),
(4909, 'artist', 'en/artist/ralph-sutton', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:17;s:4:"slug";s:12:"ralph-sutton";}'),
(4910, 'artist', 'en/artist/don-ewell', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:18;s:4:"slug";s:9:"don-ewell";}'),
(4911, 'artist', 'en/artist/fletcher-henderson', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:19;s:4:"slug";s:18:"fletcher-henderson";}'),
(4912, 'artist', 'en/artist/eubie-blake', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:20;s:4:"slug";s:11:"eubie-blake";}'),
(4913, 'artist', 'en/artist/earl-hines', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:21;s:4:"slug";s:10:"earl-hines";}'),
(4914, 'artist', 'en/artist/alex-hill', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:22;s:4:"slug";s:9:"alex-hill";}'),
(4915, 'artist', 'fr/artist/fats-waller', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:1;s:4:"slug";s:11:"fats-waller";}'),
(4916, 'artist', 'fr/artist/willie-the-lion-smith', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:2;s:4:"slug";s:21:"willie-the-lion-smith";}'),
(4917, 'artist', 'fr/artist/james-p-johnson', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:3;s:4:"slug";s:15:"james-p-johnson";}'),
(4918, 'artist', 'fr/artist/donald-lambert', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:4;s:4:"slug";s:14:"donald-lambert";}'),
(4919, 'artist', 'fr/artist/albert-ammons', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:5;s:4:"slug";s:13:"albert-ammons";}'),
(4920, 'artist', 'fr/artist/pete-johnson', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:6;s:4:"slug";s:12:"pete-johnson";}'),
(4921, 'artist', 'fr/artist/jimmy-yancey', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:7;s:4:"slug";s:12:"jimmy-yancey";}'),
(4922, 'artist', 'fr/artist/meade-lux-lewis', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:8;s:4:"slug";s:15:"meade-lux-lewis";}'),
(4923, 'artist', 'fr/artist/pine-top-smith', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:9;s:4:"slug";s:14:"pine-top-smith";}'),
(4924, 'artist', 'fr/artist/jimmy-blythe', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:10;s:4:"slug";s:12:"jimmy-blythe";}'),
(4925, 'artist', 'fr/artist/mary-lou-williams', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:11;s:4:"slug";s:17:"mary-lou-williams";}'),
(4926, 'artist', 'fr/artist/count-basie', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:12;s:4:"slug";s:11:"count-basie";}'),
(4927, 'artist', 'fr/artist/hersal-thomas', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:13;s:4:"slug";s:13:"hersal-thomas";}'),
(4928, 'artist', 'fr/artist/pat-flowers', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:14;s:4:"slug";s:11:"pat-flowers";}'),
(4929, 'artist', 'fr/artist/sammy-price', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:15;s:4:"slug";s:11:"sammy-price";}'),
(4930, 'artist', 'fr/artist/cliff-jackson', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:16;s:4:"slug";s:13:"cliff-jackson";}'),
(4931, 'artist', 'fr/artist/ralph-sutton', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:17;s:4:"slug";s:12:"ralph-sutton";}'),
(4932, 'artist', 'fr/artist/don-ewell', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:18;s:4:"slug";s:9:"don-ewell";}'),
(4933, 'artist', 'fr/artist/fletcher-henderson', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:19;s:4:"slug";s:18:"fletcher-henderson";}'),
(4934, 'artist', 'fr/artist/eubie-blake', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:20;s:4:"slug";s:11:"eubie-blake";}'),
(4935, 'artist', 'fr/artist/earl-hines', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:21;s:4:"slug";s:10:"earl-hines";}'),
(4936, 'artist', 'fr/artist/alex-hill', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:22;s:4:"slug";s:9:"alex-hill";}'),
(4937, 'style', 'en/style/boogie-woogie', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:1;s:4:"slug";s:13:"boogie-woogie";}'),
(4938, 'style', 'en/style/stride-piano', 'a:3:{s:7:"_locale";s:2:"en";s:2:"id";i:2;s:4:"slug";s:12:"stride-piano";}'),
(4939, 'style', 'fr/style/boogie-woogie', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:1;s:4:"slug";s:13:"boogie-woogie";}'),
(4940, 'style', 'fr/style/stride-piano', 'a:3:{s:7:"_locale";s:2:"fr";s:2:"id";i:2;s:4:"slug";s:12:"stride-piano";}');

-- --------------------------------------------------------

--
-- Structure de la table `Style`
--

CREATE TABLE IF NOT EXISTS `Style` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Contenu de la table `Style`
--

INSERT INTO `Style` (`id`, `title`, `slug`) VALUES
(1, 'Boogie-Woogie', 'boogie-woogie'),
(2, 'Stride Piano', 'stride-piano');

-- --------------------------------------------------------

--
-- Structure de la table `Stylesheet`
--

CREATE TABLE IF NOT EXISTS `Stylesheet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `css` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Contenu de la table `Stylesheet`
--

INSERT INTO `Stylesheet` (`id`, `css`, `title`) VALUES
(1, '#body a, #searchResults a, .col-md-3 a {\r\n    text-decoration:underline;\r\n}', 'principale');

-- --------------------------------------------------------

--
-- Structure de la table `StyleTranslation`
--

CREATE TABLE IF NOT EXISTS `StyleTranslation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `markdown` longtext COLLATE utf8_unicode_ci,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seoDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seoKeywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seoTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B8F9C7E02C2AC5D34180C698` (`translatable_id`,`locale`),
  KEY `IDX_B8F9C7E02C2AC5D3` (`translatable_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Contenu de la table `StyleTranslation`
--

INSERT INTO `StyleTranslation` (`id`, `translatable_id`, `name`, `markdown`, `locale`, `seoDescription`, `seoKeywords`, `seoTitle`) VALUES
(1, 1, 'Boogie-Woogie', '<img style="width: 250px;float: right" src="http://www.blueblackjazz.com/media/images/barrelhouse.jpg" />\r\n\r\nLe boogie-woogie est un style de piano, basé sur le blues, joué généralement rapidement. Il est difficile de dater précisément la naissance du boogie-woogie, même si certaines sources indiquent que les premières formes de Boogie-woogie seraient apparues à la fin du XIXème siècle au Texas. Mais c''est au début du XXème siècle que le Boogie-woogie se développe aux Etats-Unis, plus particulièrement à Chicago, dans les « Barrel-House » et « Honky-tonk », sorte de d''établissement dans lequel était servi de l''alcool et ou l''on dansait au son d’un piano. C''est le bruit des locomotives à vapeur qui aurait inspiré aux pianistes de blues, la musique de boogie-woogie.', 'fr', 'L''histoire du Boogie Woogie sur BlueBlackJazz avec Albert Ammons, Pete Johnon, Meade Lux Lewis...', 'boogie woogie, boogie-woogie, blues, albert ammons, pete johnson, meade lux lewis, chicago, partition, transcription, piano jazz, histoire, mp3, blueblackjazz', 'Boogie Woogie | BlueBlackJazz'),
(2, 1, 'Boogie-Woogie', '<img style="width: 250px;float: right" src="http://www.blueblackjazz.com/media/images/barrelhouse.jpg" />\r\n\r\nBoogie-woogie is a style of jazz piano, based on blues music and usually played quickly. The birth of boogie-woogie isn''t precisely known, although some sources indicate that the earliest forms of boogie-woogie would have appeared in the late 19th century in Texas. Early boogie-woogie was played in "Barrel House" and "Honky-tonk", establishments where alcohol was served and people danced around a piano. In the early 20th century the boogie-woogie starts growing the U.S., especially in Chicago. The sound of steam locomotives would have influenced blues pianists, to create boogie-woogie music.', 'en', 'Boogie Woogie history on BlueBlackJazz with Albert Ammons, Pete Johnon, Meade Lux Lewis...', 'boogie woogie, boogie-woogie, blues, albert ammons, pete johnson, meade lux lewis, chicago, sheet music, transcription, jazz piano, history, mp3, blueblackjazz', 'Boogie Woogie | BlueBlackJazz'),
(3, 2, 'Piano Stride', '<img style="width: 250px;float: left" src="http://www.blueblackjazz.com/media/images/harlem.jpg" />Le style stride est apparu à Harlem vers la fin des années 1910, à l''aube du Jazz. A cette époque, le ragtime était trés populaire et les pianiste qui connaissaient les classiques de Scott Joplin évoluèrent vers une musique plus rythmée, plus souple, emprunte de swing. Plus riche que son ancêtre le ragtime, le stride offre une plus grande liberté sonore, plus de souplesse au jeu et relève principalement de l''improvisation. Ce style de jeu qui se suffit à lui même en occupant tout l''espace sonore est trés visuel aussi : la main gauche alterne avec souplesse entre basses et accords (stride signifiant enjambée), tandis que la main droite tisse une serie d''improvisations et de variations sur l''espace restant du clavier. 	stride piano\r\n\r\n<img style="width: 155px;float: right" src="http://www.blueblackjazz.com/media/images/stride3.jpg" />\r\nLes premiers pianistes à pratiquer ce style ont été Lucket Roberts et Eubie Blake, mais c''est James P. Johnson, surnommé "le Père du Piano Stride", qui a su créer le fondement de cette musique à la sonorité noire et profondement encrée dans le blues. Le stride a joué un rôle primordial au piano dans les débuts du jazz et a ouvert la voie à toute une génération de pianistes qui ont marqué l''histoire du jazz.', 'fr', 'Harlem Piano Stride sur BlueBlackJazz avec Fats Waller, James P. Johnson, Willie "The Lion" Smith, Donald Lambert...', 'piano stride, ragtime, harlem, harlem stride, rent parties, fats waller, james p. johnson, partition, stride, transcription, piano jazz, histoire, mp3, blueblackjazz', 'Piano Stride | BlueBlackJazz'),
(4, 2, 'Stride Piano', '<img style="width: 250px;float: left" src="http://www.blueblackjazz.com/media/images/harlem.jpg" />Harlem Stride Piano appeared towards the end of 1910''s when jazz was expanding in Harlem. Richer than ragtime before, stride piano offered more freedom with sounds, more flexibility in the game and came mainly under improvisation. It is a self-sufficient style of game because it fills all the sound space and it is also very visual. As a real "rythm box", the left hand alternates with flexibility between basses and agreements, while the right hand weaves a series of improvisations and variations on the empty space of the keyboard.\r\n\r\n<img style="width: 155px;float: right" src="http://www.blueblackjazz.com/media/images/stride3.jpg" />\r\nLuckey Roberts and Eubie Blake were the first pianists to practice , but James P. Johnson, "Father of Stride Piano" created the foundations of this music, with black sonority and deeply steeped in the blues. Piano stride played on essential part at the beginnings of jazz. It has seen a generation of pianists who left their prints in jazz history.', 'en', 'Harlem Stride Piano on BlueBlackJazz with Fats Waller, James P. Johnson, Willie "The Lion" Smith, Donald Lambert...', 'stride piano, ragtime, harlem, harlem stride, rent parties, fats waller, james p. johnson, sheet music, stride, transcription, jazz piano, history, mp3, blueblackjazz', 'Stride Piano | BlueBlackJazz');

-- --------------------------------------------------------

--
-- Structure de la table `Transcription`
--

CREATE TABLE IF NOT EXISTS `Transcription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) DEFAULT NULL,
  `artist_id` int(11) DEFAULT NULL,
  `style_id` int(11) DEFAULT NULL,
  `priceEuro` decimal(10,2) NOT NULL,
  `img_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `track` smallint(6) DEFAULT NULL,
  `pdf` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mp3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `otherMp3s` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_81E2A27116A2B381` (`book_id`),
  KEY `IDX_81E2A271B7970CF8` (`artist_id`),
  KEY `IDX_81E2A271BACD6074` (`style_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=179 ;

--
-- Contenu de la table `Transcription`
--

INSERT INTO `Transcription` (`id`, `book_id`, `artist_id`, `style_id`, `priceEuro`, `img_title`, `name`, `note`, `track`, `pdf`, `mp3`, `item_number`, `slug`, `otherMp3s`) VALUES
(1, 1, 1, NULL, '7.50', 'blueblack.jpg', 'Blue Black Bottom', 'C Maj', 1, 'FF3ekfJi98nq34JX/FatsWaller_blue_black_bottom.pdf', 'blueblack.mp3', '3017', '1-fats-waller-blue-black-bottom', NULL),
(2, 1, 1, NULL, '7.50', 'numb.jpg', 'Numb Fumblin''', 'G Maj', 2, 'FF3ekfJi98nq34JX/FatsWaller_numb_fumblin.pdf', 'numb.mp3', '3019', '2-fats-waller-numb-fumblin', NULL),
(3, 1, 1, NULL, '7.50', 'loveme.jpg', 'Love Me Or Leave Me', 'F min', 3, 'FF3ekfJi98nq34JX/FatsWaller_love_me_or_leave_me.pdf', 'loveme.mp3', '3010', '3-fats-waller-love-me-or-leave-me', NULL),
(4, 1, 1, NULL, '7.50', 'valentine.jpg', 'Valentine Stomp', 'D Maj', 4, 'FF3ekfJi98nq34JX/FatsWaller_valentine_stomp.pdf', 'valentine.mp3', '3043', '4-fats-waller-valentine-stomp', NULL),
(5, 1, 1, NULL, '7.50', 'falling.jpg', 'I''ve Got A Feeling I''m Falling', 'Eb Maj', 5, 'FF3ekfJi98nq34JX/FatsWaller_i_ve_got_a_feeling_i_m_falling.pdf', 'falling.mp3', '3040', '5-fats-waller-ive-got-a-feeling-im-falling', NULL),
(6, 1, 1, NULL, '7.50', 'smashing.jpg', 'Smashing Thirds''', 'G Maj', 6, 'FF3ekfJi98nq34JX/FatsWaller_smashing_thirds.pdf', 'smashing.mp3', '3029', '6-fats-waller-smashing-thirds', NULL),
(7, 1, 1, NULL, '7.50', 'turnon.jpg', 'Turn On The Heat', 'C Maj', 7, 'FF3ekfJi98nq34JX/FatsWaller_turn_on_the_heat.pdf', 'turnon.mp3', '3041', '7-fats-waller-turn-on-the-heat', NULL),
(8, 1, 1, NULL, '7.50', 'myfate.jpg', 'My Fate Is In Your Hands', 'C Maj', 8, 'FF3ekfJi98nq34JX/FatsWaller_my_fate_is_in_your_hands.pdf', 'myfate.mp3', '3016', '8-fats-waller-my-fate-is-in-your-hands', NULL),
(9, 1, 1, NULL, '7.50', 'african.jpg', 'African Ripples', 'D Maj', 9, 'FF3ekfJi98nq34JX/FatsWaller_african_ripple.pdf', 'african.mp3', '3001', '9-fats-waller-african-ripples', NULL),
(10, 1, 1, NULL, '7.50', 'hallelujah.jpg', 'Hallelujah', 'D Maj', 10, 'FF3ekfJi98nq34JX/FatsWaller_hallelujah.pdf', 'hallelujah.mp3', '3032', '10-fats-waller-hallelujah', NULL),
(11, 1, 1, NULL, '7.50', 'california.jpg', 'California Here I Come', 'C Maj', 11, 'FF3ekfJi98nq34JX/FatsWaller_california_here_i_come.pdf', 'california.mp3', '3018', '11-fats-waller-california-here-i-come', NULL),
(12, 1, 1, NULL, '7.50', 'top.jpg', 'You''re the top', 'Eb Maj', 12, 'FF3ekfJi98nq34JX/FatsWaller_you_re_the_top.pdf', 'top.mp3', '3049', '12-fats-waller-youre-the-top', NULL),
(13, 1, 1, NULL, '7.50', 'onceupon.jpg', 'Because Of Once Upon a Time', 'Ab Maj', 13, 'FF3ekfJi98nq34JX/FatsWaller_because_of_once_upon_a_time.pdf', 'onceupon.mp3', '3013', '13-fats-waller-because-of-once-upon-a-time', NULL),
(14, 1, 1, NULL, '7.50', 'faust.jpg', 'Swaltzing with Faust', 'D Maj', 14, 'FF3ekfJi98nq34JX/FatsWaller_waltz.pdf', 'faust.mp3', '3033', '14-fats-waller-swaltzing-with-faust', NULL),
(15, 1, 1, NULL, '7.50', 'intermezzo.jpg', 'Intermezzo', 'F Maj', 15, 'FF3ekfJi98nq34JX/FatsWaller_intermezzo.pdf', 'intermezzo.mp3', '3044', '15-fats-waller-intermezzo', NULL),
(16, 1, 1, NULL, '7.50', 'carolina.jpg', 'Carolina Shout', 'G Maj', 16, 'FF3ekfJi98nq34JX/FatsWaller_carolina_shout.pdf', 'carolina.mp3', '3020', '16-fats-waller-carolina-shout', NULL),
(17, 1, 1, NULL, '7.50', 'honeysuckle.jpg', 'Honeysuckle Rose', 'F Maj', 17, 'FF3ekfJi98nq34JX/FatsWaller_honeysuckle_rose.pdf', 'honeysuckle_rose.mp3', '3038', '17-fats-waller-honeysuckle-rose', 'honeysuckle_rose_-_1941.mp3'),
(18, 2, 1, NULL, '7.50', 'muscleshoals.jpg', 'Muscle Shoals Blues', 'C Maj', 1, 'FF3ekfJi98nq34JX/FatsWaller_muscle_shoals_blues.pdf', 'muscle.mp3', '3014', '18-fats-waller-muscle-shoals-blues', NULL),
(19, 2, 1, NULL, '7.50', 'birmingham.jpg', 'Birmingham Blues', 'F Maj', 2, 'FF3ekfJi98nq34JX/FatsWaller_birmingham_blues.pdf', 'birmingham.mp3', '3015', '19-fats-waller-birmingham-blues', 'birmingham_blues_-_1922.mp3'),
(20, 2, 1, NULL, '7.50', 'handful.jpg', 'Handful Of Keys', 'F Maj', 3, 'FF3ekfJi98nq34JX/FatsWaller_handful_of_keys.pdf', 'handful.mp3', '3036', '20-fats-waller-handful-of-keys', NULL),
(21, 2, 1, NULL, '7.50', 'babyoh.jpg', 'Baby Oh, Where Can You Be ?', 'G Maj', 4, 'FF3ekfJi98nq34JX/FatsWaller_baby_oh_where_can_you_be.pdf', 'babyoh.mp3', '3009', '21-fats-waller-baby-oh-where-can-you-be-', NULL),
(22, 2, 1, NULL, '7.50', 'savannah.jpg', 'Sweet Savannah Sue', 'Bb Maj', 5, 'FF3ekfJi98nq34JX/FatsWaller_sweet_savannah_sue.pdf', 'savannah.mp3', '3035', '22-fats-waller-sweet-savannah-sue', NULL),
(23, 2, 1, NULL, '7.50', 'viper.jpg', 'Viper''s Drag', 'D min', 6, 'FF3ekfJi98nq34JX/FatsWaller_viper_s_drag.pdf', 'viper.mp3', '3045', '23-fats-waller-vipers-drag', NULL),
(24, 2, 1, NULL, '7.50', 'alligator35.jpg', 'Alligator Crawl', 'C Maj', 7, 'FF3ekfJi98nq34JX/FatsWaller_alligator_crawl_1935.pdf', 'alligator35.mp3', '3007', '24-fats-waller-alligator-crawl', NULL),
(25, 2, 1, NULL, '7.50', 'mischief.jpg', 'Keepin'' Out Of Mischief Now', 'C maj', 8, 'FF3ekfJi98nq34JX/FatsWaller_keepin_out_of_mischief_now.pdf', 'mischief.mp3', '3046', '25-fats-waller-keepin-out-of-mischief-now', NULL),
(26, 2, 1, NULL, '7.50', 'teafortwo.jpg', 'Tea For Two', 'Eb maj', 9, 'FF3ekfJi98nq34JX/FatsWaller_tea_for_two.pdf', 'teafortwo.mp3', '3039', '26-fats-waller-tea-for-two', NULL),
(27, 2, 1, NULL, '7.50', 'piccadilly.jpg', 'London Suite - I. Piccadilly', 'C min', 10, 'FF3ekfJi98nq34JX/FatsWaller_piccadilly.pdf', 'piccadilly.mp3', '3048', '27-fats-waller-london-suite-i-piccadilly', NULL),
(28, 2, 1, NULL, '7.50', 'chelsea.jpg', 'London Suite - II. Chelsea', 'G maj', 11, 'FF3ekfJi98nq34JX/FatsWaller_chelsea.pdf', 'chelsea.mp3', '3050', '28-fats-waller-london-suite-ii-chelsea', NULL),
(29, 2, 1, NULL, '7.50', 'soho.jpg', 'London Suite - III. Soho', 'G maj', 12, 'FF3ekfJi98nq34JX/FatsWaller_soho.pdf', 'soho.mp3', '3002', '29-fats-waller-london-suite-iii-soho', NULL),
(30, 2, 1, NULL, '7.50', 'bondstreet.jpg', 'London Suite - IV. Bond Street', 'F maj', 13, 'FF3ekfJi98nq34JX/FatsWaller_bond_street.pdf', 'bondstreet.mp3', '3004', '30-fats-waller-london-suite-iv-bond-street', NULL),
(31, 2, 1, NULL, '7.50', 'limehouse.jpg', 'London Suite - V. Limehouse', 'F maj', 14, 'FF3ekfJi98nq34JX/FatsWaller_limehouse.pdf', 'limehouse.mp3', '3006', '31-fats-waller-london-suite-v-limehouse', NULL),
(32, 2, 1, NULL, '7.50', 'whitechapel.jpg', 'London Suite - VI. Whitechapel', 'C min', 15, 'FF3ekfJi98nq34JX/FatsWaller_whitechapel.pdf', 'whitechapel.mp3', '3008', '32-fats-waller-london-suite-vi-whitechapel', NULL),
(33, 2, 1, NULL, '7.50', 'rockin.jpg', 'Rockin'' Chair', 'F maj', 16, 'FF3ekfJi98nq34JX/FatsWaller_rockin_chair.pdf', 'rockin.mp3', '3021', '33-fats-waller-rockin-chair', NULL),
(34, 2, 1, NULL, '7.50', 'ringdem.jpg', 'Ring Dem Bells', 'C maj', 17, 'FF3ekfJi98nq34JX/FatsWaller_ring_dem_bells.pdf', 'ringdem.mp3', '3023', '34-fats-waller-ring-dem-bells', NULL),
(35, 3, 1, NULL, '7.50', 'misbehavin.jpg', 'Ain''t Misbehavin''', 'C maj', 1, 'FF3ekfJi98nq34JX/FatsWaller_aint_misbehavin.pdf', 'misbehavin.mp3', '3003', '35-fats-waller-aint-misbehavin', 'aint_misbehavin_-_1929.mp3'),
(36, 3, 1, NULL, '7.50', 'gladyse.jpg', 'Gladyse', 'D maj', 2, 'FF3ekfJi98nq34JX/FatsWaller_gladyse.pdf', 'gladyse.mp3', '3028', '36-fats-waller-gladyse', NULL),
(37, 3, 1, NULL, '7.50', 'waiting.jpg', 'Waiting At The End Of The Road', 'C maj', 3, 'FF3ekfJi98nq34JX/FatsWaller_waiting_at_the_end_of_the_road.pdf', 'waiting.mp3', '3047', '37-fats-waller-waiting-at-the-end-of-the-road', NULL),
(38, 3, 1, NULL, '7.50', 'goinabout.jpg', 'Goin'' About', 'F maj', 4, 'FF3ekfJi98nq34JX/FatsWaller_goin_about.pdf', 'goinabout.mp3', '3030', '38-fats-waller-goin-about', NULL),
(39, 3, 1, NULL, '7.50', 'myfeelings.jpg', 'My Feelings Are Hurt', 'F maj', 5, 'FF3ekfJi98nq34JX/FatsWaller_my_feelings_are_hurts.pdf', 'myfeelings.mp3', '3052', '39-fats-waller-my-feelings-are-hurt', NULL),
(40, 3, 1, NULL, '7.50', 'clothesline.jpg', 'Clothesline Ballet', 'Ab maj', 6, 'FF3ekfJi98nq34JX/FatsWaller_clothesline_ballet.pdf', 'clothesline.mp3', '3022', '40-fats-waller-clothesline-ballet', NULL),
(41, 3, 1, NULL, '7.50', 'alligator34.jpg', 'Alligator Crawl', 'C maj', 7, 'FF3ekfJi98nq34JX/FatsWaller_alligator_crawl_1934.pdf', 'alligator34.mp3', '3005', '41-fats-waller-alligator-crawl', NULL),
(42, 3, 1, NULL, '7.50', 'flatblues.jpg', 'E-Flat Blues', 'Eb maj', 8, 'FF3ekfJi98nq34JX/FatsWaller_e-flat_blues.pdf', 'flatblues.mp3', '3024', '42-fats-waller-e-flat-blues', NULL),
(43, 3, 1, NULL, '7.50', 'zonky.jpg', 'Zonky', 'F min', 9, 'FF3ekfJi98nq34JX/FatsWaller_zonky.pdf', 'zonky.mp3', '3051', '43-fats-waller-zonky', NULL),
(44, 3, 1, NULL, '7.50', 'fantasy.jpg', 'Russian Fantasy', 'C min', 10, 'FF3ekfJi98nq34JX/FatsWaller_russian_fantasy.pdf', 'fantasy.mp3', '3025', '44-fats-waller-russian-fantasy', NULL),
(45, 3, 1, NULL, '7.50', 'basin.jpg', 'Basin Street Blues', 'C maj', 11, 'FF3ekfJi98nq34JX/FatsWaller_basin_street_blues.pdf', 'basin.mp3', '3011', '45-fats-waller-basin-street-blues', NULL),
(46, 3, 1, NULL, '7.50', 'stardust.jpg', 'Stardust', 'C maj', 12, 'FF3ekfJi98nq34JX/FatsWaller_stardust.pdf', 'stardust.mp3', '3031', '46-fats-waller-stardust', NULL),
(47, 3, 1, NULL, '7.50', 'nobody.jpg', 'I Ain''t Got Nobody', 'F maj', 13, 'FF3ekfJi98nq34JX/FatsWaller_i_aint_got_nobody.pdf', 'nobody.mp3', '3042', '47-fats-waller-i-aint-got-nobody', NULL),
(48, 3, 1, NULL, '7.50', 'hallelujah38.jpg', 'Hallelujah (Yacht Club version)', 'D maj', 14, 'FF3ekfJi98nq34JX/FatsWaller_hallelujah_1938.pdf', 'hallelujah38.mp3', '3034', '48-fats-waller-hallelujah-yacht-club-version', NULL),
(49, 3, 1, NULL, '7.50', 'louisblues.jpg', 'Saint Louis Blues', 'G min', 15, 'FF3ekfJi98nq34JX/FatsWaller_st_louis_blues.pdf', 'louisblues.mp3', '3027', '49-fats-waller-saint-louis-blues', 'louis.mp3'),
(50, 3, 1, NULL, '7.50', 'remember.jpg', 'Then You''ll Remember Me', 'Bb maj', 16, 'FF3ekfJi98nq34JX/FatsWaller_then_you_ll_remember_me.pdf', 'remember.mp3', '3037', '50-fats-waller-then-youll-remember-me', NULL),
(51, 3, 1, NULL, '7.50', 'georgia.jpg', 'Georgia On My Mind', 'F maj', 17, 'FF3ekfJi98nq34JX/FatsWaller_georgia_on_my_mind.pdf', 'georgia.mp3', '3026', '51-fats-waller-georgia-on-my-mind', NULL),
(52, 3, 1, NULL, '7.50', 'martinique.jpg', 'Martinique', 'Bb maj', 18, 'FF3ekfJi98nq34JX/FatsWaller_martinique.pdf', 'martinique.mp3', '3012', '52-fats-waller-martinique', NULL),
(53, 4, 3, NULL, '7.50', 'strut.jpg', 'Harlem Strut', 'C maj', 1, 'FF3ekfJi98nq34JX/JamesPJohnson_the_harlem_strut.pdf', 'strut.mp3', '3211', '53-james-p-johnson-harlem-strut', NULL),
(54, 4, 3, NULL, '7.50', 'carolina_johnson.jpg', 'Carolina Shout', 'G maj', 2, 'FF3ekfJi98nq34JX/JamesPJohnson_carolina_shout.pdf', 'carolina_johnson.mp3', '3203', '54-james-p-johnson-carolina-shout', NULL),
(55, 4, 3, NULL, '7.50', 'riffs.jpg', 'Riffs', 'Bb maj', 3, 'FF3ekfJi98nq34JX/JamesPJohnson_riffs.pdf', 'riffs.mp3', '3216', '55-james-p-johnson-riffs', NULL),
(56, 4, 3, NULL, '7.50', 'feeling.jpg', 'Feeling Blues', 'Bb maj', 4, 'FF3ekfJi98nq34JX/JamesPJohnson_feeling_blue.pdf', 'feeling.mp3', '3209', '56-james-p-johnson-feeling-blues', NULL),
(57, 4, 3, NULL, '7.50', 'jingles.jpg', 'Jingles', 'D maj', 5, 'FF3ekfJi98nq34JX/JamesPJohnson_jingles.pdf', 'jingles.mp3', '3206', '57-james-p-johnson-jingles', NULL),
(58, 4, 3, NULL, '7.50', 'crying.jpg', 'Crying For The Carolines', 'E min', 6, 'FF3ekfJi98nq34JX/JamesPJohnson_crying_for_the_carolines.pdf', 'crying.mp3', '3205', '58-james-p-johnson-crying-for-the-carolines', NULL),
(59, 4, 3, NULL, '7.50', 'modernistic.jpg', 'Modernistic', 'Ab maj', 7, 'FF3ekfJi98nq34JX/JamesPJohnson_modernistic.pdf', 'modernistic.mp3', '3212', '59-james-p-johnson-modernistic', NULL),
(60, 4, 3, NULL, '7.50', 'ifdreams.jpg', 'If Dreams Come True', 'C maj', 8, 'FF3ekfJi98nq34JX/JamesPJohnson_if_dreams_come_true.pdf', 'ifdreams.mp3', '3202', '60-james-p-johnson-if-dreams-come-true', NULL),
(61, 4, 3, NULL, '7.50', 'mule.jpg', 'Mule Walk Stomp', 'Bb maj', 9, 'FF3ekfJi98nq34JX/JamesPJohnson_mule_walk_stomp.pdf', 'mule.mp3', '3214', '61-james-p-johnson-mule-walk-stomp', NULL),
(62, 4, 3, NULL, '7.50', 'flatdream.jpg', 'A Flat Dream', 'Ab maj', 10, 'FF3ekfJi98nq34JX/JamesPJohnson_a_flat_dream.pdf', 'flatdream.mp3', '3201', '62-james-p-johnson-a-flat-dream', NULL),
(63, 4, 3, NULL, '7.50', 'daintiness.jpg', 'Daintiness Rag', 'Ab maj', 11, 'FF3ekfJi98nq34JX/JamesPJohnson_daintiness_rag.pdf', 'daintiness.mp3', '3207', '63-james-p-johnson-daintiness-rag', NULL),
(64, 4, 3, NULL, '7.50', 'letter.jpg', 'I''m Gonna Sit Right Down', 'Db maj', 12, 'FF3ekfJi98nq34JX/JamesPJohnson_i_m_gonna_sit_right_down.pdf', 'letter.mp3', '3215', '64-james-p-johnson-im-gonna-sit-right-down', NULL),
(65, 4, 3, NULL, '7.50', 'keepoff.jpg', 'Keep Off The Grass', 'F maj', 13, 'FF3ekfJi98nq34JX/JamesPJohnson_keep_off_the_grass.pdf', 'keepoff.mp3', '3208', '65-james-p-johnson-keep-off-the-grass', NULL),
(66, 4, 3, NULL, '7.50', 'crazy.jpg', 'I''m Crazy ''Bout My Baby', 'Eb maj', 14, 'FF3ekfJi98nq34JX/JamesPJohnson_i_m_crazy_bout_my_baby.pdf', 'crazy.mp3', '3213', '66-james-p-johnson-im-crazy-bout-my-baby', NULL),
(67, 4, 3, NULL, '7.50', 'twilight.jpg', 'Twilight Rag', 'D maj', 15, 'FF3ekfJi98nq34JX/JamesPJohnson_twilight_rag.pdf', 'twilight.mp3', '3217', '67-james-p-johnson-twilight-rag', NULL),
(68, 4, 3, NULL, '7.50', 'jersey.jpg', 'Jersey Sweet', 'Db maj', 16, 'FF3ekfJi98nq34JX/JamesPJohnson_jersey_sweet.pdf', 'jersey.mp3', '3204', '68-james-p-johnson-jersey-sweet', NULL),
(69, 4, 3, NULL, '7.50', 'liza.jpg', 'Liza', 'Eb maj', 17, 'FF3ekfJi98nq34JX/JamesPJohnson_liza.pdf', 'liza.mp3', '3210', '69-james-p-johnson-liza', NULL),
(70, 6, 3, NULL, '7.50', 'roll-mamasblues.jpg', 'Mama''s Blues', 'Bb maj', 1, 'FF3ekfJi98nq34JX/JamesPJohnson_mamas_blues.pdf', 'roll-mamasblues.mp3', '3225', '70-james-p-johnson-mamas-blues', NULL),
(71, 6, 3, NULL, '7.50', 'roll-capricerag.jpg', 'Caprice Rag', 'F# maj', 2, 'FF3ekfJi98nq34JX/JamesPJohnson_caprice_rag.pdf', 'roll-capricerag.mp3', '3221', '71-james-p-johnson-caprice-rag', NULL),
(72, 6, 3, NULL, '7.50', 'roll-steeplechase.jpg', 'Steeplechase Rag', 'D min', 3, 'FF3ekfJi98nq34JX/JamesPJohnson_steeplechase_rag.pdf', 'roll-steeplechase.mp3', '3232', '72-james-p-johnson-steeplechase-rag', NULL),
(73, 6, 3, NULL, '7.50', 'roll-stopit.jpg', 'Stop It', 'D Maj', 4, 'FF3ekfJi98nq34JX/JamesPJohnson_stop_it.pdf', 'roll-stopit.mp3', '3233', '73-james-p-johnson-stop-it', NULL),
(74, 6, 3, NULL, '7.50', 'roll-carolinashout.jpg', 'Carolina Shout', 'G Maj', 5, 'FF3ekfJi98nq34JX/JamesPJohnson_carolina_shout_roll.pdf', 'roll-carolinashout.mp3', '3222', '74-james-p-johnson-carolina-shout', NULL),
(75, 6, 3, NULL, '7.50', 'roll-eccentricity.jpg', 'Eccentricity', 'D Maj', 6, 'FF3ekfJi98nq34JX/JamesPJohnson_eccentricity.pdf', 'roll-eccentricity.mp3', '3227', '75-james-p-johnson-eccentricity', NULL),
(76, 6, 3, NULL, '7.50', 'roll-ittakeslove.jpg', 'It Takes Love To Cure The Heart''s Disease', 'G Maj', 7, 'FF3ekfJi98nq34JX/JamesPJohnson_it_takes_love_to_cure_the_hearts_disease.pdf', 'roll-ittakeslove.mp3', '3219', '76-james-p-johnson-it-takes-love-to-cure-the-hearts-disease', NULL),
(77, 6, 3, NULL, '7.50', 'roll-drjazz.jpg', 'Dr Jazzes Raz-Ma-Taz', 'F Maj', 8, 'FF3ekfJi98nq34JX/JamesPJohnson_dr_jazzes_raz-ma-taz.pdf', 'roll-drjazz.mp3', '3226', '77-james-p-johnson-dr-jazzes-raz-ma-taz', NULL),
(78, 6, 3, NULL, '7.50', 'roll-roumania.jpg', 'Roumania', 'Eb Maj', 9, 'FF3ekfJi98nq34JX/JamesPJohnson_roumania.pdf', 'roll-roumania.mp3', '3230', '78-james-p-johnson-roumania', NULL),
(79, 6, 3, NULL, '7.50', 'roll-arkansas.jpg', 'Arkansas Blues', 'F maj', 10, 'FF3ekfJi98nq34JX/JamesPJohnson_arkansas_blues.pdf', 'roll-arkansas.mp3', '3218', '79-james-p-johnson-arkansas-blues', NULL),
(80, 6, 3, NULL, '7.50', 'roll-joeturnerblues.jpg', 'Joe Turner Blues', 'Eb Maj', 11, 'FF3ekfJi98nq34JX/JamesPJohnson_joe_turner_blues.pdf', 'roll-joeturnerblues.mp3', '3223', '80-james-p-johnson-joe-turner-blues', NULL),
(81, 6, 3, NULL, '7.50', 'roll-harlemstrut.jpg', 'Harlem Strut', 'C Maj', 12, 'FF3ekfJi98nq34JX/JamesPJohnson_harlem_strut_roll.pdf', 'roll-harlemstrut.mp3', '3231', '81-james-p-johnson-harlem-strut', NULL),
(82, 6, 3, NULL, '7.50', 'roll-railroadman.jpg', 'Railroad Man', 'Eb Maj', 13, 'FF3ekfJi98nq34JX/JamesPJohnson_railroad_man.pdf', 'roll-railroadman.mp3', '3228', '82-james-p-johnson-railroad-man', NULL),
(83, 6, 3, NULL, '7.50', 'roll-blackman.jpg', 'Black Man', 'Eb Maj', 14, 'FF3ekfJi98nq34JX/JamesPJohnson_black_man.pdf', 'roll-blackman.mp3', '3220', '83-james-p-johnson-black-man', NULL),
(84, 6, 3, NULL, '7.50', 'roll-charleston.jpg', 'Charleston', 'Bb Maj', 15, 'FF3ekfJi98nq34JX/JamesPJohnson_charleston.pdf', 'roll-charleston.mp3', '3224', '84-james-p-johnson-charleston', NULL),
(85, 6, 3, NULL, '7.50', 'roll-harlemchoclate.jpg', 'Harlem Choc''late Babies On Parade', 'Bb Maj', 16, 'FF3ekfJi98nq34JX/JamesPJohnson_harlem_choclate_babies_on_parade.pdf', 'roll-harlemchoclat.mp3', '3229', '85-james-p-johnson-harlem-choclate-babies-on-parade', NULL),
(86, 6, 3, NULL, '7.50', 'roll-sugar.jpg', 'Sugar', 'F Maj', 17, 'FF3ekfJi98nq34JX/JamesPJohnson_sugar.pdf', 'roll-sugar.mp3', '3234', '86-james-p-johnson-sugar', NULL),
(87, 5, 3, NULL, '7.50', 'arkansas_blues.jpg', 'Arkansas Blues', 'Bb maj', 15, 'FF3ekfJi98nq34JX/JamesPJohnson_arkansas_blues.pdf', 'arkansas_blues.mp3', '3236', '87-james-p-johnson-arkansas-blues', NULL),
(88, 5, 3, NULL, '7.50', 'aint_cha_got_music.jpg', 'Ain''t Cha Got Music', 'Eb maj', 17, 'FF3ekfJi98nq34JX/JamesPJohnson_aint_cha_got_music.pdf', 'aint_cha_got_music.mp3', '3235', '88-james-p-johnson-aint-cha-got-music', NULL),
(89, 5, 3, NULL, '7.50', 'bleeding-hearted_blues.jpg', 'Bleeding-Hearted Blues', 'Eb maj', 1, 'FF3ekfJi98nq34JX/JamesPJohnson_bleeding-hearted_blues.pdf', 'bleeding-hearted_blues.mp3', '3237', '89-james-p-johnson-bleeding-hearted-blues', 'bleeding.mp3'),
(90, 5, 3, NULL, '7.50', 'blueberrye_rhyme.jpg', 'Blueberry Rhyme', 'F maj', 8, 'FF3ekfJi98nq34JX/JamesPJohnson_blueberry_rhyme.pdf', 'blueberrye_rhyme.mp3', '3238', '90-james-p-johnson-blueberry-rhyme', NULL),
(91, 5, 3, NULL, '7.50', 'carolina_balmoral.jpg', 'Carolina Balmoral', 'F maj', 16, 'FF3ekfJi98nq34JX/JamesPJohnson_carolina_balmoral.pdf', 'carolina_balmoral.mp3', '3239', '91-james-p-johnson-carolina-balmoral', 'balmoral.mp3'),
(92, 5, 3, NULL, '7.50', 'jazz-a-mine.jpg', 'Concerto Jazz-A-Mine', 'Ab maj', 13, 'FF3ekfJi98nq34JX/JamesPJohnson_jazz-a-mine.pdf', 'jazz-a-mine.mp3', '3240', '92-james-p-johnson-concerto-jazz-a-mine', NULL),
(93, 5, 3, NULL, '7.50', 'fascination.jpg', 'Fascination', 'F maj', 7, 'FF3ekfJi98nq34JX/JamesPJohnson_fascination.pdf', 'fascination.mp3', '3241', '93-james-p-johnson-fascination', NULL),
(94, 5, 3, NULL, '7.50', 'gut_stomp.jpg', 'Gut Stomp', 'Ab maj', 12, 'FF3ekfJi98nq34JX/JamesPJohnson_gut_stomp.pdf', 'gut_stomp.mp3', '3242', '94-james-p-johnson-gut-stomp', NULL),
(95, 5, 3, NULL, '7.50', 'honeysuckle_rose.jpg', 'Honeysuckle Rose', 'F maj', 10, 'FF3ekfJi98nq34JX/JamesPJohnson_honeysuckle_rose.pdf', 'honeysuckle_rose.mp3', '3243', '95-james-p-johnson-honeysuckle-rose', NULL),
(96, 5, 3, NULL, '7.50', 'keep_movin.jpg', 'Keep Movin''', 'Bb maj', 14, 'FF3ekfJi98nq34JX/JamesPJohnson_keep_movin.pdf', 'keep_movin.mp3', '3244', '96-james-p-johnson-keep-movin', NULL),
(97, 5, 3, NULL, '7.50', 'old_fashioned_love.jpg', 'Old Fashioned Love', 'F maj', 11, 'FF3ekfJi98nq34JX/JamesPJohnson_old_fashioned_love.pdf', 'old_fashioned_love.mp3', '3245', '97-james-p-johnson-old-fashioned-love', NULL),
(98, 5, 3, NULL, '7.50', 'scouting_around.jpg', 'Scouting Around', 'Ab maj', 4, 'FF3ekfJi98nq34JX/JamesPJohnson_scouting_around.pdf', 'scouting_around.mp3', '3246', '98-james-p-johnson-scouting-around', NULL),
(99, 5, 3, NULL, '7.50', 'snowy_morning_blues.jpg', 'Snowy Morning Blues', 'G Maj', 5, 'FF3ekfJi98nq34JX/JamesPJohnson_snowy_morning_blues.pdf', 'snowy_morning_blues.mp3', '3247', '99-james-p-johnson-snowy-morning-blues', NULL),
(100, 5, 3, NULL, '7.50', 'squeeze_me.jpg', 'Squeeze Me', 'G maj', 9, 'FF3ekfJi98nq34JX/JamesPJohnson_squeeze_me.pdf', 'squeeze_me.mp3', '3248', '100-james-p-johnson-squeeze-me', NULL),
(101, 5, 3, NULL, '7.50', 'toddlin.jpg', 'Toddlin''', 'Eb maj', 3, 'FF3ekfJi98nq34JX/JamesPJohnson_toddlin.pdf', 'toddlin.mp3', '3249', '101-james-p-johnson-toddlin', NULL),
(102, 5, 3, NULL, '7.50', 'what_is_this_thing_called_love.jpg', 'What Is This Thing Called Love?', 'C maj', 6, 'FF3ekfJi98nq34JX/JamesPJohnson_what_is_this_thing_called_love_me.pdf', 'what_is_this_thing_called_love.mp3', '3250', '102-james-p-johnson-what-is-this-thing-called-love', NULL),
(103, 5, 3, NULL, '7.50', 'you_cant_do.jpg', 'You Can''t Do What My Last Man Did', 'Db Maj', 2, 'FF3ekfJi98nq34JX/JamesPJohnson_you_cant_do.pdf', 'you_cant_do.mp3', '3251', '103-james-p-johnson-you-cant-do-what-my-last-man-did', NULL),
(104, 7, 2, NULL, '7.50', 'concentratin.jpg', 'Concentratin''', 'D maj', 1, 'FF3ekfJi98nq34JX/WillieSmith_concentratin.pdf', 'concentratin.mp3', '3101', '104-willie-the-lion-smith-concentratin', NULL),
(105, 7, 2, NULL, '7.50', 'sneakaway.jpg', 'Sneakaway', 'G maj', 2, 'FF3ekfJi98nq34JX/WillieSmith_sneakaway.pdf', 'sneakaway.mp3', '3110', '105-willie-the-lion-smith-sneakaway', NULL),
(106, 7, 2, NULL, '7.50', 'echoes.jpg', 'Echoes Of Spring', 'G maj', 3, 'FF3ekfJi98nq34JX/WillieSmith_echoes_of_spring.pdf', 'echoes.mp3', '3107', '106-willie-the-lion-smith-echoes-of-spring', NULL),
(107, 7, 2, NULL, '7.50', 'morning.jpg', 'Morning Air', 'D Maj', 4, 'FF3ekfJi98nq34JX/WillieSmith_morning_air.pdf', 'morning.mp3', '3102', '107-willie-the-lion-smith-morning-air', NULL),
(108, 7, 2, NULL, '7.50', 'finger.jpg', 'Finger Buster', 'F maj', 5, 'FF3ekfJi98nq34JX/WillieSmith_finger_buster.pdf', 'finger.mp3', '3111', '108-willie-the-lion-smith-finger-buster', NULL),
(109, 7, 2, NULL, '7.50', 'fading.jpg', 'Fading Star', 'A maj', 6, 'FF3ekfJi98nq34JX/WillieSmith_fading_star.pdf', 'fading.mp3', '3109', '109-willie-the-lion-smith-fading-star', NULL),
(110, 7, 2, NULL, '7.50', 'rippling.jpg', 'Rippling Waters', 'D maj', 7, 'FF3ekfJi98nq34JX/WillieSmith_rippling_waters.pdf', 'rippling.mp3', '3108', '110-willie-the-lion-smith-rippling-waters', NULL),
(111, 7, 2, NULL, '7.50', 'stormy.jpg', 'Stormy Weather', 'G maj', 8, 'FF3ekfJi98nq34JX/WillieSmith_stormy_weather.pdf', 'stormy.mp3', '3112', '111-willie-the-lion-smith-stormy-weather', NULL),
(112, 7, 2, NULL, '7.50', 'follow.jpg', 'I''ll Follow You', 'G maj', 9, 'FF3ekfJi98nq34JX/WillieSmith_i_ll_follow_you.pdf', 'follow.mp3', '3115', '112-willie-the-lion-smith-ill-follow-you', NULL),
(113, 7, 2, NULL, '7.50', 'passionette.jpg', 'Passionette', 'D maj', 10, 'FF3ekfJi98nq34JX/WillieSmith_passionnette.pdf', 'passionette.mp3', '3104', '113-willie-the-lion-smith-passionette', NULL),
(114, 7, 2, NULL, '7.50', 'whatisthere.JPG', 'What Is There To Say?', 'Eb maj', 11, 'FF3ekfJi98nq34JX/WillieSmith_what_is_there_to_say.pdf', 'whatisthere.mp3', '3114', '114-willie-the-lion-smith-what-is-there-to-say', NULL),
(115, 7, 2, NULL, '7.50', 'theband.jpg', 'Here Come The Band', 'A min', 12, 'FF3ekfJi98nq34JX/WillieSmith_here_come_the_band.pdf', 'theband.mp3', '3113', '115-willie-the-lion-smith-here-come-the-band', NULL),
(116, 7, 2, NULL, '7.50', 'cuttinout.jpg', 'Cuttin'' Out', 'F maj', 13, 'FF3ekfJi98nq34JX/WillieSmith_cuttin_out.pdf', 'cuttinout.mp3', '3105', '116-willie-the-lion-smith-cuttin-out', NULL),
(117, 7, 2, NULL, '7.50', 'portrait.jpg', 'Portrait Of The Duke', 'Bb maj', 14, 'FF3ekfJi98nq34JX/WillieSmith_portrait_of_the_duke.pdf', 'portrait.mp3', '3106', '117-willie-the-lion-smith-portrait-of-the-duke', NULL),
(118, 7, 2, NULL, '7.50', 'zigzag.jpg', 'Zig Zag', 'E min', 15, 'FF3ekfJi98nq34JX/WillieSmith_zig_zag.pdf', 'zigzag.mp3', '3116', '118-willie-the-lion-smith-zig-zag', NULL),
(119, 7, 2, NULL, '7.50', 'motion.jpg', 'Contrary Motion', 'D maj', 16, 'FF3ekfJi98nq34JX/WillieSmith_contrary_motion_1949.pdf', 'motion.mp3', '3103', '119-willie-the-lion-smith-contrary-motion', NULL),
(120, 8, 4, NULL, '7.50', 'anitra.jpg', 'Anitra''s Dance', 'Eb min', 1, 'FF3ekfJi98nq34JX/DonaldLambert_anitras_dance.pdf', 'anitra.mp3', '3301', '120-donald-lambert-anitras-dance', NULL),
(121, 8, 4, NULL, '7.50', 'pilgrim.jpg', 'Pilgrim''s Chorus', 'Db maj', 2, 'FF3ekfJi98nq34JX/DonaldLambert_pilgrim_s_chorus.pdf', 'pilgrim.mp3', '3315', '121-donald-lambert-pilgrims-chorus', NULL),
(122, 8, 4, NULL, '7.50', 'elegie.jpg', 'Elegie', 'Eb min', 3, 'FF3ekfJi98nq34JX/DonaldLambert_elegie.pdf', 'elegie.mp3', '3305', '122-donald-lambert-elegie', NULL),
(123, 8, 4, NULL, '7.50', 'sextet.jpg', 'Sextet (from ''Luci Di Lammermoor'')', 'Eb maj', 4, 'FF3ekfJi98nq34JX/DonaldLambert_sextet.pdf', 'sextet.mp3', '3310', '123-donald-lambert-sextet-from-luci-di-lammermoor', NULL),
(124, 8, 4, NULL, '7.50', 'lullaby.jpg', 'Russian Lullaby', 'Eb min', 5, 'FF3ekfJi98nq34JX/DonaldLambert_russian_lullaby.pdf', 'lullaby.mp3', '3304', '124-donald-lambert-russian-lullaby', NULL),
(125, 8, 4, NULL, '7.50', 'people.jpg', 'People Will Say We Are In Love', 'Db maj', 6, 'FF3ekfJi98nq34JX/DonaldLambert_people_will_say_we_are_in_love.pdf', 'people.mp3', '3313', '125-donald-lambert-people-will-say-we-are-in-love', NULL),
(126, 8, 4, NULL, '7.50', 'temper.jpg', 'Hold Your Temper', 'Ab maj', 7, 'FF3ekfJi98nq34JX/DonaldLambert_hold_your_temper.pdf', 'temper.mp3', '3307', '126-donald-lambert-hold-your-temper', NULL),
(127, 8, 4, NULL, '7.50', 'teafortwo_lamb.jpg', 'Tea For Two', 'Ab maj', 8, 'FF3ekfJi98nq34JX/DonaldLambert_tea_for_two.pdf', 'teafortwo_lamb.mp3', '3312', '127-donald-lambert-tea-for-two', NULL),
(128, 8, 4, NULL, '7.50', 'trolley.jpg', 'Trolley Song', 'Db maj', 9, 'FF3ekfJi98nq34JX/DonaldLambert_trolley_song.pdf', 'trolley.mp3', '3314', '128-donald-lambert-trolley-song', NULL),
(129, 8, 4, NULL, '7.50', 'russian.jpg', 'Russian Rag', 'Eb min', 10, 'FF3ekfJi98nq34JX/DonaldLambert_russian_rag.pdf', 'russian.mp3', '3306', '129-donald-lambert-russian-rag', NULL),
(130, 8, 4, NULL, '7.50', 'sorrow.jpg', 'Save Your Sorrow', 'Db maj', 11, 'FF3ekfJi98nq34JX/DonaldLambert_save_your_sorrow.pdf', 'sorrow.mp3', '3308', '130-donald-lambert-save-your-sorrow', NULL),
(131, 8, 4, NULL, '7.50', 'beans.jpg', 'Pork And Beans', 'F# min', 12, 'FF3ekfJi98nq34JX/DonaldLambert_pork_and_beans.pdf', 'beans.mp3', '3302', '131-donald-lambert-pork-and-beans', NULL),
(132, 8, 4, NULL, '7.50', 'harry.jpg', 'I''m Just Wild About Harry', 'Db maj', 13, 'FF3ekfJi98nq34JX/DonaldLambert_i_m_just_wild_about_Harry.pdf', 'harry.mp3', '3309', '132-donald-lambert-im-just-wild-about-harry', NULL),
(133, 8, 4, NULL, '7.50', 'astime.jpg', 'As Time Goes By', 'Db maj', 14, 'FF3ekfJi98nq34JX/DonaldLambert_das_time_goes_by.pdf', 'astime.mp3', '3303', '133-donald-lambert-as-time-goes-by', NULL),
(134, 8, 4, NULL, '7.50', 'jumps.jpg', 'Doin'' What I Please', 'Db maj', 15, 'FF3ekfJi98nq34JX/DonaldLambert_doin_what_i_please.pdf', 'jumps.mp3', '3311', '134-donald-lambert-doin-what-i-please', NULL),
(135, 9, 10, NULL, '7.50', 'chicago_stomp.jpg', 'Chicago Stomp', 'G maj', 1, 'FF3ekfJi98nq34JX/JimmyBlythe_chicago_stomp.pdf', 'chicago_stomp.mp3', '3901', '135-jimmy-blythe-chicago-stomp', NULL),
(136, 9, 9, 1, '7.50', 'steadyblues.jpg', 'Jump Steady Blues', 'Eb maj', 2, 'FF3ekfJi98nq34JX/PinetopSmith_jump_steady_blues.pdf', 'steadyblues.mp3', '3801', '136-pine-top-smith-jump-steady-blues', NULL),
(137, 9, 7, NULL, '7.50', 'yancey_stomp.jpg', 'Yancey Stomp', 'C maj', 3, 'FF3ekfJi98nq34JX/JimmyYancey_yancey_stomp.pdf', 'yancey_stomp.mp3', '3603', '137-jimmy-yancey-yancey-stomp', NULL),
(138, 9, 7, NULL, '7.50', 'mellow_blues.jpg', 'The Mellow Blues', 'C maj', 4, 'FF3ekfJi98nq34JX/JimmyYancey_the_mellow_blues.pdf', 'mellow_blues.mp3', '3601', '138-jimmy-yancey-the-mellow-blues', NULL),
(139, 9, 7, NULL, '7.50', 'bugle_call.jpg', 'Yancey Bugle Call', 'Eb maj', 5, 'FF3ekfJi98nq34JX/JimmyYancey_yancey_s_bugle_call.pdf', 'bugle_call.mp3', '3602', '139-jimmy-yancey-yancey-bugle-call', NULL),
(140, 9, 5, NULL, '7.50', 'bw_stomp.jpg', 'Boogie Woogie Stomp', 'C maj', 6, 'FF3ekfJi98nq34JX/AlbertAmmons_Boogie_Woogie_Stomp.pdf', 'bw_stomp.mp3', '3403', '140-albert-ammons-boogie-woogie-stomp', NULL),
(141, 9, 5, NULL, '7.50', 'suitcase_blues.jpg', 'Suitcase Blues', 'G maj', 7, 'FF3ekfJi98nq34JX/AlbertAmmons_suitcase_blues.pdf', 'suitcase_blues.mp3', '3404', '141-albert-ammons-suitcase-blues', NULL),
(142, 9, 5, NULL, '7.50', '12street_boogie.jpg', '12th Street Boogie', 'C maj', 8, 'FF3ekfJi98nq34JX/AlbertAmmons_12th_street_boogie.pdf', '12street_boogie.mp3', '3401', '142-albert-ammons-12th-street-boogie', NULL),
(143, 9, 5, NULL, '7.50', 'meccaflat_blues.jpg', 'Mecca Flat Blues', 'C maj', 9, 'FF3ekfJi98nq34JX/AlbertAmmons_mecca_flat_blues.pdf', 'meccaflat_blues.mp3', '3402', '143-albert-ammons-mecca-flat-blues', NULL),
(144, 9, 8, NULL, '7.50', 'yancey_special.jpg', 'Yancey Special', 'C maj', 10, 'FF3ekfJi98nq34JX/MeadeLuxLewis_yancey_special.pdf', 'yancey_special.mp3', '3702', '144-meade-lux-lewis-yancey-special', NULL),
(145, 9, 8, NULL, '7.50', 'honkytonk.jpg', 'Honky Tonk Train Blues', 'G maj', 11, 'FF3ekfJi98nq34JX/MeadeLuxLewis_honky_tonk_train_blues.pdf', 'honkytonk.mp3', '3701', '145-meade-lux-lewis-honky-tonk-train-blues', NULL),
(146, 9, 6, NULL, '7.50', 'answer.jpg', 'Answer To The Boogie', 'F maj', 12, 'FF3ekfJi98nq34JX/PeteJohnson_answer_to_the_boogie.pdf', 'answer.mp3', '3501', '146-pete-johnson-answer-to-the-boogie', NULL),
(147, 9, 6, NULL, '7.50', 'bottomland_boogie.jpg', 'Bottomland Boogie', 'G maj', 13, 'FF3ekfJi98nq34JX/PeteJohnson_bottomland_boogie.pdf', 'bottomland_boogie.mp3', '3503', '147-pete-johnson-bottomland-boogie', NULL),
(148, 9, 6, NULL, '7.50', 'freddie_blues.jpg', 'Mr. Freddie Blues', 'C maj', 14, 'FF3ekfJi98nq34JX/PeteJohnson_mr_freddie_blues.pdf', 'freddie_blues.mp3', '3502', '148-pete-johnson-mr-freddie-blues', NULL),
(149, 9, 6, NULL, '7.50', 'shuffle_boogie.jpg', 'Shuffle Boogie', 'F maj', 15, 'FF3ekfJi98nq34JX/PeteJohnson_shuffle_boogie.pdf', 'shuffle_boogie.mp3', '3504', '149-pete-johnson-shuffle-boogie', NULL),
(150, 9, 12, NULL, '7.50', 'boogie_basie.jpg', 'Boogie-Woogie', 'C maj', 16, 'FF3ekfJi98nq34JX/CountBasie_boogie_woogie.pdf', 'boogie_basie.mp3', '4101', '150-count-basie-boogie-woogie', NULL),
(151, 9, 11, NULL, '7.50', 'mary_boogie.JPG', 'Mary''s Boogie', 'C maj', 17, 'FF3ekfJi98nq34JX/MaryLouWilliams_mary_s_boogie.pdf', 'mary_boogie.mp3', '4001', '151-mary-lou-williams-marys-boogie', NULL),
(152, 10, 14, 1, '7.50', 'eight_mile_boogie.jpg', 'Eight Mile Boogie', 'C Maj', 17, 'FF3ekfJi98nq34JX/PatFlowers-eight_mile_boogie.pdf', 'eight_mile_boogie.mp3', NULL, '152-pat-flowers-eight-mile-boogie', NULL),
(153, NULL, 21, 2, '7.50', 'earl_hines_rosetta.jpg', 'Rosetta', 'F Maj', NULL, 'FF3ekfJi98nq34JX/EarlHines_rosetta.pdf', 'earl_hines_rosetta.mp3', NULL, 'earl-hines-rosetta', NULL),
(154, NULL, 22, 2, '7.50', 'alex_hill_stompin_em_down.jpg', 'Stompin'' ''Em Down', 'C Maj', NULL, 'FF3ekfJi98nq34JX/AlexHill_stompin_em_down.pdf', 'alex_hill_stompin_em_down.mp3', NULL, 'alex-hill-stompin-em-down', NULL),
(155, NULL, 16, 2, '7.50', 'cliff_jackson_crazy_rhythm.jpg', 'Crazy Rhythm', 'F Maj', NULL, 'FF3ekfJi98nq34JX/CliffJackson_crazy_rhythm.pdf', 'cliff_jackson_crazy_rhythm.mp3', NULL, 'cliff-jackson-crazy-rhythm', NULL),
(156, NULL, 16, 2, '7.50', 'cliff_jackson_royal_garden_blues.jpg', 'Royal Garden Blues', 'F Maj', NULL, 'FF3ekfJi98nq34JX/CliffJackson_royal_garen_blues.pdf', 'cliff_jackson_royal_garden_blues.mp3', NULL, '156-cliff-jackson-royal-garden-blues', NULL),
(157, NULL, 16, 2, '7.50', 'cliff_jackson_who.jpg', 'Who?', 'Eb maj', NULL, 'FF3ekfJi98nq34JX/CliffJackson_who.pdf', 'cliff_jackson_who.mp3', NULL, 'cliff-jackson-who', NULL),
(158, NULL, 16, 2, '7.50', 'cliff_jackson_you_took_advantage_of_me.jpg', 'You Took Advantage Of Me', 'Eb Maj', NULL, 'FF3ekfJi98nq34JX/CliffJackson_you_took_advantage_of_me.pdf', 'cliff_jackson_you_took_advantage_of_me.mp3', NULL, 'cliff-jackson-you-took-advantage-of-me', NULL),
(159, NULL, 18, 2, '7.50', 'don_ewell_parlor_social.jpg', 'Parlor Social', 'Bb Maj', NULL, 'FF3ekfJi98nq34JX/DonEwell_parlor_social.pdf', 'don_ewell_parlor_social.mp3', NULL, 'don-ewell-parlor-social', NULL),
(160, NULL, 19, 2, '7.50', 'fletcher_henderson_unknown_blues.jpg', 'Unknown Blues', 'D Maj', NULL, 'FF3ekfJi98nq34JX/FletcherHenderson_unknown_blues.pdf', 'fletcher_henderson_unknown_blues.mp3', NULL, 'fletcher-henderson-unknown-blues', NULL),
(161, NULL, 17, 2, '7.50', 'ralph_sutton_fussin.jpg', 'Fussin''', 'D min', NULL, 'FF3ekfJi98nq34JX/RalphSutton_fussin.pdf', 'ralph_sutton_fussin.mp3', NULL, 'ralph-sutton-fussin', NULL),
(162, NULL, 20, 2, '7.50', 'eubie_blake_troublesome_ivories.jpg', 'Troublesome Ivories (A Ragtime Rag)', 'Db Maj', NULL, 'FF3ekfJi98nq34JX/EubieBlake_troublesome_ivories.pdf', 'eubie_blake_troublesome_ivories.mp3', NULL, '162-eubie-blake-troublesome-ivories-a-ragtime-rag', NULL),
(163, 10, 5, 1, '7.50', 'alberts_special_boogie.jpg', 'Albert''s Special Boogie', 'C Maj', 12, 'FF3ekfJi98nq34JX/AlbertAmmons_alberts_special_boogie.pdf', 'alberts_special_boogie.mp3', NULL, '163-albert-ammons-alberts-special-boogie', NULL),
(164, 10, 5, 1, '7.50', 'blugle_boogie.jpg', 'Bugle Boogie', 'C Maj', 13, 'FF3ekfJi98nq34JX/AlbertAmmons_bugle_boogie.pdf', 'blugle_boogie.mp3', NULL, 'albert-ammons-bugle-boogie', NULL),
(165, 10, 5, 1, '7.50', 'swanee_river_boogie.jpg', 'Swanee River Boogie', 'C Maj', 14, 'FF3ekfJi98nq34JX/AlbertAmmons_swanee_river_boogie.pdf', 'swanee_river_boogie.mp3', NULL, '165-albert-ammons-swanee-river-boogie', NULL),
(166, 10, 13, 1, '7.50', 'hersals_blues.jpg', 'Hersal''s Blues', 'C Maj', 1, 'FF3ekfJi98nq34JX/HersalThomas_hersals_blues.pdf', 'hersals_blues.mp3', NULL, 'hersal-thomas-hersals-blues', NULL),
(167, 10, 7, 1, '7.50', 'five_oclock_blues.jpg', 'Five O''clock Blues', 'Bb Maj', 5, 'FF3ekfJi98nq34JX/JimmyYancey_five_oclock_blues.pdf', 'five_oclock_blues.mp3', NULL, 'jimmy-yancey-five-oclock-blues', NULL),
(168, 10, 7, 1, '7.50', 'rolling_the_stone.jpg', 'Rolling The Stone', 'Eb Maj', 3, 'FF3ekfJi98nq34JX/JimmyYancey_rolling_the_stone.pdf', 'rolling_the_stone.mp3', NULL, 'jimmy-yancey-rolling-the-stone', NULL),
(169, 10, 7, 1, '7.50', 'slow_and_easy_blues.jpg', 'Slow And Easy Blues', 'F Maj', 4, 'FF3ekfJi98nq34JX/JimmyYancey_slow_and_easy_blues.pdf', 'slow_and_easy_blues.mp3', NULL, '169-jimmy-yancey-slow-and-easy-blues', NULL),
(170, 10, 8, 1, '7.50', 'chicago_flyer.jpg', 'Chicago Flyer', 'C Maj', 10, 'FF3ekfJi98nq34JX/MeadeLuxLewis_chicago_flyer.pdf', 'chicago_flyer.mp3', NULL, 'meade-lux-lewis-chicago-flyer', NULL),
(171, 10, 8, 1, '7.50', 'glendale_glide.jpg', 'Glendale Glide', 'C Maj', 11, 'FF3ekfJi98nq34JX/MeadeLuxLewis_glendale_glide.pdf', 'glendale_glide.mp3', NULL, 'meade-lux-lewis-glendale-glide', NULL),
(172, 10, 9, 1, '7.50', 'pinetops_boogie_woogie.jpg', 'Pinetop''s Boogie Woogie', 'C Maj', 2, 'FF3ekfJi98nq34JX/PinetopSmith_pinetops_boogie_woogie.pdf', 'pinetops_boogie_woogie.mp3', NULL, 'pine-top-smith-pinetops-boogie-woogie', NULL),
(173, 10, 15, 1, '7.50', '133rd_street_boogie.jpg', '133rd Street Boogie', 'C Maj', 15, 'FF3ekfJi98nq34JX/SammyPrice_133rd_street_boogie.pdf', '133rd_street_boogie.mp3', NULL, 'sammy-price-133rd-street-boogie', NULL),
(174, 10, 15, 1, '7.50', 'cow_cow_blues.mp3', 'Cow Cow Blues', 'Bb Maj', 16, 'FF3ekfJi98nq34JX/SammyPrice_cow_cow_blues.pdf', 'cow_cow_blues.mp3', NULL, 'sammy-price-cow-cow-blues', NULL),
(175, 10, 6, 1, '7.50', 'climbin_and_screamin.jpg', 'Climbin'' And Screamin''', 'C Maj', 6, 'FF3ekfJi98nq34JX/PeteJohnson_climbin_and_screamin.pdf', 'climbin_and_screamin.mp3', NULL, 'pete-johnson-climbin-and-screamin', NULL),
(176, 10, 6, 1, '7.50', 'death_ray_boogie.jpg', 'Death Ray Boogie', 'C Maj', 7, 'FF3ekfJi98nq34JX/PeteJohnson_death_ray_boogie.pdf', 'death_ray_boogie.mp3', NULL, 'pete-johnson-death-ray-boogie', NULL),
(177, 10, 6, 1, '7.50', 'just_for_you.jpg', 'Just For You', 'F Maj', 8, 'FF3ekfJi98nq34JX/PeteJohnson_just_for_you.pdf', 'just_for_you.mp3', NULL, 'pete-johnson-just-for-you', NULL),
(178, 10, 6, 1, '7.50', 'rock_it_boogie.jpg', 'Rock It Boogie', 'G Maj', 9, 'FF3ekfJi98nq34JX/PeteJohnson_rock_it_boogie.pdf', 'rock_it_boogie.mp3', NULL, 'pete-johnson-rock-it-boogie', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `TranscriptionTranslation`
--

CREATE TABLE IF NOT EXISTS `TranscriptionTranslation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `translatable_id` int(11) DEFAULT NULL,
  `date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `menuLinkLanguage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `partDesc` longtext COLLATE utf8_unicode_ci,
  `locale` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_2BA5B14E2C2AC5D34180C698` (`translatable_id`,`locale`),
  KEY `IDX_2BA5B14E2C2AC5D3` (`translatable_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=357 ;

--
-- Contenu de la table `TranscriptionTranslation`
--

INSERT INTO `TranscriptionTranslation` (`id`, `translatable_id`, `date`, `menuLinkLanguage`, `description`, `partDesc`, `locale`) VALUES
(1, 1, 'Février 1927', 'blueblack_en.html', '<i>Blue Black Bottom</i>, jou&eacute; le 16 f&eacute;vrier 1927 est le seul solo de piano enregistr&eacute; entre 1922 et 1929, et l''un des tous priemiers du pianiste, &agrave; une &eacute;poque o&ugrave; il se concentre d''avantage sur l''orgue. C''est une pi&egrave;ce tr&eacute;s vive o&ugrave; l''inventivit&eacute; et le tonus de Fats sont remarquables.', 'Partition de Fats Waller &quot;Blue Black Bottom&quot; transcription originale 1927', 'fr'),
(2, 1, 'February 1927', 'blueblack.html', 'Blue Black Bottom, was played on February 1927. It is one of the first piano pieces of Fats and the only piano solo that he recorded betwen 1922 and 1929, at a time when he was most interested in organ. It is a lively piece stamped with Fats’ energy and inventiveness.', 'Fats Waller sheet music &quot;Blue Black Bottom&quot; original transcription 1927', 'en'),
(3, 2, 'Mars 1927', 'numb_en.html', NULL, 'Partition de Fats Waller Numb Fumblin transcription originale 1929', 'fr'),
(4, 2, 'March 1927', 'numb.html', NULL, 'Fats Waller sheet music &quot;Blue Black Bottom&quot; original transcription 1927', 'en'),
(5, 3, 'Août 1929', 'loveme_en.html', NULL, 'Partition de Fats Waller Love Me Or Leave Me transcription originale 1934', 'fr'),
(6, 3, 'August 1929', 'loveme.html', '<i>Love Me Or Leave Me</i> was a popular song, which Fats plays in his own way. It introduces with a minor verse and move on with variations on the chorus.', 'Fats Waller sheet music Love Me Or Leave Me original transcription 1929', 'en'),
(7, 4, 'Août 1929', 'valentine_en.html', NULL, 'Partition de Fats Waller Valentine Stomp transcription originale 1929', 'fr'),
(8, 4, 'August 1929', 'valentine.html', NULL, 'Fats Waller sheet music Valentine Stomp original transcription 1929', 'en'),
(9, 5, 'Août 1929', 'falling_en.html', NULL, 'Partition de Fats Waller I\\''ve Got A Feeling I\\''m Falling transcription originale 1929', 'fr'),
(10, 5, 'August 1929', 'falling.html', 'Fats Waller own composition, transcribed from the famous 1929 recording.', 'Fats Waller sheet music I\\''ve Got A Feeling I\\''m Falling original transcription 1929', 'en'),
(11, 6, 'Septembre 1929', 'smashing_en.html', NULL, 'Partition de Fats Waller Smashing Thirds transcription originale 1929', 'fr'),
(12, 6, 'September 1929', 'smashing.html', NULL, 'Fats Waller sheet music Smashing Thirds original transcription 1929', 'en'),
(13, 7, 'Décembre 1929', 'turnon_en.html', NULL, 'Partition de Fats Waller Turn On The Heat transcription originale 1929', 'fr'),
(14, 7, 'December 1929', 'turnon.html', '<i>Turn On The Heat</i> was a popular song that Fats played in his usual lively and entertaining style during that 1929 recording session.', 'Fats Waller sheet music Turn On The Heat original transcription 1929', 'en'),
(15, 8, 'Décembre 1929', 'myfate_en.html', NULL, 'Partition de Fats Waller My Fate Is In Your Hands transcription originale 1929', 'fr'),
(16, 8, 'December 1929', 'myfate.html', 'The Fats'' original composition, <i>My Fate Is In Your Hands</i> is transcribed from the 1929 recording.', 'Fats Waller sheet music My Fate Is In Your Hands original transcription 1929', 'en'),
(17, 9, 'Novembre 1934', 'african_en.html', NULL, 'Partition de Fats Waller African Ripples transcription originale 1934', 'fr'),
(18, 9, 'November 1934', 'african.html', NULL, '', 'en'),
(19, 10, 'Mars 1935', 'hallelujah_en.html', NULL, 'Partition de Fats Waller Hallelujah transcription originale 1935', 'fr'),
(20, 10, 'March 1935', 'hallelujah.html', NULL, 'Fats Waller sheet music Hallelujah original transcription 1935', 'en'),
(21, 11, 'Mars 1935', 'california_en.html', '<i>California, Here I come</i> : une autre chanson populaire, revue par Fats Waller en 1935.', 'Partition de Fats Waller California, Here I Come transcription originale 1935', 'fr'),
(22, 11, 'March 1935', 'california.html', '<i>California, Here I come</i> : another old popular song, revised by Fats in the 1935 recording session.', 'Fats Waller sheet music California Here I Come original transcription 1935', 'en'),
(23, 12, 'Mars 1935', 'top_en.html', NULL, 'Partition de Fats Waller You''re The Top (Cole Porter)  transcription originale 1934', 'fr'),
(24, 12, 'March 1935', 'top.html', NULL, 'Fats Waller sheet music You''re The Top (Cole Porter) original transcription 1935', 'en'),
(25, 13, 'Mars 1935', 'onceupon_en.html', NULL, 'Partition de Fats Waller Because Of Once Upon A Time transcription originale 1935', 'fr'),
(26, 13, 'March 1935', 'onceupon.html', '<i>Because Of Once Upon A Time</i> is an obscure song, played in 1935 with lot of charm and tranquillity. Fats recorded the same year with his band another version of this song, more rhythmical this time.', 'Fats Waller sheet music Because Of Once Upon A Time original transcription 1935', 'en'),
(27, 14, 'Novembre 1939', 'faust_en.html', NULL, 'Partition de Fats Waller Faust Waltz transcription originale 1939', 'fr'),
(28, 14, 'November 1939', 'faust.html', NULL, 'Fats Waller sheet music Faust Waltz original transcription 1939', 'en'),
(29, 15, 'Novembre 1939', 'intermezzo_en.html', NULL, 'Partition de Fats Waller Intermezzo transcription originale 1939', 'fr'),
(30, 15, 'November 1939', 'intermezzo.html', NULL, 'Fats Waller sheet music Intermezzo original transcription 1939', 'en'),
(31, 16, 'Mai 1941', 'carolina_en.html', NULL, 'Partition de Fats Waller Carolina Shout transcription originale 1941', 'fr'),
(32, 16, 'Mai 1941', 'carolina.html', '<i>Carolina Shout</i>, the greatest standard in the piano Stride repertory, was composed by <a href="http://www.blueblackjazz.com/johnson_en.html">James P. Johnson</a> around 1918. This piano piece quickly became a reference for every pianist beginner at that time. Fats recorded that tune only once (in two takes) in 1941. The transcription is based on the first take.', 'Fats Waller sheet music Carolina Shout original transcription 1941', 'en'),
(33, 17, 'Mai 1941', 'honeysuckle_en.html', NULL, 'Partition de Fats Waller Honeysuckle Rose transcription originale 1941', 'fr'),
(34, 17, 'Mai 1941', 'honeysuckle.html', 'Transcribed after the 1941 recording, in which Fats Waller plays one of his most celebrated composition', 'Fats Waller sheet music Honeysuckle Rose original transcription 1941', 'en'),
(35, 18, 'Octobre 1922', 'muscleshoals_en.html', NULL, 'Partition de Fats Waller Muscle shoals Blues transcription originale 1922', 'fr'),
(36, 18, 'October 1922', 'muscleshoals.html', NULL, 'Fats Waller sheet music Muscle Shoals Blues original transcription 1922', 'en'),
(37, 19, 'Octobre 1922', 'birmingham_en.html', NULL, 'Partition de Fats Waller Birmingham Blues transcription originale 1922', 'fr'),
(38, 19, 'October 1922', 'birmingham.html', 'Recorded the same day that <a href="/part/muscleshoals_en.html">Muscleshoals Blues</a>, <i>Birmingham Blues</i> uses the same musicality but with another tonality. In a more improvised temper, this tune tackles in particular the use of the tenth in the left hand, which was very innovating at this time.', 'Fats Waller sheet music Birmingham Blues original transcription 1922', 'en'),
(39, 20, 'Mars 1929', 'handful_en.html', NULL, 'Partition de Fats Waller Handful Of Keys transcription originale 1929', 'fr'),
(40, 20, 'March 1929', 'handful.html', 'Ablsolute bravery and antologial recording, this tune is maybe the most celebrated piano solo tune of Fats Waller. According to <i>Johnny Guarnieri</i>, Fats Waller was the only pianist of his time to never rush the beat : <i>Handful Of Keys</i> is model of rhythmical regularity and inexhaustible technique.', 'Fats Waller sheet music Handful Of Keys original transcription 1929', 'en'),
(41, 21, 'Août 1929', 'babyoh_en.html', NULL, 'Partition de Fats Waller Baby Oh Where Can You Be? transcription originale 1929', 'fr'),
(42, 21, 'August 1929', 'babyoh.html', 'Recorded in 1929, this tune is a revision of a popular song in which Fats uses many elegant variations in three chorus.', 'Fats Waller sheet music Baby Oh Where Can You Be? original transcription 1929', 'en'),
(43, 22, 'Décembre 1929', 'savannah_en.html', NULL, 'Fats Waller sheet music Sweet Savannah Sue original transcription 1929', 'fr'),
(44, 22, 'December 1929', 'savannah.html', NULL, 'Fats Waller sheet music Baby Oh Where Can You Be? original transcription 1929', 'en'),
(45, 23, 'Novembre 1934', 'viper_en.html', NULL, 'Partition de Fats Waller Viper''s Drag transcription originale 1934', 'fr'),
(46, 23, 'November 1934', 'viper.html', '', 'Fats Waller sheet music Viper''s Drag original transcription 1934', 'en'),
(47, 24, 'Mars 1935', 'alligator35_en.html', NULL, 'Partition de Fats Waller Viper''s Drag transcription originale 1934', 'fr'),
(48, 24, 'March 1935', 'alligator35.html', 'Transcribe from the 1935 recording, in which Fats uses more improvisation than in the famous 1934.<br>See : <a href="/part/alligator34_en.html">Alligator Crawl (1934)</a>', 'Fats Waller sheet music Alligator Crawl original transcription 1935', 'en'),
(49, 25, 'Juin 1937', 'mischief_en.html', NULL, 'Partition de Fats Waller Keepin'' Out Of Mischief Now transcription originale 1934', 'fr'),
(50, 25, 'June 1937', 'mischief.html', NULL, 'Fats Waller sheet music Keepin'' Out Of Mischief Now original transcription 1937', 'en'),
(51, 26, 'Juin 1937', 'teafortwo_en.html', NULL, 'Partition de Fats Waller Tea For Two transcription originale 1937', 'fr'),
(52, 26, 'June 1937', 'teafortwo.html', NULL, 'Fats Waller sheet music Tea For Two original transcription 1937', 'en'),
(53, 27, 'Juin 1939', 'piccadilly_en.html', NULL, 'Partition de Fats Waller London Suite - Piccadilly transcription originale 1939', 'fr'),
(54, 27, 'June 1939', 'piccadilly.html', NULL, 'Fats Waller sheet music London Suite - Piccadilly original transcription 1939', 'en'),
(55, 28, 'Juin 1939', 'chelsea_en.html', NULL, 'Partition de Fats Waller London Suite - Chelsea transcription originale 1939', 'fr'),
(56, 28, 'June 1939', 'chelsea.html', NULL, 'Fats Waller sheet music London Suite - Chelsea original transcription 1939', 'en'),
(57, 29, 'Juin 1939', 'soho_en.html', NULL, 'Partition de Fats Waller London Suite - Soho transcription originale 1939', 'fr'),
(58, 29, 'June 1939', 'soho.html', NULL, 'Fats Waller sheet music London Suite - Soho original transcription 1929', 'en'),
(59, 30, 'Juin 1939', 'bondstreet_en.html', NULL, 'Partition de Fats Waller London Suite - Bond Street transcription originale 1939', 'fr'),
(60, 30, 'June 1939', 'bondstreet.html', NULL, 'Fats Waller sheet music London Suite - Soho original transcription 1929', 'en'),
(61, 31, 'Juin 1939', 'limehouse_en.html', NULL, 'Partition de Fats Waller London Suite - Limehouse transcription originale 1939', 'fr'),
(62, 31, 'June 1939', 'limehouse.html', NULL, 'Fats Waller sheet music London Suite - Limehouse transcription 1929', 'en'),
(63, 32, 'Juin 1939', 'whitechapel_en.html', NULL, 'Partition de Fats Waller London Suite - Whitechapel transcription originale 1939', 'fr'),
(64, 32, 'June 1939', 'whitechapel.html', NULL, 'Fats Waller sheet music London Suite - Whitechapel transcription 1929', 'en'),
(65, 33, 'Mai 1941', 'rockin_en.html', NULL, 'Partition de Fats Waller Rockin\\'' Chair transcription originale 1941', 'fr'),
(66, 33, 'May 1941', 'rockin.html', '', 'Fats Waller sheet music Rockin Chair original transcription 1941', 'en'),
(67, 34, 'Mai 1941', 'ringdem_en.html', NULL, 'Partition de Fats Waller Ring Dem Bells (Duke Ellington) transcription originale 1941', 'fr'),
(68, 34, 'May 1941', 'ringdem.html', NULL, 'Fats Waller sheet music Ring Dem Bells original transcription 1941', 'en'),
(69, 35, 'Août 1929', 'misbehavin_en.html', NULL, 'Partition de Fats Waller Ain\\''t Misbehavin transcription originale 1929', 'fr'),
(70, 35, 'August 1929', 'misbehavin.html', '<i>Ain''t Misbehavin''</i> remains the most famous composition of Fats Waller, which became one of the greatest jazz standard ever. Fats recorded it first in 1929 at the piano solo. It''s the version which is transcribed here.', 'Fats Waller sheet music Ain''t Misbehavin original transcription 1929', 'en'),
(71, 36, 'Août 1929', 'gladyse_en.html', NULL, 'Partition Fats Waller Gladyse transcription originale 1929', 'fr'),
(72, 36, 'August 1929', 'gladyse.html', NULL, 'Fats Waller sheet music GLADYSE original transcription 1929', 'en'),
(73, 37, 'Août 1929', 'waiting_en.html', NULL, 'Partition Fats Waller Waiting At The End Of The Road transcription originale 1929', 'fr'),
(74, 37, 'August 1929', 'waiting.html', 'A popular song composed by <b>Irving Berlin</b>, which Fats recorded in 1929 in a wonderful piano solo version.', 'Fats Waller sheet music Waiting At The End Of The Road original transcription 1929', 'en'),
(75, 38, 'Septembre 1929', 'goinabout_en.html', NULL, 'Partition Fats Waller Goin'' About transcription originale 1929', 'fr'),
(76, 38, 'September 1929', 'goinabout.html', NULL, 'Fats Waller sheet music Goin'' About original transcription 1929', 'en'),
(77, 39, 'Septembre 1929', 'myfeelings_en.html', NULL, 'Partition Fats Waller My Feelings Are Hurts transcription originale 1929', 'fr'),
(78, 39, 'September 1929', 'myfeelings.html', 'Recorded in 1929, <i>My Feelings Are Hurts</i> is a blues in F. Fats shows originality, in particular by doubling the tempo during seven measures towards the end, pointing out the close link between blues and stride piano.', 'Fats Waller sheet music My Feelings Are Hurts original transcription 1929', 'en'),
(79, 40, 'Novembre 1934', 'clothesline_en.html', NULL, 'Partition Fats Waller Clothesline Ballet transcription originale 1934', 'fr'),
(80, 40, 'November 1934', 'clothesline.html', NULL, 'Fats Waller sheet music Clothesline Ballet original transcription 1934', 'en'),
(81, 41, 'Novembre 1934', 'alligator34_en.html', NULL, 'Partition Fats Waller Alligator Crawl transcription originale 1934', 'fr'),
(82, 41, 'November 1934', 'alligator34.html', 'One of Fats Waller most celebrated piano piece, transcribed from the 1934 famous recording.<br>See Also : <a href="http://blueblackjazz.com/part/alligator35_en.html">Alligator Crawl (1935)</a>', 'Fats Waller sheet music Alligator Crawl original transcription 1934', 'en'),
(83, 42, 'Mars 1935', 'flatblues_en.html', NULL, 'Partition Fats Waller E-Flat Blues transcription originale 1935', 'fr'),
(84, 42, 'March 1935', 'flatblues.html', '<i>''E-Flat Blues''</i> is a typical wallerian blues, transcribed from the 1935 recording.', 'Fats Waller sheet music E-Flat Blues original transcription 1935', 'en'),
(85, 43, 'Mars 1935', 'zonky_en.html', 'Composition originale de Fats Waller, il en enregistre une version au piano solo en 1935 : courte mais efficace.', 'Partition Fats Waller Zonky transcription originale 1935', 'fr'),
(86, 43, 'March 1935', 'zonky.html', 'A Fats Waller original, which he recorded in 1935 at the piano solo.', 'Fats Waller sheet music Zonky original transcription 1935', 'en'),
(87, 44, 'Mars 1935', 'fantasy_en.html', NULL, 'Partition Fats Waller Russian Fantasy transcription originale 1935', 'fr'),
(88, 44, 'March 1935', 'fantasy.html', 'Transcribed according to the recording of 1935, <i>Russian Fantasy</i> uses in C minor themes, beloved of Fats Waller (already heard in <a href="http://blueblackjazz.com/part/valentine_en.html">Valentine Stomp</a> and <a href="http://blueblackjazz.com/part/piccadilly_en.html">Piccadilly</a>).', 'Fats Waller sheet music Russian Fantasy original transcription 1935', 'en'),
(89, 45, 'Juin 1937', 'basin_en.html', NULL, 'Partition Fats Waller Basin Street Blues transcription originale 1937', 'fr'),
(90, 45, 'June 1937', 'basin.html', 'Fats Waller, could play the blues remarkably by integrating his original play and sophisticated harmonies. <i>Basin Street Blues</i>, recorded in 1937, offers still a particular conception of blues, free and full of nuance.', 'Fats Waller sheet music Basin Street Blues original transcription 1937', 'en'),
(91, 46, 'Juin 1937', 'stardust_en.html', NULL, 'Partition Fats Waller Stardust transcription originale 1937', 'fr'),
(92, 46, 'June 1937', 'stardust.html', '<i>Stardust</i> is a composition of <b>Hoagy Carmichael</b>. The transcription is based on the 1937 recording.', 'Fats Waller sheet music Stardust original transcription 1937', 'en'),
(93, 47, 'Juin 1937', 'nobody_en.html', NULL, 'Partition Fats Waller I Ain''t Got Nobody transcription originale 1937', 'fr'),
(94, 47, 'June 1937', 'nobody.html', 'Transcribed from the 1937 recording.', 'Fats Waller sheet music I Ain''t Got Nobody original transcription 1937', 'en'),
(95, 48, 'Octobre 1938', 'hallelujah38_en.html', NULL, 'Partition Fats Waller Hallelujah (Yacht Club version) transcription originale 1938', 'fr'),
(96, 48, 'October 1938', 'hallelujah38.html', NULL, 'Fats Waller sheet music Hallelujah (Yacht Club version) original transcription 1938', 'en'),
(97, 49, 'Novembre 1939', 'louisblues_en.html', NULL, 'Partition Fats Waller Saint Louis Blues transcription originale 1939', 'fr'),
(98, 49, 'November 1939', 'louisblues.html', 'Fats Waller records this standard in 1939. The theme is exposed in an unexpected way, though with a depth and a harmonic originality very common in Fats playing. The originality of <i>St Louis Blues</i> is that this piece alternates between minor and major scale : Fats Waller knows how to use this duality by offering a rich and contrasted piece.', 'Fats Waller sheet music Saint Louis Blues original transcription 1939', 'en'),
(99, 50, 'Novembre 1939', 'remember_en.html', '', 'Partition Fats Waller Then You''ll Remember Me transcription originale 1939', 'fr'),
(100, 50, 'November 1939', 'remember.html', '', 'Fats Waller sheet music Then You''ll Remember Me original transcription 1939', 'en'),
(101, 51, 'Mai 1941', 'georgia_en.html', NULL, 'Partition Fats Waller Georgia On My Mind transcription originale 1941', 'fr'),
(102, 51, 'May 1941', 'georgia.html', 'One of the most memorable piano solo recording of Fats Waller, <i>Georgia One My Mind</i> is originally a composition of <b>Hoagy Carmichael</b> (Composer of <a href="http://blueblackjazz.com/part/strut_en.html">Stardust</a> and <a href="http://blueblackjazz.com/part/rockin_en.html">Rockin'' Chair</a>). Fats plays three choruses, according to three different moods: very moderate and free at the beginning and then tempo moderated. He finishes in a fast tempo : once again, the precision and the power of Fats Waller''s playing is just amazing.', 'Fats Waller sheet music Georgia On My Mind original transcription 1941', 'en'),
(103, 52, 'Septembre 1943', 'martinique_en.html', NULL, 'Partition Fats Waller Martinique transcription originale 1941', 'fr'),
(104, 52, 'September 1943', 'martinique.html', NULL, 'Fats Waller sheet music Martinique original transcription 1941', 'en'),
(105, 53, 'Août 1921', 'strut_en.html', '<i>Harlem Strut</i> (1921) est le tout premier enregistrement de James P. Johnson. Ce morceau, jou&eacute; au tempo normal demande beaucoup d''endurance et de r&eacute;gularit&eacute;, surtout &agrave; la main droite qui ne s''arr&ecirc;te pas de tisser des variations. C''est un morceau charni&egrave;re entre le ragtime et le stride.', 'Partition de James P. Johnson Harlem Strut transcription originale 1921', 'fr'),
(106, 53, 'August 1921', 'strut.html', '<i>Harlem Strut</i> is the very first recording of James P. Johnson, and can be nearly considered as a ragtime piece. This tune, played at normal tempo, requires a lot of endurance and regularity, mostly on the right hand wich never stops running over the keyboard.', 'James P. Johnson sheet music Harlem Strut original transcription 1921', 'en'),
(107, 54, 'Octobre 1921', 'carolina_johnson_en.html', NULL, 'Partition de James P. Johnson Carolina Shout transcription originale 1921', 'fr'),
(108, 54, 'October 1921', 'carolina_johnson.html', '<i>Carolina Shout</i> is the very first stride piano tune, composed around 1918 and recoded in 1921. This recording is also considered to be the first jazz piano solo : for the first time the pianist really stand out ragtime music : discords, clusters and syncopations make this recording a historical piece.', 'James P. Johnson sheet music Carolina Shout original transcription 1921', 'en'),
(109, 55, 'Janvier 1929', 'riffs_en.html', '<i>Riff</i> est une suite variations sur un morceau qui se d&eacute;coupe en 2 parties. James P. Johnson r&eacute;enregistrera ce morceau en 1944, mais la version de 1929 est bien plus &eacute;nergique.', 'Partition de James P. Johnson Riffs transcription originale 1929', 'fr'),
(110, 55, 'January 1929', 'riffs.html', '<i>Riff</i> is a series of variations built in two parts. James P. Johnson recorded this tune a second time in 1944, but the 1929 recording is really more energic.', 'James P. Johnson sheet music Riffs original transcription 1929', 'en'),
(111, 56, 'Janvier 1929', 'feeling_en.html', NULL, 'Partition de James P. Johnson Feeling Blues transcription originale 1929', 'fr'),
(112, 56, 'January 1929', 'feeling.html', 'Transcribed from the 1929 recording', 'James P. Johnson sheet music Feeling Blues original transcription 1929', 'en'),
(113, 57, 'Janvier 1930', 'jingles_en.html', 'C''est l''un des meilleurs solos de James P. Johnson. Il est enregistr&eacute; en 1930, &agrave; une &eacute;poque ou le pianiste en plein possession de ses moyens pianistiques. Ce solo n&eacute;cessite une grande agilit&eacute; mais aussi beaucoup de precison (notemment le pont de la premi&egrave;re partie avec la polyrythmie des deux mains)', 'Partition de James P. Johnson Jingles transcription originale 1930', 'fr'),
(114, 57, 'January 1930', 'jingles.html', 'One of the best piano solo of James P. Johnson recorded at a time . This solo recquires a lot of nimbleness and also a great precision (the brigde of the first part, where the two hands have a polyrhtythmic function)', 'James P. Johnson sheet music Jingles original transcription 1930', 'en'),
(115, 58, 'Janvier 1930', 'crying_en.html', NULL, 'Partition de James P. Johnson Crying For The Carolines transcription originale 1930', 'fr'),
(116, 58, 'January 1930', 'crying.html', '<i>Crying For The Carolines</i> is transcribed from the great recording from 1930. It''s a kind of lament which alternate between minor and major tone.', 'James P. Johnson sheet music Crying For The Carolines original transcription 1930', 'en'),
(117, 59, 'Janvier 1930', 'modernistic_en.html', 'Modernistic (ou You''ve Got To Be Modernistic) est un morceau qui porte bien son nom. C''est &agrave; bien des &eacute;gards un morceau en avance sur son temps avec de nombreuses dissonances et syncopes. James P. enregistre ce morceau une premi&egrave;re fois en 1929 avec son orchestre, mais c''est la version piano de 1930 qui restera surtout dans les m&eacute;moires.', 'Partition de James P. Johnson Modernistic transcription originale 1930', 'fr'),
(118, 59, 'January 1930', 'modernistic.html', '<i>Modernistic</i> (ou <i>You''ve Got To Be Modernistic</i>) really deserves his name. In many respects, this tune is ahead of his time with a lot of dissonances and syncopations. James P. Johnson recorded it for the first time in 1929 with his orchestra, but the 1930 piano version stayed in memories.', 'James P. Johnson sheet music Modernistic original transcription 1930', 'en'),
(119, 60, 'Juin 1939', 'ifdreams_en.html', 'Enregistrement m&eacute;morable de ce standard compos&eacute; par Benny Goodman, James P. Johnson en fait une remarquable pi&egrave;ce de stride.', 'Partition de James P. Johnson If Dreams Come True transcription originale 1939', 'fr'),
(120, 60, 'June 1939', 'ifdreams.html', 'James P. Johnson made this standard (composition of Benny Goodman) a great stride tune in 1939.', 'James P. Johnson sheet music If Dreams Come True original transcription 1939', 'en'),
(121, 61, 'Juin 1939', 'mule_en.html', 'C''est peut-&ecirc;tre la plus c&eacute;l&egrave;bre composition de James P. Johnson. Il l''aurait compos&eacute; dans sa jeunesse, &agrave; l''&eacute;poque ou il jouait au ''Jungle Casino'' autour de 1913. Son enregistrement incontournable de 1939 a fait de cette pi&egrave;ce un morceau d''&eacute;cole, et James P. nous montre, sur un tempo in&eacute;branlable, que le stride est une musique qui respire.', 'Partition de James P. Johnson Mule Walk Stomp transcription originale 1939', 'fr'),
(122, 61, 'June 1939', 'mule.html', 'It''s maybe the most celebrated James P. Johnson''s compositions for the piano. He has composed it at a young age, when he was pianist at the ''Jungle Casino'' around 1913. His memorable 1939 recording made this tune a reference in stride piano. James P. Johnson show us, on an inflexible tempo, that stride is truly a breathing music.', 'James P. Johnson sheet music Mule Walk Stomp original transcription 1939', 'en'),
(123, 62, 'Juin 1939', 'flatdream_en.html', '<i>A Flat Dream</i> est un morceau tr&eacute;s riche qui &eacute;volue dans diff&eacute;rentes humeurs. La premi&egrave;re partie du morceau est une sorte de blues rapide. La deuxi&egrave;me section est une descente d''accords accompagn&eacute;s &agrave; la main gauche pas des arp&egrave;ges subtils. La derni&egrave;re partie reprend cette descente de facon rythm&eacute;e mais toujours d''une incroyable l&eacute;geret&eacute;. Du grand James P. Johnson.', 'Partition de James P. Johnson A Flat Dream transcription originale 1939', 'fr'),
(124, 62, 'June 1939', 'flatdream.html', NULL, 'James P. Johnson sheet music A Flat Dream original transcription 1939', 'en'),
(125, 63, 'Juillet 1943', 'daintiness_en.html', 'Tr&egrave;s joli rag enregist&eacute; en 1943, <i>Daintiness Rag</i> reste une des plus originales compositions de James P. Johnson au piano.', 'Partition de James P. Johnson Daintiness Rag transcription originale 1943', 'fr'),
(126, 63, 'July 1943', 'daintiness.html', 'Very nice piece recorded in 1943, <i>Daintiness Rag</i> is one of the most original piano composition of James P. Johnson.', 'James P. Johnson sheet music Daintiness Rag original transcription 1943', 'en'),
(127, 64, 'Avril 1944', 'letter_en.html', 'James P. utilise les plus belle variations pour nous livrer ce standard dans une version tr&egrave;s swing. L''enregistrement original est accompagn&eacute; d''une batterie, et la main gauche y est par moments subliminale.', 'Partition de James P. Johnson I''m Gonna Sit Right Down transcription originale 1944', 'fr'),
(128, 64, 'April 1944', 'letter.html', 'James P. Johnson uses the most beautiful variations on this high swing recording of this standard. The original recording is conducted by a drum, and left hand is sometimes subliminal.', 'James P. Johnson sheet music I''m Gonna Sit Right Down original transcription 1944', 'en'),
(129, 65, 'Avril 1944', 'keepoff_en.html', 'Enregistr&eacute; pour la premi&egrave;re fois en 1921, <i>Keep Off The Grass</i> fait partie des hymnes du stride. La transcription est bas&eacute;e sur la version de 1944 qui reprend la structure de l''enregistrement original mais avec plus de souplesse.', 'Partition de James P. Johnson Keep Off The Grass transcription originale 1944', 'fr'),
(130, 65, 'April 1944', 'keepoff.html', 'This tune was first recorded in 1921, and is one of the stride''s hymns. This transcription is based oin the 1944 recording, wich uses the same structure as the first version with much more suppleness.', 'James P. Johnson sheet music Keep Off The Grass original transcription 1944', 'en'),
(131, 66, 'Juin 1944', 'crazy_en.html', NULL, 'Partition de James P. Johnson I''m Crazy ''Bout My Baby transcription originale 1944', 'fr'),
(132, 66, 'June 1944', 'crazy.html', 'Recorded in 1944 for a radio show animated by Eddie Condon, James P. Johnson pays tribute to his pupil and friend Fats Waller, who died the previous year.', 'James P. Johnson sheet music I''m Crazy ''Bout My Baby original transcription 1944', 'en'),
(133, 67, 'Avril 1945', 'twilight_en.html', NULL, 'Partition de James P. Johnson Twilight Rag transcription originale 1945', 'fr'),
(134, 67, 'April 1945', 'twilight.html', 'James P. Johnson recorded in 1944 his <i>Twilight Rag</i>, a piece he had already recorded in 1917 as a piano roll. This version is very far from the first one, due to the evolution of his keyboard technique. It''s a two parts piece, like most of his high-tempo compositions (jingles, modernistic...).', 'James P. Johnson sheet music Twilight Rag original transcription 1945', 'en'),
(135, 68, 'Avril 1945', 'jersey_en.html', NULL, 'Partition de James P. Johnson Jersey Sweet transcription originale 1945', 'fr'),
(136, 68, 'April 1945', 'jersey.html', 'This tune, wich seems to be a James P. Johnson composition, is probably a tribute to his native New-Jersey.', 'James P. Johnson sheet music Jersey Sweet original transcription 1945', 'en'),
(137, 69, 'Mai 1945', 'liza_en.html', NULL, 'Partition de James P. Johnson Liza transcription originale 1945', 'fr'),
(138, 69, 'May 1945', 'liza.html', 'Transcribe from James P. Johnson memorable and audacious recording of this standard composed by Georges Gershwin.', 'James P. Johnson sheet music Liza original transcription 1945', 'en'),
(139, 70, '1917', 'roll-mamasblues_en.html', NULL, 'Partition de James P. Johnson Mama''s Blues transcription piano roll 1917', 'fr'),
(140, 70, '1917', 'roll-mamasblues.html', 'Many piano rolls of <i>Mama''s Blues</i> were recorded by James P. Johnson in 1917, many of them are overloaded of notes and voices. This version (probably from SingA label) is a simple two hands performance which doesn''t suffer embellishments', 'James P. Johnson sheet music Mama''s Blues piano roll transcription 1917', 'en'),
(141, 71, '1917', 'roll-capricerag_en.html', NULL, 'Partition de James P. Johnson Caprice Rag transcription piano roll 1917', 'fr'),
(142, 71, '1917', 'roll-capricerag.html', 'This is one of the first rags composed by James P. Johnson, one of the most famous. James P. Johnson recorded this tune later in the 40''s on a terrifying tempo.', 'James P. Johnson sheet music Caprice Rag piano roll transcription 1917', 'en'),
(143, 72, 'Mai 1917', 'roll-steeplechase_en.html', NULL, 'Partition de James P. Johnson Steeplechase Rag transcription piano roll 1917', 'fr'),
(144, 72, 'May 1917', 'roll-steeplechase.html', '<i>Steeplechase Rag</i> is a fast ragtime James P. Johnson recorded this tune in 1944 under the title <i>Over The Bars</i>.', 'James P. Johnson sheet music Steeplechase Rag piano roll transcription 1917', 'en'),
(145, 73, 'Août 1917', 'roll-stopit_en.html', NULL, 'Partition de James P. Johnson Stop It transcription piano roll 1917', 'fr'),
(146, 73, 'August 1917', 'roll-stopit.html', 'This obscure Johnson composition is a lively ragtime. James P. Johnson recorded this tune again in 1939 with Rosetta Crawford under the title <i>Stop It Joe.</i>', 'James P. Johnson sheet music Stop It piano roll transcription 1917', 'en'),
(147, 74, 'Février 1921', 'roll-carolinashout_en.html', NULL, 'Partition de James P. Johnson Carolina Shout transcription piano roll 1921', 'fr'),
(148, 74, 'February 1921', 'roll-carolinashout.html', 'The 1921 performance of <i>Carolina Shout</i> is definitely one of the greatest piano rolls. Despite quite close from the <a href="http://blueblackjazz.com/part/carolina_johnson_en.html">phonograph recording</a> the same year, it includes some notable differencies in the left hand playing and in the expression of the right hand .', 'James P. Johnson sheet music Carolina Shout piano roll transcription 1921', 'en'),
(149, 75, 'Mai 1921', 'roll-eccentricity_en.html', NULL, 'Partition de James P. Johnson Eccentricity transcription piano roll 1921', 'fr'),
(150, 75, 'May 1921', 'roll-eccentricity.html', 'The <i>Eccentricity</i> piano roll has the mention "Syncopated Waltz", wich were a popular kind of music at this period. The original performance has been redrafted in many parts : the transcription contains many reductions to offer a more approachable performing.', 'James P. Johnson sheet music Eccentricity piano roll transcription 1921', 'en'),
(151, 76, 'Mai 1921', 'roll-ittakeslove_en.html', NULL, 'Partition de James P. Johnson It Takes Love To Cure The Heart''s Disease transcription piano roll 1921', 'fr'),
(152, 76, 'May 1921', 'roll-ittakeslove.html', 'This is another obscure Johnson composition, wich this roll is the only testimony. However this is a quality piece, especially the verse. The original performance of James P. Johnson has been generously redrafted on the rolls, so many reductions were necessary, without changing the original musicality.', 'James P. Johnson sheet music It Takes Love To Cure The Heart''s Disease piano roll transcription 1921', 'en'),
(153, 77, 'Juin 1921', 'roll-drjazz_en.html', NULL, 'Partition de James P. Johnson Dr Jazzes Raz-Ma-Taz transcription piano roll 1921', 'fr'),
(154, 77, 'June 1921', 'roll-drjazz.html', 'One of the hotest piano roll of this collection, Johnson''s playing offers here a catchy swing.', 'James P. Johnson sheet music Dr Jazzes Raz-Ma-Taz piano roll transcription 1921', 'en'),
(155, 78, 'Juin 1921', 'roll-roumania_en.html', NULL, 'Partition de James P. Johnson Roumania transcription piano roll 1921', 'fr'),
(156, 78, 'June 1921', 'roll-roumania.html', '<i>Roumania</i> is a Clarence Williams composition beautifully played by James P. Johnson on this roll. Here again, some redrafting on the right hand were nacessary to ease the performing, without changing the original atmosphere.', 'James P. Johnson sheet music Roumania piano roll transcription 1921', 'en'),
(157, 79, 'Octobre 1921', 'roll-arkansas_en.html', 'Ce jolie blues avec son introduction originale reste l''un des meilleurs rouleaux de James P. Johnon.', 'Partition de James P. Johnson Arkansas Blues transcription piano roll 1921', 'fr'),
(158, 79, 'October 1921', 'roll-arkansas.html', 'This nice blues, with his original introduction is one of the finest James P. Johnson piano roll.', 'James P. Johnson sheet music Arkansas Blues piano roll transcription 1921', 'en'),
(159, 80, 'Mars 1922', 'roll-joeturnerblues_en.html', NULL, 'Partition de James P. Johnson Joe Turner Blues transcription piano roll 1922', 'fr'),
(160, 80, 'March 1922', 'roll-joeturnerblues.html', 'This is another great performance of James P. Johnson. The two chorus are played differently, with a chorus played in boogie-woogie.', 'James P. Johnson sheet music Joe Turner Blues piano roll transcription 1922', 'en'),
(161, 81, 'Juin 1922', 'roll-harlemstrut_en.html', NULL, 'Partition de James P. Johnson Harlem Strut transcription piano roll 1922', 'fr'),
(162, 81, 'June 1922', 'roll-harlemstrut.html', 'The piano roll of <i>Harlem Strut</i>, although pretty close from the anterior 1921 recording, many notable differences can be ear between the two versions, like the use of a single bass note in the left hand instead of octaves. The roll has not been arranged, this is the original score.', 'James P. Johnson sheet music Harlem Strut piano roll transcription 1922', 'en'),
(163, 82, 'Août 1923', 'roll-railroadman_en.html', NULL, 'Partition de James P. Johnson Railroad Man transcription piano roll 1923', 'fr'),
(164, 82, 'August 1923', 'roll-railroadman.html', '<i>Railroad Man</i> is a brillant piece dated from 1923 and played in three parts, each time differently. In the middle part, the balancing playing of the two hands offers an irresistible swing. Here again, the difficulty of the original score needed some reductions.', 'James P. Johnson sheet music Railroad Man piano roll transcription 1923', 'en'),
(165, 83, 'Novembre 1923', 'roll-blackman_en.html', NULL, 'Partition de James P. Johnson Black Man transcription piano roll 1923', 'fr'),
(166, 83, 'November 1923', 'roll-blackman.html', 'The piano roll of <i>Black Man (Be On Yo'' Way)</i> from 1923 is a delightful performance. Each of the three chorus offers a particular swing under james P. Johnson''s hands. The original transcription is very difficult to play. Thus, many reductions of the score were necessary to offer a playable transcription', 'James P. Johnson sheet music Black Man piano roll transcription 1923', 'en'),
(167, 84, 'Mars 1924', 'roll-charleston_en.html', NULL, 'Partition de James P. Johnson Charleston transcription piano roll 1924', 'fr'),
(168, 84, 'March 1924', 'roll-charleston.html', '<i>Charleston</i> is the most celebrated James P. Johnson composition. This is the transcription from the 1924 roll : <i>Runnin'' Wild Medley</i>.', 'James P. Johnson sheet music Charleston piano roll transcription 1924', 'en'),
(169, 85, 'Juillet 1926', 'roll-harlemchoclate_en.html', NULL, 'Partition de James P. Johnson Harlem Choc''late Babies On Parade transcription piano roll 1926', 'fr'),
(170, 85, 'July 1926', 'harlemchoclate.html', 'This lively piano, a Johnson composition, is the only know performance of this song. Although some minor reduction have been done in the right hand, the tune is quite difficult to play at the original tempo.', 'James P. Johnson sheet music Harlem Choc''late Babies On Parade piano roll transcription 1926', 'en'),
(171, 86, 'Novembre 1926', 'roll-sugar_en.html', NULL, 'Partition de James P. Johnson Sugar transcription piano roll 1926', 'fr'),
(172, 86, 'November 1926', 'roll-sugar.html', 'James P. Johnson take up this standard with elegancy. This is one of the last James P. Johnson piano rolls in solo.', 'James P. Johnson sheet music Sugar piano roll transcription 1926', 'en'),
(173, 87, '1945', 'arkansas_blues_en.html', '', 'Partition de James P. Johnson Arkansas Blues transcription originale 1945', 'fr'),
(174, 87, '1945', 'arkansas_blues.html', 'James P. Johnson recorded in 1944 his <i>Twilight Rag</i>, a piece he had already recorded in 1917 as a piano roll. This version is very far from the first one, due to the evolution of his keyboard technique. It''s a two parts piece, like most of his high-tempo compositions (jingles, modernistic...).', 'James P. Johnson sheet music Arkansas Blues original transcription 1945', 'en'),
(175, 88, '1947', 'aint_cha_got_music_en.html', '', 'Partition de James P. Johnson Ain''t Cha Got Music transcription originale 1947', 'fr'),
(176, 88, '1947', 'aint_cha_got_music.html', 'Transcribe from James P. Johnson memorable and audacious recording of this standard composed by Georges Gershwin.', 'James P. Johnson sheet music Ain''t Cha Got Music original transcription 1947', 'en'),
(177, 89, '1923', 'bleeding-hearted_blues_en.html', 'Un blues typique de James P. Johnson, avec ses breaks et', 'Partition de James P. Johnson Bleeding-Hearted Blues transcription originale 1923', 'fr'),
(178, 89, '1923', 'bleeding-hearted_blues.html', '<i>Harlem Strut</i> is the very first recording of James P. Johnson, and can be nearly considered as a ragtime piece. This tune, played at normal tempo, requires a lot of endurance and regularity, mostly on the right hand wich never stops running over the keyboard.', 'James P. Johnson sheet music Bleeding-Hearted Blues original transcription 1923', 'en'),
(179, 90, '1939', 'blueberrye_rhyme_en.html', NULL, 'Partition de James P. Johnson Blueberry Rhyme transcription originale 1939', 'fr'),
(180, 90, '1939', 'blueberrye_rhyme.html', NULL, 'James P. Johnson sheet music Blueberry Rhyme original transcription 1939', 'en'),
(181, 91, '1945', 'carolina_balmoral_en.html', NULL, 'Partition de James P. Johnson Carolina Balmoral transcription originale 1945', 'fr'),
(182, 91, '1945', 'carolina_balmoral.html', 'This tune, wich seems to be a James P. Johnson composition, is probably a tribute to his native New-Jersey.', 'James P. Johnson sheet music Carolina Balmoral original transcription 1945', 'en'),
(183, 92, '1945', 'jazz-a-mine_en.html', NULL, 'Partition de James P. Johnson Concerto Jazz-A-Mine transcription originale 1945', 'fr'),
(184, 92, '1945', 'jazz-a-mine.html', 'This tune was first recorded in 1921, and is one of the stride''s hymns. This transcription is based oin the 1944 recording, wich uses the same structure as the first version with much more suppleness.', 'James P. Johnson sheet music Concerto Jazz-A-Mine original transcription 1945', 'en'),
(185, 93, '1939', 'fascination_en.html', NULL, 'Partition de James P. Johnson Fascination transcription originale 1939', 'fr'),
(186, 93, '1939', 'fascination.html', '<i>Modernistic</i> (ou <i>You''ve Got To Be Modernistic</i>) really deserves his name. In many respects, this tune is ahead of his time with a lot of dissonances and syncopations. James P. Johnson recorded it for the first time in 1929 with his orchestra, but the 1930 piano version stayed in memories.', 'James P. Johnson sheet music Fascination original transcription 1939', 'en'),
(187, 94, '1944', 'gut_stomp_en.html', '', 'Partition de James P. Johnson Gut Stomp transcription originale 1944', 'fr'),
(188, 94, '1944', 'gut_stomp.html', 'James P. Johnson uses the most beautiful variations on this high swing recording of this standard. The original recording is conducted by a drum, and left hand is sometimes subliminal.', 'James P. Johnson sheet music Gut Stomp original transcription 1944', 'en'),
(189, 95, '1944', 'honeysuckle_rose_en.html', '', 'Partition de James P. Johnson Honeysuckle Rose transcription originale 1944', 'fr'),
(190, 95, '1944', 'honeysuckle_rose.html', NULL, 'James P. Johnson sheet music Honeysuckle Rose original transcription 1944', 'en'),
(191, 96, '1945', 'keep_movin_en.html', NULL, 'Partition de James P. Johnson Keep Movin'' transcription originale 1945', 'fr'),
(192, 96, '1945', 'keep_movin.html', 'Recorded in 1944 for a radio show animated by Eddie Condon, James P. Johnson pays tribute to his pupil and friend Fats Waller, who died the previous year.', 'James P. Johnson sheet music Keep Movin'' original transcription 1945', 'en'),
(193, 97, '1944', 'old_fashioned_love_en.html', NULL, 'Partition de James P. Johnson Old Fashioned Love transcription originale 1944', 'fr'),
(194, 97, '1944', 'old_fashioned_love.html', 'Very nice piece recorded in 1943, <i>Daintiness Rag</i> is one of the most original piano composition of James P. Johnson.', 'James P. Johnson sheet music Old Fashioned Love original transcription 1944', 'en'),
(195, 98, '1923', 'scouting_around_en.html', NULL, 'Partition de James P. Johnson Scouting Around transcription originale 1923', 'fr'),
(196, 98, '1923', 'scouting_around.html', 'Transcribed from the 1929 recording', 'James P. Johnson sheet music Scouting Around original transcription 1923', 'en'),
(197, 99, '1927', 'snowy_morning_blues_en.html', NULL, 'Partition de James P. Johnson Snowy Morning Blue transcription originale 1927', 'fr'),
(198, 99, '1927', 'snowy_morning_blues.html', 'One of the best piano solo of James P. Johnson recorded at a time . This solo recquires a lot of nimbleness and also a great precision (the brigde of the first part, where the two hands have a polyrhtythmic function)', 'James P. Johnson sheet music Snowy Morning Blue original transcription 1927', 'en'),
(199, 100, '1944', 'squeeze_me_en.html', '', 'Partition de James P. Johnson Squeeze Me transcription originale 1944', 'fr'),
(200, 100, '1944', 'squeeze_me.html', 'It''s maybe the most celebrated James P. Johnson''s compositions for the piano. He has composed it at a young age, when he was pianist at the ''Jungle Casino'' around 1913. His memorable 1939 recording made this tune a reference in stride piano. James P. Johnson show us, on an inflexible tempo, that stride is truly a breathing music.', 'James P. Johnson sheet music Squeeze Me original transcription 1944', 'en'),
(201, 101, '1923', 'toddlin_en.html', NULL, 'Partition de James P. Johnson Toddlin'' transcription originale 1923', 'fr'),
(202, 101, '1923', 'toddlin.html', '<i>Riff</i> is a series of variations built in two parts. James P. Johnson recorded this tune a second time in 1944, but the 1929 recording is really more energic.', 'James P. Johnson sheet music Toddlin'' original transcription 1923', 'en'),
(203, 102, '1930', 'what_is_this_thing_called_love_en.html', NULL, 'Partition de James P. Johnson What Is This Thing Called Love? transcription originale 1930', 'fr'),
(204, 102, '1930', 'what_is_this_thing_called_love.html', '<i>Crying For The Carolines</i> is transcribed from the great recording from 1930. It''s a kind of lament which alternate between minor and major tone.', 'James P. Johnson sheet music What Is This Thing Called Love? original transcription 1930', 'en'),
(205, 103, '1923', 'you_cant_do_en.html', NULL, 'Partition de James P. Johnson You Can''t Do What My Last Man Did transcription originale 1923', 'fr'),
(206, 103, '1923', 'you_cant_do.html', NULL, 'James P. Johnson sheet music You Can''t Do What My Last Man Did original transcription 1923', 'en'),
(207, 104, 'Janvier 1939', 'concentratin_en.html', '', 'Partition de Willie The Lion Smith Concentratin'' transcription originale 1939', 'fr'),
(208, 104, 'January 1939', 'concentratin.html', '', 'Willie The Lion Smith sheet music Concentratin'' original transcription 1939', 'en'),
(209, 105, 'Décembre 1950', 'sneakaway_en.html', NULL, 'Partition de Willie The Lion Smith Sneakaway transcription originale 1950', 'fr'),
(210, 105, 'December 1950', 'sneakaway.html', NULL, 'Willie The Lion Smith sheet music Sneakaway original transcription 1950', 'en'),
(211, 106, 'Janvier 1965', 'echoes_en.html', '', 'Partition de Willie The Lion Smith Echoes Of Spring transcription originale 1950', 'fr'),
(212, 106, 'January 1965', 'echoes.html', '', 'Willie The Lion Smith sheet music Echoes Of Spring original transcription 1950', 'en'),
(213, 107, 'Janvier 1939', 'morning_en.html', NULL, 'Partition de Willie The Lion Smith Morning Air transcription originale 1939', 'fr'),
(214, 107, 'January 1939', 'morning.html', NULL, 'Willie The Lion Smith sheet music Morning Air original transcription 1939', 'en'),
(215, 108, 'Janvier 1939', 'finger_en.html', '', 'Partition de Willie The Lion Smith Finger Buster transcription originale 1939', 'fr'),
(216, 108, 'January 1939', 'finger.html', '', 'Willie The Lion Smith sheet music Finger Buster original transcription 1939', 'en'),
(217, 109, 'Janvier 1939', 'fading_en.html', '', 'Partition de Willie The Lion Smith Fading Star transcription originale 1939', 'fr'),
(218, 109, 'January 1939', 'fading.html', '', 'Willie The Lion Smith sheet music Fading Star original transcription 1939', 'en'),
(219, 110, 'Janvier 1939', 'rippling_en.html', '', 'Partition de Willie The Lion Smith Rippling Waters transcription originale 1939', 'fr'),
(220, 110, 'January 1939', 'rippling.html', '', 'Willie The Lion Smith sheet music Rippling Waters original transcription 1939', 'en'),
(221, 111, 'Janvier 1939', 'stormy_en.html', '', 'Partition de Willie The Lion Smith Stormy Weather transcription originale 1939', 'fr'),
(222, 111, 'January 1939', 'stormy.html', '', 'Willie The Lion Smith sheet music Stormy Weather original transcription 1939', 'en'),
(223, 112, 'Janvier 1939', 'follow_en.html', '', 'Partition de Willie The Lion Smith I''ll Follow You transcription originale 1939', 'fr'),
(224, 112, 'January 1939', 'follow.html', '', 'Willie The Lion Smith sheet music I''ll Follow You original transcription 1939', 'en'),
(225, 113, 'Janvier 1939', 'passionette_en.html', '', 'Partition de Willie The Lion Smith Passionette transcription originale 1939', 'fr'),
(226, 113, 'January 1939', 'passionette.html', '', 'Willie The Lion Smith sheet music Passionette original transcription 1939', 'en'),
(227, 114, 'Janvier 1939', 'whatisthere_en.html', '', 'Partition de Willie The Lion Smith What Is There To Say? transcription originale 1939', 'fr'),
(228, 114, 'January 1939', 'whatisthere.html', '', 'Willie The Lion Smith sheet music What Is There To Say? original transcription 1939', 'en'),
(229, 115, 'Décembre 1949', 'theband_en.html', NULL, 'Partition de Willie The Lion Smith Here Come The Band transcription originale 1949', 'fr'),
(230, 115, 'December 1949', 'theband.html', NULL, 'Willie The Lion Smith sheet music Here Come The Band original transcription 1949', 'en'),
(231, 116, 'Décembre 1949', 'cuttinout_en.html', NULL, 'Partition de Willie The Lion Smith Cuttin'' Out transcription originale 1949', 'fr'),
(232, 116, 'December 1949', 'cuttinout.html', NULL, 'Willie The Lion Smith sheet music Cuttin'' Out original transcription 1949', 'en'),
(233, 117, 'Décembre 1949', 'portrait_en.html', NULL, 'Partition de Willie The Lion Smith Portrait Of The Duke transcription originale 1949', 'fr'),
(234, 117, 'December 1949', 'portrait.html', NULL, 'Willie The Lion Smith sheet music Portrait Of The Duke original transcription 1949', 'en'),
(235, 118, 'Décembre 1949', 'zigzag_en.html', NULL, 'Partition de Willie The Lion Smith Zig Zag transcription originale 1949', 'fr'),
(236, 118, 'December 1949', 'zigzag.html', NULL, 'Willie The Lion Smith sheet music Zig Zag original transcription 1949', 'en'),
(237, 119, 'Décembre 1949', 'motion_en.html', NULL, 'Partition de Willie The Lion Smith Contrary Motion transcription originale 1949', 'fr'),
(238, 119, 'December 1949', 'motion.html', NULL, 'Willie The Lion Smith sheet music Contrary Motion original transcription 1949', 'en'),
(239, 120, '1941', 'anitra_en.html', NULL, 'Partition de Donald Lambert Anitra''s Dance transcription originale 1941', 'fr'),
(240, 120, '1941', 'anitra.html', 'The most celebrated solo of Donald Lambert  transcribed from the 1941 recording.', 'Donald Lambert sheet music Anitra''s Dance original transcription 1941', 'en'),
(241, 121, '1941', 'pilgrim_en.html', NULL, 'Partition de Donald Lambert Pilgrim''s Chorus transcription originale 1941', 'fr'),
(242, 121, '1941', 'pilgrim.html', 'The <i>Pilgrim Chorus</i> from Wagner''s opera <i>Tannhauser</i> is one of the most amazing solos of Donald Lambert. He takes up the original structure for making a really original stride solo.', 'Donald Lambert sheet music Pilgrim''s Chorus original transcription 1941', 'en'),
(243, 122, '1941', 'elegie_en.html', NULL, 'Partition de Donald Lambert Elegie transcription originale 1941', 'fr'),
(244, 122, '1941', 'elegie.html', NULL, 'Donald Lambert sheet music Elegie original transcription 1941', 'en');
INSERT INTO `TranscriptionTranslation` (`id`, `translatable_id`, `date`, `menuLinkLanguage`, `description`, `partDesc`, `locale`) VALUES
(245, 123, '1941', 'sextet_en.html', NULL, 'Partition de Donald Lambert Sextet (from \\''Luci Di Lammermoor\\'') transcription originale 1941', 'fr'),
(246, 123, '1941', 'sextet.html', 'Maybe the most difficult solo of this serie, and the most demonstrative of his left hand skilfulness : on an extemely fast tempo, he takes up with ease the melody of the Sextet by Donizetti.', 'Donald Lambert sheet music Sextet (from \\''Luci Di Lammermoor\\'') original transcription 1941', 'en'),
(247, 124, '1949', 'lullaby_en.html', NULL, 'Partition de Donald Lambert Russian Lullaby transcription originale 1949', 'fr'),
(248, 124, '1949', 'lullaby.html', 'Transcribe from the rare 1949 recording under the label <i>Circle</i>. The 8 tunes recorded in that session were considered to be lost.', 'Donald Lambert sheet music Russian Lullaby original transcription 1949', 'en'),
(249, 125, '1959', 'people_en.html', '', 'Partition de Donald Lambert People Will Say We Are In Love transcription originale 1959', 'fr'),
(250, 125, '1959', 'people.html', '', 'Donald Lambert sheet music People Will Say We Are In Love original transcription 1959', 'en'),
(251, 126, '1960', 'temper_en.html', '', 'Partition de Donald Lambert Hold Your Temper transcription originale 1960', 'fr'),
(252, 126, '1960', 'temper.html', '', 'Donald Lambert sheet music Hold Your Temper original transcription 1960', 'en'),
(253, 127, '1960', 'teafortwo_lamb_en.html', NULL, 'Partition de Donald Lambert Tea For Two transcription originale 1960', 'fr'),
(254, 127, '1960', 'teafortwo_lamb.html', 'Transcribed from the 1960 recording, exepted the introduction which is from the 1949 recording (<a href="http://www.blueblackjazz.com/part/lullaby_en.html">Circle session</a>). Once again, Lambert show an amazing inventiveness by playin the melody on the left hand. He makes sweet variations with the right hand, which some are existing tunes, like <i>April Showers</i> and <i>Because Of You</i>.', 'Donald Lambert sheet music Tea For Two original transcription 1960', 'en'),
(255, 128, '1960', 'trolley_en.html', NULL, 'Partition de Donald Lambert Trolley Song transcription originale 1960', 'fr'),
(256, 128, '1960', 'trolley.html', 'A Lambert masterpiece which he recorded this tune several times.', 'Donald Lambert sheet music Trolley Song original transcription 1960', 'en'),
(257, 129, '1961', 'russian_en.html', NULL, 'Partition de Donald Lambert Russian Rag transcription originale 1961', 'fr'),
(258, 129, '1961', 'russian.html', NULL, 'Donald Lambert sheet music Russian Rag original transcription 1961', 'en'),
(259, 130, '1961', 'sorrow_en.html', 'Dans cet excellent solo, Donald Lambert ne joue que deux couplet du joli <i>Save your Sorrow</i>. C''est assez court, mais il semble que tout soit dit.', 'Partition de Donald Lambert Save Your Sorrow transcription originale 1961', 'fr'),
(260, 130, '1961', 'sorrow.html', 'In this great solo, Donald Lambert plays only two chorus of this nice <i>Save your Sorrow</i>. It''s quite short, but everything seems to be said.', 'Donald Lambert sheet music Save Your Sorrow original transcription 1961', 'en'),
(261, 131, '1961', 'beans_en.html', NULL, 'Partition de Donald Lambert Pork And Beans transcription originale 1961', 'fr'),
(262, 131, '1961', 'beans.html', '<i>Pork and Bean</i> is a composition by the pianist Luckey Roberts. Transcribed from the 1961 recording. The only major theme also appears in his <i>Daintiness Rag</i> (1960).', 'Donald Lambert sheet music Pork And Beans original transcription 1961', 'en'),
(263, 132, 'ca.1959-1961', 'harry_en.html', NULL, 'Partition de Donald Lambert I''m Just Wild About Harry transcription originale 1959-1961', 'fr'),
(264, 132, 'ca.1959-1961', 'harry.html', 'Donald Lambert plays here a Eubie Blake composition. The them is played gracefully in the first part, as Lambert use to do. And then it''s an explosion, the "Jersey Rocket" is launched...', 'Donald Lambert sheet music I''m Just Wild About Harry original transcription 1959-1961', 'en'),
(265, 133, 'ca.1959-1961', 'astime_en.html', NULL, 'Partition de Donald Lambert As Time Goes By transcription originale 1959-1961', 'fr'),
(266, 133, 'ca.1959-1961', 'astime.html', 'This tune became famous with the motion picture <i>Casablanca</i> in 1942, it was however composed in 1931. Lambert takes up the melody with elegance, an then plays it a second time in an original way, giving rhythm to the melody.', 'Donald Lambert sheet music As Time Goes By original transcription 1959-1961', 'en'),
(267, 134, '', 'jumps_en.html', NULL, 'Partition de Donald Lambert Doin'' What I Please transcription originale', 'fr'),
(268, 134, '', 'jumps.html', 'Transcribed from a rare recording, <i>Doin'' What I Please</i> is a Fats Waller composition. Lambert plays this lively piece in his favourite major tonality, D flat.', 'Donald Lambert sheet music Doin'' What I Please original transcription', 'en'),
(269, 135, 'Avril 1924', 'chicago_stomp_en.html', NULL, 'Partition de Jimmy Blythe Chicago Stomp transcription originale 1924', 'fr'),
(270, 135, 'April 1924', 'chicago_stomp.html', 'Chicago Stomp was recorded in April 1924 for Paramount Records and was one of the first boogie-woogie recording : for the first time, the left hand is entirely played with "walking bass".', 'Jimmy Blythe sheet music Chicago Stomp original transcription 1924', 'en'),
(271, 136, 'Janvier 1929', 'steadyblues_en.html', NULL, 'Partition de Clarence Pine Top Smith Jump Steady Blues transcription originale 1929', 'fr'),
(272, 136, 'January 1929', 'steadyblues.html', '"Pine Top" Smith, who died at a young age, made some recordings in 1928 and 1929. He always used entertain and talk over his solos. <i>Jump Steady Blues</i> is the only recording where we can hear him playing just piano.', 'Clarence Pine Top Smith sheet music Jump Steady Blues original transcription 1929', 'en'),
(273, 137, 'Octobre 1939', 'yancey_stomp_en.html', NULL, 'Partition de Jimmy Yancey Yancey Stomp transcription originale 1939', 'fr'),
(274, 137, 'October 1939', 'yancey_stomp.html', 'A fast, inventiveness blues recorded in 1939.', 'Jimmy Yancey sheet music Yancey Stomp original transcription 1939', 'en'),
(275, 138, 'Octobre 1939', 'mellow_blues_en.html', NULL, 'Partition de Jimmy Yancey The Mellow Blues transcription originale 1939', 'fr'),
(276, 138, 'October 1939', 'mellow_blues.html', NULL, 'Jimmy Yancey sheet music The Mellow Blues original transcription 1939', 'en'),
(277, 139, 'Septembre 1940', 'bugle_call_en.html', NULL, 'Partition de Jimmy Yancey Yancey Bugle Call transcription originale 1940', 'fr'),
(278, 139, 'September 1940', 'bugle_call.html', 'A boogie-woogie in E flat recorded in 1940.', 'Jimmy Yancey sheet music Yancey Bugle Call original transcription 1940', 'en'),
(279, 140, 'Janvier 1939', 'bw_stomp_en.html', NULL, 'Partition de Albert Ammons Boogie Woogie Stomp transcription originale 1939', 'fr'),
(280, 140, 'January 1939', 'bw_stomp.html', 'Already recorded in 1936 with his band Albert Ammons & his Rhythm Kings, <i>Boogie Woogie Stomp</i> is transcribed from the 1939 piano solo. The introduction is taken from Pine Top Smith''s <i>Boogie Woogie</i> (1928).', 'Albert Ammons sheet music Boogie Woogie Stomp original transcription 1939', 'en'),
(281, 141, 'Janvier 1939', 'suitcase_blues_en.html', NULL, 'Partition de Albert Ammons Suitcase Blues transcription originale 1939', 'fr'),
(282, 141, 'January 1939', 'suitcase_blues.html', '<i>Suitcase Blues</i> was composed by the blues pianist Hersal Thoams. The 1939 recording by Albert Ammons is one of the best.', 'Albert Ammons sheet music Suitcase Blues original transcription 1939', 'en'),
(283, 142, 'Novembre 1946', '12street_boogie_en.html', NULL, 'Partition de Albert Ammons 12th Street Boogie transcription originale 1946', 'fr'),
(284, 142, 'November 1946', '12street_boogie.html', 'Transcribed from the 1946 recording, <i>12th Street Boogie</i> breaks with the traditionnal boogie woogie chord line. Ammons playing is full of swing and inventivness.', 'Albert Ammons sheet music 12th Street Boogie original transcription 1946', 'en'),
(285, 143, 'Avril 1939', 'meccaflat_blues_en.html', NULL, 'Partition de Albert Ammons Mecca Flat Blues transcription originale 1939', 'fr'),
(286, 143, 'April 1939', 'meccaflat_blues.html', 'Another great Ammons blues, transcribed from the 1939 recording.', 'Albert Ammons sheet music Mecca Flat Blues original transcription 1939', 'en'),
(287, 144, 'Janvier 1936', 'yancey_special_en.html', NULL, 'Partition de Meade Lux Lewis Yancey Special transcription originale 1939', 'fr'),
(288, 144, 'January 1936', 'yancey_special.html', 'A classic of Meade Lux Lewis which he recorded in 1936. He paid tribute to pianist Jimmy Yancey, who was one of his main influences.', 'Meade Lux Lewis sheet music Yancey Special original transcription 1939', 'en'),
(289, 145, 'Novembre 1935 & Mars 1937', 'honkytonk_en.html', NULL, 'Partition de Meade Lux Lewis Honky Tonk Train Blues transcription originale 1939', 'fr'),
(290, 145, 'November 1935 & March 1937', 'honkytonk.html', 'The most famous piece of Meade Lux Lewis that he recorded for the first time in 1927.', 'Meade Lux Lewis sheet music Honky Tonk Train Blues original transcription 1939', 'en'),
(291, 146, 'Février 1944', 'answer_en.html', NULL, 'Partition de Pete Johnson Answer To The Boogie transcription originale 1944', 'fr'),
(292, 146, 'February 1944', 'answer.html', 'This very energetic boogie in F major is one of the best of Pete Johnson.', 'Pete Johnson sheet music Answer To The Boogie original transcription 1944', 'en'),
(293, 147, 'Février 1944', 'bottomland_boogie_en.html', NULL, 'Partition de Pete Johnson Bottomland Boogie transcription originale 1944', 'fr'),
(294, 147, 'February 1944', 'bottomland_boogie.html', NULL, 'Pete Johnson sheet music Bottomland Boogie original transcription 1944', 'en'),
(295, 148, 'Février 1944', 'freddie_blues_en.html', NULL, 'Partition de Pete Johnson Mr. Freddie Blues transcription originale 1944', 'fr'),
(296, 148, 'February 1944', 'freddie_blues.html', 'Mr. Freddie Blues is a classic of blue repertory, recorded by Meade Lux Lewis and Jimmy Blythe. Pete Johnson''s 1944 recording is in stride style, showing his ability to play in this style.', 'Pete Johnson sheet music Mr. Freddie Blues original transcription 1944', 'en'),
(297, 149, 'Avril 1939', 'shuffle_boogie_en.html', NULL, 'Partition de Pete Johnson Shuffle Boogie transcription originale 1939', 'fr'),
(298, 149, 'April 1939', 'shuffle_boogie.html', 'Pete Johnson could play boogie-woogie in various tonalities. This is boogie in Bb, transcribed from the recording of 1939..', 'Pete Johnson sheet music Shuffle Boogie original transcription 1939', 'en'),
(299, 150, 'Novembre 1938', 'boogie_basie_en.html', NULL, 'Partition de Count Basie Boogie-Woogie transcription originale 1939', 'fr'),
(300, 150, 'November 1938', 'boogie_basie.html', 'Although he was not a boogie-woogie pianist, Count Basie recorded several pieces of that style (like most jazz pianists). Count Basie plays with his lightness style, and his usual swing.', 'Count Basie sheet music Boogie-Woogie original transcription 1939', 'en'),
(301, 151, 'Avril 1944', 'mary_boogie_en.html', NULL, 'Partition de Pete Johnson Mary''s Boogie transcription originale 1939', 'fr'),
(302, 151, 'April 1944', 'mary_boogie.html', 'Mary Lou Williams is one of the very few women to have equaled the greatest jazz pianists. She was a great stride and bebop player. She recorded this boogie-woogie in 1944.', 'Pete Johnson sheet music Mary''s Boogie original transcription 1939', 'en'),
(303, 153, 'October 1939', 'earl_hines_rosetta.html', NULL, NULL, 'en'),
(304, 153, 'Octobre 1939', 'earl_hines_rosetta_en.html', NULL, NULL, 'fr'),
(305, 154, 'March 1929', 'alex_hill_stompin_em_down.html', NULL, NULL, 'en'),
(306, 154, 'Mars 1929', 'alex_hill_stompin_em_down_en.html', NULL, NULL, 'fr'),
(307, 155, 'January 1962', 'cliff_jackson_crazy_rhythm.html', NULL, NULL, 'en'),
(308, 155, 'Janvier 1962', 'cliff_jackson_crazy_rhythm_en.html', NULL, NULL, 'fr'),
(309, 156, 'July 1944', 'cliff_jackson_royal_garden_blues.html', NULL, NULL, 'en'),
(310, 156, 'Juillet 1944', 'cliff_jackson_royal_garden_blues_en.html', NULL, NULL, 'fr'),
(311, 157, 'July 1944', 'cliff_jackson_who.html', NULL, NULL, 'en'),
(312, 157, 'Juillet 1944', 'cliff_jackson_who_en.html', NULL, NULL, 'fr'),
(313, 158, 'December 1945', 'cliff_jackson_you_took_advantage_of_me.html', NULL, NULL, 'en'),
(314, 158, 'Décembre 1945', 'cliff_jackson_you_took_advantage_of_me_en.html', NULL, NULL, 'fr'),
(315, 159, 'c.1946', 'don_ewell_parlor_social.html', NULL, NULL, 'en'),
(316, 159, 'c.1946', 'don_ewell_parlor_social_en.html', NULL, NULL, 'fr'),
(317, 160, 'Mach 1923', 'fletcher_henderson_unknown_blues.html', NULL, NULL, 'en'),
(318, 160, 'Mars 1923', 'fletcher_henderson_unknown_blues_en.html', NULL, NULL, 'fr'),
(319, 161, 'June 1953', 'ralph_sutton_fussin.html', NULL, NULL, 'en'),
(320, 161, 'Juin 1953', 'ralph_sutton_fussin_en.html', NULL, NULL, 'fr'),
(321, 162, 'c.1960', 'eubie_blake_troublesome_ivories.html', NULL, NULL, 'en'),
(322, 162, 'c.1960', 'eubie_blake_troublesome_ivories_en.html', NULL, NULL, 'fr'),
(323, 164, 'February 1944', NULL, NULL, NULL, 'en'),
(324, 164, 'Février 1944', NULL, NULL, NULL, 'fr'),
(325, 165, 'July 1946', NULL, NULL, NULL, 'en'),
(326, 165, 'Juillet 1946', NULL, NULL, NULL, 'fr'),
(327, 166, 'February 1925', NULL, NULL, NULL, 'en'),
(328, 166, 'Février 1925', NULL, NULL, NULL, 'fr'),
(329, 167, 'October 1939', NULL, NULL, NULL, 'en'),
(330, 167, 'Octobre 1939', NULL, NULL, NULL, 'fr'),
(331, 168, 'April 1939', NULL, NULL, NULL, 'en'),
(332, 168, 'Avril 1939', NULL, NULL, NULL, 'fr'),
(333, 169, 'October 1939', NULL, NULL, NULL, 'en'),
(334, 169, 'Octobre 1939', NULL, NULL, NULL, 'fr'),
(335, 170, 'August 1944', NULL, NULL, NULL, 'en'),
(336, 170, 'Août 1944', NULL, NULL, NULL, 'fr'),
(337, 171, 'August 1944', NULL, NULL, NULL, 'en'),
(338, 171, 'Août 1944', NULL, NULL, NULL, 'fr'),
(339, 152, 'c.1945', NULL, NULL, NULL, 'en'),
(340, 152, 'c.1945', NULL, NULL, NULL, 'fr'),
(341, 172, 'December 1928', NULL, NULL, NULL, 'en'),
(342, 172, 'Décembre 1928', NULL, NULL, NULL, 'fr'),
(343, 173, 'July 1945', NULL, NULL, NULL, 'en'),
(344, 173, 'Juillet 1945', NULL, NULL, NULL, 'fr'),
(345, 174, 'July 1945', NULL, NULL, NULL, 'en'),
(346, 174, 'Juillet 1945', NULL, NULL, NULL, 'fr'),
(347, 175, 'April 1939', NULL, NULL, NULL, 'en'),
(348, 175, 'Avril 1939', NULL, NULL, NULL, 'fr'),
(349, 176, 'May 1941', NULL, NULL, NULL, 'en'),
(350, 176, 'Mai 1941', NULL, NULL, NULL, 'fr'),
(351, 177, 'May 1941', NULL, NULL, NULL, 'en'),
(352, 177, 'Mai 1941', NULL, NULL, NULL, 'fr'),
(353, 178, 'February 1944', NULL, NULL, NULL, 'en'),
(354, 178, 'Février 1944', NULL, NULL, NULL, 'fr'),
(355, 163, 'February 1944', NULL, NULL, NULL, 'en'),
(356, 163, 'Février 1944', NULL, NULL, NULL, 'fr');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Artist`
--
ALTER TABLE `Artist`
  ADD CONSTRAINT `FK_6F593B1BACD6074` FOREIGN KEY (`style_id`) REFERENCES `Style` (`id`);

--
-- Contraintes pour la table `ArtistTranslation`
--
ALTER TABLE `ArtistTranslation`
  ADD CONSTRAINT `FK_9C62D7432C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `Artist` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `Book`
--
ALTER TABLE `Book`
  ADD CONSTRAINT `FK_6BD70C0FB7970CF8` FOREIGN KEY (`artist_id`) REFERENCES `Artist` (`id`),
  ADD CONSTRAINT `FK_6BD70C0FBACD6074` FOREIGN KEY (`style_id`) REFERENCES `Style` (`id`);

--
-- Contraintes pour la table `BookTranslation`
--
ALTER TABLE `BookTranslation`
  ADD CONSTRAINT `FK_46D64EED2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `Book` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `credits`
--
ALTER TABLE `credits`
  ADD CONSTRAINT `FK_4117D17E4C3A3BB` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_4117D17E8789B572` FOREIGN KEY (`payment_instruction_id`) REFERENCES `payment_instructions` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `financial_transactions`
--
ALTER TABLE `financial_transactions`
  ADD CONSTRAINT `FK_1353F2D94C3A3BB` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_1353F2D9CE062FF9` FOREIGN KEY (`credit_id`) REFERENCES `credits` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `MyOrder`
--
ALTER TABLE `MyOrder`
  ADD CONSTRAINT `FK_6657DC4FFD913E4D` FOREIGN KEY (`paymentInstruction_id`) REFERENCES `payment_instructions` (`id`);

--
-- Contraintes pour la table `myorder_book`
--
ALTER TABLE `myorder_book`
  ADD CONSTRAINT `FK_2DD936AF16A2B381` FOREIGN KEY (`book_id`) REFERENCES `Book` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_2DD936AF732E2069` FOREIGN KEY (`myorder_id`) REFERENCES `MyOrder` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `myorder_transcription`
--
ALTER TABLE `myorder_transcription`
  ADD CONSTRAINT `FK_F875FD04732E2069` FOREIGN KEY (`myorder_id`) REFERENCES `MyOrder` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_F875FD04F678E194` FOREIGN KEY (`transcription_id`) REFERENCES `Transcription` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `PageTranslation`
--
ALTER TABLE `PageTranslation`
  ADD CONSTRAINT `FK_D29B35C02C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `Page` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `FK_65D29B328789B572` FOREIGN KEY (`payment_instruction_id`) REFERENCES `payment_instructions` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `StyleTranslation`
--
ALTER TABLE `StyleTranslation`
  ADD CONSTRAINT `FK_B8F9C7E02C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `Style` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `Transcription`
--
ALTER TABLE `Transcription`
  ADD CONSTRAINT `FK_81E2A27116A2B381` FOREIGN KEY (`book_id`) REFERENCES `Book` (`id`),
  ADD CONSTRAINT `FK_81E2A271B7970CF8` FOREIGN KEY (`artist_id`) REFERENCES `Artist` (`id`),
  ADD CONSTRAINT `FK_81E2A271BACD6074` FOREIGN KEY (`style_id`) REFERENCES `Style` (`id`);

--
-- Contraintes pour la table `TranscriptionTranslation`
--
ALTER TABLE `TranscriptionTranslation`
  ADD CONSTRAINT `FK_2BA5B14E2C2AC5D3` FOREIGN KEY (`translatable_id`) REFERENCES `Transcription` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
