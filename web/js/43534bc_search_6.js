var bbbApp = angular.module('bbb', [])
    .config(function($interpolateProvider){
        $interpolateProvider.startSymbol('<%').endSymbol('%>');
    }
);

    
bbbApp.filter('unsafe', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
});


bbbApp.controller('SearchCtrl', ['$scope', '$http', '$timeout', function($scope, $http, $timeout){
    var json_pages_url = '/' + BlueBB.locale + '/search';
    $scope.books = [];
    $scope.transcriptions = [];
    $scope.searching = false;
    $scope.searchText = '';

    // source : http://stackoverflow.com/a/15304977/1113898
    // Instantiate these variables outside the watch
    var tempFilterText = '';
    var filterTextTimeout;
    $scope.$watch('enteredText', function (val) {
        if (filterTextTimeout) $timeout.cancel(filterTextTimeout);

        tempFilterText = val;
        filterTextTimeout = $timeout(function() {
            $scope.searchText = tempFilterText;
        }, 100)
    })
    var initial = document.getElementById('searchFieldId').value;
    if(initial){
        $scope.enteredText = initial;
        $scope.searching = true;
    } else { $scope.enteredText = '' };

    $http.get(json_pages_url).then(function(response){
        $scope.books = response.data.books;
        $scope.transcriptions = response.data.transcriptions;
        console.log('got it!');
    }); 

}]);

bbbApp.filter('searchByText', ['$filter', function($filter) {
    var built_in_filter = $filter('filter');
    return function(pages, searchText) {
        if(searchText.length < 2){
            return [];
        }
        var selectedPages = pages;
        var tokens = searchText.split(' ');
        for (index = 0; index < tokens.length; ++index) {
            var token = tokens[index];
            var newSelectedPages = built_in_filter(selectedPages, token);
            if(newSelectedPages.length === 0){
                if(index === 0){ return []};
                return selectedPages;
            }
            selectedPages = newSelectedPages;
        }
        return selectedPages;
    }
  }]);


bbbApp.filter('highlight', function () {
  return function (text, search) {
    if (text && (search || angular.isNumber(search))) {
      text = text.toString();
      search = search.toString();
      tokens = search.split(/, | |,/);
      long_tokens = []
      for(var i=0; i + 1 < tokens.length; i++){
        // highlight bigram with higher priority
        var bigram = tokens[i] + ' ' + tokens[i+1];
        long_tokens.push(bigram);
      }
      for(var i in tokens){
        if(tokens[i].length > 2){
            // highlight unigram
            long_tokens.push(tokens[i]);
        }
      }
      // highlight full pattern with highest priority
      pattern = '('+ search + '|' + long_tokens.join('|') +')';
      text = text.replace(new RegExp(pattern, 'gi'), '<strong>$&</strong>');
      return text;
    } else {
      return text;
    }
  };
});
